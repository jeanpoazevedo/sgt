<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_hd_de_usuario_para".
 *
 * @property int|null $id
 * @property int $usuario_de_fk
 * @property int $usuario_para_fk
 * @property string $nome_de
 * @property string $nome_para
 */
class VwHdDeUsuarioPara extends \yii\db\ActiveRecord
{

    public $pesquisa;

    public static function tableName()
    {
        return 'vw_helpdesk_de_usuario_para';
    }

    public static function primaryKey() {
        return ['id'];
    }

    public function rules()
    {
        return [
            [['id', 'usuario_de_fk', 'usuario_para_fk'], 'default', 'value' => null],
            [['id', 'usuario_de_fk', 'usuario_para_fk'], 'integer'],
            [['nome_de', 'nome_para'], 'string', 'max' => 80],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_de_fk' => 'De Usuario',
            'usuario_para_fk' => 'Para Usuario',
            'nome_de' => 'De Nome',
            'nome_para' => 'Para Nome',
        ];
    }

    public function search($params) {
        $query = VwHdDeUsuarioPara::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome_de' => SORT_ASC,
                    'nome_para' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwHdDeUsuarioPara']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_de))', strtoupper(Setup::retirarAcento($params['VwHdDeUsuarioPara']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_para))', strtoupper(Setup::retirarAcento($params['VwHdDeUsuarioPara']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
