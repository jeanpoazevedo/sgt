<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_chaves".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $perfil_fk
 * @property string $produto
 * @property string $chave
 *
 */
class Chave extends \yii\db\ActiveRecord {
    
    public $pesquisa;
    
    public static function tableName() {
        return 'tb_chave';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'perfil_fk', 'produto', 'chave'], 'required'],
            [['unidade_fk', 'perfil_fk'], 'default', 'value' => null],
            [['unidade_fk', 'perfil_fk'], 'integer'],
            [['produto'], 'string', 'max' => 80],
            [['chave'], 'string', 'max' => 120],
            [['unidade_fk', 'produto', 'chave'], 'unique', 'targetAttribute' => ['unidade_fk', 'produto', 'chave']],
            [['perfil_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['perfil_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'perfil_fk' => 'Perfil',
            'produto' => 'Produto',
            'chave' => 'Chave',
        ];
    }

    public function getPerfilFk(){
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_fk']);
    }
    
    public function getUnidadeFk(){
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Chave::find();

        if (isset($params['Chave']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(chave))', strtoupper(Setup::retirarAcento($params['Chave']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(produto))', strtoupper(Setup::retirarAcento($params['Chave']['pesquisa']))]);
        }
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        }
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        $query->andWhere(['>=', 'perfil_fk', $usuarioperfil->perfil_fk]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'unidade_fk' => SORT_ASC,
                    'produto' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }

}
