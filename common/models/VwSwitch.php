<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_switch".
 *
 * @property int $id
 * @property int $codigo_switch_fk
 * @property string $codigo_equipamento
 * @property int $unidade_fk
 * @property string $unidade
 * @property int $local_fk
 * @property string $local
 * @property int $id_switch
 * @property string $descricao_switch
 * @property int $numero_portas
 * 
 */

class VwSwitch extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_switch';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id','codigo_switch_fk', 'unidade_fk', 'local_fk', 'id_switch', 'numero_portas'], 'default', 'value' => null],
            [['id','codigo_switch_fk', 'unidade_fk', 'local_fk', 'id_switch', 'numero_portas'], 'integer'],
            [['codigo_equipamento'], 'string', 'max' => 7],
            [['unidade'], 'string', 'max' => 40],
            [['descricao_switch', 'local'], 'string', 'max' => 80],
            [['codigo_switch_fk', 'local_fk', 'id_switch'], 'unique', 'targetAttribute' => ['codigo_switch_fk', 'local_fk', 'id_switch']],
            [['codigo_switch_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Equipamento::className(), 'targetAttribute' => ['codigo_switch_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['local_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['local_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_switch_fk' => 'Codigo Switch Fk',
            'codigo_equipamento' => 'Código Equipamento',
            'unidade_fk' => 'Unidade Fk',
            'unidade' => 'Unidade',
            'local_fk' => 'Local Fk',
            'local' => 'Local',
            'id_switch' => 'ID Switch',
            'descricao_switch' => 'Descrição do Switch',
            'numero_portas' => 'Numero Portas'
        ];
    }

    public function getCodigoSwitchFk() {
        return $this->hasOne(Equipamento::className(), ['id' => 'codigo_switch_fk']);
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }

    public function getLocalFk() {
        return $this->hasOne(Local::className(), ['id' => 'local_fk']);
    }
    
    public function search($params) {
        $query = VwSwitch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'unidade' => SORT_ASC,
                    'local' => SORT_ASC,
                    'id_switch' => SORT_ASC,
                ]
            ],
        ]);

        $query->alias('vw_switch');
        $query->leftJoin('tb_switch', 'tb_switch.id = vw_switch.id');

        if (isset($params['VwSwitch']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_switch.xml_portas))', strtoupper(Setup::retirarAcento($params['VwSwitch']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_switch))', strtoupper(Setup::retirarAcento($params['VwSwitch']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_switch.configuracao))', strtoupper(Setup::retirarAcento($params['VwSwitch']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
