<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_medida".
 *
 * @property int $id
 * @property string $simbolo
 * @property string $medida
 *
 */
class Medida extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_medida';
    }
    
    public function rules()
    {
        return [
            [['simbolo', 'medida'], 'required'],
            [['simbolo', 'medida'], 'string', 'max' => 50],
            [['medida'], 'unique'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'simbolo' => 'Simbolo',
            'medida' => 'Medida',
        ];
    }
    
    public function search($params) {
        $query = Medida::find();

        if (isset($params['Medida']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(simbolo))', strtoupper(Setup::retirarAcento($params['Medida']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(medida))', strtoupper(Setup::retirarAcento($params['Medida']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'simbolo' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
