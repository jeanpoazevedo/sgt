<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_marca_placa_video".
 *
 * @property int $id
 * @property string $marca
 * @property int $marca_fk
 * @property string $modelo
 * @property string $descricao_placa_video
 * 
 */

class VwMarcaPlacaVideo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_marca_placa_video';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'marca_fk'], 'default', 'value' => null],
            [['id', 'marca_fk'], 'integer'],
            [['marca'], 'string', 'max' => 40],
            [['modelo'], 'string', 'max' => 200],
            [['descricao_placa_video'], 'string', 'max' => 255],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca' => 'Marca',
            'marca_fk' => 'Marca Fk',
            'modelo' => 'Modelo',
            'descricao_placa_video' => 'Descricao Placa Video',
        ];
    }
    
    public function search($params) {
        $query = VwMarcaPlacaVideo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'marca' => SORT_ASC,
                    'modelo' => SORT_ASC,
                ]
            ],
        ]);
        
        if (isset($params['VwMarcaPlacaVideo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo))', strtoupper(Setup::retirarAcento($params['VwMarcaPlacaVideo']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_placa_video))', strtoupper(Setup::retirarAcento($params['VwMarcaPlacaVideo']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', strtoupper(Setup::retirarAcento($params['VwMarcaPlacaVideo']['pesquisa']))]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
