<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_email".
 *
 * @property int $id
 * @property string $host
 * @property string $username
 * @property string $password
 * @property int $port
 * @property string $nome
 * @property string $encryption
 * 
 */
class Email extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public $array_encryption = [
        '1' => 'auto',
        '2' => 'ssl',
        '3' => 'tls',
        '4' => 'none',
    ];
    
    public static function tableName()
    {
        return 'tb_email';
    }
    
    public function rules()
    {
        return [
            [['host', 'username', 'password', 'port', 'nome', 'encryption'], 'required'],
            [['port'], 'default', 'value' => null],
            [['port', 'encryption'], 'integer'],
            [['host'], 'string', 'max' => 1024],
            [['username', 'password', 'nome'], 'string', 'max' => 120],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => 'Host',
            'username' => 'Username',
            'password' => 'Password',
            'port' => 'Port',
            'nome' => 'Nome',
            'encryption'  => 'Criptografia',
        ];
    }
    
    public function search($params) {
        $query = Email::find();

        if (isset($params['Email']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(username))', strtoupper(Setup::retirarAcento($params['Email']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Email']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(host))', strtoupper(Setup::retirarAcento($params['Email']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
