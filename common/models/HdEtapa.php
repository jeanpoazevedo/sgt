<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_helpdesk_etapa".
 *
 * @property int $id
 * @property int $chamado_fk
 * @property int $de_fk
 * @property string $data
 * @property string $descricao
 *
 */
class HdEtapa extends \yii\db\ActiveRecord {

    public $pesquisa;

    public static function tableName() {
        return 'tb_helpdesk_etapa';
    }

    public function rules()
    {
        return [
            [['chamado_fk', 'de_fk'], 'required'],
            [['chamado_fk', 'de_fk'], 'default', 'value' => null],
            [['chamado_fk', 'de_fk'], 'integer'],
            [['data'], 'safe'],
            [['descricao'], 'string'],
            [['chamado_fk'], 'exist', 'skipOnError' => true, 'targetClass' => HdCadastro::className(), 'targetAttribute' => ['chamado_fk' => 'id']],
            [['de_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['de_fk' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'chamado_fk' => Yii::t('app', 'Chamado'),
            'de_fk' => Yii::t('app', 'Usuário'),
            'data' => Yii::t('app', 'Data'),
            'descricao' => Yii::t('app', 'Descricao'),
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }

    public function getChaFk() {
        return $this->hasOne(HdCadastro::className(), ['id' => 'chamado_fk']);
    }

    public function getDeFk() {
        return $this->hasOne(Usuario::className(), ['id' => 'de_fk']);
    }

    public static function renderiza($array) {
        $array_table = [];
        foreach ($array as $key => $value) {
            $array_table[] = "<tr>
                                <td>" . $value->deFk->nome . "</td>
                                <td>" . $value->data . "</td>
                                <td>" . $value->descricao . "</td>
                            </tr>";
        }
        return $array_table;
    }

}
