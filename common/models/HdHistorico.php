<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_helpdesk_histtorico".
 *
 * @property int $id
 * @property int $chamado_fk
 * @property int $de_fk
 * @property string $data
 * @property string $acao
 * @property string $alteracao
 *
 */
class HdHistorico extends \yii\db\ActiveRecord
{
    public $pesquisa;
    public $array_status = [
        '0' => '',
        '1' => 'Aguardando',
        '2' => 'Em Andamento',
        '3' => 'Pausa',
        '5' => 'Resolvido',
        '4' => 'Encerrado',
    ];
    
    public static function tableName()
    {
        return 'tb_helpdesk_historico';
    }

    public function rules()
    {
        return [
            [['chamado_fk', 'de_fk', 'acao', 'alteracao'], 'required'],
            [['chamado_fk', 'de_fk'], 'default', 'value' => null],
            [['chamado_fk', 'de_fk'], 'integer'],
            [['data'], 'safe'],
            [['acao'], 'string', 'max' => 90],
            [['alteracao'], 'string', 'max' => 120],
            [['chamado_fk'], 'exist', 'skipOnError' => true, 'targetClass' => HdCadastro::className(), 'targetAttribute' => ['chamado_fk' => 'id']],
            [['de_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['de_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'chamado_fk' => Yii::t('app', 'Chamado'),
            'de_fk' => Yii::t('app', 'Usuário'),
            'data' => Yii::t('app', 'Data'),
            'acao' => Yii::t('app', 'Ação'),
            'alteracao' => Yii::t('app', 'Alteração'),
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }

    public function getChaFk()
    {
        return $this->hasOne(HdCadastro::className(), ['id' => 'chamado_fk']);
    }

    public function getDeFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'de_fk']);
    }
    
    public function search($params) {
        $query = HdHistorico::find();

        if (isset($params['HdHistorico']['chamado_fk'])) {
            $query->andFilterWhere(['=', 'chamado_fk', $params['HdHistorico']['chamado_fk']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
