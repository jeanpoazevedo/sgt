<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_voip".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $setor_fk
 * @property int $voip
 * @property string $senha
 * @property string $telefone
 * @property int $tempo_toque
 * @property int $transferencia_automatica_fk
 * @property string $toque_simultaneo
 * @property int $grupo_fk
 * @property int $equipamento_fk
 * @property int $funcionario_fk
 * @property string $informacao
 *
 */
class Voip extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public $array_tempo_toque = [
        '1' => '1 Segundos',
        '2' => '2 Segundos',
        '3' => '3 Segundos',
        '4' => '4 Segundos',
        '5' => '5 Segundos',
        '6' => '6 Segundos',
        '7' => '7 Segundos',
        '8' => '8 Segundos',
        '9' => '9 Segundos',
        '10' => '10 Segundos',
        '11' => '11 Segundos',
        '12' => '12 Segundos',
        '13' => '13 Segundos',
        '14' => '14 Segundos',
        '15' => '15 Segundos',
        '16' => '16 Segundos',
        '17' => '17 Segundos',
        '18' => '18 Segundos',
        '19' => '19 Segundos',
        '20' => '20 Segundos',
        '21' => '21 Segundos',
        '22' => '22 Segundos',
        '23' => '23 Segundos',
        '24' => '24 Segundos',
        '25' => '25 Segundos',
        '26' => '26 Segundos',
        '27' => '27 Segundos',
        '28' => '28 Segundos',
        '29' => '29 Segundos',
        '30' => '30 Segundos',
        '31' => '31 Segundos',
        '32' => '32 Segundos',
        '33' => '33 Segundos',
        '34' => '34 Segundos',
        '35' => '35 Segundos',
        '36' => '36 Segundos',
        '37' => '37 Segundos',
        '38' => '38 Segundos',
        '39' => '39 Segundos',
        '40' => '40 Segundos',
        '41' => '41 Segundos',
        '42' => '42 Segundos',
        '43' => '43 Segundos',
        '44' => '44 Segundos',
        '45' => '45 Segundos',
        '46' => '46 Segundos',
        '47' => '47 Segundos',
        '48' => '48 Segundos',
        '49' => '49 Segundos',
        '50' => '50 Segundos',
        '51' => '51 Segundos',
        '52' => '52 Segundos',
        '53' => '53 Segundos',
        '54' => '54 Segundos',
        '55' => '55 Segundos',
        '56' => '56 Segundos',
        '57' => '57 Segundos',
        '58' => '58 Segundos',
        '59' => '59 Segundos',
        '60' => '60 Segundos',
    ];
    
    public static function tableName()
    {
        return 'tb_voip';
    }
    
    public function rules()
    {
        return [
            [['unidade_fk', 'setor_fk', 'voip', 'senha', 'tempo_toque'], 'required'],
            [['unidade_fk', 'setor_fk', 'voip', 'tempo_toque', 'transferencia_automatica_fk', 'grupo_fk', 'equipamento_fk', 'funcionario_fk'], 'default', 'value' => null],
            [['unidade_fk', 'setor_fk', 'voip', 'tempo_toque', 'transferencia_automatica_fk', 'grupo_fk', 'equipamento_fk', 'funcionario_fk'], 'integer'],
            [['senha', 'toque_simultaneo'], 'string', 'max' => 50],
            [['telefone'], 'string', 'max' => 15],
            [['informacao'], 'string', 'max' => 255],
            [['unidade_fk', 'voip'], 'unique', 'targetAttribute' => ['unidade_fk', 'voip']],
            [['equipamento_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Equipamento::className(), 'targetAttribute' => ['equipamento_fk' => 'id']],
            [['funcionario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionario::className(), 'targetAttribute' => ['funcionario_fk' => 'id']],
            [['setor_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Setor::className(), 'targetAttribute' => ['setor_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['transferencia_automatica_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Voip::className(), 'targetAttribute' => ['transferencia_automatica_fk' => 'id']],
            [['grupo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => VoipCaptura::className(), 'targetAttribute' => ['grupo_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'setor_fk' => 'Setor',
            'voip' => 'VoIP',
            'senha' => 'Senha',
            'telefone' => 'Telefone',
            'tempo_toque' => 'Tempo Toque',
            'transferencia_automatica_fk' => 'Transferencia Automatica',
            'toque_simultaneo' => 'Toque Simultaneo',
            'grupo_fk' => 'Grupo',
            'equipamento_fk' => 'Equipamento',
            'funcionario_fk' => 'Funcionario',
            'informacao' => 'Informacao',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->telefone) {
            $this->telefone = Setup::formatterTelefone($this->telefone);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->telefone) {
            $this->telefone = Setup::unformatterString($this->telefone);
        }
    }
    
    public function getEquipamentoFk()
    {
        return $this->hasOne(Equipamento::className(), ['id' => 'equipamento_fk']);
    }
    
    public function getFuncionarioFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }
    
    public function getSetorFk()
    {
        return $this->hasOne(Setor::className(), ['id' => 'setor_fk']);
    }
    
    public function getTransferenciaautomaticaFk()
    {
        return $this->hasOne(Voip::className(), ['id' => 'transferencia_automatica_fk']);
    }
    
    public function getVoips()
    {
        return $this->hasMany(Voip::className(), ['transferencia_automatica_fk' => 'id']);
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getCapturaFk()
    {
        return $this->hasOne(VoipCaptura::className(), ['id' => 'grupo_fk']);
    }
    
    public function search($params) {
        $query = VoIP::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'unidade_fk' => SORT_ASC,
                    'voip' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Voip']['pesquisa'])) {
            $query->orFilterWhere(['=', 'voip', strtoupper($params['Voip']['pesquisa'])])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(telefone))', strtoupper(Setup::retirarAcento($params['PassoaPasso']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(toque_simultaneo))', strtoupper(Setup::retirarAcento($params['PassoaPasso']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(informacao))', strtoupper(Setup::retirarAcento($params['PassoaPasso']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }
}
