<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_marca_modelo".
 *
 * @property int $id
 * @property string $marca
 * @property int $marca_fk
 * @property string $modelo
 * @property int $tipo
 * 
 */

class VwMarcaModelo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public $array_tipo = [
        '1' => 'Computador',
        '2' => 'Monitor',
        '3' => 'Equpamento'
    ];
    
    public static function tableName()
    {
        return 'vw_marca_modelo';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'marca_fk', 'tipo'], 'default', 'value' => null],
            [['id', 'marca_fk', 'tipo'], 'integer'],
            [['marca'], 'string', 'max' => 40],
            [['modelo'], 'string', 'max' => 80],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca' => 'Marca',
            'marca_fk' => 'Marca Fk',
            'modelo' => 'Modelo',
            'tipo' => 'Tipo',
        ];
    }
    
    public function search($params) {
        $query = VwMarcaModelo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'marca' => SORT_ASC,
                    'modelo' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwMarcaModelo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo))', strtoupper(Setup::retirarAcento($params['VwMarcaModelo']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', strtoupper(Setup::retirarAcento($params['VwMarcaModelo']['pesquisa']))]);
        }

        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
