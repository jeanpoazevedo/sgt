<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_usuario_unidade".
 *
 * @property int $id
 * @property int $usuario_fk
 * @property int $unidade_fk
 *
 */
class UsuarioUnidade extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_usuario_unidade';
    }
    
    public function rules()
    {
        return [
            [['usuario_fk', 'unidade_fk'], 'required'],
            [['usuario_fk', 'unidade_fk'], 'default', 'value' => null],
            [['usuario_fk', 'unidade_fk'], 'integer'],
            [['usuario_fk', 'unidade_fk'], 'unique', 'targetAttribute' => ['usuario_fk', 'unidade_fk']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['usuario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_fk' => 'Usuario',
            'unidade_fk' => 'Unidade',
        ];
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getUsuarioFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_fk']);
    }
    
    public function search($params) {
        $query = Usuariounidade::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        $query->alias('usuariounidade');
        $query->join('join', 'tb_usuario', 'tb_usuario.id = usuariounidade.usuario_fk');
        $query->join('join', 'tb_unidade', 'tb_unidade.id = usuariounidade.unidade_fk');
        if (isset($params['UsuarioUnidade']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_usuario.nome))', strtoupper(Setup::retirarAcento($params['UsuarioUnidade']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_unidade.unidade))', strtoupper(Setup::retirarAcento($params['UsuarioUnidade']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
