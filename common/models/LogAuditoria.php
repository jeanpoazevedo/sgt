<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_log_auditoria".
 *
 * @property int $id
 * @property string $acao
 * @property string $usuario
 * @property string $data_hora
 * @property string $detalhes
 * @property string $query_executada
 *
 */
class LogAuditoria extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_log_auditoria';
    }
    
    public function rules()
    {
        return [
            [['acao'], 'string', 'max' => 10],
            [['usuario'], 'string', 'max' => 50],
            [['data_hora'], 'safe'],
            [['detalhes'], 'string', 'max' => 120],
            [['query_executada'], 'string'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'acao' => 'Ação',
            'usuario' => 'Usuário',
            'data_hora' => 'Data e Hora',
            'detalhes' => 'Detalhes',
            'query_executada' => 'Query Executada',
        ];
    }
    
    public function search($params) {
        $query = LogAuditoria::find();

        if (isset($params['Codigo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(acao))', strtoupper(Setup::retirarAcento($params['LogAuditoria']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario))', strtoupper(Setup::retirarAcento($params['LogAuditoria']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(data_hora))', strtoupper(Setup::retirarAcento($params['LogAuditoria']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(query_executada))', strtoupper(Setup::retirarAcento($params['LogAuditoria']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'data_hora' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
