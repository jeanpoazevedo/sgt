<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_funcionario_computador".
 *
 * @property int $id
 * @property int $funcionario_fk
 * @property int $funcionario_unidade_fk
 * @property string $nome
 * @property int $computador_fk
 * @property int $computador_unidade_fk
 * @property string $codigo_computador
 * @property string $numero_serie
 * @property string $netbios
 * @property string $mac_c
 * @property string $mac_w
 * 
 */

class VwFuncionarioComputador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_funcionario_computador';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'funcionario_fk', 'computador_fk', 'funcionario_unidade_fk', 'computador_unidade_fk'], 'integer'],
            [['nome'], 'string', 'max' => 60],
            [['codigo_computador'], 'string', 'max' => 7],
            [['numero_serie'], 'string', 'max' => 80],
            [['netbios'], 'string', 'max' => 30],
            [['mac_c', 'mac_w'], 'string', 'max' => 12],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'funcionario_fk' => 'Funcionário FK',
            'funcionario_unidade_fk' => 'Funcionário Unidade',
            'nome' => 'Funcionário',
            'computador_fk' => 'Computador FK',
            'computador_unidade_fk' => 'Computador Unidade FK',
            'codigo_computador' => 'Código Computador',
            'numero_serie' => 'Número de Série',
            'netbios' => 'Netbios',
            'mac_c' => 'MAC Cabo',
            'mac_w' => 'MAC Wi-Fi',
        ];
    }

    public function getFuncionarioFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }

    public function getFuncionariounidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'funcionario_unidade_fk']);
    }

    public function getComputadorunidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'computador_unidade_fk']);
    }
    
    public function search($params) {
        $query = VwFuncionarioComputador::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome' => SORT_ASC,
                    'codigo_computador' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwFuncionarioComputador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['VwFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_computador))', strtoupper(Setup::retirarAcento($params['VwFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', strtoupper(Setup::retirarAcento($params['VwFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(netbios))', strtoupper(Setup::retirarAcento($params['VwFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac_c))', strtoupper(Setup::retirarAcento($params['VwFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac_w))', strtoupper(Setup::retirarAcento($params['VwFuncionarioComputador']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['funcionario_unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['funcionario_unidade_fk' => $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

    public function searchfuncionariocomputador($params) {
        $query = VwFuncionarioComputador::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwFuncionarioComputador']['computador_fk'])) {
            $query->andFilterWhere(['=', 'computador_fk', $params['VwFuncionarioComputador']['computador_fk']]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
