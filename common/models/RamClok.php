<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_ram_clok".
 *
 * @property int $id
 * @property int $clok
 *
 * @property Computadores[] $Computadores
 */
class RamClok extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_ram_clok';
    }

    public function rules()
    {
        return [
            [['clok'], 'required'],
            [['clok'], 'default', 'value' => null],
            [['clok'], 'integer'],
            [['clok'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clok' => Yii::t('app', 'Clok'),
        ];
    }

    public function getTbComputadores()
    {
        return $this->hasMany(Computadores::className(), ['ram_clok' => 'id']);
    }
    
    public function search($params) {
        $query = RamClok::find();

        if (isset($params['Clok']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(clok))', strtoupper(Setup::retirarAcento($params['Clok']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'clok' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
