<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_vlan".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $id_vlan
 * @property string $vlan
 * @property string $descricao_vlan
 *
 */
class Vlan extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_vlan';
    }
    
    public function rules()
    {
        return [
            [['unidade_fk', 'id_vlan', 'vlan', 'descricao_vlan'], 'required'],
            [['unidade_fk', 'id_vlan'], 'default', 'value' => null],
            [['unidade_fk', 'id_vlan'], 'integer'],
            [['vlan'], 'string', 'max' => 20],
            [['descricao_vlan'], 'string', 'max' => 80],
            [['unidade_fk', 'id_vlan'], 'unique', 'targetAttribute' => ['unidade_fk', 'id_vlan']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'id_vlan' => 'ID VLAN',
            'vlan' => 'Nome VLAN',
            'descricao_vlan' => 'Descricao VLAN',
        ];
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Vlan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_vlan' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Vlan']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(vlan))', strtoupper(Setup::retirarAcento($params['Vlan']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['Vlan']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }
}
