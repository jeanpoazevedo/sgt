<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_usuario_perfil".
 *
 * @property int $id
 * @property int $usuario_fk
 * @property int $perfil_fk
 *
 */
class UsuarioPerfil extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_usuario_perfil';
    }
    
    public function rules()
    {
        return [
            [['usuario_fk', 'perfil_fk'], 'required'],
            [['usuario_fk', 'perfil_fk'], 'default', 'value' => null],
            [['usuario_fk', 'perfil_fk'], 'integer'],
            [['usuario_fk', 'perfil_fk'], 'unique', 'targetAttribute' => ['usuario_fk']],
            [['perfil_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['perfil_fk' => 'id']],
            [['usuario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_fk' => 'Usuario',
            'perfil_fk' => 'Perfil',
        ];
    }
    
    public function getPerfilFk()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_fk']);
    }
    
    public function getUsuarioFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_fk']);
    }
    
    public function search($params) {
        $query = UsuarioPerfil::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        $query->alias('usuarioperfil');
        $query->leftJoin('tb_usuario', 'tb_usuario.id = usuarioperfil.usuario_fk');
        $query->leftJoin('tb_perfil', 'tb_perfil.id = usuarioperfil.perfil_fk');
        if (isset($params['UsuarioPerfil']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_usuario.nome))', strtoupper(Setup::retirarAcento($params['UsuarioPerfil']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_perfil.nome))', strtoupper(Setup::retirarAcento($params['UsuarioPerfil']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
    
}
