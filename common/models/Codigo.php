<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_codigo".
 *
 * @property int $id
 * @property string $ano
 * @property string $mes
 * @property string $dispositivo
 * @property int $codigo_fk
 *
 */
class Codigo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_codigo';
    }
    
    public function rules()
    {
        return [
            [['ano', 'mes', 'dispositivo', 'codigo_fk'], 'required'],
            [['codigo_fk'], 'integer'],
            [['ano'], 'string', 'max' => 2],
            [['mes'], 'string', 'max' => 2],
            [['dispositivo'], 'string', 'max' => 1],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ano' => 'Ano',
            'mes' => 'Mês',
            'dispositivo' => 'Dispositivo',
            'codigo_fk' => 'Código',
        ];
    }

    public function getCodigoFk()
    {
        return $this->hasOne(SequenciaCodigo::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Codigo::find();

        if (isset($params['Codigo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(ano))', strtoupper(Setup::retirarAcento($params['Codigo']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mes))', strtoupper(Setup::retirarAcento($params['Codigo']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(dispositivo))', strtoupper(Setup::retirarAcento($params['Codigo']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_fk))', strtoupper(Setup::retirarAcento($params['Codigo']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_fk' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
