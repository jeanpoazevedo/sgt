<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_sequencia_codigo".
 *
 * @property int $id
 * @property int $sequencia
 * @property string $codigo
 *
 */
class SequenciaCodigo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_sequencia_codigo';
    }
    
    public function rules()
    {
        return [
            [['sequencia', 'codigo'], 'required'],
            [['sequencia'], 'integer'],
            [['codigo'], 'string', 'max' => 2],
            [['sequencia'], 'unique'],
            [['codigo'], 'unique'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sequencia' => 'Sequência',
            'codigo' => 'Código',
        ];
    }
    
    public function search($params) {
        $query = SequenciaCodigo::find();

        if (isset($params['SequenciaCodigo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(sequencia))', strtoupper(Setup::retirarAcento($params['SequenciaCodigo']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo))', strtoupper(Setup::retirarAcento($params['SequenciaCodigo']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'sequencia' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
