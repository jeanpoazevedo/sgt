<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_ocorencia_tipo".
 *
 * @property int $id
 * @property string $tipo
 *
 */
class OcorenciaTipo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_ocorencia_tipo';
    }
    
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 40],
            [['tipo'], 'unique'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
        ];
    }
    
    public function search($params) {
        $query = Ocorenciatipo::find();

        if (isset($params['Ocorenciatipo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tipo))', strtoupper(Setup::retirarAcento($params['Ocorenciatipo']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'tipo' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
