<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_unidade".
 *
 * @property int $id
 * @property string $unidade
 * @property string $dominio
 * @property int $ddd
 * @property int|null $nivel
 * @property int|null $unidade_pai_fk
 * @property int|null $entidade
 * @property string $nome_patrimonio1
 * @property string $email_patrimonio1
 * @property string $nome_patrimonio2
 * @property string $email_patrimonio2
 * @property string $descricao_unidade
 */
class Unidade extends \yii\db\ActiveRecord {
    
    public $pesquisa;
    
    public $array_nivel = [
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
    ];

    public static function tableName() {
        return 'tb_unidade';
    }

    public function rules()
    {
        return [
            [['unidade', 'descricao_unidade'], 'required'],
            [['ddd', 'nivel', 'unidade_pai_fk'], 'default', 'value' => null],
            [['ddd', 'nivel', 'unidade_pai_fk','entidade'], 'integer'],
            [['unidade'], 'string', 'max' => 40],
            [['dominio'], 'string', 'max' => 60],
            [['nome_patrimonio1', 'nome_patrimonio2'], 'string', 'max' => 80],
            [['email_patrimonio1', 'email_patrimonio2'], 'string', 'max' => 100],
            [['descricao_unidade'], 'string', 'max' => 255],
            [['unidade', 'unidade_pai_fk', 'nivel'], 'unique', 'targetAttribute' => ['unidade', 'unidade_pai_fk', 'nivel']],
            [['unidade_pai_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_pai_fk' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'unidade' => Yii::t('app', 'Sigla da Unidade'),
            'dominio' => Yii::t('app', 'Domínio'),
            'ddd' => Yii::t('app', 'DDD'),
            'nivel' => 'Nível',
            'unidade_pai_fk' => 'Unidade Pai',
            'entidade' => 'Entidade',
            'nome_patrimonio1' => Yii::t('app', 'Nome Patrimonio 1'),
            'email_patrimonio1' => Yii::t('app', 'e-mail Patrimonio 1'),
            'nome_patrimonio2' => Yii::t('app', 'Nome Patrimonio 2'),
            'email_patrimonio2' => Yii::t('app', 'e-mail Patrimonio 2'),
            'descricao_unidade' => Yii::t('app', 'Descrição'),
        ];
    }
    
    public function getUnidadePaiFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_pai_fk']);
    }
    
    public function search($params) {
        $query = Unidade::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nivel' => SORT_ASC,
                    'unidade_pai_fk' => SORT_ASC,
                    'unidade' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Unidade']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade))', strtoupper(Setup::retirarAcento($params['Unidade']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(dominio))', strtoupper(Setup::retirarAcento($params['Unidade']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_patrimonio1))', strtoupper(Setup::retirarAcento($params['Unidade']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_patrimonio2))', strtoupper(Setup::retirarAcento($params['Unidade']['pesquisa']))]);
        }
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        if ($usuarioperfil->perfil_fk != 1){
            $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
            $query->andFilterWhere(['IN', 'id', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }

}
