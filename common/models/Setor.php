<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_setor".
 *
 * @property int $id
 * @property string $setor
 *
 */
class Setor extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_setor';
    }

    public function rules()
    {
        return [
            [['setor'], 'required'],
            [['setor'], 'string', 'max' => 50],
            [['setor'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'setor' => Yii::t('app', 'Setor'),
        ];
    }
    
    public function search($params) {
        $query = Setor::find();

        if (isset($params['Setor']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(setor))', strtoupper(Setup::retirarAcento($params['Setor']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
