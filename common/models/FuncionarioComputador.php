<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_funcionario_computador".
 *
 * @property int $id
 * @property int $funcionario_fk
 * @property int $computador_fk
 *
 */
class FuncionarioComputador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_funcionario_computador';
    }
    
    public function rules()
    {
        return [
            [['funcionario_fk', 'computador_fk'], 'required'],
            [['funcionario_fk', 'computador_fk'], 'default', 'value' => null],
            [['funcionario_fk', 'computador_fk'], 'integer'],
            [['funcionario_fk', 'computador_fk'], 'unique', 'targetAttribute' => ['funcionario_fk', 'computador_fk']],
            [['funcionario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionario::className(), 'targetAttribute' => ['funcionario_fk' => 'id']],
            [['computador_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Computador::className(), 'targetAttribute' => ['computador_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'funcionario_fk' => 'Funcionário',
            'computador_fk' => 'Computador',
        ];
    }
    
    public function getUsuariodeFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }
    
    public function getUsuarioparaFk()
    {
        return $this->hasOne(Computador::className(), ['id' => 'computador_fk']);
    }
    
    public function search($params) {
        $query = FuncionarioComputgador::find();

        if (isset($params['FuncionarioComputgador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(id))', strtoupper(Setup::retirarAcento($params['FuncionarioComputgador']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['funcionario_unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['funcionario_unidade_fk' => $usuariounidade]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'funcionario_fk' => SORT_ASC,
                    'computador_fk' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
