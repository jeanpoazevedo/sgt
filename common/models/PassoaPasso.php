<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_passo_passo".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $perfil_fk
 * @property string $titulo
 * @property string $descricao
 * @property string $passo_passo
 *
 */
class PassoaPasso extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_passo_passo';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'perfil_fk', 'titulo', 'descricao', 'passo_passo'], 'required'],
            [['unidade_fk', 'perfil_fk'], 'default', 'value' => null],
            [['unidade_fk', 'perfil_fk'], 'integer'],
            [['passo_passo'], 'string'],
            [['titulo'], 'string', 'max' => 60],
            [['descricao'], 'string', 'max' => 120],
            [['unidade_fk', 'titulo', 'descricao'], 'unique', 'targetAttribute' => ['unidade_fk', 'titulo', 'descricao']],
            [['perfil_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['perfil_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unidade_fk' => Yii::t('app', 'Unidade'),
            'perfil_fk' => Yii::t('app', 'Perfil'),
            'titulo' => Yii::t('app', 'Título'),
            'descricao' => Yii::t('app', 'Descrição'),
            'passo_passo' => Yii::t('app', 'Passo a Passo'),
        ];
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getPerfilFk()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_fk']);
    }
    
    public function search($params) {
        $query = PassoaPasso::find();

        if (isset($params['PassoaPasso']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(titulo))', strtoupper(Setup::retirarAcento($params['PassoaPasso']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['PassoaPasso']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(passo_passo))', strtoupper(Setup::retirarAcento($params['PassoaPasso']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        $query->andWhere(['>=', 'perfil_fk', $usuarioperfil->perfil_fk]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
