<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "vw_computador".
 *
 * @property int $id
 * @property string $codigo_computador
 * @property string $unidade
 * @property int $unidade_fk
 * @property string $setor
 * @property int $setor_fk
 * @property string $tipo
 * @property int $tipo_fk
 * @property string $marca
 * @property int $marca_fk
 * @property string $modelo
 * @property int $modelo_fk
 * @property string $numero_serie
 * @property string $loja
 * @property string $numero_patrimonio
 * @property string $numero_lacre
 * @property string $descricao_computador
 * @property string $netbios
 * @property string $mac_c
 * @property string $mac_w
 * @property int $status
 * @property string $url
 * 
 */

class VwComputador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_computador';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'unidade_fk', 'setor_fk', 'tipo_fk', 'marca_fk', 'modelo_fk', 'status'], 'default', 'value' => null],
            [['id', 'unidade_fk', 'setor_fk', 'tipo_fk', 'marca_fk', 'modelo_fk', 'status'], 'integer'],
            [['descricao_computador'], 'string'],
            [['codigo_computador'], 'string', 'max' => 7],
            [['unidade', 'tipo', 'marca', 'numero_patrimonio'], 'string', 'max' => 40],
            [['setor'], 'string', 'max' => 50],
            [['modelo', 'numero_serie', 'loja'], 'string', 'max' => 80],
            [['numero_lacre'], 'string', 'max' => 20],
            [['netbios'], 'string', 'max' => 30],
            [['mac_c', 'mac_w'], 'string', 'max' => 12],
            [['url'], 'string', 'max' => 255],
        ];
    }

    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_computador' => 'Codigo Computador',
            'unidade' => 'Unidade',
            'unidade_fk' => 'Unidade Fk',
            'setor' => 'Setor',
            'setor_fk' => 'Setor Fk',
            'tipo' => 'Tipo',
            'tipo_fk' => 'Tipo Fk',
            'marca' => 'Marca',
            'marca_fk' => 'Marca Fk',
            'modelo' => 'Modelo',
            'modelo_fk' => 'Modelo Fk',
            'numero_serie' => 'Numero Serie',
            'loja' => 'Loja',
            'numero_patrimonio' => 'Numero Patrimonio',
            'numero_lacre' => 'Numero Lacre',
            'descricao_computador' => 'Descricao Computador',
            'netbios' => 'Netbios',
            'mac_c' => 'Mac C',
            'mac_w' => 'Mac W',
            'status' => 'Status',
            'url' => 'Url',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->mac_c) {
            $this->mac_c = Setup::formatterMAC($this->mac_c);
        }
        if ($this->mac_w) {
            $this->mac_w = Setup::formatterMAC($this->mac_w);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->codigo_computador) {
            $this->codigo_computador = strtoupper($this->codigo_computador);
        }
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = VwComputador::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if (isset($params['VwComputador']['pesquisa'])) {
            $pesquisa = strtoupper($params['VwComputador']['pesquisa']);
            $pesquisaSemAcento = strtoupper(Setup::retirarAcento($params['VwComputador']['pesquisa']));
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_computador))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(setor))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tipo))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(mac_c)', $pesquisa])
                ->orFilterWhere(['like', 'UPPER(mac_c)', strtoupper(Setup::removeFormatterMAC($pesquisa))])
                ->orFilterWhere(['like', 'UPPER(mac_w)', $pesquisa])
                ->orFilterWhere(['like', 'UPPER(mac_w)', strtoupper(Setup::removeFormatterMAC($pesquisa))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_patrimonio))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(loja))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(netbios))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_computador))', $pesquisaSemAcento]);
            if (ctype_digit($pesquisa)) {
                $query->orFilterWhere(['=', 'numero_nota', $pesquisa])
                      ->orFilterWhere(['=', 'valor', $pesquisa]);
            }
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }
        
        if (Yii::$app->user->identity->tipo_temp_fk) {
            $query->andFilterWhere(['=', 'tipo_fk', Yii::$app->user->identity->tipo_temp_fk]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andFilterWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }
        
        if (Yii::$app->user->identity->modelo_temp_fk) {
            $query->andFilterWhere(['=', 'modelo_fk', Yii::$app->user->identity->modelo_temp_fk]);
        }
        
        $model_restricao = VwRestricaoMenu::findAll(['controller' => 'computador', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);
        
        $model_restricao ?  '' : $query->andFilterWhere(['=', 'status', 1]);

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }
}
