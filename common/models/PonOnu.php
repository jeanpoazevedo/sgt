<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_pon_onu".
 *
 * @property int $id
 * @property int $olt_fk
 * @property int $setor_fk
 * @property int $codigo_onu_fk
 * @property int $porta_pon
 * @property string|null $onu_profile
 * @property string|null $ponto
 * @property int|null $porta_pon_id
 */
class PonOnu extends \yii\db\ActiveRecord
{
    public $pesquisa;

    public static function tableName()
    {
        return 'tb_pon_onu';
    }

    public function rules()
    {
        return [
            [['olt_fk', 'setor_fk', 'codigo_onu_fk', 'porta_pon'], 'required'],
            [['olt_fk', 'setor_fk', 'codigo_onu_fk', 'porta_pon', 'porta_pon_id'], 'default', 'value' => null],
            [['olt_fk', 'setor_fk', 'codigo_onu_fk', 'porta_pon', 'porta_pon_id'], 'integer'],
            [['onu_profile', 'ponto'], 'string'],
            [['olt_fk', 'porta_pon', 'codigo_onu_fk'], 'unique', 'targetAttribute' => ['olt_fk', 'porta_pon', 'codigo_onu_fk']],
            [['codigo_onu_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Equipamento::className(), 'targetAttribute' => ['codigo_onu_fk' => 'id']],
            [['olt_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Pon::className(), 'targetAttribute' => ['olt_fk' => 'id']],
            [['setor_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Setor::className(), 'targetAttribute' => ['setor_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'olt_fk' => 'OLT ID',
            'setor_fk' => 'Setor',
            'codigo_onu_fk' => 'Código ONU',
            'porta_pon' => 'Porta PON',
            'porta_pon_id' => 'Porta PON ID',
            'onu_profile' => 'ONO-Profile',
            'ponto' => 'Ponto',
        ];
    }

    public function getCodigoOnuFk()
    {
        return $this->hasOne(Equipamento::className(), ['id' => 'codigo_onu_fk']);
    }

    public function getOltFk()
    {
        return $this->hasOne(Pon::className(), ['id' => 'olt_fk']);
    }

    public function getSetorFk()
    {
        return $this->hasOne(Setor::className(), ['id' => 'setor_fk']);
    }

    public function search($params) {
        $query = PonOnu::find();

        $query->alias('pononu');
        $query->leftJoin('tb_equipamento', 'tb_equipamento.id = pononu.codigo_onu_fk');
        $query->leftJoin('tb_setor', 'tb_setor.id = pononu.setor_fk');

        if (isset($params['PonOnu']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(onu_profile))', strtoupper(Setup::retirarAcento($params['PonOnu']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(ponto))', strtoupper(Setup::retirarAcento($params['PonOnu']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_setor.setor))', strtoupper(Setup::retirarAcento($params['PonOnu']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_equipamento.codigo_equipamento))', strtoupper(Setup::retirarAcento($params['PonOnu']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_equipamento.numero_serie))', strtoupper(Setup::retirarAcento($params['PonOnu']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_equipamento.mac))', strtoupper(Setup::retirarAcento($params['PonOnu']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }

    public function searchonu($params) {
        $query = PonOnu::find();

        if (isset($params['PonOnu']['olt_fk'])) {
            $query->andFilterWhere(['=', 'olt_fk', $params['PonOnu']['olt_fk']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
