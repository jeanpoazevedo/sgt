<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_funcionario_monitor".
 *
 * @property int $id
 * @property int $funcionario_fk
 * @property int $monitor_fk
 *
 */
class FuncionarioMonitor extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_funcionario_monitor';
    }
    
    public function rules()
    {
        return [
            [['funcionario_fk', 'monitor_fk'], 'required'],
            [['funcionario_fk', 'monitor_fk'], 'default', 'value' => null],
            [['funcionario_fk', 'monitor_fk'], 'integer'],
            [['funcionario_fk', 'monitor_fk'], 'unique', 'targetAttribute' => ['funcionario_fk', 'monitor_fk']],
            [['funcionario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionario::className(), 'targetAttribute' => ['funcionario_fk' => 'id']],
            [['monitor_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Monitor::className(), 'targetAttribute' => ['monitor_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'funcionario_fk' => 'Funcionário',
            'monitor_fk' => 'Monitor',
        ];
    }
    
    public function getUsuariodeFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }
    
    public function getUsuarioparaFk()
    {
        return $this->hasOne(Monitor::className(), ['id' => 'monitor_fk']);
    }
    
    public function search($params) {
        $query = FuncionarioMonitor::find();

        if (isset($params['FuncionarioMonitor']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(id))', strtoupper(Setup::retirarAcento($params['FuncionarioMonitor']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['funcionario_unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['funcionario_unidade_fk' => $usuariounidade]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'funcionario_fk' => SORT_ASC,
                    'monitor_fk' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
