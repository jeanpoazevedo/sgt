<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_office".
 *
 * @property int $id
 * @property string $office
 *
 */
class Office extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_office';
    }

    public function rules()
    {
        return [
            [['office'], 'required'],
            [['office'], 'string', 'max' => 200],
            [['office'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'office' => Yii::t('app', 'Office'),
        ];
    }
    
    public function search($params) {
        $query = Office::find();

        if (isset($params['Office']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(office))', strtoupper(Setup::retirarAcento($params['Office']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
