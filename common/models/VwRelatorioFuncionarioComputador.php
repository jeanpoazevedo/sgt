<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_relatorio_funcionario_computador".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property string $funcionario_setor
 * @property int $funcionario_setor_fk
 * @property string $funcao
 * @property string $nome
 * @property int $computador_unidade_fk
 * @property string $computador_setor
 * @property int $computador_setor_fk
 * @property string $tipo
 * @property int $tipo_fk
 * @property string $marca
 * @property int $marca_fk
 * @property string $modelo
 * @property int $modelo_fk
 * @property string $codigo_computador
 * @property string $numero_serie
 * @property string $netbios
 * @property string $mac_c
 * @property string $mac_w
 * 
 */

class VwRelatorioFuncionarioComputador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_relatorio_funcionario_computador';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'unidade_fk', 'funcionario_setor_fk', 'computador_unidade_fk', 'computador_setor_fk', 'tipo_fk', 'marca_fk', 'modelo_fk'], 'integer'],
            [['funcionario_setor', 'funcao', 'funcionario_setor', 'computador_setor'], 'string', 'max' => 50],
            [['nome'], 'string', 'max' => 60],
            [['tipo', 'marca'], 'string', 'max' => 40],
            [['modelo'], 'string', 'max' => 80],
            [['codigo_computador'], 'string', 'max' => 7],
            [['numero_serie'], 'string', 'max' => 80],
            [['netbios'], 'string', 'max' => 30],
            [['mac_c', 'mac_w'], 'string', 'max' => 12],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade do Funcionário',
            'funcionario_setor' => 'Setor do Funcionádio',
            'funcionario_setor_fk' => 'Setor do Funcionádio Fk',
            'funcao' => 'Função',
            'nome' => 'Funcionádio',
            'computador_unidade_fk' => 'Unidade do Computador',
            'computador_setor' => 'Setor do Computador',
            'computador_setor_fk' => 'Setor do Computador Fk',
            'tipo' => 'Tipo',
            'tipo_fk' => 'Tipo Fk',
            'marca' => 'Marca',
            'marca_fk' => 'Marca Fk',
            'modelo' => 'Modelo',
            'modelo_fk' => 'Modelo Fk',
            'codigo_computador' => 'Código Computador',
            'numero_serie' => 'Número de Série',
            'netbios' => 'Netbios',
            'mac_c' => 'MAC Cabo',
            'mac_w' => 'MAC Wi-Fi',
        ];
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }

    public function getComputadorunidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'computador_unidade_fk']);
    }
    
    public function search($params) {
        $query = VwRelatorioFuncionarioComputador::find();

        if (isset($params['VwRelatorioFuncionarioComputador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(funcionario_setor))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(funcao))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(computador_setor))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tipo))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_computador))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(netbios))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac_c))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac_w))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioComputador']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['unidade_fk' => $usuariounidade]);
        }

        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'funcionario_setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }
        
        if (Yii::$app->user->identity->tipo_temp_fk) {
            $query->andFilterWhere(['=', 'tipo_fk', Yii::$app->user->identity->tipo_temp_fk]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andFilterWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }
        
        if (Yii::$app->user->identity->modelo_temp_fk) {
            $query->andFilterWhere(['=', 'modelo_fk', Yii::$app->user->identity->modelo_temp_fk]);
        }

        if (Yii::$app->user->identity->funcao_temp != NULL) {
            $query->andWhere(['=', 'funcao', Yii::$app->user->identity->funcao_temp]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome' => SORT_ASC,
                    'codigo_computador' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
