<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_helpdesk_cadastro".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $de_fk
 * @property string $data_abertura
 * @property string $data_encerrado
 * @property int $produto_fk
 * @property int $prioridade
 * @property int $para_fk
 * @property string $resumo
 * @property string $descricao
 * @property int $ver
 * @property int $status
 *
 */
class HdCadastro extends \yii\db\ActiveRecord {

    public $pesquisa;
    public $array_status = [
        '1' => 'Aguardando',
        '2' => 'Em Andamento',
        '3' => 'Pausado',
        '5' => 'Resolvido',
        '4' => 'Encerrado',
    ];
    
    public $array_prioridade = [
        '1' => 'Baixa',
        '2' => 'Normal',
        '3' => 'Alta',
        '4' => 'Crítica',
    ];

    public static function tableName() {
        return 'tb_helpdesk_cadastro';
    }

    public function rules()
    {
        return [
            [['unidade_fk','de_fk', 'produto_fk', 'prioridade', 'para_fk', 'resumo', 'ver', 'status'], 'required'],
            [['unidade_fk','de_fk', 'produto_fk', 'prioridade', 'para_fk', 'ver', 'status'], 'default', 'value' => null],
            [['unidade_fk','de_fk', 'produto_fk', 'prioridade', 'para_fk', 'ver', 'status'], 'integer'],
            [['data_abertura', 'data_encerrado'], 'safe'],
            [['descricao'], 'string'],
            [['resumo'], 'string', 'max' => 255],
            [['produto_fk'], 'exist', 'skipOnError' => true, 'targetClass' => HdProduto::className(), 'targetAttribute' => ['produto_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['de_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['de_fk' => 'id']],
            [['para_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['para_fk' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'Chamado'),
            'unidade_fk' => Yii::t('app', 'Unidade'),
            'de_fk' => Yii::t('app', 'Solicitante'),
            'data_abertura' => Yii::t('app', 'Aberto'),
            'data_encerrado' => Yii::t('app', 'Fechado'),
            'produto_fk' => Yii::t('app', 'Produto'),
            'prioridade' => Yii::t('app', 'Prioridade'),
            'para_fk' => Yii::t('app', 'Atendente'),
            'resumo' => Yii::t('app', 'Resumo'),
            'descricao' => Yii::t('app', 'Descricao'),
            'ver' => Yii::t('app', 'Visualização Pública'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data_abertura) {
            $this->data_abertura = Setup::convertApresentacao($this->data_abertura, 'datetime');
        }
        if ($this->data_encerrado) {
            $this->data_encerrado = Setup::convertApresentacao($this->data_encerrado, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data_abertura) {
            $this->data_abertura = Setup::convertBD($this->data_abertura, 'datetime');
        }
        if ($this->data_encerrado) {
            $this->data_encerrado = Setup::convertBD($this->data_encerrado, 'datetime');
        }
    }

    public function getDeFk() {
        return $this->hasOne(Usuario::className(), ['id' => 'de_fk']);
    }

    public function getUnidadeFk() {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }

    public function getParaFk() {
        return $this->hasOne(Usuario::className(), ['id' => 'para_fk']);
    }

    public function getProdutoFk() {
        return $this->hasOne(HdProduto::className(), ['id' => 'produto_fk']);
    }

    public function search($params) {
        $query = HdCadastro::find();
        
        $query->alias('hdcadastro');
        $query->leftJoin('tb_usuario', 'tb_usuario.id = hdcadastro.de_fk');
        if (isset($params['HdCadastro']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(hdcadastro.resumo))', strtoupper(Setup::retirarAcento($params['HdCadastro']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(hdcadastro.descricao))', strtoupper(Setup::retirarAcento($params['HdCadastro']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_usuario.nome))', strtoupper(Setup::retirarAcento($params['HdCadastro']['pesquisa']))]);
        }

        if (Yii::$app->user->identity->unidade_temp_fk > '1') {
            $query->orFilterWhere(['=', 'hdcadastro.de_fk', Yii::$app->user->identity->id]);
            $query->orFilterWhere(['=', 'hdcadastro.para_fk', Yii::$app->user->identity->id]);
        }
        
        $query->andFilterWhere(['IN', 'hdcadastro.status', ['1','2','3','5']]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_ASC,
                    'id' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
    
    public function searchbuscar($params) {
        $query = HdCadastro::find();
        
        $query->alias('hdcadastro');
        $query->leftJoin('tb_usuario', 'tb_usuario.id = hdcadastro.de_fk');
        if (isset($params['HdCadastro']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(hdcadastro.resumo))', strtoupper(Setup::retirarAcento($params['HdCadastro']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(hdcadastro.descricao))', strtoupper(Setup::retirarAcento($params['HdCadastro']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_usuario.nome))', strtoupper(Setup::retirarAcento($params['HdCadastro']['pesquisa']))]);
        }
        
        $query->andFilterWhere(['=', 'hdcadastro.ver', '1']);
        $query->andFilterWhere(['=', 'hdcadastro.status', '4']);
        
        if (Yii::$app->user->identity->produto_temp_fk) {
            $query->andWhere(['=', 'hdcadastro.produto_fk', Yii::$app->user->identity->produto_temp_fk]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }

}
