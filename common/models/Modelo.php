<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_modelo".
 *
 * @property int $id
 * @property int $marca_fk
 * @property string $modelo
 * @property int $tipo
 *
 * @property Marcas $marcaFk
 */
class Modelo extends \yii\db\ActiveRecord {

    public $pesquisa;

    public $array_tipo = [
        '1' => 'Computador',
        '2' => 'Monitor',
        '3' => 'Equpamento'
    ];

    public static function tableName() {
        return 'tb_modelo';
    }

    public function rules()
    {
        return [
            [['marca_fk', 'modelo', 'tipo'], 'required'],
            [['marca_fk', 'tipo'], 'default', 'value' => null],
            [['marca_fk', 'tipo'], 'integer'],
            [['modelo'], 'string', 'max' => 80],
            [['marca_fk', 'modelo'], 'unique', 'targetAttribute' => ['marca_fk', 'modelo']],
            [['marca_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['marca_fk' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'marca_fk' => Yii::t('app', 'Marca'),
            'modelo' => Yii::t('app', 'Modelo'),
            'tipo' => Yii::t('app', 'Tipo'),
        ];
    }

    public function getMarcaFk() {
        return $this->hasOne(Marca::className(), ['id' => 'marca_fk']);
    }

    public function search($params) {
        $query = Modelo::find();

        $query->alias('modelo');
        $query->leftJoin('tb_marca', 'tb_marca.id = modelo.marca_fk');
        if (isset($params['Modelo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo.modelo))', strtoupper(Setup::retirarAcento($params['Modelo']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_marca.marca))', strtoupper(Setup::retirarAcento($params['Modelo']['pesquisa']))]);
        }

        $query->orderBy(['tb_marca.marca' => SORT_ASC, 'modelo.modelo' => SORT_ASC]);
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andWhere(['=', 'modelo.marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }

}
