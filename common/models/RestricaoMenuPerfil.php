<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tb_restricao_menu_perfil".
 *
 * @property int $id
 * @property int $perfil_fk
 * @property int $menu_fk
 * @property string $restricao
 *
 */
class RestricaoMenuPerfil extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return 'tb_restricao_menu_perfil';
    }
    
    public function rules()
    {
        return [
            [['perfil_fk', 'menu_fk', 'restricao'], 'required'],
            [['perfil_fk', 'menu_fk'], 'default', 'value' => null],
            [['perfil_fk', 'menu_fk'], 'integer'],
            [['restricao'], 'string', 'max' => 200],
            [['menu_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_fk' => 'id']],
            [['perfil_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['perfil_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'perfil_fk' => 'Perfil Fk',
            'menu_fk' => 'Menu Fk',
            'restricao' => 'Restricao',
        ];
    }
    
    public function getMenuFk()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_fk']);
    }
    
    public function getPerfilFk()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_fk']);
    }
}
