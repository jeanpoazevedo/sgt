<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_monitor".
 *
 * @property int $id
 * @property string $codigo_monitor
 * @property int $unidade_fk
 * @property int $setor_fk
 * @property int $tipo_fk
 * @property int $modelo_fk
 * @property string $numero_serie
 * @property string $data_compra
 * @property string $garantia
 * @property string $loja
 * @property int $numero_nota
 * @property string $valor
 * @property string $numero_patrimonio
 * @property string $netbios
 * @property string $mac
 * @property string $descricao_monitor
 * @property int $status
 * @property string $url
 *
 */
class Monitor extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_monitor';
    }
    
    public function rules()
    {
        return [
            [['codigo_monitor', 'unidade_fk', 'setor_fk', 'tipo_fk', 'modelo_fk', 'numero_serie', 'data_compra', 'garantia', 'status'], 'required'],
            [['unidade_fk', 'setor_fk', 'tipo_fk', 'modelo_fk', 'numero_nota', 'status'], 'default', 'value' => null],
            [['unidade_fk', 'setor_fk', 'tipo_fk', 'modelo_fk', 'numero_nota', 'status'], 'integer'],
            [['data_compra', 'garantia'], 'safe'],
            [['valor'], 'number', 'numberPattern' => '/^-?(?:[0-9]{1,3})(?:.[0-9]{3})*(?:|\,[0-9]+)$/'],
            [['descricao_monitor'], 'string'],
            [['codigo_monitor'], 'string', 'max' => 7],
            [['numero_serie', 'loja'], 'string', 'max' => 80],
            [['numero_patrimonio'], 'string', 'max' => 40],
            [['netbios'], 'string', 'max' => 30],
            [['mac'], 'string', 'max' => 17],
            [['url'], 'string', 'max' => 255],
            [['codigo_monitor'], 'unique', 'targetAttribute' => ['codigo_monitor']],
            [['numero_patrimonio'], 'unique', 'targetAttribute' => ['numero_patrimonio']],
            [['modelo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['modelo_fk' => 'id']],
            [['setor_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Setor::className(), 'targetAttribute' => ['setor_fk' => 'id']],
            [['tipo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_monitor' => 'Código Monitor',
            'unidade_fk' => 'Unidade',
            'setor_fk' => 'Setor',
            'tipo_fk' => 'Tipo',
            'modelo_fk' => 'Modelo',
            'numero_serie' => 'Número de Série',
            'data_compra' => 'Data Compra',
            'garantia' => 'Garantia',
            'loja' => 'Loja',
            'numero_nota' => 'Número da Nota',
            'valor' => 'Valor',
            'numero_patrimonio' => 'Número do Patrimonio',
            'netbios' => 'Netbios',
            'mac' => 'MAC',
            'descricao_monitor' => 'Descrição Monitor',
            'status' => 'Status',
            'url' => 'Arquivo Nota Fiscal',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data_compra) {
            $this->data_compra = Setup::convertApresentacao($this->data_compra, 'date');
        }
        if ($this->garantia) {
            $this->garantia = Setup::convertApresentacao($this->garantia, 'date');
        }
        if ($this->valor) {
            $this->valor = FormatterCurrency::formatNumber($this->valor);
        }
        if ($this->mac) {
            $this->mac = Setup::formatterMAC($this->mac);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->codigo_monitor) {
            $this->codigo_monitor = strtoupper($this->codigo_monitor);
        }
        if ($this->data_compra) {
            $this->data_compra = Setup::convertBD($this->data_compra, 'date');
        }
        if ($this->garantia) {
            $this->garantia = Setup::convertBD($this->garantia, 'date');
        }
        if ($this->valor) {
            $this->valor = FormatterCurrency::unformatNumber($this->valor);
        }
        if ($this->mac) {
            $this->mac = Setup::removeFormatterMAC($this->mac);
        }
    }
    
    public function getModeloFk()
    {
        return $this->hasOne(Modelo::className(), ['id' => 'modelo_fk']);
    }
    
    public function getSetorFk()
    {
        return $this->hasOne(Setor::className(), ['id' => 'setor_fk']);
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getTipoFk()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_fk']);
    }
    
    public function search($params) {
        $query = Monitor::find();

        $query->alias('monitor');
        $query->leftJoin('tb_modelo', 'tb_modelo.id = monitor.modelos_fk');
        if (isset($params['Monitor']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(monitor.codigo_monitor))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_modelo.modelo))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(monitor.numero_serie))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(monitor.mac))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(monitor.loja))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(monitor.numero_nota))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(monitor.numero_patrimonio))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(monitor.descricao_moniot))', strtoupper(Setup::retirarAcento($params['Monitor']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['unidade_fk' => $usuariounidade]);
        }
        
        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }
        
        if (Yii::$app->user->identity->tipo_temp_fk) {
            $query->andFilterWhere(['=', 'tipo_fk', Yii::$app->user->identity->tipo_temp_fk]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andFilterWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }
        
        if (Yii::$app->user->identity->modelo_temp_fk) {
            $query->andFilterWhere(['=', 'modelo_fk', Yii::$app->user->identity->modelo_temp_fk]);
        }
        
        $model_restricao = VwRestricaoMenu::findAll(['controller' => 'monitor', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);
        
        $model_restricao ?  '' : $query->andFilterWhere(['=', 'status', 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
