<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_sistema_operacioanl".
 *
 * @property int $id
 * @property string $sistema
 * 
 */
class SistemaOperacional extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_sistema_operacional';
    }

    public function rules()
    {
        return [
            [['sistema'], 'required'],
            [['sistema'], 'string', 'max' => 200],
            [['sistema'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sistema' => 'Sistema Operacional',
        ];
    }
    
    public function search($params) {
        $query = SistemaOperacional::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'sistema' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['SistemaOperacional']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(sistema))', strtoupper(Setup::retirarAcento($params['SistemaOperacional']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
