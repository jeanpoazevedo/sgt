<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_switch".
 *
 * @property int $id
 * @property int $codigo_switch_fk
 * @property int $local_fk
 * @property int $id_switch
 * @property string $descricao_switch
 * @property int $numero_portas
 * @property string $xml_portas
 * @property string $configuracao
 *
 */
class Switchdoc extends \yii\db\ActiveRecord {

    public $pesquisa;
    public $numero_porta;
    public $status;
    public $type;
    public $vlan;
    public $tagged;
    public $descricao;
    
    public $array_type = [
        '1' => 'Access',
        '2' => 'Trunk',
        '3' => 'Hybrid',
    ];

    public static function tableName() {
        return 'tb_switch';
    }

    public function rules()
    {
        return [
            [['codigo_switch_fk', 'local_fk', 'id_switch', 'numero_portas', 'xml_portas'], 'required'],
            [['codigo_switch_fk', 'local_fk', 'id_switch', 'numero_portas'], 'default', 'value' => null],
            [['codigo_switch_fk', 'local_fk', 'id_switch', 'numero_portas'], 'integer'],
            [['descricao_switch'], 'string', 'max' => 80],
            [['xml_portas', 'configuracao'], 'string'],
            [['codigo_switch_fk', 'local_fk', 'id_switch'], 'unique', 'targetAttribute' => ['codigo_switch_fk', 'local_fk', 'id_switch']],
            [['codigo_switch_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Equipamento::className(), 'targetAttribute' => ['codigo_switch_fk' => 'id']],
            [['local_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['local_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_switch_fk' => 'Codigo Switch',
            'local_fk' => 'Local',
            'id_switch' => 'ID Switch',
            'descricao_switch' => 'Descrição do Switch',
            'numero_portas' => 'Numero Portas',
            'xml_portas' => 'Xml Portas',
            'configuracao' => 'Backup TXT'
        ];
    }

    public function getCodigoSwitchFk() {
        return $this->hasOne(Equipamento::className(), ['id' => 'codigo_switch_fk']);
    }

    public function getLocalFk() {
        return $this->hasOne(Local::className(), ['id' => 'local_fk']);
    }
    
    public function getVlanlFk() {
        return $this->hasOne(Vlan::className(), ['id' => 'vlan']);
    }

    public function search($params) {
        $query = Switchdoc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'local_fk' => SORT_ASC,
                    'id_switch' => SORT_ASC,
                ]
            ],
        ]);

        $query->alias('switch');
        $query->leftJoin('tb_equipamento', 'tb_equipamento.id = switch.codigo_switch_fk');

        if (isset($params['Switchdoc']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(xml_portas))', strtoupper(Setup::retirarAcento($params['Switchdoc']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_switch))', strtoupper(Setup::retirarAcento($params['Switchdoc']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(configuracao))', strtoupper(Setup::retirarAcento($params['Switchdoc']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

}
