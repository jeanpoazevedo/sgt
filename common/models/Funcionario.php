<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_funcionario".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $setor_fk
 * @property string $funcao
 * @property string $nome
 * @property string $data_nascimento
 * @property string $email_1
 * @property string $email_2
 * @property string $telefone
 * @property string $celular
 * @property int $so_contato
 * @property int $status
 * @property string $nota
 *
 */
class Funcionario extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_funcionario';
    }
    
    public function rules()
    {
        return [
            [['unidade_fk', 'setor_fk', 'nome', 'so_contato', 'status'], 'required'],
            [['unidade_fk', 'setor_fk', 'so_contato', 'status'], 'default', 'value' => null],
            [['unidade_fk', 'setor_fk', 'so_contato', 'status'], 'integer'],
            [['data_nascimento'], 'safe'],
            [['nota'], 'string'],
            [['funcao'], 'string', 'max' => 50],
            [['nome'], 'string', 'max' => 60],
            [['email_1', 'email_2'], 'string', 'max' => 100],
            [['telefone', 'celular'], 'string', 'max' => 15],
            [['unidade_fk', 'nome'], 'unique', 'targetAttribute' => ['unidade_fk', 'nome']],
            [['setor_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Setor::className(), 'targetAttribute' => ['setor_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'setor_fk' => 'Setor',
            'funcao' => 'Função',
            'nome' => 'Nome',
            'data_nascimento' => 'Data de Nascimento',
            'email_1' => 'e-Mail 1',
            'email_2' => 'e-Mail 2',
            'telefone' => 'Telefone',
            'celular' => 'Celular',
            'so_contato' => 'So Contato',
            'status' => 'Status',
            'nota' => 'Nota',
        ];
    }

    public function afterFind() {
        parent::afterFind();
        if ($this->data_nascimento) {
            $this->data_nascimento = Setup::convertApresentacao($this->data_nascimento, 'date');
        }
        if ($this->telefone) {
            $this->telefone = Setup::formatterTelefone($this->telefone);
        }
        if ($this->celular) {
            $this->celular = Setup::formatterTelefone($this->celular);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data_nascimento) {
            $this->data_nascimento = Setup::convertBD($this->data_nascimento, 'date');
        }
        if ($this->telefone) {
            $this->telefone = Setup::unformatterString($this->telefone);
        }
        if ($this->celular) {
            $this->celular = Setup::unformatterString($this->celular);
        }
    }
    
    public function getSetorFk()
    {
        return $this->hasOne(Setor::className(), ['id' => 'setor_fk']);
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Funcionario::find();

        if (isset($params['Funcionario']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Funcionario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(email_1))', strtoupper(Setup::retirarAcento($params['Funcionario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(email_2))', strtoupper(Setup::retirarAcento($params['Funcionario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(telefone))', strtoupper(Setup::retirarAcento($params['Funcionario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(celular))', strtoupper(Setup::retirarAcento($params['Funcionario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(funcao))', strtoupper(Setup::retirarAcento($params['Funcionario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nota))', strtoupper(Setup::retirarAcento($params['Funcionario']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_DESC,
                    'so_contato' => SORT_ASC,
                    'nome' => SORT_ASC, 
                ]
            ],
        ]);

        return $dataProvider;
    }
}
