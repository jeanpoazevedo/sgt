<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_mapa_ip_v4".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property string $rede
 * @property string $ip
 * @property int $cidr
 * @property string $grupo
 * @property string $mac
 * @property string $descricao
 *
 */
class MapaIpV4 extends \yii\db\ActiveRecord
{
    
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_mapa_ip_v4';
    }
    
    public function rules()
    {
        return [
            [['unidade_fk', 'rede', 'ip', 'cidr'], 'required'],
            [['unidade_fk', 'cidr'], 'default', 'value' => null],
            [['unidade_fk', 'cidr'], 'integer'],
            [['rede', 'grupo'], 'string', 'max' => 80],
            [['ip', 'mac'], 'string', 'max' => 18],
            [['descricao'], 'string', 'max' => 100],
            [['unidade_fk', 'rede', 'ip', 'mac'], 'unique', 'targetAttribute' => ['unidade_fk', 'rede', 'ip', 'mac']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'rede' => 'Rede',
            'ip' => 'IP',
            'cidr' => 'CIDR',
            'grupo' => 'Grupo',
            'mac' => 'Mac Addres',
            'descricao' => 'Descrição',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->mac) {
            $this->mac = Setup::formatterMAC($this->mac);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->mac) {
            $this->mac = Setup::removeFormatterMAC($this->mac);
        }
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = MapaIpV4::find();
        
        if (isset($params['MapaIpV4']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(rede))', strtoupper(Setup::retirarAcento($params['MapaIpV4']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(grupo))', strtoupper(Setup::retirarAcento($params['MapaIpV4']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(ip))', strtoupper(Setup::retirarAcento($params['MapaIpV4']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(mac)', strtoupper($params['MapaIpV4']['pesquisa'])])
                ->orFilterWhere(['like', 'UPPER(mac)', strtoupper(Setup::removeFormatterMAC($params['MapaIpV4']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['MapaIpV4']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        if (Yii::$app->user->identity->rede_temp != NUll) {
            $query->andWhere(['=', 'rede', Yii::$app->user->identity->rede_temp]);
        }
        
        if (Yii::$app->user->identity->grupo_temp != NULL) {
            $query->andWhere(['=', 'grupo', Yii::$app->user->identity->grupo_temp]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'unidade_fk' => SORT_ASC,
                    'rede' => SORT_ASC,
                    'id' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
