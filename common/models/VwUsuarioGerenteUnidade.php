<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_usuario_gerente_unidade".
 *
 * @property int $id
 * @property int $usuario_fk
 * @property int $usuario_unidade_fk
 * @property string $nome
 * @property int $unidade_fk
 * @property string $unidade
 * @property int $gerencia
 *
 */
class VwUsuarioGerenteUnidade extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_usuario_gerente_unidade';
    }

    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['usuario_fk', 'unidade_fk', 'gerencia'], 'required'],
            [['usuario_fk', 'unidade_fk', 'gerencia'], 'default', 'value' => null],
            [['usuario_fk', 'unidade_fk', 'gerencia', 'usuario_unidade_fk'], 'integer'],
            [['usuario', 'unidade'], 'string', 'max' => 40],
            [['usuario_fk', 'unidade_fk'], 'unique', 'targetAttribute' => ['usuario_fk', 'unidade_fk']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['usuario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_fk' => 'Usuário FK',
            'usuario_unidade_fk' => 'Usuário Unidade FK',
            'nome' => 'Nome',
            'unidade_fk' => 'Unidade FK',
            'unidade' => 'Unidade',
            'gerencia' => 'Gerencia',
        ];
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getUsuarioFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_fk']);
    }
    
    public function search($params) {
        $query = VwUsuarioGerenteUnidade::find();

        if (isset($params['VwUsuarioGerenteUnidade']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['VwUsuarioGerenteUnidade']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade))', strtoupper(Setup::retirarAcento($params['VwUsuarioGerenteUnidade']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['usuario_unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'usuario_unidade_fk', $usuariounidade]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome' => SORT_ASC,
                    'unidade_fk' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
