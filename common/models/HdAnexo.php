<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_helpdesk_anexo".
 *
 * @property int $id
 * @property int $chamado_fk
 * @property int $de_fk
 * @property string|null $data
 * @property string|null $descricao_anexo
 * @property string|null $url
 *
 */
class HdAnexo extends \yii\db\ActiveRecord
{
    public $pesquisa;

    public static function tableName()
    {
        return 'tb_helpdesk_anexo';
    }

    public function rules()
    {
        return [
            [['chamado_fk', 'de_fk'], 'required'],
            [['chamado_fk', 'de_fk'], 'default', 'value' => null],
            [['chamado_fk', 'de_fk'], 'integer'],
            [['data'], 'safe'],
            [['descricao_anexo'], 'string', 'max' => 120],
            [['url'], 'string', 'max' => 255],
            [['chamado_fk'], 'exist', 'skipOnError' => true, 'targetClass' => HdCadastro::className(), 'targetAttribute' => ['chamado_fk' => 'id']],
            [['de_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['de_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chamado_fk' => 'Chamado',
            'de_fk' => 'De',
            'data' => 'Data',
            'descricio_anexo' => 'Descrição Anexo',
            'url' => 'Anexo',
        ];
    }

    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }

    public function getChamadoFk()
    {
        return $this->hasOne(HdCadastro::className(), ['id' => 'chamado_fk']);
    }

    public function getDeFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'de_fk']);
    }

    public function search($params) {
        $query = HdAnexo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['HdAnexo']['chamado_fk'])) {
            $query->andFilterWhere(['=', 'chamado_fk', $params['HdAnexo']['chamado_fk']]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

    public static function renderiza($array) {
        $array_table_anexo = [];
        foreach ($array as $key => $value) {
            $array_table_anexo[] = '<tr>
                                <td>' . $value->deFk->nome . '</td>
                                <td>' . $value->data . '</td>
                                <td>' . $value->descricao_anexo . ' | ' . '<a href="/arquivo/hdanexo/' . $value->url . '" target="_blank">' . 'Anexo' . '</a> </td>
                            </tr>';
        }
        return $array_table_anexo;
    }
}
