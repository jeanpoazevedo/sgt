<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_configuracao".
 *
 * @property int $id
 * @property string $configuracao
 * @property string $variavel1
 * @property string $variavel2
 * @property string $variavel3
 * @property string $variavel4
 * @property string $nome
 * @property int $ordem
 * @property int $ver
 * @property string $titulo_variavel1
 * @property string $titulo_variavel2
 * @property string $titulo_variavel3
 * @property string $titulo_variavel4
 * @property int $ativo_variavel1
 * @property int $ativo_variavel2
 * @property int $ativo_variavel3
 * @property int $ativo_variavel4
 * @property int $tipo_variavel1
 * @property int $tipo_variavel2
 * @property int $tipo_variavel3
 * @property int $tipo_variavel4
 */
class Configuracao extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_configuracao';
    }
    
    public function rules()
    {
        return [
            [['configuracao', 'nome', 'ver'], 'required'],
            [['ordem', 'ver', 'ativo_variavel1', 'ativo_variavel2', 'ativo_variavel3', 'ativo_variavel4', 'tipo_variavel1', 'tipo_variavel2', 'tipo_variavel3', 'tipo_variavel4'], 'default', 'value' => null],
            [['ordem', 'ver', 'ativo_variavel1', 'ativo_variavel2', 'ativo_variavel3', 'ativo_variavel4', 'tipo_variavel1', 'tipo_variavel2', 'tipo_variavel3', 'tipo_variavel4'], 'integer'],
            [['configuracao'], 'string', 'max' => 20],
            [['variavel1', 'variavel2', 'variavel3', 'variavel4'], 'string', 'max' => 100],
            [['nome'], 'string', 'max' => 45],
            [['titulo_variavel1', 'titulo_variavel2', 'titulo_variavel3', 'titulo_variavel4'], 'string', 'max' => 40],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'configuracao' => 'Configuracao',
            'variavel1' => 'Variavel1',
            'variavel2' => 'Variavel2',
            'variavel3' => 'Variavel3',
            'variavel4' => 'Variavel4',
            'nome' => 'Nome',
            'ordem' => 'Ordem',
            'ver' => 'Ver',
            'titulo_variavel1' => 'Titulo Variavel1',
            'titulo_variavel2' => 'Titulo Variavel2',
            'titulo_variavel3' => 'Titulo Variavel3',
            'titulo_variavel4' => 'Titulo Variavel4',
            'ativo_variavel1' => 'Ativo Variavel1',
            'ativo_variavel2' => 'Ativo Variavel2',
            'ativo_variavel3' => 'Ativo Variavel3',
            'ativo_variavel4' => 'Ativo Variavel4',
            'tipo_variavel1' => 'Tipo Variavel1',
            'tipo_variavel2' => 'Tipo Variavel2',
            'tipo_variavel3' => 'Tipo Variavel3',
            'tipo_variavel4' => 'Tipo Variavel4',
        ];
    }
    
    public function search($params) {
        $query = Configuracao::find();

        if (isset($params['Configuracao']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Configuracao']['pesquisa']))]);
        }
        $query->orFilterWhere(['=', 'ver', strtoupper(Setup::retirarAcento('1'))]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'ordem' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
