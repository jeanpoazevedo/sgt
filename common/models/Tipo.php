<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_tipo".
 *
 * @property int $id
 * @property string $tipo
 * @property int $computador
 * @property int $equipamento
 * @property int $monitor
 *
 */
class Tipo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_tipo';
    }

    public function rules()
    {
        return [
            [['tipo', 'computador', 'monitor', 'equipamento'], 'required'],
            [['computador', 'monitor', 'equipamento'], 'default', 'value' => null],
            [['computador', 'monitor', 'equipamento'], 'integer'],
            [['tipo'], 'string', 'max' => 40],
            [['tipo'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tipo' => Yii::t('app', 'Tipo'),
            'computador' => Yii::t('app', 'Computador'),
            'equipamento' => Yii::t('app', 'Equipamento'),
            'monitor' => Yii::t('app', 'Monitor'),
        ];
    }
    
    public function search($params) {
        $query = Tipo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'tipo' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Tipo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tipo))', strtoupper(Setup::retirarAcento($params['Tipo']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
