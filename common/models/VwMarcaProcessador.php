<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_marca_processador".
 *
 * @property int $id
 * @property string $marca
 * @property int $marca_fk
 * @property string $processador
 * @property string $descricao_processador
 * 
 */

class VwMarcaProcessador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_marca_processador';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'marca_fk'], 'default', 'value' => null],
            [['id', 'marca_fk'], 'integer'],
            [['marca'], 'string', 'max' => 40],
            [['processador'], 'string', 'max' => 200],
            [['descricao_processador'], 'string', 'max' => 255],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca' => 'Marca',
            'marca_fk' => 'Marca Fk',
            'processador' => 'Processador',
            'descricao_processador' => 'Descricao Processador',
        ];
    }

    public function getProcessadorFk()
    {
        return $this->hasOne(Processador::className(), ['id' => 'id']);
    }
    
    public function search($params) {
        $query = VwMarcaProcessador::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'marca' => SORT_ASC,
                    'processador' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwMarcaProcessador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(processador))', strtoupper(Setup::retirarAcento($params['VwMarcaProcessador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_processador))', strtoupper(Setup::retirarAcento($params['VwMarcaProcessador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', strtoupper(Setup::retirarAcento($params['VwMarcaProcessador']['pesquisa']))]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
