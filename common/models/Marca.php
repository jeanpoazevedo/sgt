<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_marca".
 *
 * @property int $id
 * @property string $marca
 * @property int $computador
 * @property int $monitor
 * @property int $equipamento
 * @property int $processador
 * @property int $placa_video
 *
 */
class Marca extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_marca';
    }

    public function rules()
    {
        return [
            [['marca', 'computador', 'monitor', 'equipamento', 'processador', 'placa_video'], 'required'],
            [['computador', 'monitor', 'equipamento', 'processador', 'placa_video'], 'default', 'value' => null],
            [['computador', 'monitor', 'equipamento', 'processador', 'placa_video'], 'integer'],
            [['marca'], 'string', 'max' => 40],
            [['marca'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'marca' => Yii::t('app', 'Marca'),
            'computador' => Yii::t('app', 'Computadores'),
            'monitor' => Yii::t('app', 'Monitores'),
            'equipamento' => Yii::t('app', 'Equipamentos'),
            'processador' => Yii::t('app', 'Processadores'),
            'placa_video' => Yii::t('app', 'Placa de Vídeo'),
        ];
    }
    
    public function search($params) {
        $query = Marca::find();

        if (isset($params['Marca']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', strtoupper(Setup::retirarAcento($params['Marca']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'marca' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
