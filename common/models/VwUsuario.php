<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "vw_usuario".
 *
 * @property int|null $id
 * @property string|null $usuario
 * @property string|null $senha
 * @property string|null $nome_usuario
 * @property string|null $email
 * @property int|null $status
 * @property string|null $telefone
 * @property int|null $unidade_fk
 * @property string|null $unidade
 * @property int|null $perfil_fk
 * @property string|null $nome_perfil
 */

class VwUsuario extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_usuario';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'status', 'unidade_fk', 'perfil_fk'], 'default', 'value' => null],
            [['id', 'status', 'unidade_fk', 'perfil_fk'], 'integer'],
            [['usuario', 'unidade'], 'string', 'max' => 40],
            [['senha', 'nome_perfil'], 'string', 'max' => 1024],
            [['nome_usuario'], 'string', 'max' => 80],
            [['email'], 'string', 'max' => 100],
            [['telefone'], 'string', 'max' => 15],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuario',
            'senha' => 'Senha',
            'nome_usuario' => 'Nome',
            'email' => 'Email',
            'status' => 'Status',
            'telefone' => 'Telefone',
            'unidade_fk' => 'Unidade',
            'unidade' => 'Unidade',
            'perfil_fk' => 'Perfil Fk',
            'nome_perfil' => 'Perfil',
        ];
    }
    
    public function getUnidadeFk() {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = VwUsuario::find();
        
        if (isset($params['VwUsuario']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_usuario))', strtoupper(Setup::retirarAcento($params['VwUsuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario))', strtoupper(Setup::retirarAcento($params['VwUsuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade))', strtoupper(Setup::retirarAcento($params['VwUsuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_perfil))', strtoupper(Setup::retirarAcento($params['VwUsuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(email))', strtoupper(Setup::retirarAcento($params['VwUsuario']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_DESC,
                    'unidade' => SORT_ASC,
                    'usuario' => SORT_ASC,
                    'nome_usuario' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
