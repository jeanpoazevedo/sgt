<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $senha
 * @property string $nome
 * @property string $email
 * @property int $unidade_fk
 * @property int $unidade_temp_fk
 * @property int $setor_temp_fk
 * @property int $local_temp_fk
 * @property int $tipo_temp_fk
 * @property int $marca_temp_fk
 * @property int $modelo_temp_fk
 * @property int $produto_temp_fk
 * @property int $patch_panel_temp
 * @property int $switch_temp
 * @property int|null $hd_unidade_temp_fk
 * @property int|null $hd_filtro
 * @property string $rede_temp
 * @property string $grupo_temp
 * @property string $funcao_temp
 * @property int $help_desk_master
 * @property int $status
 * @property string $telefone
 * @property string $senha_hash
 * @property string $senha_reset_token
 * @property string $auth_key
 * @property string $token
 * @property int $trocar_senha
 *
 */
class Usuario extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_usuario';
    }
    
    public function rules()
    {
        return [
            [['usuario', 'nome', 'email', 'unidade_fk', 'unidade_temp_fk', 'status', 'senha', 'senha_hash', 'auth_key', 'token'], 'required'],
            [['unidade_fk', 'unidade_temp_fk', 'setor_temp_fk', 'local_temp_fk', 'tipo_temp_fk', 'marca_temp_fk', 'modelo_temp_fk', 'produto_temp_fk', 'status', 'patch_panel_temp', 'switch_temp', 'hd_unidade_temp_fk', 'hd_filtro', 'grupo_temp', 'funcao_temp'], 'default', 'value' => null],
            [['unidade_fk', 'unidade_temp_fk', 'setor_temp_fk', 'local_temp_fk', 'tipo_temp_fk', 'marca_temp_fk', 'modelo_temp_fk', 'produto_temp_fk', 'help_desk_master', 'status', 'patch_panel_temp', 'switch_temp', 'hd_unidade_temp_fk', 'hd_filtro', 'trocar_senha'], 'integer'],
            [['usuario'], 'string', 'max' => 40],
            [['senha', 'senha_hash', 'senha_reset_token', 'token'], 'string', 'max' => 1024],
            [['nome', 'rede_temp', 'grupo_temp'], 'string', 'max' => 80],
            [['funcao_temp'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['telefone'], 'string', 'max' => 15],
            [['auth_key'], 'string', 'max' => 32],
            [['usuario'], 'unique'],
            [['produto_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => HdProduto::className(), 'targetAttribute' => ['produto_temp_fk' => 'id']],
            [['local_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['local_temp_fk' => 'id']],
            [['marca_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['marca_temp_fk' => 'id']],
            [['modelo_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['modelo_temp_fk' => 'id']],
            [['setor_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Setor::className(), 'targetAttribute' => ['setor_temp_fk' => 'id']],
            [['setor_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Setor::className(), 'targetAttribute' => ['setor_temp_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['unidade_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_temp_fk' => 'id']],
            [['hd_unidade_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['hd_unidade_temp_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuario',
            'senha' => 'Senha',
            'nome' => 'Nome',
            'email' => 'Email',
            'unidade_fk' => 'Unidade',
            'unidade_temp_fk' => 'Unidade Temporaria',
            'setor_temp_fk' => 'Setor Temporario',
            'local_temp_fk' => 'Local Temporario',
            'tipo_temp_fk' => 'Tipo Temporario',
            'marca_temp_fk' => 'Marca Temporaria',
            'modelo_temp_fk' => 'Modelo Temporario',
            'produto_temp_fk' => 'Produto Temporario',
            'patch_panel_temp' => 'Patch Panel Temp',
            'switch_temp' => 'Switch Temp',
            'hd_unidade_temp_fk' => 'Hd Unidade Temp',
            'hd_filtro' => 'Hd Filtro',
            'rede_temp' => 'Rede Temp',
            'grupo_temp' => 'Grupo Temp',
            'funcao_temp' => 'Função Temp',
            'help_desk_master' => 'Help Desk Master',
            'status' => 'Status',
            'telefone' => 'Telefone',
            'senha_hash' => 'Senha Hash',
            'senha_reset_token' => 'Senha Reset Token',
            'auth_key' => 'Auth Key',
            'token' => 'Token',
            'trocar_senha' => 'Força trocar senha',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->telefone) {
            $this->telefone = Setup::formatterTelefone($this->telefone);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->telefone) {
            $this->telefone = Setup::unformatterString($this->telefone);
        }
    }
    
    public function getLocalTempFk()
    {
        return $this->hasOne(Local::className(), ['id' => 'local_temp_fk']);
    }
    
    public function getMarcaTempFk()
    {
        return $this->hasOne(Marca::className(), ['id' => 'marca_temp_fk']);
    }
    
    public function getModeloTempFk()
    {
        return $this->hasOne(Modelo::className(), ['id' => 'modelo_temp_fk']);
    }
    
    public function getProdutoTempFk()
    {
        return $this->hasOne(HdProduto::className(), ['id' => 'produto_temp_fk']);
    }
    
    public function getSetorTempFk()
    {
        return $this->hasOne(Setor::className(), ['id' => 'setor_temp_fk']);
    }
    
    public function getTipoTempFk()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_temp_fk']);
    }

    public function getUnidTempFk() {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_temp_fk']);
    }

    public function getUnidadeFk() {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getHdUnidadeTempFk()
    {
        return $this->hasOne(TbUnidade::className(), ['id' => 'hd_unidade_temp_fk']);
    }
    
    public function search($params) {
        $query = Usuario::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_DESC,
                    'usuario' => SORT_ASC,
                    'nome' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Usuario']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(email))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }
    
}
