<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_helpdesk".
 *
 * @property int|null $id
 * @property int $de_fk
 * @property string $nome_de
 * @property int $unidade_fk_de
 * @property string $unidade_de
 * @property int $para_fk
 * @property string $nome_para
 * @property int $unidade_fk_para
 * @property string $unidade_para
 * @property string $data_abertura
 * @property string $data_encerrado
 * @property int $produto_fk
 * @property int $produto
 * @property int $prioridade
 * @property string $resumo
 * @property string $descricao
 * @property int $ver
 * @property int $status
 */
class VwHelpdesk extends \yii\db\ActiveRecord
{

    public $pesquisa;
    
    public $array_status = [
        '1' => 'Aguardando',
        '2' => 'Em Andamento',
        '3' => 'Pausado',
        '5' => 'Resolvido',
        '4' => 'Encerrado',
    ];
    
    public $array_prioridade = [
        '1' => 'Baixa',
        '2' => 'Normal',
        '3' => 'Alta',
        '4' => 'Crítica',
    ];
    
    public $array_filtro = [
        '1' => 'Todos os chamados',
        '2' => 'Meus chamados',
    ];

    public static function tableName()
    {
        return 'vw_helpdesk';
    }

    public static function primaryKey() {
        return ['id'];
    }

    public function rules()
    {
        return [
            [['id', 'de_fk', 'para_fk', 'unidade_fk_de', 'unidade_fk_para', 'produto_fk', 'prioridade', 'ver', 'resumo', 'status'], 'default', 'value' => null],
            [['id', 'de_fk', 'unidade_fk_de', 'para_fk', 'unidade_fk_para', 'produto_fk', 'prioridade', 'ver', 'status'], 'integer'],
            [['data_abertura', 'data_encerrado'], 'safe'],
            [['resumo'], 'string', 'max' => 255],
            [['nome_de', 'nome_para'], 'string', 'max' => 80],
            [['unidade_de', 'unidade_para'], 'string', 'max' => 40],
            [['descricao'], 'string'],
            [['produto_fk'], 'exist', 'skipOnError' => true, 'targetClass' => HdProduto::className(), 'targetAttribute' => ['produto_fk' => 'id']],
            [['de_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['de_fk' => 'id']],
            [['para_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['para_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Chamdo',
            'de_fk' => 'De Usuario',
            'nome_de' => 'Solicitante',
            'unidade_fk_de' => 'Unidade',
            'unidade_de' => 'De Unidade',
            'para_fk' => 'Para Usuario',
            'nome_para' => 'Atendente',
            'unidade_fk_para' => 'Para Unidade',
            'unidade_para' => 'Para Unidade',
            'data_abertura' => 'Aberto',
            'data_encerrado' => 'Fechado',
            'produto_fk' => 'ID Produto',
            'produto' => 'Produto',
            'resumo' => 'Resumo',
            'descricao' => 'Descricao',
            'ver' => 'Visualização Pública',
            'status' => 'Status',
        ];
    }

    public function afterFind() {
        parent::afterFind();
        if ($this->data_abertura) {
            $this->data_abertura = Setup::convertApresentacao($this->data_abertura, 'datetime');
        }
        if ($this->data_encerrado) {
            $this->data_encerrado = Setup::convertApresentacao($this->data_encerrado, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data_abertura) {
            $this->data_abertura = Setup::convertBD($this->data_abertura, 'datetime');
        }
        if ($this->data_encerrado) {
            $this->data_encerrado = Setup::convertBD($this->data_encerrado, 'datetime');
        }
    }

    public function getDeFk() {
        return $this->hasOne(Usuario::className(), ['id' => 'de_fk']);
    }

    public function getParaFk() {
        return $this->hasOne(Usuario::className(), ['id' => 'para_fk']);
    }

    public function getProdutoFk() {
        return $this->hasOne(HdProduto::className(), ['id' => 'produto_fk']);
    }

    public function getUnidadeFkDe()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk_de']);
    }

    public function getUnidadeFkPara()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk_para']);
    }

    public function search($params) {
        $query = VwHelpdesk::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome_de' => SORT_ASC,
                    'nome_para' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwHelpdesk']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_de))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade_de))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_para))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade_para))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(produto))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(resumo))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))]);
        }
        
        $model_restricao_atendente = VwRestricaoMenu::findAll(['controller' => 'helpdesk', 'restricao' => 'atendente', 'usuario_fk' => Yii::$app->user->getId()]);
        
        $usuariounidade = HdUsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if($model_restricao_atendente){
            if (Yii::$app->user->identity->hd_unidade_temp_fk > '1') {
                if(Yii::$app->user->identity->hd_filtro > '1'){
                    $query->orFilterWhere(['=', 'de_fk', Yii::$app->user->identity->id]);
                    $query->orFilterWhere(['=', 'para_fk', Yii::$app->user->identity->id]);
                    $query->andFilterWhere(['=', 'unidade_fk_de', Yii::$app->user->identity->hd_unidade_temp_fk]);
                } else {
                    $query->orFilterWhere(['=', 'unidade_fk_de', Yii::$app->user->identity->hd_unidade_temp_fk]);
                }
            } else {
                if(Yii::$app->user->identity->hd_filtro > '1'){
                    $query->orFilterWhere(['=', 'de_fk', Yii::$app->user->identity->id]);
                    $query->orFilterWhere(['=', 'para_fk', Yii::$app->user->identity->id]);
                } else {
                    $query->andFilterWhere(['IN', 'unidade_fk_para', $usuariounidade]);
                }
            }
        } else {
            $query->orFilterWhere(['=', 'de_fk', Yii::$app->user->identity->id]);
            $query->orFilterWhere(['=', 'para_fk', Yii::$app->user->identity->id]);
        }
        
        $query->andFilterWhere(['IN', 'status', ['1','2','3','5']]);

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

    public function searchbuscar($params) {
        $query = VwHelpdesk::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        
        if (isset($params['VwHelpdesk']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_de))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade_de))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_para))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade_para))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(produto))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(resumo))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['VwHelpdesk']['pesquisa']))]);
        }

        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $perfil = $usuarioperfil->perfil_fk;

        if($perfil != 1){
            $query->andFilterWhere(['=', 'ver', '1']);
        }

        $query->andFilterWhere(['=', 'status', '4']);
        
        if (Yii::$app->user->identity->produto_temp_fk) {
            $query->andWhere(['=', 'produto_fk', Yii::$app->user->identity->produto_temp_fk]);
        }

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }
}
