<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_helpdesk_usuario_unidade".
 *
 * @property int $id
 * @property int $usuario_fk
 * @property int $unidade_fk
 *
 */
class HdUsuarioUnidade extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_helpdesk_usuario_unidade';
    }
    
    public function rules()
    {
        return [
            [['usuario_fk', 'unidade_fk'], 'required'],
            [['usuario_fk', 'unidade_fk'], 'default', 'value' => null],
            [['usuario_fk', 'unidade_fk'], 'integer'],
            [['usuario_fk', 'unidade_fk'], 'unique', 'targetAttribute' => ['usuario_fk', 'unidade_fk']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['usuario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_fk' => 'Usuario Fk',
            'unidade_fk' => 'Unidade Fk',
        ];
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }

    public function getUsuarioFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_fk']);
    }
    
    public function search($params) {
        $query = HdUsuarioUnidade::find();

        if (isset($params['HdUsuarioUnidade']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario_fk))', strtoupper(Setup::retirarAcento($params['HdUsuarioUnidade']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        $dataProvider->setTotalCount($query->count());

    } 
}