<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_mapa_canal".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $local_fk
 * @property int $switch_fk
 * @property int $switch_porta
 * @property int $patch_panel
 * @property int $patch_panel_porta
 * @property string $patch_cord_switch
 * @property string $link
 * @property string $patch_cord_dispositivo
 * @property string $descricao
 *
 */
class MapaCanal extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public $array_patch_panel = [
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25',
        '26' => '26',
        '27' => '27',
        '28' => '28',
        '29' => '29',
        '30' => '30',
        '31' => '31',
        '32' => '32',
        '33' => '33',
        '34' => '34',
        '35' => '35',
        '36' => '36',
        '37' => '37',
        '38' => '38',
        '39' => '39',
        '40' => '40',
        '41' => '41',
        '42' => '42',
        '43' => '43',
        '44' => '44',
    ];
    
    public $array_patch_panel_porta = [
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25',
        '26' => '26',
        '27' => '27',
        '28' => '28',
        '29' => '29',
        '30' => '30',
        '31' => '31',
        '32' => '32',
        '33' => '33',
        '34' => '34',
        '35' => '35',
        '36' => '36',
        '37' => '37',
        '38' => '38',
        '39' => '39',
        '40' => '40',
        '41' => '41',
        '42' => '42',
        '43' => '43',
        '44' => '44',
        '45' => '45',
        '46' => '46',
        '47' => '47',
        '48' => '48',
    ];
    
    public static function tableName()
    {
        return 'tb_mapa_canal';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'local_fk', 'patch_panel', 'patch_panel_porta', 'descricao'], 'required'],
            [['unidade_fk', 'local_fk', 'switch_fk', 'switch_porta', 'patch_panel', 'patch_panel_porta'], 'default', 'value' => null],
            [['unidade_fk', 'local_fk', 'switch_fk', 'switch_porta', 'patch_panel', 'patch_panel_porta'], 'integer'],
            [['patch_cord_switch', 'link', 'patch_cord_dispositivo'], 'number', 'numberPattern' => '/^-?(?:[0-9]{1,3})(?:.[0-9]{3})*(?:|\,[0-9]+)$/'],
            [['descricao'], 'string', 'max' => 50],
            [['unidade_fk', 'local_fk', 'patch_panel', 'patch_panel_porta'], 'unique', 'targetAttribute' => ['unidade_fk', 'local_fk', 'patch_panel', 'patch_panel_porta']],
            [['local_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['local_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['switch_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Switchdoc::className(), 'targetAttribute' => ['switch_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'local_fk' => 'Local',
            'switch_fk' => 'Switch',
            'switch_porta' => 'Switch Porta',
            'patch_panel' => 'Patch Panel',
            'patch_panel_porta' => 'Patch Panel Porta',
            'patch_cord_switch' => 'Patch Cord Switch',
            'link' => 'Link',
            'patch_cord_dispositivo' => 'Patch Cord Dispositivo',
            'descricao' => 'Descricao',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->patch_cord_switch) {
            $this->patch_cord_switch = FormatterCurrency::formatNumber($this->patch_cord_switch);
        }
        if ($this->link) {
            $this->link = FormatterCurrency::formatNumber($this->link);
        }
        if ($this->patch_cord_dispositivo) {
            $this->patch_cord_dispositivo = FormatterCurrency::formatNumber($this->patch_cord_dispositivo);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->patch_cord_switch) {
            $this->patch_cord_switch = FormatterCurrency::unformatNumber($this->patch_cord_switch);
        }
        if ($this->link) {
            $this->link = FormatterCurrency::unformatNumber($this->link);
        }
        if ($this->patch_cord_dispositivo) {
            $this->patch_cord_dispositivo = FormatterCurrency::unformatNumber($this->patch_cord_dispositivo);
        }
    }

    public function getLocalFk()
    {
        return $this->hasOne(Local::className(), ['id' => 'local_fk']);
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getSwitchFk()
    {
        return $this->hasOne(Switchdoc::className(), ['id' => 'switch_fk']);
    }
    
    public function search($params) {
        $query = MapaCanal::find();

        if (isset($params['MapaCanal']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['MapaCanal']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        if (Yii::$app->user->identity->local_temp_fk != NUll) {
            $query->andWhere(['=', 'local_fk', Yii::$app->user->identity->local_temp_fk]);
        }
        
        if (Yii::$app->user->identity->switch_temp != NUll) {
            $query->andWhere(['=', 'switch_fk', Yii::$app->user->identity->switch_temp]);
        }
        
        if (Yii::$app->user->identity->patch_panel_temp != NUll) {
            $query->andWhere(['=', 'patch_panel', Yii::$app->user->identity->patch_panel_temp]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 24,
            ],
            'sort' => [
                'defaultOrder' => [
                    'unidade_fk' => SORT_ASC,
                    'local_fk' => SORT_ASC,
                    'patch_panel' => SORT_ASC,
                    'patch_panel_porta' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
