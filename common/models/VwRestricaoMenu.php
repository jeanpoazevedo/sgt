<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vw_restricao_menu".
 *
 * @property int $id
 * @property string $controller
 * @property string $restricao
 * @property int $usuario_fk
 * 
 */
class VwRestricaoMenu extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'vw_restricao_menu';
    }

    public function rules()
    {
        return [
            [['id', 'usuario_fk'], 'default', 'value' => null],
            [['id', 'usuario_fk'], 'integer'],
            [['controller', 'restricao'], 'string', 'max' => 200],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller' => 'Controller',
            'restricao' => 'Restricao',
            'usuario_fk' => 'Usuario',
        ];
    }
}
