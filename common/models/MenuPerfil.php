<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_menu_perfil".
 *
 * @property int $id
 * @property int $menu_fk
 * @property int $perfil_fk
 *
 */
class MenuPerfil extends \yii\db\ActiveRecord
{
    
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_menu_perfil';
    }
    
    public function rules()
    {
        return [
            [['menu_fk', 'perfil_fk'], 'required'],
            [['menu_fk', 'perfil_fk'], 'default', 'value' => null],
            [['menu_fk', 'perfil_fk'], 'integer'],
            [['menu_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_fk' => 'id']],
            [['perfil_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['perfil_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_fk' => 'Menu',
            'perfil_fk' => 'Perfil',
        ];
    }
    
    public function getMenuFk()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_fk']);
    }
    
    public function getPerfilFk()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_fk']);
    }
    
    public function search($params) {
        $query = MenuPerfil::find();

        if (isset($params['MenuPerfil']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(menu))', strtoupper(Setup::retirarAcento($params['MenuPerfil']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }
}
