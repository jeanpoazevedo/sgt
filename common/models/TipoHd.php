<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_tipo_hd".
 *
 * @property int $id
 * @property string $tipo
 *
 */
class Tipohd extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_tipo_hd';
    }

    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 200],
            [['tipo'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tipo' => Yii::t('app', 'Tipo'),
        ];
    }
    
    public function search($params) {
        $query = Tipohd::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'tipo' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Tipohd']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tipo))', strtoupper(Setup::retirarAcento($params['Tipohd']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
