<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_funcionario_equipamento".
 *
 * @property int $id
 * @property int $funcionario_fk
 * @property int $equipamento_fk
 *
 */
class FuncionarioEquipamento extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_funcionario_equipamento';
    }
    
    public function rules()
    {
        return [
            [['funcionario_fk', 'equipamento_fk'], 'required'],
            [['funcionario_fk', 'equipamento_fk'], 'default', 'value' => null],
            [['funcionario_fk', 'equipamento_fk'], 'integer'],
            [['funcionario_fk', 'equipamento_fk'], 'unique', 'targetAttribute' => ['funcionario_fk', 'equipamento_fk']],
            [['funcionario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionario::className(), 'targetAttribute' => ['funcionario_fk' => 'id']],
            [['equipamento_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Equipamento::className(), 'targetAttribute' => ['equipamento_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'funcionario_fk' => 'Funcionário',
            'equipamento_fk' => 'Equipamento',
        ];
    }
    
    public function getUsuariodeFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }
    
    public function getUsuarioparaFk()
    {
        return $this->hasOne(Equipamento::className(), ['id' => 'equipamento_fk']);
    }
    
    public function search($params) {
        $query = FuncionarioEquipamento::find();

        if (isset($params['FuncionarioEquipamento']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(id))', strtoupper(Setup::retirarAcento($params['FuncionarioEquipamento']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['funcionario_unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['funcionario_unidade_fk' => $usuariounidade]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'funcionario_fk' => SORT_ASC,
                    'equipamento_fk' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
