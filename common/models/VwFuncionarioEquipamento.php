<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_funcionario_equipamento".
 *
 * @property int $id
 * @property int $funcionario_fk
 * @property int $funcionario_unidade_fk
 * @property string $nome
 * @property int $equipamento_fk
 * @property int $equipamento_unidade_fk
 * @property string $codigo_equipamento
 * @property string $numero_serie
 * @property string $netbios
 * @property string $mac
 * 
 */

class VwFuncionarioEquipamento extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_funcionario_equipamento';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'funcionario_fk', 'equipamento_fk', 'funcionario_unidade_fk', 'equipamento_unidade_fk'], 'integer'],
            [['nome'], 'string', 'max' => 60],
            [['codigo_equipamento'], 'string', 'max' => 7],
            [['numero_serie'], 'string', 'max' => 80],
            [['netbios'], 'string', 'max' => 30],
            [['mac'], 'string', 'max' => 12],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'funcionario_fk' => 'Funcionario FK',
            'funcionario_unidade_fk' => 'Funcionário Unidade',
            'nome' => 'Funcionário',
            'equipamento_fk' => 'Equipamento FK',
            'equipamento_unidade_fk' => 'Equipamento Unidade FK',
            'codigo_equipamento' => 'Código Equipamento',
            'numero_serie' => 'Número de Série',
            'netbios' => 'Netbios',
            'mac' => 'MAC',
        ];
    }

    public function getFuncionarioFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }

    public function getFuncionariounidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'funcionario_unidade_fk']);
    }

    public function getEquipamentounidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'equipamento_unidade_fk']);
    }
    
    public function search($params) {
        $query = VwFuncionarioEquipamento::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome' => SORT_ASC,
                    'codigo_equipamento' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwFuncionarioEquipamento']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['VwFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_equipamento))', strtoupper(Setup::retirarAcento($params['VwFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', strtoupper(Setup::retirarAcento($params['VwFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(netbios))', strtoupper(Setup::retirarAcento($params['VwFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac))', strtoupper(Setup::retirarAcento($params['VwFuncionarioEquipamento']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['funcionario_unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['funcionario_unidade_fk' => $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

    public function searchfuncionarioequipamento($params) {
        $query = VwFuncionarioEquipamento::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwFuncionarioEquipamento']['equipamento_fk'])) {
            $query->andFilterWhere(['=', 'equipamento_fk', $params['VwFuncionarioEquipamento']['equipamento_fk']]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
