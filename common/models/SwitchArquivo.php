<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_switch_arquivo".
 *
 * @property int $id
 * @property int $switch_fk
 * @property string|null $data
 * @property string|null $descricao_arquivo
 * @property string|null $url
 *
 */
class SwitchArquivo extends \yii\db\ActiveRecord
{
    public $pesquisa;

    public static function tableName()
    {
        return 'tb_switch_arquivo';
    }

    public function rules()
    {
        return [
            [['switch_fk'], 'required'],
            [['switch_fk'], 'default', 'value' => null],
            [['switch_fk'], 'integer'],
            [['data'], 'safe'],
            [['descricao_arquivo'], 'string', 'max' => 120],
            [['url'], 'string', 'max' => 255],
            [['switch_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Switchdoc::className(), 'targetAttribute' => ['switch_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'switch_fk' => 'Switch',
            'data' => 'Data',
            'descricao_arquivo' => 'Descrição Arquivo',
            'url' => 'Anexo',
        ];
    }

    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }

    public function getChamadoFk()
    {
        return $this->hasOne(Switchdoc::className(), ['id' => 'switch_fk']);
    }

    public function search($params) {
        $query = SwitchArquivo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'descricao_arquivo' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['SwitchArquivo']['switch_fk'])) {
            $query->andFilterWhere(['=', 'switch_fk', $params['SwitchArquivo']['switch_fk']]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

    public static function renderiza($array) {
        $array_table_arquivo = [];
        foreach ($array as $key => $value) {
            $array_table_arquivo[] = '<tr>
                                <td>' . $value->data . '</td>
                                <td>' . $value->descricao_arquivo . ' | ' . '<a href="/arquivo/switcharquivo/' . $value->url . '" target="_blank">' . 'Arquivo' . '</a> </td>
                            </tr>';
        }
        return $array_table_arquivo;
    }
}
