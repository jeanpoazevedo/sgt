<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_voip_grupo_captura".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $grupo
 * @property string $nome
 * 
 */
class Voipcaptura extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_voip_grupo_captura';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'grupo', 'nome'], 'required'],
            [['unidade_fk', 'grupo'], 'default', 'value' => null],
            [['unidade_fk', 'grupo'], 'integer'],
            [['nome'], 'string', 'max' => 50],
            [['unidade_fk', 'grupo'], 'unique', 'targetAttribute' => ['unidade_fk', 'grupo']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unidade_fk' => Yii::t('app', 'Unidade'),
            'grupo' => Yii::t('app', 'Grupo'),
            'nome' => Yii::t('app', 'Nome'),
        ];
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Voipcaptura::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Voipcaptura']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(grupo))', strtoupper(Setup::retirarAcento($params['Voipcaptura']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Voipcaptura']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }
}
