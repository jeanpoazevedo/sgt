<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_ocorencia_computador".
 *
 * @property int $id
 * @property string $data
 * @property string $usuario_nome
 * @property int $codigo_computador_fk
 * @property int $tipo_fk
 * @property string $descricao
 * @property string $url
 *
 */
class OcorenciaComputador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_ocorencia_computador';
    }

    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['usuario_nome', 'codigo_computador_fk', 'tipo_fk'], 'required'],
            [['codigo_computador_fk', 'tipo_fk'], 'default', 'value' => null],
            [['codigo_computador_fk', 'tipo_fk'], 'integer'],
            [['descricao'], 'string'],
            [['url'], 'string', 'max' => 255],
            [['usuario_nome'], 'string', 'max' => 80],
            [['codigo_computador_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Computador::className(), 'targetAttribute' => ['codigo_computador_fk' => 'id']],
            [['tipo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => OcorenciaTipo::className(), 'targetAttribute' => ['tipo_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data / Hora'),
            'usuario_nome' => Yii::t('app', 'Nome do Usuário'),
            'codigo_computador_fk' => Yii::t('app', 'Codigo Computador'),
            'tipo_fk' => Yii::t('app', 'Tipo'),
            'descricao' => Yii::t('app', 'Descrição'),
            'url' => 'Anexo',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }

    public function getCodigocomputadorFk()
    {
        return $this->hasOne(Computador::className(), ['id' => 'codigo_computador_fk']);
    }
    
    public function getTipoFk()
    {
        return $this->hasOne(OcorenciaTipo::className(), ['id' => 'tipo_fk']);
    }
    
    public function search($params) {
        $query = OcorenciaComputador::find();

        if (isset($params['OcorenciaComputador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario_nome))', strtoupper(Setup::retirarAcento($params['OcorenciaComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['OcorenciaComputador']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $codigos = Computador::find()->select('id')->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);            
            $query->andFilterWhere(['codigo_computador_fk' => $codigos]);
        } else {
            $codigos = Computador::find()->select('id')->where(['unidade_fk' => $usuariounidade]);
            $query->andFilterWhere(['codigo_computador_fk' => $codigos]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
    
    public function searchocorencia($params) {
        $query = OcorenciaComputador::find();

        if (isset($params['OcorenciaComputador']['codigo_computador_fk'])) {
            $query->andFilterWhere(['=', 'codigo_computador_fk', $params['OcorenciaComputador']['codigo_computador_fk']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
