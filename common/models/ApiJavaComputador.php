<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_api_java_computador".
 *
 * @property int $id
 * @property string $data
 * @property int $codigo_computador_fk
 * @property int $unidade_fk
 * @property string $versao_so
 * @property string $nome_so
 * @property string $arquitetura_so
 * @property string $memoria_ram
 * @property string $memoria_live
 * @property string $processador
 * @property string $macaddres_1
 * @property string $macaddres_2
 * @property string $macaddres_3
 *
 */
class ApiJavaComputador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_api_java_computador';
    }

    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['codigo_computador_fk', 'unidade_fk'], 'required'],
            [['codigo_computador_fk', 'unidade_fk'], 'default', 'value' => null],
            [['codigo_computador_fk', 'unidade_fk'], 'integer'],
            [['versao_so', 'nome_so', 'arquitetura_so', 'memoria_ram', 'memoria_live', 'processador', 'macaddres_1', 'macaddres_2', 'macaddres_3'], 'string', 'max' => 255],
            [['codigo_computador_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Computador::className(), 'targetAttribute' => ['codigo_computador_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data / Hora'),
            'codigo_computador_fk' => Yii::t('app', 'Codigo Computador'),
            'unidade_fk' => Yii::t('app', 'Unidade'),
            'versao_so' => Yii::t('app', 'Verssão Sistema Operacional'),
            'nome_so' => Yii::t('app', 'Nome Sistema Operacional'),
            'arquitetura_so' => Yii::t('app', 'Arquitetura'),
            'memoria_ram' => Yii::t('app', 'Total de Memória RAM'),
            'memoria_live' => Yii::t('app', 'Memória Livre'),
            'processador' => Yii::t('app', 'Processador'),
            'macaddres_1' => Yii::t('app', 'Mac Addres - 1'),
            'macaddres_2' => Yii::t('app', 'Mac Addres - 2'),
            'macaddres_3' => Yii::t('app', 'Mac Addres - 3'),
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }

    public function getCodigocomputadorFk()
    {
        return $this->hasOne(Computador::className(), ['id' => 'codigo_computador_fk']);
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = ApiJavaComputador::find();

        if (isset($params['ApiJavaComputador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_computador_fk))', strtoupper(Setup::retirarAcento($params['ApiJavaComputador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome_so))', strtoupper(Setup::retirarAcento($params['ApiJavaComputador']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $codigos = Computador::find()->select('id')->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);            
            $query->andFilterWhere(['codigo_computador_fk' => $codigos]);
        } else {
            $codigos = Computador::find()->select('id')->where(['unidade_fk' => $usuariounidade]);
            $query->andFilterWhere(['codigo_computador_fk' => $codigos]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $dataProvider;
    }
    
    public function searchocorencia($params) {
        $query = ApiJavaComputador::find();

        if (isset($params['ApiJavaComputador']['codigo_computador_fk'])) {
            $query->andFilterWhere(['=', 'codigo_computador_fk', $params['ApiJavaComputador']['codigo_computador_fk']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
