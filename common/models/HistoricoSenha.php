<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_historico_senha".
 *
 * @property int $id
 * @property string $data
 * @property string $usuario_nome
 * @property int $id_senha_fk
 * @property int $tipo
 * @property string $descricao
 * @property string senha_antiga
 *
 */
class HistoricoSenha extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_historico_senha';
    }

    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['usuario_nome', 'id_senha_fk', 'tipo_fk','descricao'], 'required'],
            [['id_senha_fk', 'tipo_fk'], 'default', 'value' => null],
            [['id_senha_fk', 'tipo_fk'], 'integer'],
            [['senha_antiga','descricao'], 'string',  'max' => 255],
            [['usuario_nome'], 'string', 'max' => 80],
            [['id_senha_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Senha::className(), 'targetAttribute' => ['id_senha_fk' => 'id']],
            [['tipo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => OcorenciaTipo::className(), 'targetAttribute' => ['tipo_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data / Hora'),
            'usuario_nome' => Yii::t('app', 'Nome do Usuário'),
            'id_senha_fk' => Yii::t('app', 'ID Senha'),
            'tipo_fk' => Yii::t('app', 'Tipo'),
            'descricao' => Yii::t('app', 'Descrição'),
            'senha_antiga' => Yii::t('app', 'Senha Antiga'),
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }
    
    public function getIdsenhaFk()
    {
        return $this->hasOne(Senha::className(), ['id' => 'id_senha_fk']);
    }
    
    public function getTipoFk()
    {
        return $this->hasOne(OcorenciaTipo::className(), ['id' => 'tipo_fk']);
    }
    
    public function search($params) {
        $query = HistoricoSenha::find();

        if (isset($params['HistoricoSenha']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario_nome))', strtoupper(Setup::retirarAcento($params['HistoricoSenha']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['HistoricoSenha']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $codigos = Senha::find()->select('id')->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);            
            $query->andFilterWhere(['codigoid_senha_fk_monitor_fk' => $codigos]);
        } else {
            $codigos = Senha::find()->select('id')->where(['unidade_fk' => $usuariounidade]);
            $query->andFilterWhere(['id_senha_fk' => $codigos]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $dataProvider;
    }
    
    public function searchhistorico($params) {
        $query = HistoricoSenha::find();

        if (isset($params['HistoricoSenha']['id_senha_fk'])) {
            $query->andFilterWhere(['=', 'id_senha_fk', $params['HistoricoSenha']['id_senha_fk']]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
