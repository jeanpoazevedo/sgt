<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "vw_monitor".
 *
 * @property int $id
 * @property string $codigo_monitor
 * @property string $unidade
 * @property int $unidade_fk
 * @property string $setor
 * @property int $setor_fk
 * @property string $tipo
 * @property int $tipo_fk
 * @property string $marca
 * @property int $marca_fk
 * @property string $modelo
 * @property int $modelo_fk
 * @property string $numero_serie
 * @property string $loja
 * @property string $numero_patrimonio
 * @property string $descricao_monitor
 * @property string $netbios
 * @property string $mac
 * @property int $status
 * @property string $url
 * 
 */

class VwMonitor extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_monitor';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'unidade_fk', 'setor_fk', 'tipo_fk', 'marca_fk', 'modelo_fk', 'status'], 'default', 'value' => null],
            [['id', 'unidade_fk', 'setor_fk', 'tipo_fk', 'marca_fk', 'modelo_fk', 'status'], 'integer'],
            [['descricao_monitor'], 'string'],
            [['codigo_monitor'], 'string', 'max' => 7],
            [['unidade', 'tipo', 'marca', 'numero_patrimonio'], 'string', 'max' => 40],
            [['setor'], 'string', 'max' => 50],
            [['modelo', 'numero_serie', 'loja'], 'string', 'max' => 80],
            [['netbios'], 'string', 'max' => 30],
            [['mac'], 'string', 'max' => 12],
            [['url'], 'string', 'max' => 255],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_monitor' => 'Codigo Monitor',
            'unidade' => 'Unidade',
            'unidade_fk' => 'Unidade Fk',
            'setor' => 'Setor',
            'setor_fk' => 'Setor Fk',
            'tipo' => 'Tipo',
            'tipo_fk' => 'Tipo Fk',
            'marca' => 'Marca',
            'marca_fk' => 'Marca Fk',
            'modelo' => 'Modelo',
            'modelo_fk' => 'Modelo Fk',
            'numero_serie' => 'Numero Serie',
            'loja' => 'Loja',
            'numero_patrimonio' => 'Numero Patrimonio',
            'descricao_monitor' => 'Descricao Monitor',
            'netbios' => 'Netbios',
            'mac' => 'Mac',
            'status' => 'Status',
            'url' => 'Url',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->mac) {
            $this->mac = Setup::formatterMAC($this->mac);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->codigo_monitor) {
            $this->codigo_monitor = strtoupper($this->codigo_monitor);
        }
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = VwMonitor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if (isset($params['VwMonitor']['pesquisa'])) {
            $pesquisa = strtoupper($params['VwMonitor']['pesquisa']);
            $pesquisaSemAcento = strtoupper(Setup::retirarAcento($params['VwMonitor']['pesquisa']));
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_monitor))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(setor))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tipo))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(mac)', $pesquisa])
                ->orFilterWhere(['like', 'UPPER(mac)', strtoupper(Setup::removeFormatterMAC($pesquisa))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_patrimonio))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(loja))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(netbios))', $pesquisaSemAcento])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_monitor))', $pesquisaSemAcento]);
            if (ctype_digit($pesquisa)) {
                $query->orFilterWhere(['=', 'numero_nota', $pesquisa])
                      ->orFilterWhere(['=', 'valor', $pesquisa]);
            }
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }
        
        if (Yii::$app->user->identity->tipo_temp_fk) {
            $query->andFilterWhere(['=', 'tipo_fk', Yii::$app->user->identity->tipo_temp_fk]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andFilterWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }
        
        if (Yii::$app->user->identity->modelo_temp_fk) {
            $query->andFilterWhere(['=', 'modelo_fk', Yii::$app->user->identity->modelo_temp_fk]);
        }
        
        $model_restricao = VwRestricaoMenu::findAll(['controller' => 'monitor', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);
        
        $model_restricao ?  '' : $query->andFilterWhere(['=', 'status', 1]);

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
