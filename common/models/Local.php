<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_local".
 *
 * @property int $id
 * @property string $local
 * @property string $descricao
 *
 */
class Local extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_local';
    }

    public function rules()
    {
        return [
            [['local', 'descricao'], 'required'],
            [['local'], 'string', 'max' => 80],
            [['descricao'], 'string', 'max' => 120],
            [['local'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'local' => Yii::t('app', 'Local'),
            'descricao' => Yii::t('app', 'Descricao'),
        ];
    }
    
    public function search($params) {
        $query = Local::find();

        if (isset($params['Local']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(local))', strtoupper(Setup::retirarAcento($params['Local']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'local' => SORT_ASC,
                ]
            ],
        ]);
        
        return $dataProvider;
    }

}
