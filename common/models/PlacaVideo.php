<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_placa_video".
 *
 * @property int $id
 * @property int $marca_fk
 * @property string $modelo
 * @property string $descricao_placa_video
 *
 */
class PlacaVideo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_placa_video';
    }

    public function rules()
    {
        return [
            [['marca_fk', 'modelo', 'descricao_placa_video'], 'required'],
            [['marca_fk'], 'default', 'value' => null],
            [['marca_fk'], 'integer'],
            [['modelo'], 'string', 'max' => 200],
            [['descricao_placa_video'], 'string', 'max' => 255],
            [['marca_fk', 'modelo'], 'unique', 'targetAttribute' => ['marca_fk', 'modelo']],
            [['marca_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['marca_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'marca_fk' => Yii::t('app', 'Marca Fk'),
            'modelo' => Yii::t('app', 'Modelo'),
            'descricao_placa_video' => Yii::t('app', 'Descricao Placa de Video'),
        ];
    }

    public function getMarcaFk()
    {
        return $this->hasOne(Marca::className(), ['id' => 'marca_fk']);
    }
    
    public function search($params) {
        $query = PlacaVideo::find();
        
        $query->alias('video');
        $query->leftJoin('tb_marca', 'tb_marca.id = video.marca_fk');
        if (isset($params['PlacaVideo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(video.modelo))', strtoupper(Setup::retirarAcento($params['PlacaVideo']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(video.descricao_placa_video))', strtoupper(Setup::retirarAcento($params['PlacaVideo']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_marca.marca))', strtoupper(Setup::retirarAcento($params['PlacaVideo']['pesquisa']))]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andWhere(['=', 'video.marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
