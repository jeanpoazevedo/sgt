<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_helpdesk_produto".
 *
 * @property int $id
 * @property string $produto
 *
 */
class HdProduto extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_helpdesk_produto';
    }

    public function rules()
    {
        return [
            [['produto'], 'required'],
            [['produto'], 'string', 'max' => 90],
            [['produto'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'produto' => Yii::t('app', 'Produto'),
        ];
    }
    
    public function search($params) {
        $query = Hdproduto::find();

        if (isset($params['HdProduto']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(produto))', strtoupper(Setup::retirarAcento($params['HdProduto']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
