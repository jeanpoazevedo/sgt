<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_codigo".
 *
 * @property int $id
 * @property string $ano
 * @property string $mes
 * @property string $dispositivo
 * @property int $codigo_fk
 * @property int $sequencia
 * @property string $codigo
 * 
 */

class VwCodigo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public $array_tipo = [
        'C' => 'Computador',
        'M' => 'Monitor',
        'E' => 'Equpamento'
    ];
    
    public static function tableName()
    {
        return 'vw_codigo';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'ano', 'mes', 'dispositivo', 'codigo_fk', 'sequencia', 'codigo'], 'default', 'value' => null],
            [['id', 'codigo_fk', 'sequencia'], 'integer'],
            [['ano', 'mes', 'codigo'], 'string', 'max' => 2],
            [['mes'], 'string', 'max' => 2],
            [['dispositivo'], 'string', 'max' => 1],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ano' => 'Ano',
            'mes' => 'Mês',
            'dispositivo' => 'Dispositivo',
            'codigo_fk' => 'Código FK',
            'sequencia' => 'Sequência',
            'codigo' => 'Código',
        ];
    }
    
    public function search($params) {
        $query = VwCodigo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'ano' => SORT_ASC,
                    'mes' => SORT_ASC,
                    'sequencia' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Vw_Codigo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(ano))', strtoupper(Setup::retirarAcento($params['VwCodigo']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mes))', strtoupper(Setup::retirarAcento($params['VwCodigo']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(dispositivo))', strtoupper(Setup::retirarAcento($params['VwCodigo']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_fk))', strtoupper(Setup::retirarAcento($params['VwCodigo']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(sequencia))', strtoupper(Setup::retirarAcento($params['VwCodigo']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo))', strtoupper(Setup::retirarAcento($params['VwCodigo']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
