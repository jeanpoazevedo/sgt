<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "contrato".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $perfil_fk
 * @property string|null $data
 * @property string|null $data_alteracao
 * @property string|null $data_fim
 * @property string|null $descricao
 * @property string|null $url
 *
 */
class Contrato extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_contrato';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'perfil_fk', 'descricao'], 'required'],
            [['unidade_fk', 'perfil_fk'], 'default', 'value' => null],
            [['unidade_fk', 'perfil_fk'], 'integer'],
            [['descricao'], 'string', 'max' => 120],
            [['url'], 'string', 'max' => 255],
            [['data', 'data_alteracao', 'data_fim'], 'safe'],
            [['unidade_fk', 'descricao'], 'unique', 'targetAttribute' => ['unidade_fk', 'descricao']],
            [['perfil_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['perfil_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unidade_fk' => Yii::t('app', 'Unidade'),
            'perfil_fk' => Yii::t('app', 'Perfil'),
            'data' => Yii::t('app', 'Data Criação'),
            'data_alteracao' => Yii::t('app', 'Data Alteração'),
            'data_fim' => Yii::t('app', 'Data Fim do Contrato'),
            'descricao' => Yii::t('app', 'Descrição'),
            'url' => Yii::t('app', 'Arquivo'),
        ];
    }

    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
        if ($this->data_alteracao) {
            $this->data_alteracao = Setup::convertApresentacao($this->data_alteracao, 'datetime');
        }
        if ($this->data_fim) {
            $this->data_fim = Setup::convertApresentacao($this->data_fim, 'date');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
        if ($this->data_alteracao) {
            $this->data_alteracao = Setup::convertBD($this->data_alteracao, 'datetime');
        }
        if ($this->data_fim) {
            $this->data_fim = Setup::convertBD($this->data_fim, 'date');
        }
    }

    public function getPerfilFk()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_fk']);
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Contrato::find();

        if (isset($params['Contrato']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['Contrato']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $perfil = $usuarioperfil->perfil_fk;
        
        if($perfil > 5){
            $query->andFilterWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);
            $query->andWhere(['=', 'perfil_fk', $usuarioperfil->perfil_fk]);
        } else {
            if (Yii::$app->user->identity->unidade_temp_fk != '1') {
                $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
            } else {
                $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
            }
            if($perfil != 1){
                if($perfil < 5){
                    if($perfil != 2){
                        $query->andWhere(['>=', 'perfil_fk', $usuarioperfil->perfil_fk])->andWhere(['<', 'perfil_fk', 6]);
                    } else {
                        $query->andWhere(['>=', 'perfil_fk', $usuarioperfil->perfil_fk]);
                    }
                } else {
                    $query->andWhere(['=', 'perfil_fk', $usuarioperfil->perfil_fk]);
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'unidade_fk' => SORT_ASC,
                    'descricao' => SORT_ASC
                ]
            ],
        ]);
        
        return $dataProvider;
    }

}
