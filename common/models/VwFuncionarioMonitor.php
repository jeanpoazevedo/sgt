<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_funcionario_monitor".
 *
 * @property int $id
 * @property int $funcionario_fk
 * @property int $funcionario_unidade_fk
 * @property string $nome
 * @property int $monitor_fk
 * @property int $monitor_unidade_fk
 * @property string $codigo_monitor
 * @property string $numero_serie
 * @property string $netbios
 * @property string $mac
 * 
 */

class VwFuncionarioMonitor extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_funcionario_monitor';
    }
    
    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'funcionario_fk', 'monitor_fk', 'funcionario_unidade_fk', 'monitor_unidade_fk'], 'integer'],
            [['nome'], 'string', 'max' => 60],
            [['codigo_monitor'], 'string', 'max' => 7],
            [['numero_serie'], 'string', 'max' => 80],
            [['netbios'], 'string', 'max' => 30],
            [['mac'], 'string', 'max' => 12],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'funcionario_fk' => 'Funcionario FK',
            'funcionario_unidade_fk' => 'Funcionário Unidade',
            'nome' => 'Funcionário',
            'monitor_fk' => 'Monitor FK',
            'monitor_unidade_fk' => 'Monitor Unidade FK',
            'codigo_monitor' => 'Código Monitor',
            'numero_serie' => 'Número de Série',
            'netbios' => 'Netbios',
            'mac' => 'MAC',
        ];
    }

    public function getFuncionarioFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }

    public function getFuncionariounidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'funcionario_unidade_fk']);
    }

    public function getMonitorunidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'monitor_unidade_fk']);
    }
    
    public function search($params) {
        $query = VwFuncionarioMonitor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome' => SORT_ASC,
                    'codigo_monitor' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwFuncionarioMonitor']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['VwFuncionarioMonitor']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_monitor))', strtoupper(Setup::retirarAcento($params['VwFuncionarioMonitor']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', strtoupper(Setup::retirarAcento($params['VwFuncionarioMonitor']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(netbios))', strtoupper(Setup::retirarAcento($params['VwFuncionarioMonitor']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac))', strtoupper(Setup::retirarAcento($params['VwFuncionarioMonitor']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['funcionario_unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['funcionario_unidade_fk' => $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

    public function searchfuncionariomonitor($params) {
        $query = VwFuncionarioMonitor::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwFuncionarioMonitor']['monitor_fk'])) {
            $query->andFilterWhere(['=', 'monitor_fk', $params['VwFuncionarioMonitor']['monitor_fk']]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
