<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_processador".
 *
 * @property int $id
 * @property int $marca_fk
 * @property int $tipo_processador_fk
 * @property string $processador
 * @property int $total_core
 * @property string $frequencia
 * @property int $cache
 * @property string $banda_memoria
 * @property string $descricao_processador
 *
 */
class Processador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_processador';
    }
    
    public function rules()
    {
        return [
            [['marca_fk', 'processador', 'descricao_processador','tipo_processador_fk', 'total_core', 'cache', 'frequencia', 'banda_memoria'], 'required'],
            [['tipo_processador_fk', 'total_core', 'cache'], 'integer'],
            [['frequencia', 'banda_memoria'], 'number', 'numberPattern' => '/^-?(?:[0-9]{1,3})(?:.[0-9]{3})*(?:|\,[0-9]+)$/'],
            [['marca_fk'], 'default', 'value' => null],
            [['marca_fk'], 'integer'],
            [['processador'], 'string', 'max' => 200],
            [['descricao_processador'], 'string', 'max' => 255],
            [['marca_fk', 'processador'], 'unique', 'targetAttribute' => ['marca_fk', 'processador']],
            [['marca_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Marca::className(), 'targetAttribute' => ['marca_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'marca_fk' => Yii::t('app', 'Marca'),
            'tipo_processador_fk' => Yii::t('app', 'Tipo de Processador'),
            'processador' => Yii::t('app', 'Processador'),
            'total_core' => Yii::t('app', 'Total de Core'),
            'frequencia' => Yii::t('app', 'Frequência'),
            'cache' => Yii::t('app', 'Total Cache'),
            'banda_memoria' => Yii::t('app', 'Largura de banda da memória'),
            'descricao_processador' => Yii::t('app', 'Descrição Processador'),
        ];
    }

    public function afterFind() {
        parent::afterFind();
        if ($this->frequencia) {
            $this->frequencia = FormatterCurrency::formatNumber($this->frequencia);
        }
        if ($this->banda_memoria) {
            $this->banda_memoria = FormatterCurrency::formatNumber($this->banda_memoria);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->frequencia) {
            $this->frequencia = FormatterCurrency::unformatNumber($this->frequencia);
        }
        if ($this->banda_memoria) {
            $this->banda_memoria = FormatterCurrency::unformatNumber($this->banda_memoria);
        }
    }
    
    public function getMarcaFk()
    {
        return $this->hasOne(Marca::className(), ['id' => 'marca_fk']);
    }

    public function getTipoProcessadorFk()
    {
        return $this->hasOne(TipoProcessador::className(), ['id' => 'tipo_processador_fk']);
    }
    
    public function search($params) {
        $query = Processador::find();

        $query->alias('processador');
        $query->leftJoin('tb_marca', 'tb_marca.id = processador.marca_fk');
        if (isset($params['Processador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(processador.processador))', strtoupper(Setup::retirarAcento($params['Processador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(processador.descricao_processador))', strtoupper(Setup::retirarAcento($params['Processador']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_marca.marca))', strtoupper(Setup::retirarAcento($params['Processador']['pesquisa']))]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andWhere(['=', 'processador.marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
