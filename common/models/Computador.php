<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_computador".
 *
 * @property int $id
 * @property string $codigo_computador
 * @property int $unidade_fk
 * @property int $setor_fk
 * @property int $tipo_fk
 * @property int $modelo_fk
 * @property string $numero_serie
 * @property string $data_compra
 * @property string $garantia
 * @property string $placa_mae
 * @property int $processador_fk
 * @property int $numero_processador
 * @property int $ram
 * @property int $ram_medida_fk
 * @property int $ram_clok_fk
 * @property int $hd
 * @property int $hd_medida_fk
 * @property int $tipo_hd_fk
 * @property int $leitura_hd
 * @property int $escrita_hd
 * @property string $descricao_hd
 * @property string $loja
 * @property int $numero_nota
 * @property string $valor
 * @property string $numero_patrimonio
 * @property int $numero_lacre
 * @property string $netbios
 * @property string $mac_c
 * @property string $mac_w
 * @property int $sistema_operacional_fk
 * @property int $office_fk
 * @property int $placa_video_fk
 * @property int $numero_palca_video
 * @property int $codigo_monitor1_fk
 * @property int $codigo_monitor2_fk
 * @property string $descricao_computador
 * @property int $status
 * @property string $url
 *
 */
class Computador extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_computador';
    }
    
    public function rules()
    {
        return [
            [['codigo_computador', 'unidade_fk', 'setor_fk', 'tipo_fk', 'modelo_fk', 'numero_serie', 'data_compra', 'garantia', 'placa_mae', 'numero_processador', 'ram', 'ram_medida_fk', 'ram_clok_fk', 'hd', 'hd_medida_fk', 'tipo_hd_fk', 'status'], 'required'],
            [['unidade_fk', 'setor_fk', 'tipo_fk', 'modelo_fk', 'processador_fk', 'numero_processador', 'ram', 'ram_medida_fk', 'ram_clok_fk', 'hd', 'hd_medida_fk', 'tipo_hd_fk', 'numero_nota', 'sistema_operacional_fk', 'office_fk', 'placa_video_fk', 'numero_palca_video', 'codigo_monitor1_fk', 'codigo_monitor2_fk', 'status'], 'default', 'value' => null],
            [['unidade_fk', 'setor_fk', 'tipo_fk', 'modelo_fk', 'processador_fk', 'numero_processador', 'ram', 'ram_medida_fk', 'ram_clok_fk', 'hd', 'hd_medida_fk', 'tipo_hd_fk', 'leitura_hd', 'escrita_hd', 'numero_nota', 'sistema_operacional_fk', 'office_fk', 'placa_video_fk', 'numero_palca_video', 'codigo_monitor1_fk', 'codigo_monitor2_fk', 'status'], 'integer'],
            [['data_compra', 'garantia'], 'safe'],
            [['valor'], 'number', 'numberPattern' => '/^-?(?:[0-9]{1,3})(?:.[0-9]{3})*(?:|\,[0-9]+)$/'],
            [['descricao_computador'], 'string'],
            [['codigo_computador'], 'string', 'max' => 7],
            [['numero_serie', 'placa_mae', 'loja'], 'string', 'max' => 80],
            [['descricao_hd'], 'string', 'max' => 120],
            [['numero_patrimonio'], 'string', 'max' => 40],
            [['numero_lacre'], 'string', 'max' => 20],
            [['netbios'], 'string', 'max' => 30],
            [['mac_c', 'mac_w'], 'string', 'max' => 17],
            [['url'], 'string', 'max' => 255],
            [['codigo_computador'], 'unique', 'targetAttribute' => ['codigo_computador']],
            [['numero_patrimonio'], 'unique', 'targetAttribute' => ['numero_patrimonio']],
            [['codigo_monitor1_fk'], 'unique', 'targetAttribute' => ['codigo_monitor1_fk']],
            [['codigo_monitor2_fk'], 'unique', 'targetAttribute' => ['codigo_monitor2_fk']],
            [['ram_medida_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Medida::className(), 'targetAttribute' => ['ram_medida_fk' => 'id']],
            [['hd_medida_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Medida::className(), 'targetAttribute' => ['hd_medida_fk' => 'id']],
            [['modelo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['modelo_fk' => 'id']],
            [['codigo_monitor1_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Monitor::className(), 'targetAttribute' => ['codigo_monitor1_fk' => 'id']],
            [['codigo_monitor2_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Monitor::className(), 'targetAttribute' => ['codigo_monitor2_fk' => 'id']],
            [['office_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_fk' => 'id']],
            [['placa_video_fk'], 'exist', 'skipOnError' => true, 'targetClass' => PlacaVideo::className(), 'targetAttribute' => ['placa_video_fk' => 'id']],
            [['processador_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Processador::className(), 'targetAttribute' => ['processador_fk' => 'id']],
            [['ram_clok_fk'], 'exist', 'skipOnError' => true, 'targetClass' => RamClok::className(), 'targetAttribute' => ['ram_clok_fk' => 'id']],
            [['setor_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Setor::className(), 'targetAttribute' => ['setor_fk' => 'id']],
            [['sistema_operacional_fk'], 'exist', 'skipOnError' => true, 'targetClass' => SistemaOperacional::className(), 'targetAttribute' => ['sistema_operacional_fk' => 'id']],
            [['tipo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_fk' => 'id']],
            [['tipo_hd_fk'], 'exist', 'skipOnError' => true, 'targetClass' => TipoHd::className(), 'targetAttribute' => ['tipo_hd_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_computador' => 'Código Computador',
            'unidade_fk' => 'Unidade',
            'setor_fk' => 'Setor',
            'tipo_fk' => 'Tipo',
            'modelo_fk' => 'Modelo',
            'numero_serie' => 'Número de Série',
            'data_compra' => 'Data Compra',
            'garantia' => 'Garantia',
            'placa_mae' => 'Placa Mãe',
            'processador_fk' => 'Processador',
            'numero_processador' => 'Número Processador',
            'ram' => 'RAM',
            'ram_medida_fk' => 'RAM Medida',
            'ram_clok_fk' => 'RAM Clok',
            'hd' => 'Tamanho do HD',
            'hd_medida_fk' => 'HD Medida',
            'tipo_hd_fk' => 'Tipo HD',
            'leitura_hd' => 'Leitura',
            'escrita_hd' => 'Gravação',
            'descricao_hd' => 'Descricao HD',
            'loja' => 'Loja',
            'numero_nota' => 'Número da Nota',
            'valor' => 'Valor',
            'numero_patrimonio' => 'Número do Patrimonio',
            'numero_lacre' => 'Número do Lacre',
            'netbios' => 'Netbios',
            'mac_c' => 'MAC Cabo',
            'mac_w' => 'MAC Wi-Fi',
            'sistema_operacional_fk' => 'Sistema Operacional',
            'office_fk' => 'Office',
            'placa_video_fk' => 'Placa Video',
            'numero_palca_video' => 'Numero Palca Video',
            'codigo_monitor1_fk' => 'Codigo Monitor 1',
            'codigo_monitor2_fk' => 'Codigo Monitor 2',
            'descricao_computador' => 'Descricao Computador',
            'status' => 'Status',
            'url' => 'Arquivo Nota Fiscal',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data_compra) {
            $this->data_compra = Setup::convertApresentacao($this->data_compra, 'date');
        }
        if ($this->garantia) {
            $this->garantia = Setup::convertApresentacao($this->garantia, 'date');
        }
        if ($this->valor) {
            $this->valor = FormatterCurrency::formatNumber($this->valor);
        }
        if ($this->mac_c) {
            $this->mac_c = Setup::formatterMAC($this->mac_c);
        }
        if ($this->mac_w) {
            $this->mac_w = Setup::formatterMAC($this->mac_w);
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->codigo_computador) {
            $this->codigo_computador = strtoupper($this->codigo_computador);
        }
        if ($this->data_compra) {
            $this->data_compra = Setup::convertBD($this->data_compra, 'date');
        }
        if ($this->garantia) {
            $this->garantia = Setup::convertBD($this->garantia, 'date');
        }
        if ($this->valor) {
            $this->valor = FormatterCurrency::unformatNumber($this->valor);
        }
        if ($this->mac_c) {
            $this->mac_c = Setup::removeFormatterMAC($this->mac_c);
        }
        if ($this->mac_w) {
            $this->mac_w = Setup::removeFormatterMAC($this->mac_w);
        }
    }
    
    public function getModeloFk()
    {
        return $this->hasOne(Modelo::className(), ['id' => 'modelo_fk']);
    }
    
    public function getCodigoMonitor1Fk()
    {
        return $this->hasOne(Monitor::className(), ['id' => 'codigo_monitor1_fk']);
    }
    
    public function getCodigoMonitor2Fk()
    {
        return $this->hasOne(Monitor::className(), ['id' => 'codigo_monitor2_fk']);
    }
    
    public function getOfficeFk()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_fk']);
    }
    
    public function getPlacaVideoFk()
    {
        return $this->hasOne(PlacaVideo::className(), ['id' => 'placa_video_fk']);
    }
    
    public function getProcessadorFk()
    {
        return $this->hasOne(Processador::className(), ['id' => 'processador_fk']);
    }
    
    public function getRamClokFk()
    {
        return $this->hasOne(RamClok::className(), ['id' => 'ram_clok_fk']);
    }
    
    public function getSetorFk()
    {
        return $this->hasOne(Setor::className(), ['id' => 'setor_fk']);
    }
    
    public function getSistemaOperacionalFk()
    {
        return $this->hasOne(SistemaOperacional::className(), ['id' => 'sistema_operacional_fk']);
    }
    
    public function getTipoFk()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_fk']);
    }
    
    public function getTipoHdFk()
    {
        return $this->hasOne(TipoHd::className(), ['id' => 'tipo_hd_fk']);
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getRamMedidaFk()
    {
        return $this->hasOne(Medida::className(), ['id' => 'ram_medida_fk']);
    }

    public function getHdMedidaFk()
    {
        return $this->hasOne(Medida::className(), ['id' => 'hd_medida_fk']);
    }

    
    public function search($params) {
        $query = Computador::find();

        $query->alias('computador');
        if (isset($params['Computador']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_computador))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac_c))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(mac_w))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_computador.loja))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_computador.numero_nota))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_patrimonio))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_computador.descricao_copmutador))', strtoupper(Setup::retirarAcento($params['Computador']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['unidade_fk' => $usuariounidade]);
        }
        
        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }
        
        if (Yii::$app->user->identity->tipo_temp_fk) {
            $query->andFilterWhere(['=', 'tipo_fk', Yii::$app->user->identity->tipo_temp_fk]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andFilterWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }
        
        if (Yii::$app->user->identity->modelo_temp_fk) {
            $query->andFilterWhere(['=', 'modelo_fk', Yii::$app->user->identity->modelo_temp_fk]);
        }
        
        $model_restricao = VwRestricaoMenu::findAll(['controller' => 'computador', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);
        
        $model_restricao ?  '' : $query->andFilterWhere(['=', 'status', 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
