<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_voip_pin".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $funcionario_fk
 * @property string $pin
 * @property int $particular
 *
 */
class Voippin extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_voip_pin';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'funcionario_fk', 'pin', 'particular'], 'required'],
            [['unidade_fk', 'funcionario_fk', 'pin', 'particular'], 'default', 'value' => null],
            [['unidade_fk', 'funcionario_fk', 'pin', 'particular'], 'integer'],
            [['unidade_fk', 'pin'], 'unique', 'targetAttribute' => ['unidade_fk', 'pin']],
            [['funcionario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionario::className(), 'targetAttribute' => ['funcionario_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unidade_fk' => Yii::t('app', 'Unidade'),
            'funcionario_fk' => Yii::t('app', 'Funcionario'),
            'pin' => Yii::t('app', 'Pin'),
            'particular' => Yii::t('app', 'Particular'),
        ];
    }

    public function getFuncionarioFk()
    {
        return $this->hasOne(Funcionario::className(), ['id' => 'funcionario_fk']);
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Voippin::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);
        
        $query->alias('voippin');
        $query->leftJoin('tb_funcionario', 'tb_funcionario.id = voippin.funcionario_fk');
        if (isset($params['Voippin']['pesquisa'])) {
            if (is_numeric($params['Voippin']['pesquisa'])) {
                $query->orFilterWhere(['=', 'voippin.pin', $params['Voippin']['pesquisa']]);
                
            } else {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_funcionario.nome))', strtoupper(Setup::retirarAcento($params['Voippin']['pesquisa']))]);
            }
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['voippin.unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'voippin.unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());
        
        return $dataProvider;
    }
}
