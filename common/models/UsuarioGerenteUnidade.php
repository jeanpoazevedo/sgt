<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_usuario_gerente_unidade".
 *
 * @property int $id
 * @property int $usuario_fk
 * @property int $unidade_fk
 * @property int $gerencia
 *
 */
class UsuarioGerenteUnidade extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_usuario_gerente_unidade';
    }
    
    public function rules()
    {
        return [
            [['usuario_fk', 'unidade_fk', 'gerencia'], 'required'],
            [['usuario_fk', 'unidade_fk', 'gerencia'], 'default', 'value' => null],
            [['usuario_fk', 'unidade_fk', 'gerencia'], 'integer'],
            [['usuario_fk', 'unidade_fk'], 'unique', 'targetAttribute' => ['usuario_fk', 'unidade_fk']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['usuario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_fk' => 'Usuario',
            'unidade_fk' => 'Unidade',
            'gerencia' => 'Gerencia',
        ];
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getUsuarioFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_fk']);
    }
    
    public function search($params) {
        $query = Usuariogerenteunidade::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        $query->alias('usuariogerenteunidade');
        $query->join('join', 'tb_usuario', 'tb_usuario.id = usuariogerenteunidade.usuario_fk');
        $query->join('join', 'tb_unidade', 'tb_unidade.id = usuariogerenteunidade.unidade_fk');
        if (isset($params['UsuarioUnidade']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_usuario.nome))', strtoupper(Setup::retirarAcento($params['UsuarioGerenteUnidade']['pesquisa']))])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tb_unidade.unidade))', strtoupper(Setup::retirarAcento($params['UsuarioGerenteUnidade']['pesquisa']))]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
