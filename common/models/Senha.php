<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_senha".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $perfil_fk
 * @property string $usuario
 * @property string $senha
 * @property string $descricao
 * @property int status
 *
 */
class Senha extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_senha';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'perfil_fk', 'usuario', 'senha', 'descricao', 'status'], 'required'],
            [['unidade_fk', 'perfil_fk'], 'default', 'value' => null],
            [['unidade_fk', 'perfil_fk', 'status'], 'integer'],
            [['usuario'], 'string', 'max' => 120],
            [['senha', 'descricao'], 'string', 'max' => 255],
            [['unidade_fk', 'usuario', 'senha', 'descricao'], 'unique', 'targetAttribute' => ['unidade_fk', 'usuario', 'senha', 'descricao']],
            [['perfil_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['perfil_fk' => 'id']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'descricao' => Yii::t('app', 'Descrição'),
            'usuario' => Yii::t('app', 'Usuário'),
            'senha' => Yii::t('app', 'Senha'),
            'perfil_fk' => Yii::t('app', 'Perfil'),
            'unidade_fk' => Yii::t('app', 'Unidade'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getPerfilFk()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'perfil_fk']);
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Senha::find();

        if (isset($params['Senha']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['Senha']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario))', strtoupper(Setup::retirarAcento($params['Senha']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(senha))', strtoupper(Setup::retirarAcento($params['Senha']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $perfil = $usuarioperfil->perfil_fk;
        
        if($perfil > 5){
            $query->andFilterWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);
            $query->andWhere(['=', 'perfil_fk', $usuarioperfil->perfil_fk]);
        } else {
            if (Yii::$app->user->identity->unidade_temp_fk != '1') {
                $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
            } else {
                $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
            }
            if($perfil != 1){
                if($perfil < 5){
                    if($perfil != 2){
                        $query->andWhere(['>=', 'perfil_fk', $usuarioperfil->perfil_fk])->andWhere(['<', 'perfil_fk', 6]);
                    } else {
                        $query->andWhere(['>=', 'perfil_fk', $usuarioperfil->perfil_fk]);
                    }
                } else {
                    $query->andWhere(['=', 'perfil_fk', $usuarioperfil->perfil_fk]);
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_DESC,
                    'unidade_fk' => SORT_ASC,
                    'descricao' => SORT_ASC,
                    'usuario' => SORT_ASC
                ]
            ],
        ]);
        
        return $dataProvider;
    }

}
