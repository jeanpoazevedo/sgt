<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vw_menu".
 *
 * @property int $id
 * @property int $usuario_fk
 * @property int $sistema_fk
 * @property int $menu_pai_fk
 * @property int $nivel
 * @property string $titulo
 * @property string $controller
 * @property string $icon
 */
class VwMenu extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'vw_menu';
    }

    public function rules()
    {
        return [
            [['id', 'usuario_fk', 'sistema_fk', 'menu_pai_fk', 'nivel'], 'default', 'value' => null],
            [['id', 'usuario_fk', 'sistema_fk', 'menu_pai_fk', 'nivel'], 'integer'],
            [['titulo'], 'string', 'max' => 120],
            [['controller', 'icon'], 'string', 'max' => 200],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_fk' => 'Usuario',
            'sistema_fk' => 'Sistema',
            'menu_pai_fk' => 'Menu Pai',
            'nivel' => 'Nivel',
            'titulo' => 'Titulo',
            'controller' => 'Controller',
            'icon' => 'Icon',
        ];
    }
}
