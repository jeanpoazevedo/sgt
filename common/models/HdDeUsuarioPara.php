<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_helpdesk_de_usuario_para".
 *
 * @property int $id
 * @property int $usuario_de_fk
 * @property int $usuario_para_fk
 *
 */
class HdDeUsuarioPara extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_helpdesk_de_usuario_para';
    }
    
    public function rules()
    {
        return [
            [['usuario_de_fk', 'usuario_para_fk'], 'required'],
            [['usuario_de_fk', 'usuario_para_fk'], 'default', 'value' => null],
            [['usuario_de_fk', 'usuario_para_fk'], 'integer'],
            [['usuario_de_fk', 'usuario_para_fk'], 'unique', 'targetAttribute' => ['usuario_de_fk', 'usuario_para_fk']],
            [['usuario_de_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_de_fk' => 'id']],
            [['usuario_para_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_para_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_de_fk' => 'Solicitante',
            'usuario_para_fk' => 'Atendente',
        ];
    }
    
    public function getUsuariodeFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_de_fk']);
    }
    
    public function getUsuarioparaFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_para_fk']);
    }
    
    public function search($params) {
        $query = Hddeusuariopara::find();

        if (isset($params['Hddeusuariopara']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(id))', strtoupper(Setup::retirarAcento($params['Hddeusuariopara']['pesquisa']))]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'usuario_de_fk' => SORT_ASC,
                    'usuario_para_fk' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
