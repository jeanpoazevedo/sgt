<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_patch_panel".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property string $unidade
 * @property int $local_fk
 * @property string $local
 * @property int $id_patch_panel
 * @property string $descricao_patch_panel
 * @property int $numero_portas
 * @property string $xml_portas
 * 
 */

class VwPatchPanel extends \yii\db\ActiveRecord
{
    public $pesquisa;
    public static function tableName()
    {
        return 'vw_patch_panel';
    }
    public static function primaryKey() {
        return ['id'];
    }
    public function rules()
    {
        return [
            [['id', 'unidade_fk', 'local_fk', 'id_patch_panel', 'numero_portas'], 'default', 'value' => null],
            [['id', 'unidade_fk', 'local_fk', 'id_patch_panel', 'numero_portas'], 'integer'],
            [['unidade'], 'string', 'max' => 40],
            [['descricao_patch_panel', 'local'], 'string', 'max' => 80],
            [['xml_portas'], 'string'],
            [['unidade_fk', 'local_fk', 'id_patch_panel'], 'unique', 'targetAttribute' => ['unidade_fk', 'local_fk', 'id_patch_panel']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['local_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['local_fk' => 'id']],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade Fk',
            'unidade' => 'Unidade',
            'local_fk' => 'Local Fk',
            'local' => 'Local',
            'id_patch_panel' => 'ID Patch Panel',
            'descricao_patch_panel' => 'Descrição do Patch Panel',
            'numero_portas' => 'Numero Portas',
            'xml_portas' => 'Xml Portas'
        ];
    }
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    public function getLocalFk() {
        return $this->hasOne(Local::className(), ['id' => 'local_fk']);
    }
    public function search($params) {
        $query = VwPatchPanel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'unidade' => SORT_ASC,
                    'local' => SORT_ASC,
                    'id_patch_panel' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['VwPatchPanel']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(xml_portas))', strtoupper(Setup::retirarAcento($params['VwPatchPanel']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_patch_panel))', strtoupper(Setup::retirarAcento($params['VwPatchPanel']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(xml_portas))', strtoupper(Setup::retirarAcento($params['VwPatchPanel']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
}
