<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_perfil".
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 *
 */
class Perfil extends \yii\db\ActiveRecord
{
    public $pesquisa;
    public $menu;
    
    public static function tableName()
    {
        return 'tb_perfil';
    }
    
    public function rules()
    {
        return [
            [['nome', 'descricao'], 'required'],
            [['nome', 'descricao'], 'string', 'max' => 1024],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descricao',
        ];
    }
    
    public function search($params) {
        $query = Perfil::find();

        if (isset($params['Perfil']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Perfil']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['Perfil']['pesquisa']))]);
        }

        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $perfil = $usuarioperfil->perfil_fk;
        
        if($perfil > 5){
            $query->andWhere(['=', 'id', $usuarioperfil->perfil_fk]);
        } else {
            if($perfil != 1){
                if($perfil < 5){
                    if($perfil != 2){
                        $query->andWhere(['>=', 'id', $usuarioperfil->perfil_fk])->andWhere(['<', 'id', 6]);
                    } else {
                        $query->andWhere(['>=', 'id', $usuarioperfil->perfil_fk]);
                    }
                } else {
                    $query->andWhere(['=', 'id', $usuarioperfil->perfil_fk]);
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
