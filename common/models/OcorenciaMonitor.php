<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_ocorencia_monitor".
 *
 * @property int $id
 * @property string $data
 * @property string $usuario_nome
 * @property int $codigo_monitor_fk
 * @property int $tipo
 * @property string $descricao
 * @property string $url
 *
 */
class OcorenciaMonitor extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_ocorencia_monitor';
    }

    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['usuario_nome', 'codigo_monitor_fk', 'tipo_fk'], 'required'],
            [['codigo_monitor_fk', 'tipo_fk'], 'default', 'value' => null],
            [['codigo_monitor_fk', 'tipo_fk'], 'integer'],
            [['descricao'], 'string'],
            [['url'], 'string', 'max' => 255],
            [['usuario_nome'], 'string', 'max' => 80],
            [['codigo_monitor_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Monitor::className(), 'targetAttribute' => ['codigo_monitor_fk' => 'id']],
            [['tipo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => OcorenciaTipo::className(), 'targetAttribute' => ['tipo_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data / Hora'),
            'usuario_nome' => Yii::t('app', 'Nome do Usuário'),
            'codigo_monitor_fk' => Yii::t('app', 'Código Monitor'),
            'tipo_fk' => Yii::t('app', 'Tipo'),
            'descricao' => Yii::t('app', 'Descrição'),
            'url' => 'Anexo',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data) {
            $this->data = Setup::convertApresentacao($this->data, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data) {
            $this->data = Setup::convertBD($this->data, 'datetime');
        }
    }
    
    public function getCodigomonitorFk()
    {
        return $this->hasOne(Monitor::className(), ['id' => 'codigo_monitor_fk']);
    }
    
    public function getTipoFk()
    {
        return $this->hasOne(OcorenciaTipo::className(), ['id' => 'tipo_fk']);
    }
    
    public function search($params) {
        $query = OcorenciaMonitor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['OcorenciaMonitor']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario_nome))', strtoupper(Setup::retirarAcento($params['OcorenciaMonitor']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper(Setup::retirarAcento($params['OcorenciaMonitor']['pesquisa']))]);
        }
        
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $codigos = Monitor::find()->select('id')->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);            
            $query->andFilterWhere(['codigo_monitor_fk' => $codigos]);
        } else {
            $codigos = Monitor::find()->select('id')->where(['unidade_fk' => $usuariounidade]);
            $query->andFilterWhere(['codigo_monitor_fk' => $codigos]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }
    
    public function searchocorencia($params) {
        $query = OcorenciaMonitor::find();

        if (isset($params['OcorenciaMonitor']['codigo_monitor_fk'])) {
            $query->andFilterWhere(['=', 'codigo_monitor_fk', $params['OcorenciaMonitor']['codigo_monitor_fk']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000000,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
