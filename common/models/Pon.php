<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_pon".
 *
 * @property int $id
 * @property int $codigo_pon_fk
 * @property int $local_fk
 * @property int $id_pon
 * @property int $numero_portas_pon
 * @property int $numero_portas_bridge
 * @property string|null $running_geral
 * @property string|null $running_bridge
 * @property string|null $running_pon_dba_profile
 * @property string|null $running_pon_extended_profile
 * @property string|null $running_pon_traffic_profile
 * @property string|null $running_pon_onu_profile
 * @property string|null $running_pon_olt
 * @property string|null $conf
 */
class Pon extends \yii\db\ActiveRecord
{

    public $pesquisa;

    public static function tableName()
    {
        return 'tb_pon';
    }

    public function rules()
    {
        return [
            [['codigo_pon_fk', 'local_fk', 'id_pon', 'numero_portas_pon', 'numero_portas_bridge'], 'required'],
            [['codigo_pon_fk', 'local_fk', 'id_pon', 'numero_portas_pon', 'numero_portas_bridge'], 'default', 'value' => null],
            [['codigo_pon_fk', 'local_fk', 'id_pon', 'numero_portas_pon', 'numero_portas_bridge'], 'integer'],
            [['running_geral', 'running_bridge', 'running_pon_dba_profile', 'running_pon_extended_profile', 'running_pon_traffic_profile', 'running_pon_onu_profile', 'running_pon_olt', 'conf'], 'string'],
            [['codigo_pon_fk', 'local_fk', 'id_pon'], 'unique', 'targetAttribute' => ['codigo_pon_fk', 'local_fk', 'id_pon']],
            [['codigo_pon_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Equipamento::className(), 'targetAttribute' => ['codigo_pon_fk' => 'id']],
            [['local_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['local_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_pon_fk' => 'Código PON',
            'local_fk' => 'Local',
            'id_pon' => 'ID PON',
            'numero_portas_pon' => 'Número Portas PON',
            'numero_portas_bridge' => 'Número Portas Bridge',
            'running_geral' => 'Running Geral',
            'running_bridge' => 'Running Bridge',
            'running_pon_dba_profile' => 'Running DBA Profile',
            'running_pon_extended_profile' => 'Running Extended Profile',
            'running_pon_traffic_profile' => 'Running Traffic Profile',
            'running_pon_onu_profile' => 'Running ONU Profile',
            'running_pon_olt' => 'Running OLT',
            'conf' => 'Backup TXT',
        ];
    }

    public function getCodigoPonFk()
    {
        return $this->hasOne(Equipamento::className(), ['id' => 'codigo_pon_fk']);
    }

    public function getLocalFk()
    {
        return $this->hasOne(Local::className(), ['id' => 'local_fk']);
    }

    public function search($params) {
        $query = Pon::find();

        $query->alias('pon');
        $query->leftJoin('tb_equipamento', 'tb_equipamento.id = pon.codigo_pon_fk');

        if (isset($params['Pon']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(conf))', strtoupper(Setup::retirarAcento($params['Pon']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }

}
