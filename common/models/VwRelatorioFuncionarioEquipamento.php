<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "vw_relatorio_funcionario_equipamento".
 *
 * @property int|null $id
 * @property int|null $unidade_fk
 * @property string|null $funcionario_setor
 * @property int|null $funcionario_setor_fk
 * @property string|null $funcao
 * @property string|null $nome
 * @property int|null $equipamento_unidade_fk
 * @property string|null $equipamento_setor
 * @property int|null $equipamento_setor_fk
 * @property string|null $tipo
 * @property int|null $tipo_fk
 * @property string|null $marca
 * @property int|null $marca_fk
 * @property string|null $modelo
 * @property int|null $modelo_fk
 * @property string|null $codigo_equipamento
 * @property string|null $numero_serie
 * @property string|null $netbios
 */
class VwRelatorioFuncionarioEquipamento extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'vw_relatorio_funcionario_equipamento';
    }

    public static function primaryKey() {
        return ['id'];
    }
    
    public function rules()
    {
        return [
            [['id', 'unidade_fk', 'funcionario_setor_fk', 'equipamento_unidade_fk', 'equipamento_setor_fk', 'tipo_fk', 'marca_fk', 'modelo_fk'], 'default', 'value' => null],
            [['id', 'unidade_fk', 'funcionario_setor_fk', 'equipamento_unidade_fk', 'equipamento_setor_fk', 'tipo_fk', 'marca_fk', 'modelo_fk'], 'integer'],
            [['funcionario_setor', 'funcao', 'equipamento_setor'], 'string', 'max' => 50],
            [['nome'], 'string', 'max' => 60],
            [['tipo', 'marca'], 'string', 'max' => 40],
            [['modelo', 'numero_serie'], 'string', 'max' => 80],
            [['codigo_equipamento'], 'string', 'max' => 7],
            [['netbios'], 'string', 'max' => 30],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade do Funcionário',
            'funcionario_unidade_fk' => 'Unidade do Funcionário',
            'funcionario_setor' => 'Setor do Funcionário',
            'funcionario_setor_fk' => 'Setor do Funcionário Fk',
            'funcao' => 'Função',
            'nome' => 'Funcionário',
            'uipamento_unidade_fk' => 'Unidade do Equipamento',
            'equipamento_setor' => 'Setor do Equipamento',
            'equipamento_setor_fk' => 'Setor do Equipamento Fk',
            'tipo' => 'Tipo',
            'tipo_fk' => 'Tipo Fk',
            'marca' => 'Marca',
            'marca_fk' => 'Marca Fk',
            'modelo' => 'Modelo',
            'modelo_fk' => 'Modelo Fk',
            'codigo_equipamento' => 'Código Equipamento',
            'numero_serie' => 'Número de Série',
            'netbios' => 'Netbios',
        ];
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }

    public function getEquipamentounidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'equipamento_unidade_fk']);
    }
    
    public function search($params) {
        $query = VwRelatorioFuncionarioEquipamento::find();

        if (isset($params['VwRelatorioFuncionarioEquipamento']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(funcionario_setor))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(funcao))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(equipamento_setor))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(tipo))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(modelo))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(marca))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(codigo_equipamento))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(numero_serie))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(netbios))', strtoupper(Setup::retirarAcento($params['VwRelatorioFuncionarioEquipamento']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id]);

        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['unidade_fk' => $usuariounidade]);
        }

        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'funcionario_setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }
        
        if (Yii::$app->user->identity->tipo_temp_fk) {
            $query->andFilterWhere(['=', 'tipo_fk', Yii::$app->user->identity->tipo_temp_fk]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andFilterWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }
        
        if (Yii::$app->user->identity->modelo_temp_fk) {
            $query->andFilterWhere(['=', 'modelo_fk', Yii::$app->user->identity->modelo_temp_fk]);
        }

        if (Yii::$app->user->identity->funcao_temp != NULL) {
            $query->andWhere(['=', 'funcao', Yii::$app->user->identity->funcao_temp]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'nome' => SORT_ASC,
                    'codigo_equipamento' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }
}
