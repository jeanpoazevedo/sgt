<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_patch_panel".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $local_fk
 * @property int $id_patch_panel
 * @property string $descricao_patch_panel
 * @property int $numero_portas
 * @property string $xml_portas
 *
 */
class PatchPanel extends \yii\db\ActiveRecord {

    public $pesquisa;
    public $numero_porta;
    public $status;
    public $descricao;

    public static function tableName() {
        return 'tb_patch_panel';
    }

    public function rules()
    {
        return [
            [['unidade_fk', 'local_fk', 'id_patch_panel', 'numero_portas', 'xml_portas'], 'required'],
            [['unidade_fk', 'local_fk', 'id_patch_panel', 'numero_portas'], 'default', 'value' => null],
            [['unidade_fk', 'local_fk', 'id_patch_panel', 'numero_portas'], 'integer'],
            [['descricao_patch_panel'], 'string', 'max' => 80],
            [['xml_portas'], 'string'],
            [['unidade_fk', 'local_fk', 'id_patch_panel'], 'unique', 'targetAttribute' => ['unidade_fk', 'local_fk', 'id_patch_panel']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['local_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['local_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'local_fk' => 'Local',
            'id_patch_panel' => 'ID Patch Panel',
            'descricao_patch_panel' => 'Descrição do Patch Panel',
            'numero_portas' => 'Número Portas',
            'xml_portas' => 'Xml Portas'
        ];
    }

    public function getUnidadeFk() {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }

    public function getLocalFk() {
        return $this->hasOne(Local::className(), ['id' => 'local_fk']);
    }

    public function search($params) {
        $query = PatchPanel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'local_fk' => SORT_ASC,
                    'id_patch_panel' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['PatchPanel']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(xml_portas))', strtoupper(Setup::retirarAcento($params['PatchPanel']['pesquisa']))])
            ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_patch_panel))', strtoupper(Setup::retirarAcento($params['PatchPanel']['pesquisa']))]);
        }

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        $dataProvider->setTotalCount($query->count());

        return $dataProvider;
    }

}
