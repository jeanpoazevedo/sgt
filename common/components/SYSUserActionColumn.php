<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn;

class SYSUserActionColumn extends ActionColumn {

    public $headerOptions = ['class' => 'actions-buttons text-center'];
    public $contentOptions = ['class' => 'text-center'];
    public $width = '130px';
    public $viewButtonVisible = true;
    public $updateButtonVisible = true;
    public $deleteButtonVisible = true;
    public $passwordButtonVisible = true;

    function init() {
        parent::init();
        $this->template = "<span>{view} {update} {updatepassword} {delete} {password} {unidade} {hdunidade}</span>";
        $this->initDefaultButtons();
    }

    public function run() {
        return Html::decode($this->contentOptions);
    }

    protected function initDefaultButtons() {
        if (($this->viewButtonVisible) && (!isset($this->buttons['view']))) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'View'),
                    'aria-label' => Yii::t('app', 'View'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-default loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (($this->updateButtonVisible) && (!isset($this->buttons['update']))) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Edit'),
                    'aria-label' => Yii::t('app', 'Edit'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-primary loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (($this->updateButtonVisible) && (!isset($this->buttons['updatepassword']))) {
            $this->buttons['updatepassword'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Alterar a Senha'),
                    'aria-label' => Yii::t('app', 'Alterar a Senha'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-primary loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="md md-vpn-key"></span>', $url, $options);
            };
        }
        if (($this->deleteButtonVisible) && (!isset($this->buttons['delete']))) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Delete'),
                    'aria-label' => Yii::t('app', 'Delete'),
                    'data-confirm' => Yii::t('app', 'Tem certeza que deseja excluir este item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-danger',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
        if (($this->passwordButtonVisible) && (!isset($this->buttons['password']))) {
            $this->buttons['password'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Resetar Senha'),
                    'aria-label' => Yii::t('app', 'Resetar Senha'),
                    'data-confirm' => Yii::t('app', 'Tem certeza que deseja resetar a senha?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-warning',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, $options);
            };
        }
        if (($this->updateButtonVisible) && (!isset($this->buttons['unidade']))) {
            $this->buttons['unidade'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Ajuste de Unidade ao Usuário'),
                    'aria-label' => Yii::t('app', 'Ajuste de Unidade ao Usuário'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-primary loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-home"></span>', $url, $options);
            };
        }
        if (($this->updateButtonVisible) && (!isset($this->buttons['hdunidade']))) {
            $this->buttons['hdunidade'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Ajuste de Help Desk Unidade ao Usuário'),
                    'aria-label' => Yii::t('app', 'Ajuste de Help Desk Unidade ao Usuário'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-info loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-question-sign"></span>', $url, $options);
            };
        }
    }

}
?>