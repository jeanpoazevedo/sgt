<?php

namespace common\components\XML;

class Switchxml {
    
    public static function generationXML($array) {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<switch>';
        
        foreach ($array as $key => $value) {
            $xml .= '<port>';
            $xml .= '<porta>' . $value['porta'] . '</porta>';
            $xml .= '<status>' . $value['status'] . '</status>';
            $xml .= '<vlan>' . $value['vlan'] . '</vlan>';
            $xml .= '<type>' . $value['type'] . '</type>';
            $xml .= '<tagged>' . $value['tagged'] . '</tagged>';
            $xml .= '<description>' . $value['description'] . '</description>';
            $xml .= '</port>';
        }
        
        $xml .= '</switch>';
        return $xml;
    }
    
}
