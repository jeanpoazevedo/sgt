<?php

namespace common\components\XML;

class Patchpanelxml {
    
    public static function generationXML($array) {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<patchpanel>';
        
        foreach ($array as $key => $value) {
            $xml .= '<port>';
            $xml .= '<porta>' . $value['porta'] . '</porta>';
            $xml .= '<status>' . $value['status'] . '</status>';
            $xml .= '<descricao>' . $value['descricao'] . '</descricao>';
            $xml .= '</port>';
        }
        
        $xml .= '</patchpanel>';
        return $xml;
    }
    
}
