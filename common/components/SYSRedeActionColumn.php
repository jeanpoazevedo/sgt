<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn;

class SYSRedeActionColumn extends ActionColumn {

    public $headerOptions = ['class' => 'actions-buttons text-center'];
    public $contentOptions = ['class' => 'text-center'];
    public $width = '130px';
    public $viewButtonVisible = true;
    public $updateButtonVisible = true;
    public $deleteButtonVisible = true;
    public $imageButtonVisible = true;
    public $addButtonVisible = true;
    public $pdfButtonVisible = true;

    function init() {
        parent::init();
        if (!$this->template) {
            $this->template = "<span>{update} {delete}</span>";
        } else {
            $this->template = "<span>" . $this->template . "</span>";
        }
        $this->initDefaultButtons();
    }

    public function run() {
        return Html::decode($this->contentOptions);
    }

    protected function initDefaultButtons() {
        if (($this->updateButtonVisible) && (!isset($this->buttons['update']))) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Edit'),
                    'aria-label' => Yii::t('app', 'Edit'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-primary loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (($this->deleteButtonVisible) && (!isset($this->buttons['delete']))) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Delete'),
                    'aria-label' => Yii::t('app', 'Delete'),
                    'data-confirm' => Yii::t('app', 'Tem certeza que deseja excluir está rede?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-danger',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
        if (($this->addButtonVisible) && (!isset($this->buttons['add']))) {
            $this->buttons['add'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Adicionar'),
                    'aria-label' => Yii::t('app', 'Adicionar'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-warning loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-plus-sign"></span>', $url, $options);
            };
        }
    }

}
