<?php

namespace common\components;

use common\models\Email;
use Yii;

class SendMail {

    public static function submit($emails, $model, $conteudo) {
        $email = Email::findOne(1);
        
        $scheme = 'smtp';
        if ($email->array_encryption[$email->encryption] === 'ssl') {
            $scheme = 'smtps';
        }

        $mailer = Yii::createObject([
            'class' => 'yii\symfonymailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'scheme' => $scheme,
                'host' => $email->host,
                'username' => $email->username,
                'password' => $email->password,
                'port' => $email->port,
            ],
        ]);

        $mailer->compose([
                    'html' => $conteudo,
                    'text' => 'sgt_email_text'
                        ], [
                    'model' => $model
                ])->setFrom([
                    $email->username => $email->nome
                ])->setTo($emails)
                ->setSubject('SGT')
                ->send();
    }
    
    public static function submitanterior($emails, $model, $model_anterior, $ocorencia_unidade_anterior, $ocorencia_unidade, $conteudo) {
        $email = Email::findOne(1);
        
        $scheme = 'smtp';
        if ($email->array_encryption[$email->encryption] === 'ssl') {
            $scheme = 'smtps';
        }

        $mailer = Yii::createObject([
            'class' => 'yii\symfonymailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'scheme' => $scheme,
                'host' => $email->host,
                'username' => $email->username,
                'password' => $email->password,
                'port' => $email->port,
            ],
        ]);

        $mailer->compose([
                    'html' => $conteudo,
                    'text' => 'sgt_email_text'
                        ], [
                    'model' => $model,
                    'model_anterior' => $model_anterior,
                    'ocorencia_unidade_anterior' => $ocorencia_unidade_anterior,
                    'ocorencia_unidade' => $ocorencia_unidade
                ])->setFrom([
                    $email->username => $email->nome
                ])->setTo($emails)
                ->setSubject('SGT')
                ->send();
    }
    
    public static function submithd($emails, $model, $model_anterior, $model_historio, $conteudo) {
        $email = Email::findOne(1);
        
        $scheme = 'smtp';
        if ($email->array_encryption[$email->encryption] === 'ssl') {
            $scheme = 'smtps';
        }

        $mailer = Yii::createObject([
            'class' => 'yii\symfonymailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'scheme' => $scheme,
                'host' => $email->host,
                'username' => $email->username,
                'password' => $email->password,
                'port' => $email->port,
            ],
        ]);

        $mailer->compose([
                    'html' => $conteudo,
                    'text' => 'sgt_email_text'
                        ], [
                    'model' => $model,
                    'model_anterior' => $model_anterior,
                    'model_historio' => $model_historio
                ])->setFrom([
                    $email->username => $email->nome
                ])->setTo($emails)
                ->setSubject('SGT')
                ->send();
    }
    
    public static function submithdetapa($emails, $model, $model_anterior, $model_etapas, $conteudo) {
        $email = Email::findOne(1);
        
        $scheme = 'smtp';
        if ($email->array_encryption[$email->encryption] === 'ssl') {
            $scheme = 'smtps';
        }

        $mailer = Yii::createObject([
            'class' => 'yii\symfonymailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'scheme' => $scheme,
                'host' => $email->host,
                'username' => $email->username,
                'password' => $email->password,
                'port' => $email->port,
            ],
        ]);

        $mailer->compose([
                    'html' => $conteudo,
                    'text' => 'sgt_email_text'
                        ], [
                    'model' => $model,
                    'model_anterior' => $model_anterior,
                    'model_etapas' => $model_etapas
                ])->setFrom([
                    $email->username => $email->nome
                ])->setTo($emails)
                ->setSubject('SGT')
                ->send();
    }
    
    public static function submithdcron($emails, $model, $model_historio, $conteudo) {
        $email = Email::findOne(1);
        
        $scheme = 'smtp';
        if ($email->array_encryption[$email->encryption] === 'ssl') {
            $scheme = 'smtps';
        }

        $mailer = Yii::createObject([
            'class' => 'yii\symfonymailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'scheme' => $scheme,
                'host' => $email->host,
                'username' => $email->username,
                'password' => $email->password,
                'port' => $email->port,
            ],
        ]);

        $mailer->compose([
                    'html' => $conteudo,
                    'text' => 'sgt_email_text'
                        ], [
                    'model' => $model,
                    'model_historio' => $model_historio
                ])->setFrom([
                    $email->username => $email->nome
                ])->setTo($emails)
                ->setSubject('SGT')
                ->send();
    }
    
    public static function submitpassword($emails, $model, $conteudo) {
        $email = Email::findOne(1);
        
        $scheme = 'smtp';
        if ($email->array_encryption[$email->encryption] === 'ssl') {
            $scheme = 'smtps';
        }

        $mailer = Yii::createObject([
            'class' => 'yii\symfonymailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'scheme' => $scheme,
                'host' => $email->host,
                'username' => $email->username,
                'password' => $email->password,
                'port' => $email->port,
            ],
        ]);

        $mailer->compose([
                    'html' => $conteudo,
                    'text' => 'sgt_email_text'
                        ], [
                    'model' => $model
                ])->setFrom([
                    $email->username => $email->nome
                ])->setTo($emails)
                ->setSubject('Atualização senha - SGT')
                ->send();
    }

}
