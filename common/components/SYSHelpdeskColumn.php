<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn;

class SYSHelpdeskColumn extends ActionColumn {

    public $headerOptions = ['class' => 'actions-buttons text-center'];
    public $contentOptions = ['class' => 'text-center'];
    public $width = '130px';
    public $viewButtonVisible = true;
    public $atenderButtonVisible = true;

    function init() {
        parent::init();
        $this->template = "<span>{view} {atender}</span>";
        $this->initDefaultButtons();
    }

    public function run() {
        return Html::decode($this->contentOptions);
    }

    protected function initDefaultButtons() {
        if (($this->viewButtonVisible) && (!isset($this->buttons['view']))) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Ver'),
                    'aria-label' => Yii::t('app', 'Ver'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-default loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (($this->atenderButtonVisible) && (!isset($this->buttons['atender']))) {
            $this->buttons['atender'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Responder'),
                    'aria-label' => Yii::t('app', 'Responder'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-primary loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
    }

}
