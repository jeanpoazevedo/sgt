<?php

namespace common\components;

use DateTime;
use NFePHP\Gtin\Gtin;

class Setup {

    const DATE_FORMAT = 'Y-m-d';
    const DATE_FORMAT_APRESENTACAO = "d/m/Y";
    const DATETIME_FORMAT = 'Y-m-d H:i:s';
    const DATETIME_FORMAT_APRESENTACAO = "d/m/Y H:i:s";
    const TIME_FORMAT = 'H:i:s';
    const MESES = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

    public static function convertBD($dateStr, $type = 'date', $format = null) {
        if ($type === 'datetime') {
            $fmt = ($format == null) ? self::DATETIME_FORMAT : $format;
            $fmta = ($format == null) ? self::DATETIME_FORMAT_APRESENTACAO : $format;
        } elseif ($type === 'time') {
            $fmt = ($format == null) ? self::TIME_FORMAT : $format;
            $fmta = ($format == null) ? self::TIME_FORMAT : $format;
        } else {
            $fmt = ($format == null) ? self::DATE_FORMAT : $format;
            $fmta = ($format == null) ? self::DATE_FORMAT_APRESENTACAO : $format;
        }
        $date = DateTime::createFromFormat($fmta, $dateStr);
        return $date->format($fmt);
    }

    public static function convertApresentacao($dateStr, $type = 'date', $format = null) {
        $value = null;
        if ($dateStr) {
            if ($type === 'datetime') {
                $fmt = ($format == null) ? self::DATETIME_FORMAT : $format;
                $fmta = ($format == null) ? self::DATETIME_FORMAT_APRESENTACAO : $format;
            } elseif ($type === 'time') {
                $fmt = ($format == null) ? self::TIME_FORMAT : $format;
                $fmta = ($format == null) ? self::TIME_FORMAT : $format;
            } else {
                $fmt = ($format == null) ? self::DATE_FORMAT : $format;
                $fmta = ($format == null) ? self::DATE_FORMAT_APRESENTACAO : $format;
            }
            $date = DateTime::createFromFormat($fmt, $dateStr);
            $value = $date->format($fmta);
        }
        return $value;
    }

    public static function convertSoapToApresentacao($dateStr) {
        $date = DateTime::createFromFormat('Y-m-d\TH:i:sO', $dateStr);
        return $date->format(self::DATETIME_FORMAT_APRESENTACAO);
    }

    public static function convertDatatoSPED($dateStr) {
        $date = DateTime::createFromFormat(self::DATE_FORMAT_APRESENTACAO, $dateStr);
        return $date->format('dmY');
    }

    public static function retirarAcento($string) {
        return preg_replace([
            "/(á|à|ã|â|ä)/",
            "/(Á|À|Ã|Â|Ä)/",
            "/(é|è|ê|ë)/",
            "/(É|È|Ê|Ë)/",
            "/(í|ì|î|ï)/",
            "/(Í|Ì|Î|Ï)/",
            "/(ó|ò|õ|ô|ö)/",
            "/(Ó|Ò|Õ|Ô|Ö)/",
            "/(ú|ù|û|ü)/",
            "/(Ú|Ù|Û|Ü)/",
            "/(ç)/",
            "/(Ç)/",
            "/(ñ)/",
            "/(Ñ)/"
                ], explode(" ", "a A e E i I o O u U c C n N")
                , $string);
    }

    public static function unformatterString($string) {
        $value = str_replace('.', '', $string);
        $value = str_replace('-', '', $value);
        $value = str_replace('/', '', $value);
        $value = str_replace('(', '', $value);
        $value = str_replace(')', '', $value);
        $value = str_replace(' ', '', $value);
        return $value;
    }
    
    public static function removeCharacterSpace($string) {
        $value = str_replace('&nbsp;', '', $string);
        return $value;
    }

    public static function formatterCNPJ($strCNPJ) {
        if (!empty($strCNPJ)) {
            return substr($strCNPJ, 0, 2) . '.' . substr($strCNPJ, 2, 3) . '.' . substr($strCNPJ, 5, 3) . '/' . substr($strCNPJ, 8, 4) . '-' . substr($strCNPJ, 12, 2);
        } else {
            return null;
        }
    }

    public static function formatterCEP($strCEP) {
        if (!empty($strCEP)) {
            return substr($strCEP, 0, 2) . '.' . substr($strCEP, 2, 3) . '-' . substr($strCEP, 5, 3);
        } else {
            return null;
        }
    }
    
    public static function formatterMAC($strMAC) {
        if (!empty($strMAC)) {
            return strtoupper(substr($strMAC, 0, 2) . ':' . substr($strMAC, 2, 2) . ':' . substr($strMAC, 4, 2) . ':' . substr($strMAC, 6, 2) . ':' . substr($strMAC, 8, 2) . ':' . substr($strMAC, 10, 2));
        } else {
            return null;
        }
    }

    public static function removeFormatterMAC($strMAC) {
        if (!empty($strMAC)) {
            $value = str_replace(':', '', $strMAC);
            $value = str_replace('-', '', $value);
            $value = strtoupper($value);
            return $value;
        } else {
            return null;
        }
    }
    
    public static function formatterPlaca($strPlaca) {
        if (!empty($strPlaca)) {
            return substr($strPlaca, 0, 3) . '-' . substr($strPlaca, 3, 4);
        } else {
            return null;
        }
    }

    public static function formatterCPF($strCPF) {
        if (!empty($strCPF)) {
            return substr($strCPF, 0, 3) . '.' . substr($strCPF, 3, 3) . '.' . substr($strCPF, 6, 3) . '-' . substr($strCPF, 9, 2);
        } else {
            return null;
        }
    }

    public static function formatterTelefone($strTelefone) {
        if (!empty($strTelefone)) {
            if (strlen($strTelefone) > 10) {
                return '(' . substr($strTelefone, 0, 2) . ') ' . substr($strTelefone, 2, 5) . '-' . substr($strTelefone, 7, 4);
            } else {
                return '(' . substr($strTelefone, 0, 2) . ') ' . substr($strTelefone, 2, 4) . '-' . substr($strTelefone, 6, 4);
            }
        } else {
            return null;
        }
    }

    public static function somarData($data, $dias = 0, $meses = 0, $ano = 0) {
        $data = explode('/', $data);
        $newData = date('d/m/Y', mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano));
        return $newData;
    }

    public static function setMessageErrorModel($descricao, $array) {
        $conteudo = $descricao . '<br><ul>';
        foreach ($array as $key => $value) {
            foreach ($value as $keyItem => $item) {
                $conteudo .= '<li>' . $item . '</li>';
            }
        }
        $conteudo .= '</ul>';
        return $conteudo;
    }

    public static function setMessageErrorModelMobile($descricao, $array) {
        $conteudo = $descricao;
        foreach ($array as $key => $value) {
            foreach ($value as $keyItem => $item) {
                $conteudo .= ' ' . $item;
            }
        }
        return $conteudo;
    }

    public static function validateMultiArrayNull($array) {
        $return = true;
        foreach ($array as $key => $value) {
            if ($value) {
                $return = false;
                break;
            }
        }
        return $return;
    }

    public static function arrayToObject(array $array) {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[$key] = self::arrayToObject($value);
            }
        }
        return (object) $array;
    }

    public static function validateCreditCard($cartao, $cvc = false) {
        $cartao = preg_replace("/[^0-9]/", "", $cartao);
        if ($cvc)
            $cvc = preg_replace("/[^0-9]/", "", $cvc);

        $cartoes = [
            'Visa' => [
                'len' => [13, 16],
                'cvc' => 3
            ],
            'Master' => [
                'len' => [16],
                'cvc' => 3
            ],
            'Diners' => [
                'len' => [14, 16],
                'cvc' => 3
            ],
            'Elo' => [
                'len' => [16],
                'cvc' => 3
            ],
            'Amex' => [
                'len' => [15],
                'cvc' => 4
            ],
            'Discover' => [
                'len' => [16],
                'cvc' => 4
            ],
            'Aura' => [
                'len' => [16],
                'cvc' => 3
            ],
            'JCB' => [
                'len' => [16],
                'cvc' => 3
            ],
            'Hipercard' => [
                'len' => [13, 16, 19],
                'cvc' => 3
            ],
        ];

        switch ($cartao) {
            case (bool) preg_match('/^(636368|438935|504175|451416|636297)/', $cartao) :
                $bandeira = 'Elo';
                break;

            case (bool) preg_match('/^(606282)/', $cartao) :
                $bandeira = 'Hipercard';
                break;

            case (bool) preg_match('/^(5067|4576|4011)/', $cartao) :
                $bandeira = 'Elo';
                break;

            case (bool) preg_match('/^(3841)/', $cartao) :
                $bandeira = 'Hipercard';
                break;

            case (bool) preg_match('/^(6011)/', $cartao) :
                $bandeira = 'Discover';
                break;

            case (bool) preg_match('/^(622)/', $cartao) :
                $bandeira = 'Discover';
                break;

            case (bool) preg_match('/^(301|305)/', $cartao) :
                $bandeira = 'Diners';
                break;

            case (bool) preg_match('/^(34|37)/', $cartao) :
                $bandeira = 'Amex';
                break;

            case (bool) preg_match('/^(36,38)/', $cartao) :
                $bandeira = 'Diners';
                break;

            case (bool) preg_match('/^(64,65)/', $cartao) :
                $bandeira = 'Discover';
                break;

            case (bool) preg_match('/^(50)/', $cartao) :
                $bandeira = 'Aura';
                break;

            case (bool) preg_match('/^(35)/', $cartao) :
                $bandeira = 'JCB';
                break;

            case (bool) preg_match('/^(60)/', $cartao) :
                $bandeira = 'Hipercard';
                break;

            case (bool) preg_match('/^(4)/', $cartao) :
                $bandeira = 'Visa';
                break;

            case (bool) preg_match('/^(5)/', $cartao) :
                $bandeira = 'Master';
                break;
            default :
                $bandeira = 'Visa';
                break;
        }

        $dados_cartao = $cartoes[$bandeira];
        if (!is_array($dados_cartao))
            return [false, false, false];

        $valid = Setup::CreditCard($cartao);
        $valid_cvc = false;

        if (!in_array(strlen($cartao), $dados_cartao['len']))
            $valid = false;
        if ($cvc && strlen($cvc) <= $dados_cartao['cvc'] && strlen($cvc) != 0)
            $valid_cvc = true;
        return [$bandeira, $valid, $valid_cvc];
    }

    private static function CreditCard($ccnumber, $allowTest = false) {
        if ($allowTest == false && $ccnumber == '4111111111111111') {
            return false;
        }

        $ccnumber = preg_replace('/[^0-9]/', '', $ccnumber);

        $creditcard = array(
            'visa' => "/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/",
            'mastercard' => "/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/",
            'discover' => "/^6011-?\d{4}-?\d{4}-?\d{4}$/",
            'amex' => "/^3[4,7]\d{13}$/",
            'diners' => "/^3[0,6,8]\d{12}$/",
            'bankcard' => "/^5610-?\d{4}-?\d{4}-?\d{4}$/",
            'jcb' => "/^[3088|3096|3112|3158|3337|3528]\d{12}$/",
            'enroute' => "/^[2014|2149]\d{11}$/",
            'switch' => "/^[4903|4911|4936|5641|6333|6759|6334|6767]\d{12}$/"
        );

        $match = false;
        foreach ($creditcard as $cardtype => $pattern) {
            if (preg_match($pattern, $ccnumber) == 1) {
                $match = true;
                break;
            }
        }
        if (!$match) {
            return false;
        }
        return Setup::LuhnCheck($ccnumber);
    }

    private static function LuhnCheck($ccnum) {
        $checksum = 0;
        for ($i = (2 - (strlen($ccnum) % 2)); $i <= strlen($ccnum); $i += 2) {
            $checksum += (int) ($ccnum[$i - 1]);
        }
        for ($i = (strlen($ccnum) % 2) + 1; $i < strlen($ccnum); $i += 2) {
            $digit = (int) ($ccnum[$i - 1]) * 2;
            
            if ($digit < 10) {
                $checksum += $digit;
            } else {
                $checksum += ($digit - 9);
            }
        }
        if (($checksum % 10) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function validateCPF($value) {
        $valid = true;
        $cpf = preg_replace('/[^0-9]/', '', $value);
        for ($x = 0; $x < 10; $x ++) {
            if ($cpf == str_repeat($x, 11)) {
                $valid = false;
            }
        }
        if ($valid) {
            if (strlen($cpf) != 11) {
                $valid = false;
            } else {
                for ($t = 9; $t < 11; $t++) {
                    $d = 0;
                    for ($c = 0; $c < $t; $c++) {
                        $d += $cpf[$c] * (($t + 1) - $c);
                    }
                    $d = ((10 * $d) % 11) % 10;
                    if ($cpf[$c] != $d) {
                        $valid = false;
                        break;
                    }
                }
            }
        }
        return $valid;
    }

    public static function validGTIN($ean) {
        $return = true;
        try {
            if (Gtin::check($ean)->isValid()) {
                $return = true;
            } else {
                $return = false;
            }
        } catch (\Exception $e) {
            $return = false;
        }
        return $return;
    }

}
