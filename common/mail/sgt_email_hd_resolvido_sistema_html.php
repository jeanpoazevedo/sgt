<?php
use common\models\Sistema;
$url = Sistema::findOne(1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Help Desk - SGT</title>
    </head>
    <body>
        <tbody>
            <tr>
                <td align="center" valign="top" style="padding:20px 0 20px 0">
                    <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #e0e0e0">
                        <tbody>
                            <tr>
                                <td valign="top">
                                    <h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Olá, <?php
                                        if ($model->de_fk != $model_historio->de_fk){
                                            if ($model->para_fk != $model_historio->de_fk){
                                                echo $model->deFk->nome.' e '.$model->paraFk->nome.'</h1>';
                                            } else {
                                                echo $model->deFk->nome.'</h1>';
                                            }
                                        } else {
                                            echo $model->paraFk->nome.'</h1>';
                                        }
                                    ?>
                                    <p style="font-size:12px;line-height:16px;margin:0">
                                        O chamado: <a href="<?php echo $url->url."/helpdesk/atender/".$model->id; ?>"><?php echo $model->id; ?></a> recebeu alteração automática, por: Administrador do Sistema.
                                    </p>
                                    <p style="font-size:12px;line-height:16px;margin:0">
                                        O status foi alterado de: Resolvido para: Encerrado.
                                    </p>
                                    <p style="font-size:12px;line-height:16px;margin:0">
                                        O chamado foi encerrado automaticamente por falta de atividade.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 style="font-size:18px;font-weight:normal;margin:0">Faça seu <a href="<?php echo $url->url; ?>">login</a>.</h2>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </body>
</html>
