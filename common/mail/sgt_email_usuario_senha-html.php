<?php
use yii\helpers\Html;
use common\models\Sistema;
$url = Sistema::findOne(1);
?>
?>
<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0"><div class="adM">
    </div>
    <div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0"><div class="adM">
        </div>
        <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td align="center" valign="top" style="padding:20px 0 20px 0">
                        <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #e0e0e0">
                            <tbody>
                                <tr>
                                    <td valign="top">
                                        <h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Prezado(a) Usuário, <?php echo Html::encode($model->nome) ?>,</h1>
                                        <p style="font-size:12px;line-height:16px;margin:0 0 8px 0">Para trocar sua senha, favor acessar o seguinte link.</p>
                                        <p style="font-size:12px;line-height:16px;margin:0 0 8px 0"><a href="<?php echo $url->url."/site/new-password?token=".$model->token; ?>" style="color:#1e7ec8" target="_blank">Trocar Senha</a></p>
                                        <br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>