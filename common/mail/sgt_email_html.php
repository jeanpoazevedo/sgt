<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>NFe</title>
    </head>
    <body>
        <tbody>
            <tr>
                <td align="center" valign="top" style="padding:20px 0 20px 0">
                    <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #e0e0e0">
                        <tbody>
                            <tr>
                                <td valign="top">
                                    <h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Olá, <?php echo $model->unidaeFk->nome; ?></h1>
                                    <p style="font-size:12px;line-height:16px;margin:0">
                                        Obrigado por comprar na <?php echo $model->agenteEmitenteFk->nome; ?>.
                                        Em anexo segue o DANFe, XML e arquivo PDF.
                                    </p>
                                    <p style="font-size:12px;line-height:16px;margin:0">Esta é uma confirmação da sua compra. Agradecemos a confiança.</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 style="font-size:18px;font-weight:normal;margin:0">NFe <?php echo $model->numero_nota_fiscal; ?> <small>(feito em <?php echo $model->data_nota; ?>)</small></h2>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
                                    <center>
                                        <p style="font-size:12px;margin:0">Obrigado, <strong><?php echo $model->agenteEmitenteFk->nome; ?></strong></p>
                                    </center>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </body>
</html>
