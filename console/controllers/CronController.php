<?php
    
    namespace console\controllers;
    
    use Yii;
    use yii\console\Controller;
    use DateTime;
    use common\models\HdCadastro;
    use common\models\HdHistorico;
    use common\models\HdProduto;
    use common\models\Usuario;
    use common\models\UsuarioPerfil;
    use common\components\SendMail;
    
    class CronController extends Controller {
        
        public function actionCron() {
            
            $data = date('Y-m-d H:i:s');
            $data_calc = new DateTime($data);
            $model_historico = new HdHistorico();
            $model = HdCadastro::find()->Where(['=', 'status', 5])->all();
            
            foreach ($model as $dados) {
                
                $resolvido = DateTime::createFromFormat('d/m/Y H:i:s', $dados->data_encerrado)->format('Y-m-d h:i:s');
                $resolvido = new DateTime($resolvido);
                $calc_tempo = $data_calc->diff($resolvido);
                $hora_atual = substr($data, 11, 2);
                $hora_resolvido = substr($dados->data_encerrado, 11, 2);
                $dias = $calc_tempo->d - 1;
                if($hora_atual > $hora_resolvido){
                    if($calc_tempo->d > 1){
                        $tempo = (($calc_tempo->d - 1) * 24) + 12 + $calc_tempo->h;
                    } else{
                        if ($calc_tempo->d == 1) {
                            $tempo =  12 + $calc_tempo->h;
                        } else {
                            $tempo = $calc_tempo->h;
                        }
                    }
                } else {
                    if($calc_tempo->d > 1){
                        $tempo = (($calc_tempo->d - 1) * 24) + 12 + $calc_tempo->h;
                    } else {
                        $tempo = 12 + $calc_tempo->h;
                    }
                }
                $tempo = (((($tempo * 60) + $calc_tempo->i) * 60) +$calc_tempo->s) / 3600;
                
                if ($tempo >= 24) {
                    
                    $model_historico->chamado_fk = $dados->id;
                    $model_historico->de_fk = 1;
                    $model_historico->data = date('d/m/Y H:i:s');
                    $model_historico->acao = 'Status do chamado foram alterados altomáticamente!';
                    $model_historico->alteracao = 'Resolvido > Encerrado';
                    $model_historico->save();
                    
                    $emails = [
                        $dados->deFk->email,
                        $dados->paraFk->email
                    ];
                    
                    SendMail::submithdcron($emails, $dados, $model_historico, 'sgt_email_hd_resolvido_sistema_html');
                    $dados->status = 4;
                    $dados->save();
                    
                }
            }
        }
    }
