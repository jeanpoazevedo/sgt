yii.allowAction = function ($e) {
    var message = $e.data('confirm');
    return message === undefined || yii.confirm(message, $e);
};
yii.confirm = function (message, ok, cancel) {
    bootbox.confirm(message, function (confirmed) {
        if (confirmed) {
            !ok || ok();
        } else {
            !cancel || cancel();
        }
    });
    return false;
}

$("a.loading").click(function(event) {
    $('body').loading({
        stoppable: false,
        message: 'Aguarde, carregando...',
        theme: 'dark'
    });
    event.stopPropagation();
    return true;    
});

$('form').on('beforeSubmit', function(event) {
    if ($(this).data('submitting')) {
        event.preventDefault();
        return false;
    }
    $(this).data('submitting', true);
    $('body').loading({
        stoppable: false,
        message: 'Aguarde, carregando...',
        theme: 'dark'
    });
    return true;
});

$('index').ready(function($) {
    $('.counter').counterUp({
        delay: 100,
        time: 1200
    });

    $(".knob").knob();

});