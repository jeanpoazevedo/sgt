<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tb_perfil".
 *
 * @property integer $id
 * @property string $nome
 * @property string $descricao
 *
 */
class Perfil extends \yii\db\ActiveRecord {

    public $pesquisa;
    public $menu;

    public static function tableName() {
        return 'tb_perfil';
    }

    public function rules() {
        return [
            [['nome', 'descricao'], 'required'],
            [['nome', 'descricao'], 'string', 'max' => 1024],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'nome' => Yii::t('app', 'Nome'),
            'descricao' => Yii::t('app', 'Descrição'),
        ];
    }

    public function search($params) {
        $query = Perfil::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'nome',
                'descricao',
            ]
        ]);
        
        $this->load($params);

        if (isset($params['Perfil']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper($params['Perfil']['pesquisa'])])
                    ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao))', strtoupper($params['Perfil']['pesquisa'])]);
        }

        return $dataProvider;
    }

}
