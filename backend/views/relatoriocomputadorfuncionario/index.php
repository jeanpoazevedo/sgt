<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\Tipo;
use common\models\Setor;
use common\models\Marca;
use common\models\Modelo;
use common\models\VwRelatorioComputadorFuncionario;

$this->title = 'Relatório  Computador para Funcionario';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Relatório de  Computador para Funcionario</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', 'Limpa filtros'), ['clean'], ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php
                                    if($model_restricao_export){
                                        echo Html::a(Yii::t('app', 'Exportar'), ['export'], ['class' => 'btn btn-success']);
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Setor do Computador</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Setor::find()->orderBy(['setor' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->setor;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'setor_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->setor_temp_fk,
                                        'options' => [
                                            'id' => 'setor_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-setor", {setor_temp_fk: $("#setor_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Tipo</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Tipo::find()->orWhere(['computador' => '1'])->orderBy(['tipo' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->tipo;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'tipo_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->tipo_temp_fk,
                                        'options' => [
                                            'id' => 'tipo_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-tipo", {tipo_temp_fk: $("#tipo_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Marca</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Marca::find()->orWhere(['computador' => '1'])->orderBy(['marca' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->marca;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'marca_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->marca_temp_fk,
                                        'options' => [
                                            'id' => 'marca_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-marca", {marca_temp_fk: $("#marca_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Modelo</label>
                                <?php
                                    if(Yii::$app->user->identity->marca_temp_fk){
                                        $dataModel = ArrayHelper::map(Modelo::find()->andWhere(['tipo' => '1'])->andWhere(['marca_fk' => Yii::$app->user->identity->marca_temp_fk])->orderBy(['modelo' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->modelo;
                                        });
                                    } else {
                                        $dataModel = ArrayHelper::map(Modelo::find()->orWhere(['tipo' => '1'])->orderBy(['modelo' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->modelo;
                                        });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'modelo_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->modelo_temp_fk,
                                        'options' => [
                                            'id' => 'modelo_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-modelo", {modelo_temp_fk: $("#modelo_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Função do Funcionario</label>
                                <?php
                                    if (Yii::$app->user->identity->unidade_temp_fk != '1') {
                                        $dataModel = ArrayHelper::map(VwRelatorioComputadorFuncionario::find()->distinct()->andWhere(['<>', 'funcao', ''])->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['funcao' => SORT_ASC])->all(), 'funcao', function($model) {
                                                return $model->funcao;
                                            });
                                    } else {
                                        $dataModel = ArrayHelper::map(VwRelatorioComputadorFuncionario::find()->distinct()->andWhere(['<>', 'funcao', ''])->orderBy(['funcao' => SORT_ASC])->all(), 'funcao', function($model) {
                                                return $model->funcao;
                                            });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'funcao_temp',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->funcao_temp,
                                        'options' => [
                                            'id' => 'funcao_temp',
                                            'onchange' => '
                                                $.post("/relatoriocomputadorfuncionario/set-session-funcao", {funcao_temp: $("#funcao_temp").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'marca',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            [
                                'attribute' => 'unidade_fk',
                                'value' => function ($data) {
                                    return ($data->unidadeFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                            : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidade);
                                },
                            ],
                            'codigo_computador',
                            'computador_setor',
                            'tipo',
                            'marca',
                            'modelo',
                            [
                                'attribute' => 'funcionario_unidade_fk',
                                'value' => function ($data) {
                                    return ($data->nome ?
                                        ($data->funcionariounidadeFk->unidade_pai_fk ?
                                            ($data->funcionariounidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade
                                                            : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                                        : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                                    : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                                : $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                            : $data->funcionariounidadeFk->unidade)
                                        : "" );
                                },
                            ],
                            [
                                'attribute' => 'funcionario_setor',
                                'value' => function ($data) {
                                    return ($data->funcionario_setor ? $data->funcionario_setor : "");
                                },
                            ],
                            [
                                'attribute' => 'funcao',
                                'value' => function ($data) {
                                    return ($data->funcao ? $data->funcao : "");
                                },
                            ],
                            [
                                'attribute' => 'nome',
                                'value' => function ($data) {
                                    return ($data->nome ? $data->nome : "");
                                },
                            ],
                            [
                                'attribute' => 'Pontos',
                                'value' => function ($data) {

                                    $banda_memoria = $data->computadorFk->processadorFk->banda_memoria;
                                    $frequencia = $data->computadorFk->processadorFk->frequencia;

                                    if (!is_numeric($banda_memoria)) {
                                        $banda_memoria = (float) $banda_memoria;
                                    }

                                    if (!is_numeric($frequencia)) {
                                        $frequencia = (float) $frequencia;
                                    }
                                    //Coeficientes do calculo
                                    $BandaProcessador = 0.35;
                                    $FrequenciaProcessador = 0.2;
                                    $CoresProcessador = 0.15;
                                    $CacheProcessador = 0.1;
                                    $ClokRAM = 0.2;
                                    $MemoriaRAM = 0.2;
                                    $VelocidadeLeituraHD = 0.15;
                                    $VelocidadeGravacaoHD = 0.05;

                                    $valor = $data->computadorFk->ram;
                                    switch ($data->computadorFk->ramMedidaFk->simbolo) {
                                        case 'KB':
                                            $ram = $valor * 1024;
                                            break;
                                        case 'MB':
                                            $ram = $valor * 1024 * 1024;
                                            break;
                                        case 'GB':
                                            $ram = $valor * 1024 * 1024 * 1024;
                                            break;
                                        case 'TB':
                                            $ram = $valor * 1024 * 1024 * 1024 * 1024;
                                            break;
                                        case 'PB':
                                            $ram = $valor * 1024 * 1024 * 1024 * 1024 * 1024;
                                            break;
                                        case 'EB':
                                            $ram = $valor * 1024 * 1024 * 1024 * 1024 * 1024 * 1024;
                                            break;
                                        case 'ZB':
                                            $ram = $valor * 1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024;
                                            break;
                                        case 'YB':
                                            $ram = $valor * 1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024;
                                            break;
                                        default:
                                            $ram = $valor;
                                            break;
                                    }

                                    // Calculando o score
                                    $score = ($banda_memoria * $BandaProcessador) +
                                            (
                                                (
                                                    ($frequencia * $FrequenciaProcessador) +
                                                    ($data->computadorFk->processadorFk->total_core * $CoresProcessador) +
                                                    ($data->computadorFk->processadorFk->cache * $CacheProcessador)
                                                ) * $data->computadorFk->numero_processador
                                            )+
                                            ($data->computadorFk->ramClokFk->clok * $ClokRAM) +
                                            (($ram * $MemoriaRAM) / 1000000000) +
                                            ($data->computadorFk->leitura_hd * $VelocidadeLeituraHD) +
                                            ($data->computadorFk->escrita_hd * $VelocidadeGravacaoHD);

                                    $score_formatado = number_format($score, 2, ',', '.');

                                    return ($score_formatado);
                                },
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>