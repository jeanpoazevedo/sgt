<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\Mapaipv4;
use common\models\Computador;
use common\models\Equipamento;
use common\models\Monitor;

$this->title = 'Controle de cadastro de Mapa de IP';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de Mapa de IP</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Nova Rede'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', 'Limpa filtros'), ['clean'], ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Rede</label>
                                <?php
                                    if (Yii::$app->user->identity->unidade_temp_fk != '1') {
                                        $dataModel = ArrayHelper::map(MapaIpV4::find()->distinct()->Where(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])->orderBy(['rede' => SORT_ASC])->all(), 'rede', function($model) {
                                                return $model->rede;
                                            });
                                    } else {
                                        $dataModel = ArrayHelper::map(MapaIpV4::find()->distinct()->orderBy(['rede' => SORT_ASC])->all(), 'rede', function($model) {
                                                return $model->rede;
                                            });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'rede_temp',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->rede_temp,
                                        'options' => [
                                            'id' => 'rede_temp',
                                            'onchange' => '
                                                $.post("/mapaipv4/set-session-rede", {rede_temp: $("#rede_temp").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Grupo</label>
                                <?php
                                    if (Yii::$app->user->identity->unidade_temp_fk != '1') {
                                        $dataModel = ArrayHelper::map(MapaIpV4::find()->distinct()->andWhere(['<>', 'grupo', ''])->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['grupo' => SORT_ASC])->all(), 'grupo', function($model) {
                                                return $model->grupo;
                                            });
                                    } else {
                                        $dataModel = ArrayHelper::map(MapaIpV4::find()->distinct()->andWhere(['<>', 'grupo', ''])->orderBy(['grupo' => SORT_ASC])->all(), 'grupo', function($model) {
                                                return $model->grupo;
                                            });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'grupo_temp',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->grupo_temp,
                                        'options' => [
                                            'id' => 'grupo_temp',
                                            'onchange' => '
                                                $.post("/mapaipv4/set-session-grupo", {grupo_temp: $("#grupo_temp").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'ip',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            [
                                'attribute' => 'unidade_fk',
                                'value' => function ($data) {
                                    return ($data->unidadeFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                            : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidade);
                                },
                            ],
                            'rede',
                            [
                                'attribute' => 'grupo',
                                'value' => function ($data) {
                                    return ($data->grupo ? $data->grupo : "");
                                },
                            ],
                            [
                                'attribute' => 'ip',
                                'value' => function ($data) {
                                    return $data->ip . ' / ' . $data->cidr;
                                },
                            ],
                            [
                                'attribute' => 'mac',
                                'value' => function ($data) {
                                    return ($data->mac ? $data->mac : "");
                                },
                            ],
                            [
                                'attribute' => 'descricao',
                                'value' => function ($data) {
                                    return ($data->descricao ? $data->descricao : "");
                                },
                            ],
                            [
                                'attribute' => 'Dispositivo',
                                'value' => function ($data) {
                                    $retorno = "";
                                    if ($data->mac == true){
                                        $mac = explode(':', $data->mac);
                                        $mac = strtoupper($mac[0].$mac[1].$mac[2].$mac[3].$mac[4].$mac[5]);
                                        if ($computador = Computador::find()->where(['mac_c' => $mac])->one()){
                                            $retorno = $computador->codigo_computador . " - Cabo";
                                        } elseif ($computador = Computador::find()->where(['mac_w' => $mac])->one()){
                                            $retorno = $computador->codigo_computador . " - Wi-Fi";
                                        } elseif ($moniotor = Monitor::find()->where(['mac' => $mac])->one()) {
                                            $retorno = $moniotor->codigo_monitor;
                                        } elseif ($equipamento = Equipamento::find()->where(['mac' => $mac])->one()) {
                                            $retorno = $equipamento->codigo_equipamento;
                                        }
                                    }
                                    return ($data->mac ? $retorno : "");
                                },
                            ],
                            ['class' => 'common\components\SYSRedeActionColumn',
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>