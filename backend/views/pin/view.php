<?php

use yii\helpers\Html;

$this->title = 'VoIP - PIN';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('unidade_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->unidadeFk->unidade; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('funcionario_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->funcionarioFk->nome; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('pin'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->pin; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('particular'); ?></label>
                            <input type="checkbox" class="form-control" disabled="" <?php echo ($model->particular != 0 ? 'CHECKED' : '')?>>
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlPinSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>