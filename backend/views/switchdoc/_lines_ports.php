<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Vlan;

?>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-1">
                <label class="control-label">Porta</label>
            </div>
            <div class="col-sm-1">
                <label class="control-label">Status</label>
            </div>
            <div class="col-sm-2">
                <label class="control-label">Type</label>
            </div>
            <div class="col-sm-2">
                <label class="control-label">VLAN</label>
            </div>
            <div class="col-sm-3">
                <label class="control-label">Tagged</label>
            </div>
            <div class="col-sm-3">
                <label class="control-label">Descrição</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-1">
                <label class="control-label">Porta</label>
            </div>
            <div class="col-sm-1">
                <label class="control-label">Status</label>
            </div>
            <div class="col-sm-2">
                <label class="control-label">Type</label>
            </div>
            <div class="col-sm-2">
                <label class="control-label">VLAN</label>
            </div>
            <div class="col-sm-3">
                <label class="control-label">Tagged</label>
            </div>
            <div class="col-sm-3">
                <label class="control-label">Descrição</label>
            </div>
        </div>
    </div>
</div>

<?php
for ($i = 0; $i < $request['quantidade']; $i++) {
    $model->numero_porta = str_pad(($i + 1), 2, '0', STR_PAD_LEFT);
?>
    <div class="row">
        <?php
            if ($i < $request['quantidade']) {
        ?>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-1">
                    <?php echo $model->numero_porta; ?>
                </div>
                <div class="col-sm-1">
                    <?php echo Html::checkbox('status_' . $i, true, ['data-plugin' => "switchery"]); ?>
                </div>
                <div class="col-sm-2">
                    <?php echo Html::dropDownList('type_' . $i, 'type', $model->array_type, ['class' => 'form-control']); ?>
                </div>
                <div class="col-sm-2">
                    <?php
                    echo Html::dropDownList('vlan_' . $i, 'vlan', ArrayHelper::map(Vlan::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['id_vlan' => SORT_ASC])->all(), 'id', function($model) {
                                return $model->id_vlan . ' - ' . $model->vlan;
                            }), ['class' => 'form-control']);
                    ?>
                </div>
                <div class="col-sm-3">
                    <?php echo Html::input('text', 'tagged_' . $i, $model->tagged, ['class' => 'form-control']); ?>
                </div>
                <div class="col-sm-3">
                    <?php echo Html::input('text', 'descricao_' . $i, $model->descricao, ['class' => 'form-control']); ?>
                </div>
            </div>
        </div>
        <?php
            }
            $i++;
            $model->numero_porta = str_pad(($i + 1), 2, '0', STR_PAD_LEFT);
            if ($i < $request['quantidade']) {
        ?>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-1">
                    <?php echo $model->numero_porta; ?>
                </div>
                <div class="col-sm-1">
                    <?php echo Html::checkbox('status_' . $i, true, ['data-plugin' => "switchery"]); ?>
                </div>
                <div class="col-sm-2">
                    <?php echo Html::dropDownList('type_' . $i, 'type', $model->array_type, ['class' => 'form-control']); ?>
                </div>
                <div class="col-sm-2">
                    <?php
                    echo Html::dropDownList('vlan_' . $i, 'vlan', ArrayHelper::map(Vlan::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['id_vlan' => SORT_ASC])->all(), 'id', function($model) {
                                return $model->id_vlan . ' - ' . $model->vlan;
                            }), ['class' => 'form-control']);
                    ?>
                </div>
                <div class="col-sm-3">
                    <?php echo Html::input('text', 'tagged_' . $i, $model->tagged, ['class' => 'form-control']); ?>
                </div>
                <div class="col-sm-3">
                    <?php echo Html::input('text', 'descricao_' . $i, $model->descricao, ['class' => 'form-control']); ?>
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
<?php
    }
?>