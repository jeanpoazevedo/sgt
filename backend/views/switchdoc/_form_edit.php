<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Equipamento;
use common\models\Local;
use common\models\Vlan;

$this->title = 'Switch';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($model, 'codigo_switch_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Equipamento::find()->andWhere(['tipo_fk' => '7'])->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->all(), 'id', function ($model) {
                                return $model->codigo_equipamento;
                            }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione uma Equipamento'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($model, 'local_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Local::find()->all(), 'id', function ($model) {
                                return $model->local;
                            }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione uma Local'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'id_switch')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label
                                class="control-label"><?php echo $model->getAttributeLabel('numero_portas'); ?></label>
                            <input type="text" class="form-control" disabled="" id="switchdoc-numero_portas"
                                name="Switchdoc[numero_portas]" value="<?php echo $model->numero_portas; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'descricao_switch')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-pills m-b-30 pull-right">
                        <li class="active">
                            <a href="#cli" data-toggle="tab" aria-expanded="true">CLI</a>
                        </li>
                        <li class="">
                            <a href="#html" data-toggle="tab" aria-expanded="false">HTML</a>
                        </li>
                        <li class="">
                            <a href="#arquivo" data-toggle="tab" aria-expanded="false">Arquivos</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content br-n pn">
                    <div id="cli" class="tab-pane active">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php echo $form->field($model, 'configuracao')->textArea(['class' => 'form-control', 'rows' => '30', 'maxlength' => true]); ?>
                            </div>
                        </div>
                    </div>
                    <div id="arquivo" class="tab-pane">
                        <div class="row">
                            <div class="col-sm-9">
                                <h3 class="m-t-0 header-title">Arquivos</h3>
                            </div>
                            <div class="col-sm-3">
                                <div class="m-b-30">
                                    <?php echo Html::a('<i class="ion-plus-round"></i>Adicionar Arquivo', ['arquivo', 'id' => $model->id], ['class' => 'btn btn-success loading']); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table m-0">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Descrição Arquivo</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_arquivo">
                                        <?php
                                        foreach ($array_item_arquivo as $key => $value) {
                                            echo $value;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="html" class="tab-pane">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <label class="control-label">Porta</label>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="control-label">Status</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label">Type</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label">VLAN</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label">Tagged</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label">Descrição</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <label class="control-label">Porta</label>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="control-label">Status</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label">Type</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="control-label">VLAN</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label">Tagged</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label">Descrição</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $xml = simplexml_load_string($model->xml_portas);
                        $i = 0;
                        $fim = 0;

                        foreach ($xml->port as $port) {

                            if ($i % 2 == 0) {
                                echo '<div class="row">';
                            }
                            ?>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <?php echo $port->porta; ?>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" class="form-control" name="status_<?php echo $i; ?>" <?php echo ($port->status != 0 ? 'CHECKED' : '') ?>>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo Html::dropDownList('type_' . $i, (string) $port->type, (array) $model->array_type, ['class' => 'form-control']); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php
                                        echo Html::dropDownList('vlan_' . $i, (string) $port->vlan, ArrayHelper::map(Vlan::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['id_vlan' => SORT_ASC])->all(), 'id', function ($model) {
                                            return $model->id_vlan . ' - ' . $model->vlan;
                                        }), ['class' => 'form-control']);
                                        ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" name="tagged_<?php echo $i; ?>" class="form-control"
                                            value="<?php echo $port->tagged; ?>">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" name="descricao_<?php echo $i; ?>" class="form-control"
                                            value="<?php echo $port->description; ?>">
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($i % 2 == 0) {
                                $fim = 1;
                            } else {
                                echo '</div>';
                                $fim = 0;
                            }
                            $i++;
                            ?>
                            <?php
                        }
                        if ($fim == 1) {
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlSwitchdocSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs(" 

        $('#switchdoc-numero_portas').blur(function() {
            if ($('#switchdoc-numero_portas').val()) {
                $.post('/switchdoc/get-ports', {quantidade: $('#switchdoc-numero_portas').val()}, function (data) {
                    $('#row_lines').html(data);
                });
            }
        });

       ",
    View::POS_END,
    'functions-switch-xml'
);
?>