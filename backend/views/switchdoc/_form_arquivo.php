<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\select2\Select2;
use common\models\Switchdoc;
use common\models\SwitchArquivo;

$this->title = 'Switch Arquivo';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('codigo_switch_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->codigoSwitchFk->codigo_equipamento; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <?php echo $form->field($model_arquivo, 'descricao_arquivo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model_arquivo, 'url')->widget(FileInput::className(), [
                                'options' => [
                                    'multiple' => false,
                                ],
                                'language' => 'pt',
                                'pluginOptions' => [
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => 'Procurar',
                                    'initialCaption' => "Selecione um arquivo",
                                ]
                            ]);
                        ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', ['update', 'id' => $model->id], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']); ?>
                    <?php echo Html::submitButton($model_arquivo->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>