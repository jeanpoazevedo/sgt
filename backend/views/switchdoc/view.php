<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Equipamento;
use common\models\Local;
use common\models\Vlan;


$this->title = 'Switch';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('codigo_switch_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->codigoSwitchFk->codigo_equipamento; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('local_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->localFk->local; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('id_switch'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->id_switch; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('descricao_switch'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->descricao_switch; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('numero_portas'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_portas; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-1">
                                <label class="control-label">Porta</label>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Type</label>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">VLAN</label>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Descrição</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-1">
                                <label class="control-label">Porta</label>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Type</label>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">VLAN</label>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Descrição</label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    $xml = simplexml_load_string($model->xml_portas);
                    $i = 0;
                    $fim = 0;
                    
                    foreach($xml->port as $port){
                    
                    if($i % 2 == 0){
                        echo '<div class="row">';
                    }
                        
                ?>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-1">
                            <?php echo $port->porta; ?>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" class="form-control" disabled="" <?php echo ($port->status != 0 ? 'CHECKED' : '')?>>
                        </div>
                        <div class="col-sm-2">
                            <?php echo Html::dropDownList('type_' . $i, (string)$port->type, (array)$model->array_type, ['class' => 'form-control', 'disabled' => '']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                echo Html::dropDownList('vlan_' . $i, (string)$port->vlan, ArrayHelper::map(Vlan::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['id_vlan' => SORT_ASC])->all(), 'id', function($model) {
                                    return $model->id_vlan . ' - ' . $model->vlan;
                                }), ['class' => 'form-control', 'disabled' => '']);
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" disabled="" value="<?php echo $port->description; ?>">
                        </div>
                    </div>
                </div>
                <?php
                    if($i % 2 == 0){
                        $fim = 1;
                    } else {
                       echo '</div>';
                       $fim = 0;
                    }
                    $i ++;
                ?>
                <?php
                    }
                    if($fim == 1 ){
                        echo '</div>';
                    }
                ?>
                
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlSwitchdocSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>