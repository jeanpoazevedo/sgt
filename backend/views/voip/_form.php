<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use common\models\UsuarioUnidade;
use common\models\Setor;
use common\models\Voip;
use common\models\VoipCaptura;
use common\models\Equipamento;
use common\models\Funcionario;

$this->title = 'VoIP';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function($model) {
                                    return ($model->unidadeFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidade);
                                    }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'setor_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Setor::find()->all(), 'id', function($model) {
                                            return $model->setor;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Funcionario'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'voip')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'senha')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'telefone')->widget(MaskedInput::className(), [
                                'mask' => '(99) 9999-9999',
                                'clientOptions' => [
                                    'removeMaskOnSubmit' => true,
                                ]
                            ])->textInput(['class' => 'form-control', 'maxlength' => true]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'tempo_toque')->widget(Select2::className(), [
                                'data' => $model->array_tempo_toque,
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um Tempo'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'transferencia_automatica_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Voip::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->all(), 'id', function($model) {
                                            return $model->voip;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Voip'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'toque_simultaneo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            if (Yii::$app->user->identity->unidade_temp_fk != 1) {
                                echo $form->field($model, 'grupo_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Voipcaptura::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione um grupo'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else {
                                echo $form->field($model, 'grupo_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Voipcaptura::find()->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione um grupo'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            if (Yii::$app->user->identity->unidade_temp_fk != 1) {
                                echo $form->field($model, 'equipamento_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Equipamento::find()->andwhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->andWhere(['tipo_fk' => '8'])->all(), 'id', function($model) {
                                                return $model->codigo_equipamento;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Equipamento'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else {
                                echo $form->field($model, 'equipamento_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Equipamento::find()->Where(['tipo_fk' => '8'])->all(), 'id', function($model) {
                                                return $model->codigo_equipamento;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Equipamento'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            if (Yii::$app->user->identity->unidade_temp_fk != 1) {
                                echo $form->field($model, 'funcionario_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Funcionario::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->andWhere(['so_contato' => 0])->all(), 'id', function($model) {
                                        return $model->nome;
                                    }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione um Funcionário'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else {
                                echo $form->field($model, 'funcionario_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Funcionario::find()->where(['IN', 'unidade_fk', $usuariounidade])->andWhere(['so_contato' => 0])->all(), 'id', function($model) {
                                        return $model->nome;
                                    }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione um Funcionário'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'informacao')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlVoipSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>