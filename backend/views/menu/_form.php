<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Sistema;
use common\models\Menu;

$this->title = 'Menu';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('sistema_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->sistemaFk->id . ' - ' . $model->sistemaFk->nome; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-6">
                        <?php
                        echo $form->field($model, 'menu_pai_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Menu::find()->all(), 'id', function($model) {
                                        return $model->titulo;
                                    }),
                            'options' => [
                                'prompt' => 'Seleciona um menu',
                            ],
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ])
                        ?>
                    </div>            
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'titulo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>            
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'nivel')->widget(Select2::className(), [
                                'data' => $model->array_nivel,
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um nivel'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('controller'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->controller; ?>">
                        </div>                                        
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('icon'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->icon; ?>">
                        </div>                                        
                    </div>            
                </div>            

                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlMenuSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']); ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
