<?php

use yii\helpers\Html;

$this->title = 'Menu - Consulta';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('sistema_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->sistemaFk->id . ' - ' . $model->sistemaFk->nome; ?>">
                        </div>                                        
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('menu_pai_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo ($model->menu_pai_fk ? $model->sistemaFk->nome . ' - ' . $model->menuPaiFk->titulo : ''); ?>">
                        </div>                                        
                    </div>            
                </div>            

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('titulo'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->titulo; ?>">
                        </div>                                        
                    </div>            
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('nivel'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->nivel; ?>">
                        </div>                                        
                    </div>            
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('controller'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->controller; ?>">
                        </div>                                        
                    </div>            
                </div>            
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('icon'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->icon; ?>">
                        </div>                                        
                    </div>            
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlMenuSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>
