<?php

use common\models\Sistema;
use common\models\VwMenu;

$model = Sistema::find()->orderBy('id')->all();
foreach ($model as $key => $value) {
    $menu = VwMenu::find()->where(['usuario_fk' => Yii::$app->user->getId(), 'sistema_fk' => $value->id])->one();
    if ($menu) {
        echo '<li class="dropdown hidden-xs">
                <a href="' . $value->url . '/site/index/' . $value->id . '" class="waves-effect waves-light loading">'
                . $value->nome .
               '</a>
            </li>';
    }
}
