<?php

use common\widgets\Menu;
use common\models\VwMenu;
use common\models\Sistema;

$sistema_fk = (Yii::$app->session->get("sistema_fk") ? Yii::$app->session->get("sistema_fk") : 1);
$sistema = Sistema::findOne(['id' => $sistema_fk]);
$menu = VwMenu::find()->where(['usuario_fk' => Yii::$app->user->getId(), 'sistema_fk' => $sistema_fk])->orderBy("id")->all();
$itens = [
    [
        'label' => $sistema->nome,
        'options' => [
            'class' => 'text-muted menu-title',
        ],
    ],
    [
        'label' => Yii::t('app', 'Home'),
        'url' => Yii::$app->homeUrl,
        'icon' => 'fa fa-home',
        'active' => Yii::$app->request->url === Yii::$app->homeUrl
    ],
];

foreach ($menu as $key => $value) {
    if ($value->nivel == 0) {
        $sub_itens = [];
        foreach ($menu as $key_sub => $value_sub) {
            if ($value_sub->nivel == 1 && $value_sub->menu_pai_fk == $value->id) {
                $sub_itens[] = [
                    'label' => $value_sub->titulo,
                    'url' => ['/' . $value_sub->controller . '/index'],
                    'icon' => $value_sub->icon,
                    'active' => (strpos(Yii::$app->request->url, '/' . $value_sub->controller) !== false ? true : false)
                ];
            }
        }
        $itens[] = [
            'label' => $value->titulo,
            'url' => ['#'],
            'icon' => $value->icon,
            'options' => [
                'class' => 'has_sub',
            ],
            'items' => $sub_itens,
        ];
    }
}
echo Menu::widget(['items' => $itens]);
?>
