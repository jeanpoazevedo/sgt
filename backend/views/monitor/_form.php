<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\widgets\MaskedInput;
use kartik\money\MaskMoney;
use common\models\UsuarioUnidade;
use common\models\Setor;
use common\models\Tipo;
use common\models\VwMarcaModelo;
use common\models\Funcionario;

$this->title = 'Monitor';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading">
                    <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <ul class="nav nav-pills m-b-30 pull-right">
                            <li class="active">
                                <a href="#geral" data-toggle="tab" aria-expanded="true">Geral</a>
                            </li>
                            <li class="">
                                <a href="#anexo" data-toggle="tab" aria-expanded="true">Anexo</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content br-n pn">
                        <div id="geral" class="tab-pane active">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label
                                            class="control-label"><?php echo $model->getAttributeLabel('codigo_monitor'); ?></label>
                                        <input type="text" class="form-control" disabled=""
                                            value="<?php echo $model->codigo_monitor; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                            'data' => ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function ($model) {
                                                return ($model->unidadeFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                    $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidade);
                                            }),
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => [
                                                'prompt' => 'Selecione uma Unidade'
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => false
                                            ],
                                        ]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('unidade_fk') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' .
                                            ($model->unidadeFk->unidade_pai_fk ?
                                                ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade
                                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade)
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                : $model->unidadeFk->unidade)
                                            . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    echo $form->field($model, 'setor_fk')->widget(Select2::className(), [
                                        'data' => ArrayHelper::map(Setor::find()->all(), 'id', function ($model) {
                                            return $model->setor;
                                        }),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'options' => [
                                            'prompt' => 'Selecione um Setor'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => false
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'tipo_fk')->widget(Select2::className(), [
                                            'data' => ArrayHelper::map(Tipo::find()->where(['monitor' => '1'])->orderBy(['tipo' => SORT_ASC])->all(), 'id', function ($model) {
                                                return $model->tipo;
                                            }),
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => [
                                                'prompt' => 'Selecione um Tipo'
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => false
                                            ],
                                        ]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('tipo_fk') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' . $model->tipoFk->tipo . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php
                                    echo $form->field($model, 'modelo_fk')->widget(Select2::className(), [
                                        'data' => ArrayHelper::map(VwMarcaModelo::find()->where(['tipo' => '2'])->orderBy(['marca' => SORT_ASC])->orderBy(['modelo' => SORT_ASC])->all(), 'id', function ($model) {
                                            return $model->marca . ' / ' . $model->modelo;
                                        }),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'options' => [
                                            'prompt' => 'Selecione Marca e Modelo'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => false
                                        ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'numero_serie')->textInput(['class' => 'form-control', 'maxlength' => true]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('numero_serie') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' . $model->numero_serie . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'numero_patrimonio')->textInput(['class' => 'form-control', 'maxlength' => true]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('numero_patrimonio') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' . $model->numero_patrimonio . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label
                                            class="control-label"><?php echo $model->getAttributeLabel('data_compra'); ?></label>
                                        <input type="text" class="form-control" disabled=""
                                            value="<?php echo $model->data_compra; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'garantia')
                                            ->widget(MaskedInput::className(), ['mask' => '99/99/9999',])
                                            ->widget(DatePicker::classname(), [
                                                'language' => 'pt-BR',
                                                'removeButton' => false,
                                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'orientation' => 'bottom left',
                                                    'format' => 'dd/mm/yyyy',
                                                ]
                                            ]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('garantia') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' . $model->garantia . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    echo $form->field($model, 'mac')->widget(MaskedInput::className(), [
                                        'mask' => '**:**:**:**:**:**',
                                        'clientOptions' => [
                                            'removeMaskOnSubmit' => true,
                                        ]
                                    ])->textInput(['class' => 'form-control', 'maxlength' => true, 'autofocus' => true]);
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php echo $form->field($model, 'netbios')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'loja')->textInput(['class' => 'form-control', 'maxlength' => true]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('loja') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' . $model->loja . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'numero_nota')->textInput(['class' => 'form-control', 'maxlength' => true]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('numero_nota') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' . $model->numero_nota . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($model_restricao) {
                                        echo $form->field($model, 'valor', [
                                            'template' => '{label}<div class="input-group"><span class="input-group-addon">R$</span>{input}</div>',
                                        ])->textInput([
                                            'class' => 'form-control',
                                            'style' => 'text-align: right;',
                                            'maxlength' => true,
                                            'id' => 'valor-input',
                                            'onkeyup' => 'formatCurrency(this)',
                                        ]);
                                    } else {
                                        echo '<div class="form-group">'
                                            . '<label class="control-label">' . $model->getAttributeLabel('valor') . '</label>'
                                            . '<input type="text" class="form-control" disabled="" value="' . $model->valor . '">'
                                            . '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php
                                        if ($model_restricao) {
                                            echo $form->field($model, 'status', ['template' => '{label}<div class="input-group">{input}</div>',])
                                                ->checkbox(['data-plugin' => "switchery"]);
                                        } else {
                                            echo $form->field($model, 'status', ['template' => '{label}<div class="input-group">{input}</div>',])
                                                ->checkbox(['data-plugin' => "switchery", 'disabled' => ""]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php echo $form->field($model, 'descricao_monitor')->textArea(['class' => 'form-control', 'rows' => '5', 'maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php
                                    echo $form->field($model, 'url')->widget(FileInput::className(), [
                                        'options' => [
                                            'multiple' => false,
                                            'accept' => 'file/pdf'
                                        ],
                                        'language' => 'pt',
                                        'pluginOptions' => [
                                            'allowedFileExtensions' => ['pdf'],
                                            'showPreview' => false,
                                            'showCaption' => true,
                                            'showRemove' => false,
                                            'showUpload' => false,
                                            'browseLabel' => 'Procurar',
                                            'initialCaption' => $model->url,
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div id="anexo" class="tab-pane">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-border panel-primary">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if ($model->url != NULL) {
                                                        ?>
                                                        <div class="form-group">
                                                            <iframe src="/arquivo/monitor/<?php echo $model->url; ?>"
                                                                width="100%" height="600px;">
                                                                Este navegador não suporta PDFs. Faça o download do PDF para
                                                                visualizá-lo: <a
                                                                    href="/arquivo/monitor/<?php echo $model->url; ?>">Baixar
                                                                    o arquivo PDF</a>
                                                            </iframe>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <label class="control-label">Sem PDF</label>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right m-b-0">
                        <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlMonitorSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="card-box">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel-heading">
                        <h3 class="m-t-0 header-title">Quem fáz uso deste monitor</h3>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="m-b-30">
                        <label class="control-label">Funcionario</label>
                        <?php
                        if (Yii::$app->user->identity->unidade_temp_fk != 1) {
                            $dataModel = ArrayHelper::map(Funcionario::find()->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->andWhere(['so_contato' => 0])->andWhere(['status' => 1])->orderBy(['nome' => SORT_ASC])->all(), 'id', function ($model) {
                                return $model->nome;
                            });
                        } else {
                            $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
                            $dataModel = ArrayHelper::map(Funcionario::find()->andWhere(['IN', 'unidade_fk', $usuariounidade])->andWhere(['so_contato' => 0])->andWhere(['status' => 1])->orderBy(['nome' => SORT_ASC])->all(), 'id', function ($model) {
                                return $model->nome;
                            });
                        }
                        $dataArray = [NULL => "Selecione um funcionario para adicionar ao monitor!"];
                        echo Select2::widget([
                            'name' => 'funcionario_fk',
                            'data' => ArrayHelper::merge($dataArray, $dataModel),
                            'value' => null,
                            'options' => [
                                'id' => 'funcionario_fk',
                                'onchange' => '
                                        $.post("/monitor/set-session-funcionario", {funcionario_fk: $("#funcionario_fk").val(), monitor_fk: ' . $model->id . '}, function (data) {
                                            location.reload(true);
                                        });
                                    ',
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    Pjax::begin([
                        'id' => 'funcionariomonitor',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProviderFuncionariomonitor,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            [
                                'attribute' => 'funcionario_unidade_fk',
                                'value' => function ($data) {
                                    return ($data->funcionariounidadeFk->unidade_pai_fk ?
                                        ($data->funcionariounidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade
                                                        : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                                    : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                                : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                            : $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                        : $data->funcionariounidadeFk->unidade);
                                },
                            ],
                            [
                                'attribute' => 'Setor',
                                'value' => function ($data) {
                                    return ($data->funcionarioFk->setorFk->setor ? $data->funcionarioFk->setorFk->setor : "");
                                },
                            ],
                            [
                                'attribute' => 'Função',
                                'value' => function ($data) {
                                    return ($data->funcionarioFk->funcao ? $data->funcionarioFk->funcao : "");
                                },
                            ],
                            'nome',
                            [
                                'class' => 'common\components\SYSActionColumn',
                                'template' => '{deletefuncionario}',
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
        <div class="card-box">
            <div class="row">
                <div class="col-sm-9">
                    <div class="panel-heading">
                        <h3 class="m-t-0 header-title">Histótico deste monitor</h3>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="m-b-30">
                        <?php echo Html::a('<i class="ion-plus-round"></i> Nova Ocorencia', ['createocorencia', 'id' => $model->id], ['class' => 'btn btn-success loading']); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    Pjax::begin([
                        'id' => 'equipamento',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProviderOcorencia,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            [
                                'attribute' => 'Data',
                                'value' => function ($data) {
                                    return $data->data;
                                },
                            ],
                            [
                                'attribute' => 'Usuário',
                                'value' => function ($data) {
                                    return $data->usuario_nome;
                                },
                            ],
                            [
                                'attribute' => 'Ação',
                                'value' => function ($data) {
                                    return $data->tipoFk->tipo;
                                },
                            ],
                            [
                                'attribute' => 'Descrição',
                                'value' => function ($data) {
                                    return $data->descricao;
                                },
                            ],
                            [
                                'class' => 'common\components\SYSOcorenciaActionColumn',
                                'template' => '{ocorenciapdf}',
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    // Define o script JavaScript para aplicar formatação customizada
    $this->registerJs("
        function formatCurrency(input) {
            // Remove todos os caracteres que não são números
            let value = input.value.replace(/[^0-9]/g, '');

            // Formata para incluir centavos sempre (dois últimos dígitos)
            if (value.length <= 2) {
                value = value.padStart(3, '0'); // Garante ao menos '0,00'
            }

            let integerPart = value.slice(0, -2); // Parte inteira
            let decimalPart = value.slice(-2); // Parte decimal (centavos)

            // Remove o zero inicial da parte inteira, se aplicável
            integerPart = integerPart.replace(/^0+/, '');

            // Adiciona pontos para milhares
            integerPart = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, '.');

            // Exibe o valor formatado no campo de entrada
            input.value = integerPart + ',' + decimalPart;
        }
    ", \yii\web\View::POS_END, 'formatacao-valor');
?>