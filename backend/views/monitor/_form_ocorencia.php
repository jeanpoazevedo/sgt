<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\select2\Select2;
use common\models\Monitor;
use common\models\OcorenciaTipo;

$this->title = 'Ocorencia Monitor';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('codigo_monitor'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->codigo_monitor; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($modelocorencia, 'tipo_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(OcorenciaTipo::find()->orderBy(['tipo' => SORT_ASC])->all(), 'id', function($model) {
                                        return $model->tipo;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione um Tipo'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                            echo $form->field($modelocorencia, 'url')->widget(FileInput::className(), [
                                'options' => [
                                    'multiple' => false,
                                    'accept' => 'file/pdf'
                                ],
                                'language' => 'pt',
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['pdf'],
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => 'Procurar',
                                    'initialCaption' => "Selecione um arquivo PDF",
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $form->field($modelocorencia, 'descricao')->textArea(['class' => 'form-control', 'rows' => '4', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', ['update', 'id' => $model->id], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']); ?>
                    <?php echo Html::submitButton($modelocorencia->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>