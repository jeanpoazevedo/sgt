<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use common\models\Equipamento;
use common\models\Setor;

$this->title = 'Controle de cadastro de ONU';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de ONU</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Nova ONU'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                        Pjax::begin([
                            'id' => 'pononu',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                        ]);

                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'setor_fk',
                                    'value' => function ($data) {
                                        return $data->setorFk->setor;
                                    },
                                ],
                                [
                                    'attribute' => 'codigo_onu_fk',
                                    'value' => function ($data) {
                                        return $data->codigoOnuFk->codigo_equipamento;
                                    },
                                ],
                                [
                                    'attribute' => 'Número de Série',
                                    'value' => function ($data) {
                                        return $data->codigoOnuFk->numero_serie;
                                    },
                                ],
                                [
                                    'attribute' => 'MAC',
                                    'value' => function ($data) {
                                        return $data->codigoOnuFk->mac;
                                    },
                                ],
                                [
                                    'attribute' => 'porta_pon',
                                    'value' => function ($data) {
                                        return $data->porta_pon;
                                    },
                                ],
                                [
                                    'attribute' => 'porta_pon_id',
                                    'value' => function ($data) {
                                        return $data->porta_pon_id;
                                    },
                                ],
                                [
                                    'attribute' => 'onu_profile',
                                    'value' => function ($data) {
                                        return $data->onu_profile;
                                    },
                                ],
                                [
                                    'attribute' => 'ponto',
                                    'value' => function ($data) {
                                        return $data->ponto;
                                    },
                                ],
                                [
                                    'class' => 'common\components\SYSActionColumn',
                                    'template' => '{update} {delete}',
                                ],
                            ],
                        ]);
                        Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>