<?php

use yii\helpers\Html;

$this->title = 'Configuracao';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <h2><?php echo $model->nome; ?></h2>
                        </div>                                        
                    </div>
                </div>
                <?php 
                    if($model->ativo_variavel1 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel1 ==! 0){
                                        echo '<label class="control-label">'; echo $model->titulo_variavel1; echo '</label>';
                                        echo '<input type="checkbox" data-plugin="switchery" data-color="#81c868" disabled=""'; echo $model->variavel1 ? 'CHECKED' : ''; echo '>';
                                    } else {
                                        echo '<label class="control-label">'; echo $model->titulo_variavel1; echo '</label>';
                                        echo '<textarea class="form-control" maxlength="100" rows="2" disabled="" >'; echo $model->variavel1; echo '</textarea>';
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                    if($model->ativo_variavel2 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel2 ==! 0){
                                        echo '<label class="control-label">'; echo $model->titulo_variavel2; echo '</label>';
                                        echo '<input type="checkbox" data-plugin="switchery" data-color="#81c868" disabled=""'; echo $model->variavel2 ? 'CHECKED' : ''; echo '>';
                                    } else {
                                        echo '<label class="control-label">'; echo $model->titulo_variavel2; echo '</label>';
                                        echo '<textarea class="form-control" maxlength="100" rows="2" disabled="" >'; echo $model->variavel2; echo '</textarea>';
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                    if($model->ativo_variavel3 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel3 ==! 0){
                                        echo '<label class="control-label">'; echo $model->titulo_variavel3; echo '</label>';
                                        echo '<input type="checkbox" data-plugin="switchery" data-color="#81c868" disabled=""'; echo $model->variavel3 ? 'CHECKED' : ''; echo '>';
                                    } else {
                                        echo '<label class="control-label">'; echo $model->titulo_variavel3; echo '</label>';
                                        echo '<textarea class="form-control" maxlength="100" rows="2" disabled="" >'; echo $model->variavel3; echo '</textarea>';
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                    if($model->ativo_variavel4 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel4 ==! 0){
                                        echo '<label class="control-label">'; echo $model->titulo_variavel4; echo '</label>';
                                        echo '<input type="checkbox" data-plugin="switchery" data-color="#81c868" disabled=""'; echo $model->variavel4 ? 'CHECKED' : ''; echo '>';
                                    } else {
                                        echo '<label class="control-label">'; echo $model->titulo_variavel4; echo '</label>';
                                        echo '<textarea class="form-control" maxlength="100" rows="2" disabled="" >'; echo $model->variavel4; echo '</textarea>';
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                ?>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlConfiguracaoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>