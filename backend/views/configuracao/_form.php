<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$this->title = 'Configuracao';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>
                <h2><?php echo $model->nome; ?></h2>
                <?php 
                    if($model->ativo_variavel1 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel1 ==! 0){
                                        echo $form->field($model, 'variavel1', ['template' => '{label}<div class="input-group">{input}</div>',])->checkbox(['label' => '', 'data-plugin' => "switchery"])->label($model->titulo_variavel1);
                                    } else {
                                        echo $form->field($model, 'variavel1')->textarea(['class' => 'form-control', 'maxlength' => true])->label($model->titulo_variavel1);
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                    if($model->ativo_variavel2 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel2 ==! 0){
                                        echo $form->field($model, 'variavel2', ['template' => '{label}<div class="input-group">{input}</div>',])->checkbox(['label' => '', 'data-plugin' => "switchery"])->label($model->titulo_variavel2);
                                    } else {
                                        echo $form->field($model, 'variavel2')->textarea(['class' => 'form-control', 'maxlength' => true])->label($model->titulo_variavel2);
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                    if($model->ativo_variavel3 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel3 ==! 0){
                                        echo $form->field($model, 'variavel3', ['template' => '{label}<div class="input-group">{input}</div>',])->checkbox(['label' => '', 'data-plugin' => "switchery"])->label($model->titulo_variavel3);
                                    } else {
                                        echo $form->field($model, 'variavel3')->textarea(['class' => 'form-control', 'maxlength' => true])->label($model->titulo_variavel3);
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                    if($model->ativo_variavel4 ==! 0){
                        echo '<div class="row">'
                            .'<div class="col-sm-12">'
                                .'<div class="form-group">';
                                    if ($model->tipo_variavel4 ==! 0){
                                        echo $form->field($model, 'variavel4', ['template' => '{label}<div class="input-group">{input}</div>',])->checkbox(['label' => '', 'data-plugin' => "switchery"])->label($model->titulo_variavel4);
                                    } else {
                                        echo $form->field($model, 'variavel4')->textarea(['class' => 'form-control', 'maxlength' => true])->label($model->titulo_variavel4);
                                    }
                                echo '</div>'
                            .'</div>'
                        .'</div>';
                    }
                ?>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlConfiguracaoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>