<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Controle de cadastro de Marca';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de Marca</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Nova Marca'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'marca',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            'marca',
                            [
                                'attribute' => 'computador',
                                'value' => function ($data) {
                                    return $data->computador ? 'Sim' : 'Não';
                                },
                            ],
                            [
                                'attribute' => 'monitor',
                                'value' => function ($data) {
                                    return $data->monitor ? 'Sim' : 'Não';
                                },
                            ],
                            [
                                'attribute' => 'equipamento',
                                'value' => function ($data) {
                                    return $data->equipamento ? 'Sim' : 'Não';
                                },
                            ],
                            [
                                'attribute' => 'processador',
                                'value' => function ($data) {
                                    return $data->processador ? 'Sim' : 'Não';
                                },
                            ],
                            [
                                'attribute' => 'placa_video',
                                'value' => function ($data) {
                                    return $data->placa_video ? 'Sim' : 'Não';
                                },
                            ],
                            ['class' => 'common\components\SYSActionColumn'],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>