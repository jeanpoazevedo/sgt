<?php

use yii\helpers\Html;

$this->title = 'Marca';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('marca'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->marca; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('computador'); ?></label>
                            <input type="checkbox" data-plugin="switchery" disabled="" <?php echo $model->computador ? 'CHECKED' : ''; ?>>
                        </div>                                        
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('monitor'); ?></label>
                            <input type="checkbox" data-plugin="switchery" disabled="" <?php echo $model->monitor ? 'CHECKED' : ''; ?>>
                        </div>                                        
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('equipamento'); ?></label>
                            <input type="checkbox" data-plugin="switchery" disabled="" <?php echo $model->equipamento ? 'CHECKED' : ''; ?>>
                        </div>                                        
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('processador'); ?></label>
                            <input type="checkbox" data-plugin="switchery" disabled="" <?php echo $model->processador ? 'CHECKED' : ''; ?>>
                        </div>                                        
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('placa_video'); ?></label>
                            <input type="checkbox" data-plugin="switchery" disabled="" <?php echo $model->placa_video ? 'CHECKED' : ''; ?>>
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlMarcaSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>