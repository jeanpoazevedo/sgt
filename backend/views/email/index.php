<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Controle de e-Mial';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de e-mail</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <?php
                    Pjax::begin([
                        'id' => 'nome',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            'nome',
                            'username',
                            'password',
                            'host',
                            'port',
                            [
                                'attribute' => 'encryption',
                                'value' => function ($data) {
                                    return $data->array_encryption[$data->encryption];
                                },
                            ],
                            ['class' => 'common\components\SYSActionColumn',
                                'template' =>  '{view} {update}',
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>