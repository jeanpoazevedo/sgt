<?php

use yii\helpers\Html;

$this->title = 'e-Mail';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('nome'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->nome; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('username'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->username; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('password'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->password; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('host'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->host; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('port'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->port; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('encryption'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->array_encryption[$model->encryption]; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlEmailSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                </div>

            </div>
        </div>
    </div>
</div>