<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Unidade;
use common\models\Local;
use common\models\Vlan;
use common\models\UsuarioUnidade;

$this->title = 'Patch Panel';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function ($model) {
                                    return ($model->unidadeFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidade);
                                }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($model, 'local_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Local::find()->all(), 'id', function ($model) {
                                return $model->local;
                            }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione uma Local'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'id_patch_panel')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label
                                class="control-label"><?php echo $model->getAttributeLabel('numero_portas'); ?></label>
                            <input type="text" class="form-control" disabled="" id="switchdoc-numero_portas"
                                name="Switchdoc[numero_portas]" value="<?php echo $model->numero_portas; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'descricao_patch_panel')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-2">
                                <label class="control-label">Porta</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-8">
                                <label class="control-label">Descrição</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-2">
                                <label class="control-label">Porta</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-8">
                                <label class="control-label">Descrição</label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    $xml = simplexml_load_string($model->xml_portas);
                    $i = 0;
                    $fim = 0;

                    foreach ($xml->port as $port) {

                        if ($i % 2 == 0) {
                            echo '<div class="row">';
                        }
                        ?>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2">
                                    <?php echo $port->porta; ?>
                                </div>
                                <div class="col-sm-2">
                                    <input type="checkbox" class="form-control" name="status_<?php echo $i; ?>" <?php echo ($port->status != 0 ? 'CHECKED' : '') ?>>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="descricao_<?php echo $i; ?>" class="form-control"
                                        value="<?php echo $port->descricao; ?>">
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($i % 2 == 0) {
                            $fim = 1;
                        } else {
                            echo '</div>';
                            $fim = 0;
                        }
                        $i++;
                        ?>
                        <?php
                    }
                    if ($fim == 1) {
                        echo '</div>';
                    }
                ?>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlPatchPanelSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
    $this->registerJs(" 

        $('#patchpanel-numero_portas').blur(function() {
            if ($('#patchpanel-numero_portas').val()) {
                $.post('/patchpanel/get-ports', {quantidade: $('#patchpanel-numero_portas').val()}, function (data) {
                    $('#row_lines').html(data);
                });
            }
        });

       ", View::POS_END, 'functions-patchpanel-xml'
    );
?>