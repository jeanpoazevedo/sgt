<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Vlan;

?>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-2">
                <label class="control-label">Porta</label>
            </div>
            <div class="col-sm-2">
                <label class="control-label">Status</label>
            </div>
            <div class="col-sm-8">
                <label class="control-label">Descrição</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-2">
                <label class="control-label">Porta</label>
            </div>
            <div class="col-sm-2">
                <label class="control-label">Status</label>
            </div>
            <div class="col-sm-8">
                <label class="control-label">Descrição</label>
            </div>
        </div>
    </div>
</div>

<?php
for ($i = 0; $i < $request['quantidade']; $i++) {
    $model->numero_porta = str_pad(($i + 1), 2, '0', STR_PAD_LEFT);
?>
    <div class="row">
        <?php
            if ($i < $request['quantidade']) {
        ?>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-2">
                    <?php echo $model->numero_porta; ?>
                </div>
                <div class="col-sm-2">
                    <?php echo Html::checkbox('status_' . $i, true, ['data-plugin' => "switchery"]); ?>
                </div>
                <div class="col-sm-8">
                    <?php echo Html::input('text', 'descricao_' . $i, $model->descricao, ['class' => 'form-control']); ?>
                </div>
            </div>
        </div>
        <?php
            }
            $i++;
            $model->numero_porta = str_pad(($i + 1), 2, '0', STR_PAD_LEFT);
            if ($i < $request['quantidade']) {
        ?>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-2">
                    <?php echo $model->numero_porta; ?>
                </div>
                <div class="col-sm-2">
                    <?php echo Html::checkbox('status_' . $i, true, ['data-plugin' => "switchery"]); ?>
                </div>
                <div class="col-sm-8">
                    <?php echo Html::input('text', 'descricao_' . $i, $model->descricao, ['class' => 'form-control']); ?>
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
<?php
    }
?>