<?php

/* @var $this yii\web\View */

$this->title = 'Home';
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="md md-devices text-dark"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">
                        <?php echo count($computador_total);?>
                        </b></h3>
                    <p class="text-muted">Total de Computadores</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="md md-desktop-windows text-dark"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">
                        <?php echo count($monitor_total);?>
                        </b></h3>
                    <p class="text-muted">Total de Monitores</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="md md-print text-dark"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">
                        <?php echo count($equipamento_total);?>
                        </b></h3>
                    <p class="text-muted">Total de Equipametnos</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-2">
            
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="md md-live-help text-dark"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">
                        <?php echo count($hd_total);?>
                        </b></h3>
                    <p class="text-muted">Chamados encerrados</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="md md-person text-dark"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">
                        <?php echo count($usuario_total);?>
                        </b></h3>
                    <p class="text-muted">Usuarios Ativos</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-6 col-lg-2">
            
        </div>
    </div>
</div>
