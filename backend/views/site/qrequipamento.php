<?php

/* @var $this yii\web\View */

$this->title = 'QR';
?>
<div class="site-index">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('codigo_equipamento'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->codigo_equipamento; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('unidade_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->unidadeFk->unidade; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('setor_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php
                    echo ($model->setor_fk != NULL ? $model->setorFk->setor : "Setor não informado" )
                ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('tipo_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->tipoFk->tipo; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Marca</label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->modeloFk->marcaFk->marca; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('modelo_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->modeloFk->modelo; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('numero_serie'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_serie; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('netbios'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->netbios; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('mac'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->mac; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('numero_patrimonio'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_patrimonio; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('data_compra'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->data_compra; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('garantia'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->garantia; ?>">
            </div>                                        
        </div>
    </div>
</div>
