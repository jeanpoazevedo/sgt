<?php

/* @var $this yii\web\View */

$this->title = 'QR';
?>
<div class="site-index">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('codigo_computador'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->codigo_computador; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('unidade_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->unidadeFk->unidade; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('setor_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php
                    echo ($model->setor_fk != NULL ? $model->setorFk->setor : "Setor não informado" )
                ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('tipo_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->tipoFk->tipo; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Marca</label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->modeloFk->marcaFk->marca; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('modelo_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->modeloFk->modelo; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('numero_serie'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_serie; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('netbios'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->netbios; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('mac_c'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->mac_c; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('mac_w'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->mac_w; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('data_compra'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->data_compra; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('garantia'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->garantia; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('numero_lacre'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_lacre; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('placa_mae'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->placa_mae; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('processador_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php
                    echo ($model->processador_fk != NULL ? $model->processadorFk->processador : "Sem Processaor" )
                ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('numero_processador'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_processador; ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('ram'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->ram.' '.$model->ramMedidaFk->simbolo; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('ram_clok_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php
                    echo ($model->ram_clok_fk != NULL ? $model->ramClokFk->clok.' MHz' : "Sem Clok" )
                ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('hd'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->hd.' '.$model->hdMedidaFk->simbolo; ?>">
            </div>                                        
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('tipo_hd_fk'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php
                    echo ($model->placa_video_fk != NULL ? $model->tipoHdFk->tipo : "Sem Tipo de HD" )
                ?>">
            </div>                                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label"><?php echo $model->getAttributeLabel('numero_patrimonio'); ?></label>
                <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_patrimonio; ?>">
            </div>                                        
        </div>
    </div>
</div>
