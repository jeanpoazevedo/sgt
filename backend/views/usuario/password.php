<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use common\models\UsuarioUnidade;
use common\models\Perfil;

$this->title = 'Usuário';

?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('nome'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->nome; ?>">
                        </div>                                        
                    </div> 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('email'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->email; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('usuario'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->usuario; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?php echo $form->field($model, 'senha')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'onkeyup' => 'javascript:verifica_forca(this.value)']) ?>
                    </div>
                    <div class="col-sm-1">
                        <br>
                        <span id="mostra"></span>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlUsuarioSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

<?php
    $this->registerJs(" 

            function verifica_forca(senha){
                forca = 0;
                mostra = document.getElementById('mostra');
                if((senha.length > 7) && (senha.length <= 12)){
                    forca += 10;
                }else if((senha.length>12)){
                    forca += 25;
                }
                if(senha.match(/[a-z]+/)){
                    forca += 10;
                }
                if(senha.match(/[A-Z]+/)){
                    forca += 20;
                }
                if(senha.match(/\d+/)){
                    forca += 20;
                }
                if(senha.match(/\W+/)){
                    forca += 25;
                }
                return mostra_res();
                return libera_aplicar();
            }
            function mostra_res(){
                if(forca <=30){
                    mostra.innerHTML = '<span style=\"color:#FF0000;\"> Fraca </span>';
                }else if((forca > 30) && (forca < 60)){
                    mostra.innerHTML = '<span style=\"color:#FFBE00;\"> Boa </span>'; 
                }else if((forca >= 60) && (forca < 85)){
                    mostra.innerHTML = '<span style=\"color:#0000FF;\"> Forte </span>';
                }else {
                    mostra.innerHTML = '<span style=\"color:#00CD00;\"> Excelente </span>';
                }
            }

       ", View::POS_END, 'functions-usuario-password'
    );
?>