<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Ajuste de Unidade ao Usuário';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'form-unidade-usuario',
                            'action' => '/usuario/save-unidade-usuario',
                            'method' => 'post',
                            'enableClientValidation' => true,
                ]);
                ?>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="text-dark header-title m-t-0 m-b-30">Usuário do sistema: <?php echo $model->nome; ?></h4>

                            <div id="checkTree">
                                <ul>
                                    <?php
                                        echo $data;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>            
                </div>            

                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlUsuarioSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton(Yii::t('app', '<i class="ion-checkmark-round"></i> Ajustar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
<?php
$this->registerJS("
    $('form#form-unidade-usuario').on('beforeSubmit', function(e) {
        var items = $('ul.jstree-children>li');
        var submit = [];
        $.each(items,function(index, node) {
            var myJSON = JSON.parse(node.getAttribute('data-jstree'));
            if (myJSON.key) {
                var objeto = {tipo : myJSON.tipo, key : myJSON.key, selected : node.getAttribute('aria-selected')}
                submit.push(objeto);
            }
        });
        var form = $(this);
        if (form.find('.has-error').length) {
             return false;
        }
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: 'id=" . $model->id . "&dados=' + JSON.stringify(submit),
            dataType: 'json',
            success: function (response) {
                switch (response.status) {
                    case 'error':
                        break;
                    case 'success':
                        swal('Atenção', 'Atualização realizada com sucesso!');
                        break;
                }
                $('body').loading('stop');
            }
        });
        return false;
    }).on('submit', function(e){
        e.preventDefault();
    });
");
?>