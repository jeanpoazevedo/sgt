<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\UsuarioUnidade;
use common\models\Perfil;
use dominus77\tinymce\TinyMce;

$this->title = 'Passoapasso';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            $dataModel = ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function($model) {
                                return ($model->unidadeFk->unidade_pai_fk ?
                                    ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                    : $model->unidadeFk->unidade);
                                });
                            $dataArray = [ 1 => "Geral"];
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::merge($dataArray, $dataModel),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            if($model_perfil > 6){
                                echo $form->field($model, 'perfil_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Perfil::find()->where(['id' => $model_perfil])->orderBy(['id' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else {
                                echo $form->field($model, 'perfil_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Perfil::find()->orderBy(['id' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'titulo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $form->field($model, 'descricao')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>            
                </div> 
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                            echo $form->field($model, 'passo_passo')->widget(TinyMce::className(), [
                                'options' => [
                                    'id' => 'tinymce_conteudo',
                                ],
                                'language' => 'pt_BR',
                                'clientOptions' => [
                                    'height' => 400,
                                    'branding' => false,
                                    'theme' => 'modern',
                                    'skin' => 'lightgray-gradient',
                                    'plugins' => [
                                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                                        "insertdatetime media nonbreaking save table contextmenu directionality",
                                        "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc fontawesome noneditable",
                                    ],
                                    'noneditable_noneditable_class' => 'fa',
                                    'extended_valid_elements' => 'span[class|style]',
                                    'toolbar' => "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons fontawesome | codesample",
                                    'image_advtab' => true,
                                ]
                            ])->label('');
                        ?>
                    </div>            
                </div> 
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlPassoapassoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>