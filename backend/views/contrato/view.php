<?php

use yii\helpers\Html;

$this->title = 'Contrato';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills m-b-30 pull-right">
                    <li class="active">
                        <a href="#geral" data-toggle="tab" aria-expanded="true">Geral</a>
                    </li>
                    <li class="">
                        <a href="#anexo" data-toggle="tab" aria-expanded="true">Anexo</a>
                    </li>
                </ul>
                <div class="tab-content br-n pn">
                    <div id="geral" class="tab-pane active">
                    <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-border panel-primary">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('unidade_fk'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="
                                                        <?php echo ($model->unidadeFk->unidade_pai_fk ?
                                                            ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                                                $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                            : $model->unidadeFk->unidade);
                                                    ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('perfil_fk'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->perfilFk->nome; ?>">
                                                </div>                                        
                                            </div> 
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('data_fim'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->data_fim; ?>">
                                                </div>                                        
                                            </div>           
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('descricao'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->descricao; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="anexo" class="tab-pane">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-border panel-primary">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?php
                                                    if ($model->url != NULL) {
                                                ?>
                                                    <div class="form-group">
                                                        <iframe src="/arquivo/contrato/<?php echo $model->url; ?>" width="100%" height="600px;">
                                                            Este navegador não suporta PDFs. Faça o download do PDF para visualizá-lo: <a href="/arquivo/cotrato/<?php echo $model->url; ?>">Baixar o arquivo PDF</a>
                                                        </iframe>
                                                    </div>
                                                <?php
                                                    } else {
                                                ?>
                                                    <label class="control-label">Sem PDF</label>
                                                <?php
                                                    }
                                                ?>
                                            </div>            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlContratoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>