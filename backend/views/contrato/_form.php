<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\file\FileInput;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;
use common\models\UsuarioUnidade;
use common\models\Perfil;

$this->title = 'Contrato';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                            $dataModel = ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function($model) {
                                return ($model->unidadeFk->unidade_pai_fk ?
                                    ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                    : $model->unidadeFk->unidade);
                                });
                            $dataArray = [ 1 => "Geral"];
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::merge($dataArray, $dataModel),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            if($model_perfil > 6){
                                echo $form->field($model, 'perfil_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Perfil::find()->where(['id' => $model_perfil])->orderBy(['id' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else {
                                echo $form->field($model, 'perfil_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Perfil::find()->orderBy(['id' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'data_fim')
                                ->widget(MaskedInput::className(), ['mask' => '99/99/9999',])
                                ->widget(DatePicker::classname(), [
                                'language' => 'pt-BR',
                                'removeButton' => false,
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'orientation' => 'bottom left',
                                    'format' => 'dd/mm/yyyy',
                                ]
                             ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <?php echo $form->field($model, 'descricao')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'url')->widget(FileInput::className(), [
                                'options' => [
                                    'multiple' => false,
                                    'accept' => 'file/pdf'
                                ],
                                'language' => 'pt',
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['pdf'],
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => 'Procurar',
                                    'initialCaption' => $model->url,
                                ]
                            ]);
                        ?>
                    </div>

                </div>
                    </div>
                </div>

                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlContratoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>