<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = '';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('usuario_de_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->deFk->nome; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('usuario_para_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->paraFk->nome; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('produto_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->produtoFk->produto; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('resumo'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->resumo; ?>">
                        </div>                                        
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('descricao'); ?></label>
                            <?php echo $model->descricao; ?>
                        </div>
                    </div>            
                </div>
                <div class="row">
                    <h4 class="page-title">Etapas</h4>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class = "table m-0">
                            <thead>
                                <tr>
                                    <th>De Usuário</th>
                                    <th>Data</th>
                                    <th>Descrição</th>
                                </tr>
                            </thead>
                            <tbody id = "table_etapas">
                                <?php
                                foreach ($array_item as $key => $value) {
                                    echo $value;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>                    
                </div>
                <div class="row">
                    <h4 class="page-title">Anexos</h4>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class = "table m-0">
                            <thead>
                                <tr>
                                    <th>De Usuário</th>
                                    <th>Data</th>
                                    <th>Descrição Anexo</th>
                                </tr>
                            </thead>
                            <tbody id = "table_anexo">
                                <?php
                                foreach ($array_item_anexo as $key => $value) {
                                    echo $value;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>                    
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', ['helpdesk/index'], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Atender', ['atender', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                </div>
                <div class="row">
                    <h4 class="page-title">Histórico</h4>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        Pjax::begin([
                            'id' => 'hdhistorico',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                        ]);
                        echo GridView::widget([
                            'dataProvider' => $dataProviderHistorico,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'Data',
                                    'value' => function ($data) {
                                        return $data->data;
                                    },
                                ],
                                [
                                    'attribute' => 'Usuário',
                                    'value' => function ($data) {
                                        return $data->deFk->nome;
                                    },
                                ],
                                [
                                    'attribute' => 'Ação',
                                    'value' => function ($data) {
                                        return $data->acao;
                                    },
                                ],
                                [
                                    'attribute' => 'Alteração',
                                    'value' => function ($data) {
                                        return $data->alteracao;
                                    },
                                ],
                            ],
                        ]);
                        Pjax::end();
                        ?>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>