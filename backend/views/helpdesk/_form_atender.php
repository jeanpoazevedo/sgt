<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\VwHdDeUsuarioPara;
use common\models\HdProduto;
use common\models\HdCadastro;
use dominus77\tinymce\TinyMce;
use kartik\file\FileInput;

$this->title = 'Chamado';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading">
                    <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <h4 class="page-title">Chamado: <?php echo $model->id; ?></h4>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label"><?php echo $model->getAttributeLabel('de'); ?></label>
                                <input type="text" class="form-control" disabled="" value="<?php echo $model->deFk->nome; ?>">
                            </div>                                        
                        </div>
                        <div class="col-sm-3">
                            <?php
                            if($usuarioperfil->perfil_fk < 3 || Yii::$app->user->identity->help_desk_master == 1){
                                    $dataModel = ArrayHelper::map(VwHdDeUsuarioPara::find()->where(['usuario_de_fk' => Yii::$app->user->identity->id])->all(), 'usuario_para_fk', function($model) {
                                        return $model->nome_para;
                                    });
                                    $dataArray = [ Yii::$app->user->identity->id => Yii::$app->user->identity->nome];
                                    echo $form->field($model, 'para_fk')->widget(Select2::className(), [
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'options' => [
                                            'prompt' => 'Selecione um Atendete'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => false
                                        ],
                                    ]);
                                } else {
                                    echo '<div class="form-group">'
                                        . '<label class="control-label">'.$model->getAttributeLabel('usuario_de_fk').'</label>'
                                        . '<input type="text" class="form-control" disabled="" value="'.$model->paraFk->nome.'">'
                                    . '</div>';
                                }
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                echo $form->field($model, 'produto_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(HdProduto::find()->all(), 'id', function($model) {
                                                return $model->produto;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione um Produto'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $form->field($model, 'ver')->checkbox(['class' => 'form-control', 'maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <?php
                            echo $form->field($model, 'prioridade')->widget(Select2::className(), [
                                'data' => $model->array_prioridade,
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um Status'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-10">
                            <?php echo $form->field($model, 'resumo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                        </div>            
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo $model->getAttributeLabel('descricao'); ?></label>
                                <?php echo $model->descricao; ?>
                            </div>
                        </div>            
                    </div>
                    <div class="row">
                        <h4 class="page-title">Etapas</h4>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class = "table m-0">
                                <thead>
                                    <tr>
                                        <th>De Usuário</th>
                                        <th>Data</th>
                                        <th>Descrição</th>
                                    </tr>
                                </thead>
                                <tbody id = "table_etapas">
                                    <?php
                                    foreach ($array_item as $key => $value) {
                                        echo $value;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>                    
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            echo $form->field($model_etapas, 'descricao')->widget(TinyMce::className(), [
                                'options' => [
                                    'id' => 'tinymce_conteudo',
                                ],
                                'language' => 'pt_BR',
                                'clientOptions' => [
                                    'height' => 200,
                                    'branding' => false,
                                    'theme' => 'modern',
                                    'skin' => 'lightgray-gradient',
                                    'plugins' => [
                                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                                        "insertdatetime media nonbreaking save table contextmenu directionality",
                                        "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc fontawesome noneditable",
                                    ],
                                    'noneditable_noneditable_class' => 'fa',
                                    'extended_valid_elements' => 'span[class|style]',
                                    'toolbar' => "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons fontawesome | codesample",
                                    'image_advtab' => true,
                                ]
                            ])->label('');
                            ?>
                        </div>            
                    </div>
                    <div class="row">
                        <h4 class="page-title">Anexos</h4>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class = "table m-0">
                                <thead>
                                    <tr>
                                        <th>De Usuário</th>
                                        <th>Data</th>
                                        <th>Descrição Anexo</th>
                                    </tr>
                                </thead>
                                <tbody id = "table_anexo">
                                    <?php
                                    foreach ($array_item_anexo as $key => $value) {
                                        echo $value;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>                    
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo $form->field($model_anexo, 'descricao_anexo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?php
                                echo $form->field($model_anexo, 'url')->widget(FileInput::className(), [
                                    'options' => [
                                        'multiple' => false,
                                    ],
                                    'language' => 'pt',
                                    'pluginOptions' => [
                                        'showPreview' => false,
                                        'showCaption' => true,
                                        'showRemove' => false,
                                        'showUpload' => false,
                                        'browseLabel' => 'Procurar',
                                        'initialCaption' => "Selecione um arquivo",
                                    ]
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($model, 'status')->widget(Select2::className(), [
                                'data' => $model->array_status,
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um Status'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group text-right m-b-0">
                        <?php echo Html::a('<i class="ion-reply"></i> Voltar', ['helpdesk/index'], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Salvar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Salvar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="card-box">
            <div class="row">
                <div class="panel-heading">
                    <h3 class="m-t-0 header-title">Histórico</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php
                        Pjax::begin([
                            'id' => 'hdhistorico',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                        ]);
                        echo GridView::widget([
                            'dataProvider' => $dataProviderHistorico,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'Data',
                                    'value' => function ($data) {
                                        return $data->data;
                                    },
                                ],
                                [
                                    'attribute' => 'Usuário',
                                    'value' => function ($data) {
                                        return $data->deFk->nome;
                                    },
                                ],
                                [
                                    'attribute' => 'Ação',
                                    'value' => function ($data) {
                                        return $data->acao;
                                    },
                                ],
                                [
                                    'attribute' => 'Alteração',
                                    'value' => function ($data) {
                                        return $data->alteracao;
                                    },
                                ],
                            ],
                        ]);
                        Pjax::end();
                    ?>
                </div>                    
            </div>
        </div>
    </div>
</div>