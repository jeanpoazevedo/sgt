<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\VwHdDeUsuarioPara;
use common\models\HdProduto;
use common\models\HdCadastro;
use dominus77\tinymce\TinyMce;
use kartik\file\FileInput;

$this->title = 'Abrir Chamado';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'para_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(VwHdDeUsuarioPara::find()->where(['usuario_de_fk' => Yii::$app->user->identity->id])->orderBy(['nome_para' => SORT_ASC])->all(), 'usuario_para_fk', function($model) {
                                            return $model->nome_para;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um Atendente'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'produto_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(HdProduto::find()->orderBy(['produto' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->produto;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um Produto'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <?php echo $form->field($model, 'ver')->checkbox(['class' => 'form-control', 'maxlength', 'CHECKED' => false]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <?php
                            echo $form->field($model, 'prioridade')->widget(Select2::className(), [
                                'data' => $model->array_prioridade,
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um Status'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-10">
                        <?php echo $form->field($model, 'resumo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                            echo $form->field($model, 'descricao')->widget(TinyMce::className(), [
                                'options' => [
                                    'id' => 'tinymce_conteudo',
                                ],
                                'language' => 'pt_BR',
                                'clientOptions' => [
                                    'height' => 400,
                                    'branding' => false,
                                    'theme' => 'modern',
                                    'skin' => 'lightgray-gradient',
                                    'plugins' => [
                                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                                        "insertdatetime media nonbreaking save table contextmenu directionality",
                                        "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc fontawesome noneditable",
                                    ],
                                    'noneditable_noneditable_class' => 'fa',
                                    'extended_valid_elements' => 'span[class|style]',
                                    'toolbar' => "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons fontawesome | codesample",
                                    'image_advtab' => true,
                                ]
                            ])->label('');
                        ?>
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model_anexo, 'descricao_anexo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-6">
                        <?php
                            echo $form->field($model_anexo, 'url')->widget(FileInput::className(), [
                                'options' => [
                                    'multiple' => false,
                                ],
                                'language' => 'pt',
                                'pluginOptions' => [
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => 'Procurar',
                                    'initialCaption' => "Selecione um arquivo",
                                ]
                            ]);
                        ?>
                    </div>

                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', ['helpdesk/index'], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Abrir') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>