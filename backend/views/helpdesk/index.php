<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\HdUsuarioUnidade;

$this->title = 'Controle de cadastro de Chamado';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Chamados</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-success pull-left">
                                    <i class="md md-question-answer text-success"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                           <?php echo count($hd_fila);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Na fila para atendimento</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-question-answer text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo count($hd_atendimento);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Em atendimento</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-pink pull-left">
                                    <i class="md md-question-answer text-pink"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo count($hd_resolvido);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Resolvidos</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-pink pull-left">
                                    <i class="md md-question-answer text-pink"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo count($hd_encerrado);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Concluidos</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Novo Chamado'), ['abrir'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                        if($model_restricao_atendente){
                            echo '<div class="row">'.
                                '<div class="col-sm-3">'.
                                    '<div class="m-b-30">'.
                                        '<label class="control-label">Filtra: Unidade</label>';
                                            echo Select2::widget([
                                                'name' => 'hd_unidade_temp_fk',
                                                'data' => ArrayHelper::map(HdUsuarioUnidade::find()->orWhere(['usuario_fk' => Yii::$app->user->identity->id])->all(), 'unidade_fk', function($model) {
                                                    return ($model->unidadeFk->unidade_pai_fk ?
                                                                ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                                                $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                            : $model->unidadeFk->unidade);
                                                        }),
                                                'value' => Yii::$app->user->identity->hd_unidade_temp_fk,
                                                'pluginOptions' => [
                                                    'width' => '400px',
                                                ],
                                                'options' => [
                                                    'id' => 'hd_unidade_temp_fk',
                                                    'onchange' => '
                                                        if ($("#hd_unidade_temp_fk").val()) {
                                                            $.post("/helpdesk/set-session-hd-unidade", {hd_unidade_temp_fk: $("#hd_unidade_temp_fk").val()}, function (data) {
                                                                location.reload(true);
                                                            });
                                                        }
                                                    ',
                                                ],
                                            ]);
                                    echo '</div>'.
                                '</div>'.
                                '<div class="col-sm-3">'.
                                    '<div class="m-b-30">'.
                                        '<label class="control-label">Visualizar:</label>';
                                            echo Select2::widget([
                                                'name' => 'hd_filtro',
                                                'data' => $model->array_filtro,
                                                'theme' => Select2::THEME_BOOTSTRAP,
                                                'options' => [
                                                    'prompt' => 'Selecione um Status'
                                                ],
                                                'pluginOptions' => [
                                                    'width' => '400px',
                                                ],
                                                'value' => Yii::$app->user->identity->hd_filtro,
                                                'options' => [
                                                    'id' => 'hd_filtro',
                                                    'onchange' => '
                                                        if ($("#hd_filtro").val()) {
                                                            $.post("/helpdesk/set-session-hd-filtro", {hd_filtro: $("#hd_filtro").val()}, function (data) {
                                                                location.reload(true);
                                                            });
                                                        }
                                                    ',
                                                ],
                                            ]);
                                    echo '</div>'.
                                '</div>'.
                            '</div>';
                        }
                    ?>
                    <?php
                    Pjax::begin([
                        'id' => 'helpdesk',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'unidade_fk_de',
                                'value' => function ($data) {
                                    return ($data->unidadeFkDe->unidade_pai_fk ?
                                        ($data->unidadeFkDe->unidadePaiFk->unidade_pai_fk ?
                                            ($data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade
                                                        : $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                                    : $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                                : $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                            : $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                        : $data->unidadeFkDe->unidade);
                                },
                            ],
                            'data_abertura',
                            'nome_de',
                            'nome_para',
                            'produto',
                            [
                                'attribute' => 'prioridade',
                                'value' => function ($data) {
                                    return $data->array_prioridade[$data->prioridade];
                                },
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return $data->array_status[$data->status];
                                },
                            ],
                            ['class' => 'common\components\SYSHelpdeskColumn'],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>