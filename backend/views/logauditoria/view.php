<?php

use yii\helpers\Html;

$this->title = 'Log Auditoria';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('acao'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->acao; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('usuario'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->usuario; ?>">
                        </div>                                        
                    </div>       
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('data_hora'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->data_hora; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('detalhes'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->detalhes; ?>">
                        </div>                                        
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('query_executada'); ?></label>
                            <?php echo $model->query_executada; ?>
                        </div>
                    </div>            
                </div>            
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlLogauditoriaSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                </div>

            </div>
        </div>
    </div>
</div>