<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\HdProduto;

$this->title = 'Controle de cadastro de Chamado';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Buscar Chamados</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="m-b-30">
                                <?php
                                    $dataModel = ArrayHelper::map(HdProduto::find()->all(), 'id', function($model) {
                                                return $model->produto;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro de Produto"];
                                    echo Select2::widget([
                                        'name' => 'produto_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->produto_temp_fk,
                                        'options' => [
                                            'id' => 'produto_temp_fk',
                                            'onchange' => '
                                                $.post("/buscar/set-session-produto", {produto_temp_fk: $("#produto_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'helpdesk',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'unidade_fk_de',
                                'value' => function ($data) {
                                    return ($data->unidadeFkDe->unidade_pai_fk ?
                                        ($data->unidadeFkDe->unidadePaiFk->unidade_pai_fk ?
                                            ($data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade
                                                        : $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                                    : $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                                : $data->unidadeFkDe->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                            : $data->unidadeFkDe->unidadePaiFk->unidade . " - " . $data->unidadeFkDe->unidade)
                                        : $data->unidadeFkDe->unidade);
                                },
                            ],
                            'data_abertura',
                            'nome_de',
                            'nome_para',
                            'produto',
                            'resumo',
                            [
                                'attribute' => 'prioridade',
                                'value' => function ($data) {
                                    return $data->array_prioridade[$data->prioridade];
                                },
                            ],
                            ['class' => 'common\components\SYSHelpdeskColumn'],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>