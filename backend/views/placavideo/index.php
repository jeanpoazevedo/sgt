<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\Marca;

$this->title = 'Controle de cadastro de Placa de Vídeo';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de Placas de Vídeo</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Nova Placa de Vídeo'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Marca</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Marca::find()->orWhere(['placa_video' => '1'])->orderBy(['marca' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->marca;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'marca_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->marca_temp_fk,
                                        'options' => [
                                            'id' => 'marca_temp_fk',
                                            'onchange' => '
                                                $.post("/placavideo/set-session-marca", {marca_temp_fk: $("#marca_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-8"></div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'marca',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            'marca',
                            'modelo',
                            'descricao_placa_video',
                            ['class' => 'common\components\SYSActionColumn'],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>