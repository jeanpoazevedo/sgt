<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Marca;

$this->title = 'Placa de Vídeo';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($model, 'marca_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Marca::find()->where(['placa_video' => '1'])->orderBy(['marca' => SORT_ASC])->all(), 'id', function($model) {
                                        return $model->marca;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione uma Marca'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $form->field($model, 'modelo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-5">
                        <?php echo $form->field($model, 'descricao_placa_video')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlPlacavideoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>