<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use common\models\Unidade;
use common\models\Usuario;
use common\models\UsuarioGerenteUnidade;

$this->title = 'Usuário Gerente de Unidade';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-4">
                        <?php
                            if (Yii::$app->user->identity->unidade_temp_fk != 1) {
                                echo $form->field($model, 'usuario_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Usuario::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['nome' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione um Usuário'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else {
                                echo $form->field($model, 'usuario_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Usuario::find()->orderBy(['nome' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione um Usuário'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(UsuarioGerenteUnidade::find()->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['gerencia' => 1])->all(), 'unidade_fk', function($model) {
                                    return ($model->unidadeFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidade);
                                    }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <?php
                        if ($model_restricao_perfil->perfil_fk == 1){
                            echo '<div class="col-sm-4">'
                                . '<div class="form-group">'
                                    . $form->field($model, 'gerencia', ['template' => '{label}<div class="input-group">{input}</div>',])
                                    ->checkbox(['data-plugin' => "switchery"])
                                . '</div>'
                            . '</div>';
                        } elseif ($model_restricao_unidade == 1 ) {
                            echo '<div class="col-sm-4">'
                                . '<div class="form-group">'
                                    . $form->field($model, 'gerencia', ['template' => '{label}<div class="input-group">{input}</div>',])
                                    ->checkbox(['data-plugin' => "switchery"])
                                . '</div>'
                            . '</div>';
                        }
                    ?>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlUsuariogerenteunidadeSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>