<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\Tipo;
use common\models\Setor;
use common\models\Marca;
use common\models\Modelo;
use common\models\VwRelatorioFuncionarioComputador;

$this->title = 'Relatório Funcionario para Computador';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Relatório de Funcionario para Computador</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', 'Limpa filtros'), ['clean'], ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php
                                    if($model_restricao_export){
                                        echo Html::a(Yii::t('app', 'Exportar'), ['export'], ['class' => 'btn btn-success']);
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Setor do Funcionário</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Setor::find()->orderBy(['setor' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->setor;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'setor_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->setor_temp_fk,
                                        'options' => [
                                            'id' => 'setor_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-setor", {setor_temp_fk: $("#setor_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Função do Funcionário</label>
                                <?php
                                    if (Yii::$app->user->identity->unidade_temp_fk != '1') {
                                        $dataModel = ArrayHelper::map(VwRelatorioFuncionarioComputador::find()->distinct()->andWhere(['<>', 'funcao', ''])->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['funcao' => SORT_ASC])->all(), 'funcao', function($model) {
                                                return $model->funcao;
                                            });
                                    } else {
                                        $dataModel = ArrayHelper::map(VwRelatorioFuncionarioComputador::find()->distinct()->andWhere(['<>', 'funcao', ''])->orderBy(['funcao' => SORT_ASC])->all(), 'funcao', function($model) {
                                                return $model->funcao;
                                            });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'funcao_temp',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->funcao_temp,
                                        'options' => [
                                            'id' => 'funcao_temp',
                                            'onchange' => '
                                                $.post("/relatoriofuncionariocomputador/set-session-funcao", {funcao_temp: $("#funcao_temp").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Tipo</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Tipo::find()->orWhere(['computador' => '1'])->orderBy(['tipo' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->tipo;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'tipo_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->tipo_temp_fk,
                                        'options' => [
                                            'id' => 'tipo_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-tipo", {tipo_temp_fk: $("#tipo_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Marca</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Marca::find()->orWhere(['computador' => '1'])->orderBy(['marca' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->marca;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'marca_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->marca_temp_fk,
                                        'options' => [
                                            'id' => 'marca_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-marca", {marca_temp_fk: $("#marca_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Modelo</label>
                                <?php
                                    if(Yii::$app->user->identity->marca_temp_fk){
                                        $dataModel = ArrayHelper::map(Modelo::find()->andWhere(['tipo' => '1'])->andWhere(['marca_fk' => Yii::$app->user->identity->marca_temp_fk])->orderBy(['modelo' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->modelo;
                                        });
                                    } else {
                                        $dataModel = ArrayHelper::map(Modelo::find()->orWhere(['tipo' => '1'])->orderBy(['modelo' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->modelo;
                                        });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'modelo_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->modelo_temp_fk,
                                        'options' => [
                                            'id' => 'modelo_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-modelo", {modelo_temp_fk: $("#modelo_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'marca',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            [
                                'attribute' => 'unidade_fk',
                                'value' => function ($data) {
                                    return ($data->unidadeFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                            : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidade);
                                },
                            ],
                            'funcionario_setor',
                            'funcao',
                            'nome',
                            [
                                'attribute' => 'codigo_computador',
                                'value' => function ($data) {
                                    return ($data->codigo_computador ? $data->codigo_computador : "");
                                },
                            ],
                            [
                                'attribute' => 'computador_unidade_fk',
                                'value' => function ($data) {
                                    return ($data->codigo_computador ?
                                        ($data->computadorunidadeFk->unidade_pai_fk ?
                                            ($data->computadorunidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidade
                                                            : $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidade)
                                                        : $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidade)
                                                    : $data->computadorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidade)
                                                : $data->computadorunidadeFk->unidadePaiFk->unidade . " - " . $data->computadorunidadeFk->unidade)
                                            : $data->computadorunidadeFk->unidade)
                                        : "" );
                                },
                            ],
                            [
                                'attribute' => 'computador_setor',
                                'value' => function ($data) {
                                    return ($data->computador_setor ? $data->computador_setor : "");
                                },
                            ],
                            [
                                'attribute' => 'tipo',
                                'value' => function ($data) {
                                    return ($data->tipo ? $data->tipo : "");
                                },
                            ],
                            [
                                'attribute' => 'marca',
                                'value' => function ($data) {
                                    return ($data->marca ? $data->marca : "");
                                },
                            ],
                            [
                                'attribute' => 'modelo',
                                'value' => function ($data) {
                                    return ($data->modelo ? $data->modelo : "");
                                },
                            ],
                            [
                                'attribute' => 'netbios',
                                'value' => function ($data) {
                                    return ($data->netbios ? $data->netbios : "");
                                },
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>