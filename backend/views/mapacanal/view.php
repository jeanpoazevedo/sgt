<?php

use yii\helpers\Html;

$this->title = 'Mapa de Canal';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('unidade'); ?></label>
                            <input type="text" class="form-control" disabled="" value="
                                <?php echo ($model->unidadeFk->unidade_pai_fk ?
                                    ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                    : $model->unidadeFk->unidade);
                            ?>">
                        </div>                                       
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('local'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->localFk->local; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('switch'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->switch; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('switch_porta'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->switch_porta; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('patch_panel'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->patch_panel; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('patch_panel_porta'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->patch_panel_porta; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('link'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->link; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('link'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->link; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('link'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->link; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('descricao'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->descricao; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlMapacanalSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>