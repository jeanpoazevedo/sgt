<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\Local;
use common\models\Switchdoc;

$this->title = 'Controle de cadastro de Canal';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de Canal</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Novo Canal'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Local</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Local::find()->distinct()->orderBy(['local' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->local;
                                        });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'local_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->local_temp_fk,
                                        'options' => [
                                            'id' => 'local_temp_fk',
                                            'onchange' => '
                                                $.post("/mapacanal/set-session-local", {local_temp_fk: $("#local_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Switch</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Switchdoc::find()->orderBy(['id_switch' => SORT_ASC])->all(), 'id_switch', function($model) {
                                            return $model->id_switch;
                                        });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'switch_temp',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->switch_temp,
                                        'options' => [
                                            'id' => 'switch_temp',
                                            'onchange' => '
                                                $.post("/mapacanal/set-session-switch", {switch_temp: $("#switch_temp").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Patch Panel</label>
                                <?php
                                    $dataModel = $model->array_patch_panel;
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'patch_panel_temp',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->patch_panel_temp,
                                        'options' => [
                                            'id' => 'patch_panel_temp',
                                            'onchange' => '
                                                $.post("/mapacanal/set-session-patchpanel", {patch_panel_temp: $("#patch_panel_temp").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', 'Limpa filtros'), ['clean'], ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'mapacanal',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            [
                                'attribute' => 'unidade_fk',
                                'value' => function ($data) {
                                    return ($data->unidadeFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                            : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidade);
                                },
                            ],
                            [
                                'attribute' => 'local_fk',
                                'value' => function ($data) {
                                    return $data->localFk->local;
                                },
                            ],
                            [
                                'attribute' => 'switch_fk',
                                'value' => function ($data) {
                                    return ($data->switch_fk ? $data->switchFk->id_switch : "");
                                },
                            ],
                            [
                                'attribute' => 'switch_porta',
                                'value' => function ($data) {
                                    return ($data->switch_porta ? $data->switch_porta : "");
                                },
                            ],
                            [
                                'attribute' => 'Ponto',
                                'value' => function ($data) {
                                    return $data->patch_panel . ' / ' . $data->patch_panel_porta;
                                },
                            ],
                            'patch_cord_switch',
                            'link',
                            'patch_cord_dispositivo',
                            'descricao',
                            ['class' => 'common\components\SYSActionColumn'],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>