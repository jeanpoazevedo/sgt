<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\color\ColorInput;
use common\models\Unidade;
use common\models\Local;
use common\models\Switchdoc;

$this->title = 'Mapa de Canal';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('unidade'); ?></label>
                            <input type="text" class="form-control" disabled="" value="
                                <?php echo ($model->unidadeFk->unidade_pai_fk ?
                                    ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                    : $model->unidadeFk->unidade);
                            ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('local'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->localFk->local; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <?php
                            echo $form->field($model, 'switch_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Switchdoc::find()->all(), 'id', function($model) {
                                            return $model->id_switch;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um Swtich'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <?php echo $form->field($model, 'switch_porta')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('patch_panel'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->patch_panel; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('patch_panel_porta'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->patch_panel_porta; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?php echo $form->field($model, 'patch_cord_switch')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $form->field($model, 'link')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $form->field($model, 'patch_cord_dispositivo')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <?php echo $form->field($model, 'descricao')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlMapacanalSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>