<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\select2\Select2;
use common\models\Equipamento;
use common\models\OcorenciaTipo;

$this->title = 'Ocorencia Equipamento';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($model, 'codigo_equipamento_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Equipamento::find()->where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->orderBy(['codigo_equipamento' => SORT_ASC])->all(), 'id', function($model) {
                                        return $model->codigo_equipamento . ' / SN: ' . $model->numero_serie;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione um Código'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($model, 'tipo_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(OcorenciaTipo::find()->orderBy(['tipo' => SORT_ASC])->all(), 'id', function($model) {
                                        return $model->tipo;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione um Tipo'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                            echo $form->field($model, 'url')->widget(FileInput::className(), [
                                'options' => [
                                    'multiple' => false,
                                    'accept' => 'file/pdf'
                                ],
                                'language' => 'pt',
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['pdf'],
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => 'Procurar',
                                    'initialCaption' => "Selecione um arquivo PDF",
                                ]
                            ]);
                            ?>
                        </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $form->field($model, 'descricao')->textArea(['class' => 'form-control', 'rows' => '4', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlOcorenciaequipamentoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>