<?php

use yii\helpers\Html;

$this->title = 'Ocorencia de Equipamento';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('codigo_equipamento_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->codigoequipamentoFk->codigo_equipamento; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('tipo_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->tipoFk->tipo; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('url'); ?></label>
                            <div class="row">
                                <?php echo '<a href="/arquivo/ocorenciaequipamento/' . $model->url . '" target="_blank">' . 'Arquivo' . '</a>'; ?>
                            </div>
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('descricao'); ?></label>
                            <textarea class="form-control" rows="4" disabled=""><?php echo $model->descricao; ?></textarea>
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlOcorenciaequipamentoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                </div>

            </div>
        </div>
    </div>
</div>