<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Equipamento;
use common\models\Setor;

$this->title = 'PON';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-6">
                        <?php
                            echo $form->field($model, 'codigo_onu_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Equipamento::find()->andWhere(['tipo_fk' => '11'])->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->all(), 'id', function($model) {
                                            return $model->codigo_equipamento . ' / SN: ' . $model->numero_serie . ' / MAC: ' . $model->mac;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Equipamento'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                            echo $form->field($model, 'setor_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Setor::find()->all(), 'id', function($model) {
                                            return $model->setor;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Setor'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'porta_pon')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'porta_pon_id')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'onu_profile')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'ponto')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', ['pon/update/'.$olt_id_temp], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>