<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use yii\grid\GridView;
use yii\widgets\Pjax;

use common\models\Equipamento;
use common\models\Local;
use common\models\Setor;

$this->title = 'PON';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'codigo_pon_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Equipamento::find()->andWhere(['tipo_fk' => '10'])->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->all(), 'id', function($model) {
                                            return $model->codigo_equipamento;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Equipamento'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'local_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Local::find()->all(), 'id', function($model) {
                                            return $model->local;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Local'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <?php echo $form->field($model, 'id_pon')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-2">
                        <?php echo $form->field($model, 'numero_portas_pon')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-2">
                        <?php echo $form->field($model, 'numero_portas_bridge')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a('<i class="ion-plus-round"></i> Nova ONU', ['pon/createonu/'.$model->id], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        Pjax::begin([
                            'id' => 'pononu',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                        ]);

                        echo GridView::widget([
                            'dataProvider' => $dataProviderOnu,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'setor_fk',
                                    'value' => function ($data) {
                                        return $data->setorFk->setor;
                                    },
                                ],
                                [
                                    'attribute' => 'codigo_onu_fk',
                                    'value' => function ($data) {
                                        return $data->codigoOnuFk->codigo_equipamento;
                                    },
                                ],
                                [
                                    'attribute' => 'Número de Série',
                                    'value' => function ($data) {
                                        return $data->codigoOnuFk->numero_serie;
                                    },
                                ],
                                [
                                    'attribute' => 'MAC',
                                    'value' => function ($data) {
                                        return $data->codigoOnuFk->mac;
                                    },
                                ],
                                [
                                    'attribute' => 'porta_pon',
                                    'value' => function ($data) {
                                        return $data->porta_pon;
                                    },
                                ],
                                [
                                    'attribute' => 'porta_pon_id',
                                    'value' => function ($data) {
                                        return $data->porta_pon_id;
                                    },
                                ],
                                [
                                    'attribute' => 'onu_profile',
                                    'value' => function ($data) {
                                        return $data->onu_profile;
                                    },
                                ],
                                [
                                    'attribute' => 'ponto',
                                    'value' => function ($data) {
                                        return $data->ponto;
                                    },
                                ],
                                [
                                    'class' => 'common\components\SYSPONActionColumn',
                                    'template' => '{updateonu} {deleteonu}',
                                ],
                            ],
                        ]);
                        Pjax::end();
                        ?>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'running_geral')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'running_bridge')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'running_pon_dba_profile')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'running_pon_extended_profile')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'running_pon_traffic_profile')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'running_pon_onu_profile')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'running_pon_olt')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'conf')->textArea(['class' => 'form-control', 'rows' => '10', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlPonSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>