<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\widgets\MaskedInput;
use kartik\money\MaskMoney;
use common\models\UsuarioUnidade;
use common\models\Setor;
use common\models\Tipo;
use common\models\VwMarcaModelo;

$this->title = 'Equipamento';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function($model) {
                                    return ($model->unidadeFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidade);
                                    }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'setor_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Setor::find()->all(), 'id', function($model) {
                                        return $model->setor;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione um Setor'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $form->field($model, 'tipo_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Tipo::find()->where(['equipamento' => '1'])->orderBy(['tipo' => SORT_ASC])->all(), 'id', function($model) {
                                        return $model->tipo;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione um Tipo'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php
                        echo $form->field($model, 'modelo_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(VwMarcaModelo::find()->where(['tipo' => '3'])->orderBy(['modelo' => SORT_ASC])->orderBy(['marca' => SORT_ASC])->all(), 'id', function($model) {
                                        return $model->marca . ' / ' . $model->modelo;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione Marca e Modelo'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>                                       
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'numero_serie')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'numero_patrimonio')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'data_compra')
                                ->widget(MaskedInput::className(), ['mask' => '99/99/9999',])
                                ->widget(DatePicker::classname(), [
                                'language' => 'pt-BR',
                                'removeButton' => false,
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'orientation' => 'bottom left',
                                    'format' => 'dd/mm/yyyy',
                                ]
                             ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'garantia')
                                ->widget(MaskedInput::className(), ['mask' => '99/99/9999',])
                                ->widget(DatePicker::classname(), [
                                'language' => 'pt-BR',
                                'removeButton' => false,
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'orientation' => 'bottom left',
                                    'format' => 'dd/mm/yyyy',
                                ]
                             ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'mac')->widget(MaskedInput::className(), [
                                'mask' => '**:**:**:**:**:**',
                                'clientOptions' => [
                                    'removeMaskOnSubmit' => true,
                                ]
                            ])->textInput(['class' => 'form-control', 'maxlength' => true]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'netbios')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php echo $form->field($model, 'loja')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                                <?php echo $form->field($model, 'numero_nota')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php
                                    echo $form->field($model, 'valor', [
                                        'template' => '{label}<div class="input-group"><span class="input-group-addon">R$</span>{input}</div>',
                                    ])->textInput([
                                        'class' => 'form-control',
                                        'style' => 'text-align: right;',
                                        'maxlength' => true,
                                        'id' => 'valor-input',
                                        'onkeyup' => 'formatCurrency(this)',
                                    ]);
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?php
                                    echo $form->field($model, 'url')->widget(FileInput::className(), [
                                        'options' => [
                                            'multiple' => false,
                                            'accept' => 'file/pdf'
                                        ],
                                        'language' => 'pt',
                                        'pluginOptions' => [
                                            'allowedFileExtensions' => ['pdf'],
                                            'showPreview' => false,
                                            'showCaption' => true,
                                            'showRemove' => false,
                                            'showUpload' => false,
                                            'browseLabel' => 'Procurar',
                                            'initialCaption' => "Selecione um arquivo PDF",
                                        ]
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <?php
                            echo $form->field($model, 'descricao_equipamento')->textArea(['class' => 'form-control', 'rows' => '5', 'maxlength' => true]);
                        ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlEquipamentoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
    // Define o script JavaScript para aplicar formatação customizada
    $this->registerJs("
        function formatCurrency(input) {
            // Remove todos os caracteres que não são números
            let value = input.value.replace(/[^0-9]/g, '');

            // Formata para incluir centavos sempre (dois últimos dígitos)
            if (value.length <= 2) {
                value = value.padStart(3, '0'); // Garante ao menos '0,00'
            }

            let integerPart = value.slice(0, -2); // Parte inteira
            let decimalPart = value.slice(-2); // Parte decimal (centavos)

            // Remove o zero inicial da parte inteira, se aplicável
            integerPart = integerPart.replace(/^0+/, '');

            // Adiciona pontos para milhares
            integerPart = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, '.');

            // Exibe o valor formatado no campo de entrada
            input.value = integerPart + ',' + decimalPart;
        }
    ", \yii\web\View::POS_END, 'formatacao-valor');
?>