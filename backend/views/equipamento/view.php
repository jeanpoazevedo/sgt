<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dominus77\tinymce\TinyMce;

$this->title = 'Equipamento';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills m-b-30 pull-right">
                    <li class="active">
                        <a href="#geral" data-toggle="tab" aria-expanded="true">Geral</a>
                    </li>
                    <li class="">
                        <a href="#anexo" data-toggle="tab" aria-expanded="true">Anexo</a>
                    </li>
                </ul>
                <div class="tab-content br-n pn">
                    <div id="geral" class="tab-pane active">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-border panel-primary">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('codigo_equipamento'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->codigo_equipamento; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('unidade_fk'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="
                                                        <?php echo ($model->unidadeFk->unidade_pai_fk ?
                                                            ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                                                $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                            : $model->unidadeFk->unidade);
                                                    ?>">
                                                </div>                                       
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('setor_fk'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->setorFk->setor; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('tipo_fk'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->tipoFk->tipo; ?>">
                                                </div>                                        
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label">Marca</label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->modeloFk->marcaFk->marca; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('modelo_fk'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->modeloFk->modelo; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('numero_serie'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_serie; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('numero_patrimonio'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_patrimonio; ?>">
                                                </div>                                        
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('data_compra'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->data_compra; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('garantia'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->garantia; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('mac'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->mac; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('netbios'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->netbios; ?>">
                                                </div>                                        
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('loja'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->loja; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('numero_nota'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->numero_nota; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('valor'); ?></label>
                                                    <input type="text" class="form-control" disabled="" value="<?php echo $model->valor; ?>">
                                                </div>                                        
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('status'); ?></label>
                                                    <input type="checkbox" data-plugin="switchery" disabled="" <?php echo $model->status ? 'CHECKED' : ''; ?>>
                                                </div>                                        
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $model->getAttributeLabel('descricao_equipamento'); ?></label>
                                                     <textarea class="form-control" rows="5" disabled=""><?php echo $model->descricao_equipamento; ?></textarea>
                                                </div>                                        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="anexo" class="tab-pane">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-border panel-primary">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?php
                                                    if ($model->url != NULL) {
                                                ?>
                                                    <div class="form-group">
                                                        <iframe src="/arquivo/equipamento/<?php echo $model->url; ?>" width="100%" height="600px;">
                                                            Este navegador não suporta PDFs. Faça o download do PDF para visualizá-lo: <a href="/arquivo/equipamento/<?php echo $model->url; ?>">Baixar o arquivo PDF</a>
                                                        </iframe>
                                                    </div>
                                                <?php
                                                    } else {
                                                ?>
                                                    <label class="control-label">Sem PDF</label>
                                                <?php
                                                    }
                                                ?>
                                            </div>            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlEquipamentoSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        Pjax::begin([
                            'id' => 'equipamento',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                        ]);

                        echo GridView::widget([
                            'dataProvider' => $dataProviderOcorencia,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'Data',
                                    'value' => function ($data) {
                                        return $data->data;
                                    },
                                ],
                                [
                                    'attribute' => 'Usuário',
                                    'value' => function ($data) {
                                        return $data->usuario_nome;
                                    },
                                ],
                                [
                                    'attribute' => 'Ação',
                                    'value' => function ($data) {
                                        return $data->tipoFk->tipo;
                                    },
                                ],
                                [
                                    'attribute' => 'Descrição',
                                    'value' => function ($data) {
                                        return $data->descricao;
                                    },
                                ],
                            ],
                        ]);
                        Pjax::end();
                        ?>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>