<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\widgets\MaskedInput;
use kartik\money\MaskMoney;
use common\models\UsuarioUnidade;
use common\models\Perfil;

$this->title = 'Senha';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading">
                    <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php
                                $dataModel = ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function($model) {
                                    return ($model->unidadeFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidade);
                                    });
                                $dataArray = [ 1 => "Geral"];
                                echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::merge($dataArray, $dataModel),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            ?>
                        </div>
                        <div class="col-sm-6">
                            <?php
                                if($model_perfil > 6){
                                    echo $form->field($model, 'perfil_fk')->widget(Select2::className(), [
                                        'data' => ArrayHelper::map(Perfil::find()->where(['id' => $model_perfil])->orderBy(['id' => SORT_ASC])->all(), 'id', function($model) {
                                                    return $model->nome;
                                                }),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'options' => [
                                            'prompt' => 'Selecione uma Unidade'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => false
                                        ],
                                    ]);
                                } else {
                                    echo $form->field($model, 'perfil_fk')->widget(Select2::className(), [
                                        'data' => ArrayHelper::map(Perfil::find()->orderBy(['id' => SORT_ASC])->all(), 'id', function($model) {
                                                    return $model->nome;
                                                }),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'options' => [
                                            'prompt' => 'Selecione uma Unidade'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => false
                                        ],
                                    ]);
                                }
                            ?>
                        </div>
                    </div> 
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo $form->field($model, 'descricao')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?php echo $form->field($model, 'usuario')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <?php echo $form->field($model, 'senha')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?php
                                    echo $form->field($model, 'status', ['template' => '{label}<div class="input-group">{input}</div>',])
                                        ->checkbox(['data-plugin' => "switchery"]);
                                ?>
                            </div>                                        
                        </div>
                    </div>

                    <div class="form-group text-right m-b-0">
                        <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlSenhaSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="card-box">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                        Pjax::begin([
                            'id' => 'computador',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                        ]);

                        echo GridView::widget([
                            'dataProvider' => $dataProviderHistorico,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'Data',
                                    'value' => function ($data) {
                                        return $data->data;
                                    },
                                ],
                                [
                                    'attribute' => 'Usuário',
                                    'value' => function ($data) {
                                        return $data->usuario_nome;
                                    },
                                ],
                                [
                                    'attribute' => 'Ação',
                                    'value' => function ($data) {
                                        return $data->tipoFk->tipo;
                                    },
                                ],
                                [
                                    'attribute' => 'Descrição',
                                    'value' => function ($data) {
                                        return $data->descricao;
                                    },
                                ],
                                [
                                    'attribute' => 'Senha Antiga',
                                    'value' => function ($data) {
                                        $data->senha_antiga = base64_decode($data->senha_antiga);
                                        return $data->senha_antiga;
                                    },
                                ],
                            ],
                        ]);
                        Pjax::end();
                    ?>
                </div>                    
            </div>
        </div>
    </div>
</div>