<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use common\models\UsuarioUnidade;
use common\models\Perfil;

$this->title = 'Importa Funcionário para Usuario';

?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'nome')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model_usuario->getAttributeLabel('email'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model_usuario->email; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model_usuario->getAttributeLabel('usuario'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model_usuario->usuario; ?>">
                        </div>                                        
                    </div>  
                    <div class="col-sm-3">
                        <?php echo $form->field($model_usuario, 'senha')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'autofocus' => true, 'onkeyup' => 'javascript:verifica_forca(this.value)']) ?>
                    </div>
                    <div class="col-sm-1">
                        <br>
                        <span id="mostra"></span>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model_usuario->getAttributeLabel('telefone'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model_usuario->telefone; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                <div class="col-sm-3">
                        <?php
                            echo $form->field($model_perfil, 'perfil_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Perfil::find()->orderBy(['nome' => SORT_DESC])->all(), 'id', function($model) {
                                            return $model->nome;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Perfil'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function($model) {
                                    return ($model->unidadeFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidade);
                                    }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model_usuario->getAttributeLabel('help_desk_master'); ?></label>
                            <input type="checkbox" class="form-control" disabled="" <?php echo ($model_usuario->help_desk_master != 0 ? 'CHECKED' : '')?>>
                        </div>                                        
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model_usuario->getAttributeLabel('status'); ?></label>
                            <input type="checkbox" class="form-control" disabled="" <?php echo ($model_usuario->status != 0 ? 'CHECKED' : '')?>>
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlFuncionarioSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Importar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

<?php
    $this->registerJs(" 

            function verifica_forca(senha){
                forca = 0;
                mostra = document.getElementById('mostra');
                if((senha.length > 7) && (senha.length <= 12)){
                    forca += 10;
                }else if((senha.length>12)){
                    forca += 25;
                }
                if(senha.match(/[a-z]+/)){
                    forca += 10;
                }
                if(senha.match(/[A-Z]+/)){
                    forca += 20;
                }
                if(senha.match(/\d+/)){
                    forca += 20;
                }
                if(senha.match(/\W+/)){
                    forca += 25;
                }
                return mostra_res();
                return libera_aplicar();
            }
            function mostra_res(){
                if(forca <=30){
                    mostra.innerHTML = '<span style=\"color:#FF0000;\"> Fraca </span>';
                }else if((forca > 30) && (forca < 60)){
                    mostra.innerHTML = '<span style=\"color:#FFBE00;\"> Boa </span>'; 
                }else if((forca >= 60) && (forca < 85)){
                    mostra.innerHTML = '<span style=\"color:#0000FF;\"> Forte </span>';
                }else {
                    mostra.innerHTML = '<span style=\"color:#00CD00;\"> Excelente </span>';
                }
            }

       ", View::POS_END, 'functions-usuario-password'
    );
?>