<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\Setor;

$this->title = 'Controle de cadastro de Funcionário';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de Funcionários</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Novo Funcionário'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php
                                    if($model_restricao_export){
                                        echo Html::a(Yii::t('app', 'Exportar'), ['export'], ['class' => 'btn btn-success']);
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Setor</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Setor::find()->orderBy(['setor' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->setor;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'setor_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->setor_temp_fk,
                                        'options' => [
                                            'id' => 'setor_temp_fk',
                                            'onchange' => '
                                                $.post("/computador/set-session-setor", {setor_temp_fk: $("#setor_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-5">
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', 'Limpa filtros'), ['clean'], ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'usuarios',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    if ($model_restricao_usuario){
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'unidade_fk',
                                    'value' => function ($data) {
                                        return ($data->unidadeFk->unidade_pai_fk ?
                                            ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                                            : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                            : $data->unidadeFk->unidade);
                                    },
                                ],
                                [
                                    'attribute' => 'setor_fk',
                                    'value' => function ($data) {
                                        return $data->setorFk->setor;
                                    },
                                ],
                                [
                                    'attribute' => 'funcao',
                                    'value' => function ($data) {
                                        return ($data->funcao ? $data->funcao : "");
                                    },
                                ],
                                'nome',
                                [
                                    'attribute' => 'so_contato',
                                    'value' => function ($data) {
                                        return $data->so_contato ? 'Sim' : 'Não';
                                    },
                                ],
                                [
                                    'attribute' => 'status',
                                    'value' => function ($data) {
                                        return $data->status ? 'Sim' : 'Não';
                                    },
                                ],
                                ['class' => 'common\components\SYSFuncionarioActionColumn',
                                    'template' =>  '{view} {update} {delete} {inportuser}',
                                ],
                            ],
                        ]);
                    } else {
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                [
                                    'attribute' => 'unidade_fk',
                                    'value' => function ($data) {
                                        return ($data->unidadeFk->unidade_pai_fk ?
                                            ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                                            : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                                : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                            : $data->unidadeFk->unidade);
                                    },
                                ],
                                [
                                    'attribute' => 'setor_fk',
                                    'value' => function ($data) {
                                        return $data->setorFk->setor;
                                    },
                                ],
                                [
                                    'attribute' => 'funcao',
                                    'value' => function ($data) {
                                        return ($data->funcao ? $data->funcao : "");
                                    },
                                ],
                                'nome',
                                [
                                    'attribute' => 'so_contato',
                                    'value' => function ($data) {
                                        return $data->so_contato ? 'Sim' : 'Não';
                                    },
                                ],
                                [
                                    'attribute' => 'status',
                                    'value' => function ($data) {
                                        return $data->status ? 'Sim' : 'Não';
                                    },
                                ],
                                ['class' => 'common\components\SYSFuncionarioActionColumn',
                                    'template' =>  '{view} {update} {delete}',
                                ],
                            ],
                        ]);
                    }
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>