<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;
use common\models\UsuarioUnidade;
use common\models\Setor;

$this->title = 'Funcionario';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(UsuarioUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1'])->all(), 'unidade_fk', function($model) {
                                    return ($model->unidadeFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidade);
                                    }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Unidade'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                        echo $form->field($model, 'setor_fk')->widget(Select2::className(), [
                            'data' => ArrayHelper::map(Setor::find()->orderBy(['setor' => SORT_ASC])->all(), 'id', function($model) {
                                        return $model->setor;
                                    }),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'prompt' => 'Selecione um Setor'
                            ],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'nome')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <?php echo $form->field($model, 'funcao')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'data_nascimento')
                                ->widget(MaskedInput::className(), ['mask' => '99/99/9999',])
                                ->widget(DatePicker::classname(), [
                                'language' => 'pt-BR',
                                'removeButton' => false,
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'orientation' => 'bottom left',
                                    'format' => 'dd/mm/yyyy',
                                ]
                             ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'email_1')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'email_2')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'telefone')->widget(MaskedInput::className(), [
                                'mask' => '(99) 9999-9999',
                                'clientOptions' => [
                                    'removeMaskOnSubmit' => true,
                                ]
                            ])->textInput(['class' => 'form-control', 'maxlength' => true]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            echo $form->field($model, 'celular')->widget(MaskedInput::className(), [
                                'mask' => '(99) 99999-9999',
                                'clientOptions' => [
                                    'removeMaskOnSubmit' => true,
                                ]
                            ])->textInput(['class' => 'form-control', 'maxlength' => true]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php
                                echo $form->field($model, 'so_contato', ['template' => '{label}<div class="input-group">{input}</div>',])
                                    ->checkbox(['data-plugin' => "switchery"]);
                            ?>
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php
                                echo $form->field($model, 'status', ['template' => '{label}<div class="input-group">{input}</div>',])
                                    ->checkbox(['data-plugin' => "switchery"]);
                            ?>
                        </div>                                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $form->field($model, 'nota')->textArea(['class' => 'form-control', 'rows' => '5', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlFuncionarioSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>