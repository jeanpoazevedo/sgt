<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\models\Marca;

$this->title = 'Controle de cadastro de Processador';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de Processador</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Novo Processador'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Marca</label>
                                <?php
                                    $dataModel = ArrayHelper::map(Marca::find()->orWhere(['processador' => '1'])->orderBy(['marca' => SORT_ASC])->all(), 'id', function($model) {
                                                return $model->marca;
                                            });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'marca_temp_fk',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => Yii::$app->user->identity->marca_temp_fk,
                                        'options' => [
                                            'id' => 'marca_temp_fk',
                                            'onchange' => '
                                                $.post("/processador/set-session-marca", {marca_temp_fk: $("#marca_temp_fk").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-8"></div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'processador',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            'marca',
                            'processador',
                            'descricao_processador',
                            [
                                'attribute' => 'Pontos',
                                'value' => function ($data) {

                                    $banda_memoria = $data->processadorFk->banda_memoria;
                                    $frequencia = $data->processadorFk->frequencia;

                                    if (!is_numeric($banda_memoria)) {
                                        $banda_memoria = (float) $banda_memoria;
                                    }

                                    if (!is_numeric($frequencia)) {
                                        $frequencia = (float) $frequencia;
                                    }
                                    //Coeficientes do calculo
                                    $BandaProcessador = 0.35;
                                    $FrequenciaProcessador = 0.2;
                                    $CoresProcessador = 0.15;
                                    $CacheProcessador = 0.1;

                                    // Calculando o score
                                    $score = ($banda_memoria * $BandaProcessador) +
                                            ($frequencia * $FrequenciaProcessador) +
                                            ($data->processadorFk->total_core * $CoresProcessador) +
                                            ($data->processadorFk->cache * $CacheProcessador);

                                    $score_formatado = number_format($score, 2, ',', '.');

                                    return ($score_formatado);
                                },
                            ],
                            ['class' => 'common\components\SYSActionColumn'],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>