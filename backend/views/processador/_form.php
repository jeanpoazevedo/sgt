<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\money\MaskMoney;
use common\models\Marca;
use common\models\TipoProcessador;

$this->title = 'Processador';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'marca_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(Marca::find()->where(['processador' => '1'])->orderBy(['marca' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->marca;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Marca'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'tipo_processador_fk')->widget(Select2::className(), [
                                'data' => ArrayHelper::map(TipoProcessador::find()->orderBy(['tipo' => SORT_ASC])->all(), 'id', function($model) {
                                            return $model->tipo;
                                        }),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione uma Tipo de Processador'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'processador')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'total_core')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'frequencia', [
                                'template' => '{label}<div class="input-group">{input}<span class="input-group-addon">GHz</span></div>',
                            ])->widget(MaskMoney::className(), [
                                'pluginOptions' => [
                                    'allowNegative' => true,
                                    'thousands' => '.',
                                    'decimal' => ',',
                                    'affixesStay' => true,
                                    'removeMaskOnSubmit' => true,
                                ],
                            ])->textInput(['class' => 'form-control', 'style' => 'text-align: right;', 'maxlength' => true]);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            echo $form->field($model, 'cache', [
                                'template' => '{label}<div class="input-group">{input}<span class="input-group-addon">MB</span></div>',
                            ])->widget(MaskMoney::className(), [
                                'pluginOptions' => [
                                    'allowNegative' => true,
                                    'thousands' => '.',
                                    'decimal' => ',',
                                    'affixesStay' => true,
                                    'removeMaskOnSubmit' => true,
                                ],
                            ])->textInput(['class' => 'form-control', 'style' => 'text-align: right;', 'maxlength' => true]);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                            echo $form->field($model, 'banda_memoria', [
                                'template' => '{label}<div class="input-group">{input}<span class="input-group-addon">GB/s</span></div>',
                            ])->widget(MaskMoney::className(), [
                                'pluginOptions' => [
                                    'allowNegative' => true,
                                    'thousands' => '.',
                                    'decimal' => ',',
                                    'affixesStay' => true,
                                    'removeMaskOnSubmit' => true,
                                ],
                            ])->textInput(['class' => 'form-control', 'style' => 'text-align: right;', 'maxlength' => true]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $form->field($model, 'descricao_processador')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlProcessadorSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>