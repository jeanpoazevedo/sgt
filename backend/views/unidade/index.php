<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Controle de cadastro de Unidade';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title">Lista de Unidade</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Nova Unidade'), ['create'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    Pjax::begin([
                        'id' => 'unidade',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]);

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                        'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                        'columns' => [
                            'nivel',
                            [
                                'attribute' => 'unidade',
                                'value' => function ($data) {
                                    return ($data->unidade_pai_fk ?
                                            ($data->unidadePaiFk->unidade_pai_fk ?
                                            ($data->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                            $data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidade . " - " . $data->unidade
                                                            : $data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidade . " - " . $data->unidade)
                                                        : $data->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidade . " - " . $data->unidade)
                                                    : $data->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadePaiFk->unidade . " - " . $data->unidade)
                                            : $data->unidadePaiFk->unidade . " - " . $data->unidade)
                                        : $data->unidade);
                                },
                            ],
                            'dominio',
                            [
                                'attribute' => 'ddd',
                                'value' => function ($data) {
                                    return ($data->ddd ? $data->ddd : "");
                                },
                            ],
                            'descricao_unidade',
                            ['class' => 'common\components\SYSActionColumn'],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>