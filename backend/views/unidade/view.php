<?php

use yii\helpers\Html;

$this->title = 'Unidade';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('unidade'); ?></label>
                            <input type="text" class="form-control" disabled="" value="
                                <?php echo ($model->unidade_pai_fk ?
                                    ($model->unidadePaiFk->unidade_pai_fk ?
                                        ($model->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                ($model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                    ($model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                        $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade
                                                        : $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                                    : $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                                : $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                        : $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                    : $model->unidade);
                            ?>">
                        </div>                                       
                    </div> 
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('ddd'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->ddd; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('dominio'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->dominio; ?>">
                        </div>                                        
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('descricao_unidade'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->descricao_unidade; ?>">
                        </div>                                        
                    </div>           
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('nome_patrimonio1'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->nome_patrimonio1; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('email_patrimonio1'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->email_patrimonio1; ?>">
                        </div>                                        
                    </div>            
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('nome_patrimonio2'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->nome_patrimonio1; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('email_patrimonio2'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->email_patrimonio1; ?>">
                        </div>                                        
                    </div>            
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlUnidadeSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>