<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use common\models\Unidade;
use common\models\UsuarioGerenteUnidade;

$this->title = 'Unidade';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="m-t-0 header-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $form->field($model, 'unidade')->textInput(['class' => 'form-control', 'maxlength' => true, 'autofocus' => true]) ?>
                    </div>
                    <div class="col-sm-2">
                        <?php
                            echo $form->field($model, 'ddd')->widget(MaskedInput::className(), [
                                'mask' => '999',
                                'clientOptions' => [
                                    'removeMaskOnSubmit' => true,
                                ]
                            ])->textInput(['class' => 'form-control', 'maxlength' => true]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php
                                echo $form->field($model, 'entidade', ['template' => '{label}<div class="input-group">{input}</div>',])
                                    ->checkbox(['data-plugin' => "switchery"]);
                            ?>
                        </div>                                        
                    </div>
                    <div class="col-sm-2">
                        <?php
                            echo $form->field($model, 'nivel')->widget(Select2::className(), [
                                'data' => $model->array_nivel,
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'prompt' => 'Selecione um nivel'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            if ($model_perfil->perfil_fk == 1){
                                echo $form->field($model, 'unidade_pai_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Unidade::find()->Where(['entidade' => 0])->all(), 'id', function($model) {
                                        return ($model->unidade_pai_fk ?
                                            ($model->unidadePaiFk->unidade_pai_fk ?
                                                ($model->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                            ($model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                                $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade
                                                                : $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                                            : $model->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                                        : $model->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                                : $model->unidadePaiFk->unidade . " - " . $model->unidade)
                                            : $model->unidade);
                                        }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else {
                                echo $form->field($model, 'unidade_pai_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(UsuarioGerenteUnidade::find()->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['gerencia' => 1])->all(), 'id', function($model) {
                                                return ($model->unidadeFk->unidade_pai_fk ?
                                                    ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                                        $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                                : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                        : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                                    : $model->unidadeFk->unidade);
                                                }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'dominio')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'descricao_unidade')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'nome_patrimonio1')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'email_patrimonio1')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'nome_patrimonio2')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'email_patrimonio2')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlUnidadeSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>