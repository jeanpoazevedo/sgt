<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\HdCadastro;
use common\models\HdHistorico;
use common\models\HdEtapa;
use common\models\HdProduto;
use common\models\HdAnexo;
use common\models\Usuario;
use common\models\UsuarioPerfil;
use common\models\VwHelpdesk;
use common\components\SendMail;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\UploadedFile;
use kartik\date\DatePicker;
use yii\helpers\FileHelper;

class BuscarController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'atender', 'view'],
                'rules' => AccessRulesControl::getRulesControl('buscar'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwHelpdesk();

        $dataProvider = $searchModel->searchbuscar(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlBuscarSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);

        $model_historico = new HdHistorico();
        $model_etapas = new HdEtapa();
        $model_anexo = new HdAnexo();
        $model_anexo_historico = new HdAnexo();
        $params['HdHistorico']['chamado_fk'] = $model->id;
        $dataProviderHistorico = $model_historico->search($params);
        $params['HdAnexo']['chamado_fk'] = $model->id;
        $dataProviderAnexoHistorico = $model_anexo_historico->search($params);
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $usuariohdmaster = Usuario::find()->where(['id' => Yii::$app->user->identity->id])->one();
        
        if ($model->ver != '1' && $model->de_fk != Yii::$app->user->identity->id && $model->para_fk != Yii::$app->user->identity->id && $usuariohdmaster->help_desk_master != 1){
            Yii::$app->session->setFlash('danger', 'Ops. Este chamado não está disponivel!');
            return $this->redirect([Yii::$app->session->get('urlBuscarSearch', array())]);
        }
        
        return $this->render('view', [
            'title' => 'Detalhes do Chamado',
            'model' => $model,
            'model_historio' => $model_historico,
            'model_etapas' => $model_etapas,
            'usuarioperfil' => $usuarioperfil,
            'array_item' => $model_etapas->renderiza(HdEtapa::find()->where(['chamado_fk' => $model->id])->all()),
            'array_item_anexo' => $model_anexo_historico->renderiza(HdAnexo::find()->where(['chamado_fk' => $model->id])->all()),
            'dataProviderHistorico' => $dataProviderHistorico,
            'dataProviderAnexoHistorico' => $dataProviderAnexoHistorico,
            'model_anexo' => $model_anexo,
        ]);
    }

    public function actionAtender($id) {
        $model = $this->findModel($id);
        $model_historico = new HdHistorico();
        $model_etapas = new HDEtapa();
        $model_anexo = new HDAnexo();
        $model_anexo_historico = new HDAnexo();
        $params['HdHistorico']['chamado_fk'] = $model->id;
        $dataProviderHistorico = $model_historico->search($params);
        $params['HdAnexo']['chamado_fk'] = $model->id;
        $dataProviderAnexoHistorico = $model_anexo_historico->search($params);
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $usuariohdmaster = Usuario::find()->where(['id' => Yii::$app->user->identity->id])->one();

        if ($model->ver != '1' && $model->de_fk != Yii::$app->user->identity->id && $model->para_fk != Yii::$app->user->identity->id && $usuariohdmaster->help_desk_master != 1){
            Yii::$app->session->setFlash('danger', 'Ops. Este chamado não está disponivel!');
            return $this->redirect([Yii::$app->session->get('urlBuscarSearch', array())]);
        }
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->status == 4){
                    $model->data_encerrado = date('d/m/Y H:i:s');
                }
                $request = Yii::$app->request->post();
                $arquivo = UploadedFile::getInstance($model_anexo, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/hdanexo');
                $gravar = false;
                if ($arquivo) {
                    $model_anexo->url = $model->id . "_" . date('YmdHis') . "." . $arquivo->extension;
                    $gravar = true;
                } else {
                    $model_anexo->url = NULL;
                }
                if ($gravar) {
                    $model_anexo->chamado_fk = $model->id;
                    $model_anexo->de_fk = Yii::$app->user->identity->id;
                    $model_anexo->data = date('d/m/Y H:i:s');
                    $model_anexo->descricao_anexo = $request['HDAnexo']['descricao_anexo'];
                    if ($model_anexo->save()) {
                        $arquivo->saveAs($directory . 'arquivo/hdanexo/' . $model_anexo->url);
                    }
                }
                if ($model_etapas->load(Yii::$app->request->post())) {
                    $model_anterior = $this->findModel($id);
                    $html_desc = strpos($model_etapas->descricao, '<p>');
                    if ($model->status != 4) {
                        if ($html_desc !== false) {
                            $model->status = 2;
                        }
                    }
                    if ($model_anterior->produto_fk != $model->produto_fk) {
                        $model_historico->chamado_fk = $model->id;
                        $model_historico->de_fk = Yii::$app->user->identity->id;
                        $model_historico->data = date('d/m/Y H:i:s');
                        $model_historico->acao = 'Produto foi alterado!';
                        $model_historico->alteracao = $model_anterior->produtoFk->produto.' > '.$model->produtoFk->produto;
                        if ($model_historico->save()){
                            if ($model_historico->de_fk != $model->de_fk){
                                if ($model_historico->de_fk != $model->para_fk){
                                    $emails = [
                                        $model->deFk->email,
                                        $model->paraFk->email
                                    ];
                                } else {
                                    $emails = [
                                        $model->deFk->email
                                    ];
                                }
                            } else {
                                $emails = [
                                    $model->paraFk->email
                                ];
                            }
                            SendMail::submithd($emails, $model, $model_anterior, $model_historico, 'sgt_email_hd_produto_html');
                        }
                    }
                    if ($model_anterior->prioridade != $model->prioridade) {
                        $model_historico->chamado_fk = $model->id;
                        $model_historico->de_fk = Yii::$app->user->identity->id;
                        $model_historico->data = date('d/m/Y H:i:s');
                        $model_historico->acao = 'Prioridade foi alterado!';
                        $model_historico->alteracao = $model->array_prioridade[$model_anterior->prioridade].' > '.$model->array_prioridade[$model->prioridade];
                        if ($model_historico->save()){
                            if ($model_historico->de_fk != $model->de_fk){
                                if ($model_historico->de_fk != $model->para_fk){
                                    $emails = [
                                        $model->deFk->email,
                                        $model->paraFk->email
                                    ];
                                } else {
                                    $emails = [
                                        $model->deFk->email
                                    ];
                                }
                            } else {
                                $emails = [
                                    $model->paraFk->email
                                ];
                            }
                            SendMail::submithd($emails, $model, $model_anterior, $model_historico, 'sgt_email_hd_prioridade_html');
                        }
                    }
                    if ($model_anterior->para_fk != $model->para_fk) {
                        $model_historico->chamado_fk = $model->id;
                        $model_historico->de_fk = Yii::$app->user->identity->id;
                        $model_historico->data = date('d/m/Y H:i:s');
                        $model_historico->acao = 'Foi atribuido para outro!';
                        $model_historico->alteracao = $model_anterior->paraFk->nome.' > '.$model->paraFk->nome;
                        if ($model_historico->save()){
                            if ($model_historico->de_fk != $model->de_fk){
                                if ($model_historico->de_fk != $model->para_fk){
                                    $emails = [
                                        $model->deFk->email,
                                        $model->paraFk->email,
                                        $model_anterior->paraFk->email
                                    ];
                                } else {
                                    $emails = [
                                        $model->deFk->email,
                                        $model_anterior->paraFk->email
                                    ];
                                }
                            } else {
                                $emails = [
                                    $model->paraFk->email,
                                    $model_anterior->paraFk->email
                                ];
                            }
                            SendMail::submithd($emails, $model, $model_anterior, $model_anterior, $model_historico, 'sgt_email_hd_para_html');
                        }
                    }
                    if ($model_anterior->ver != $model->ver) {
                        $model_historico->chamado_fk = $model->id;
                        $model_historico->de_fk = Yii::$app->user->identity->id;
                        $model_historico->data = date('d/m/Y H:i:s');
                        $model_historico->acao = 'Status de visibilidade foram alterados!';
                        $model_historico->alteracao = $model->ver ? 'Privado > Publico' : 'Publico > Privado';
                        if ($model_historico->save()){
                            if ($model_historico->de_fk != $model->de_fk){
                                if ($model_historico->de_fk != $model->para_fk){
                                    $emails = [
                                        $model->deFk->email,
                                        $model->paraFk->email
                                    ];
                                } else {
                                    $emails = [
                                        $model->deFk->email
                                    ];
                                }
                            } else {
                                $emails = [
                                    $model->paraFk->email
                                ];
                            }
                            SendMail::submithd($emails, $model, $model_anterior, $model_historico, 'sgt_email_hd_publico_html');
                        }
                    }
                    if ($model_anterior->status != $model->status) {
                        $model_historico->chamado_fk = $model->id;
                        $model_historico->de_fk = Yii::$app->user->identity->id;
                        $model_historico->data = date('d/m/Y H:i:s');
                        $model_historico->acao = 'Status do chamado foram alterados!';
                        $model_historico->alteracao = $model->array_status[$model_anterior->status].' > '.$model->array_status[$model->status];
                        if ($model_historico->save()){
                            if ($model_historico->de_fk != $model->de_fk){
                                if ($model_historico->de_fk != $model->para_fk){
                                    $emails = [
                                        $model->deFk->email,
                                        $model->paraFk->email
                                    ];
                                } else {
                                    $emails = [
                                        $model->deFk->email
                                    ];
                                }
                            } else {
                                $emails = [
                                    $model->paraFk->email
                                ];
                            }
                            SendMail::submithd($emails, $model, $model_anterior, $model_historico, 'sgt_email_hd_status_html');
                        }
                    }
                    if ($model->save() && $html_desc !== true) {
                        $model_etapas->chamado_fk = $model->id;
                        $model_etapas->de_fk = Yii::$app->user->identity->id;
                        $model_etapas->data = date('d/m/Y H:i:s');
                        if ($model_etapas->save()) {
                            if ($model_etapas->de_fk != $model->de_fk){
                                if ($model_etapas->de_fk != $model->para_fk){
                                    $emails = [
                                        $model->deFk->email,
                                        $model->paraFk->email
                                    ];
                                } else {
                                    $emails = [
                                        $model->deFk->email
                                    ];
                                }
                            } else {
                                $emails = [
                                    $model->paraFk->email
                                ];
                            }
                            SendMail::submithdetapa($emails, $model, $model_anterior, $model_etapas, 'sgt_email_hd_atendimento_html');
                            Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                            return $this->redirect([Yii::$app->session->get('urlBuscarSearch', array())]);
                        }
                    }
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Atender Chamado',
                    'model' => $model,
                    'model_historico' => $model_historico,
                    'model_etapas' => $model_etapas,
                    'usuarioperfil' => $usuarioperfil,
                    'array_item' => $model_etapas->renderiza(HDEtapa::find()->where(['chamado_fk' => $model->id])->all()),
                    'array_item_anexo' => $model_anexo_historico->renderiza(HdAnexo::find()->where(['chamado_fk' => $model->id])->all()),
                    'dataProviderHistorico' => $dataProviderHistorico,
                    'dataProviderAnexoHistorico' => $dataProviderAnexoHistorico,
                    'model_anexo' => $model_anexo,
        ]);
    }

    protected function findModel($id) {
        if (($model = HDCadastro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionSetSessionProduto() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->produto_temp_fk = $request['produto_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->produto_temp_fk = $request['produto_temp_fk'];
    }

}
