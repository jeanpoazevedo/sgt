<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\PatchPanel;
use common\models\VwPatchPanel;
use common\models\Vlan;
use common\components\XML\Patchpanelxml;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Json;

class PatchpanelController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => AccessRulesControl::getRulesControl('patchpanel'),
            ],
        ];
    }
    public function actionIndex() {
        $searchModel = new VwPatchPanel();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlPatchPanelSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes da Patch Panel',
                    'model' => $this->findModel($id),
        ]);
    }
    public function actionCreate() {
        $model = new PatchPanel();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $request = Yii::$app->request->post();
                $array_portas = [];
                for ($i = 0; $i < $request['PatchPanel']['numero_portas']; $i++) {
                    $array_portas[] = [
                        'porta' => $i + 1,
                        'status' => $request['status_' . $i],
                        'descricao' => $request['descricao_' . $i]
                    ];
                }
                $model->xml_portas = Patchpanelxml::generationXML($array_portas);
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlPatchPanelSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar nova Patch Panel',
                    'model' => $model,
        ]);
    }
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $request = Yii::$app->request->post();
                $array_portas = [];
                for ($i = 0; $i < $model->numero_portas; $i++) {
                    $array_portas[] = [
                        'porta' => $i + 1,
                        'status' => isset($request['status_' . $i]) ? ($request['status_' . $i] == 'on' ? '1' : '0') : '0',
                        'descricao' => $request['descricao_' . $i]
                    ];
                }
                $model->xml_portas = Patchpanelxml::generationXML($array_portas);
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlPatchPanelSearch', array())]);
                }
            }
        }
        return $this->render('_form_edit', [
                    'title' => 'Alterar Patch Panel',
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlPatchPanelSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = PatchPanel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionGetPorts() {
        $request = Yii::$app->request->post();
        return $this->renderPartial('_lines_ports', [
                    'request' => $request,
                    'model' => new PatchPanel(),
        ]);
    }

}
