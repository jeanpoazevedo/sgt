<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\VwRelatorioFuncionarioMonitor;
use common\models\Usuario;
use common\models\UsuarioUnidade;
use common\models\VwRestricaoMenu;
use common\components\Setup;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii2tech\spreadsheet\Spreadsheet;
use yii\data\ActiveDataProvider;

class RelatoriofuncionariomonitorController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'export', 'clean'],
                'rules' => AccessRulesControl::getRulesControl('relatoriofuncionariomonitor'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwRelatorioFuncionarioMonitor();

        $model_restricao_export = VwRestricaoMenu::findAll(['controller' => 'relatoriofuncionariomonitor', 'restricao' => 'export', 'usuario_fk' => Yii::$app->user->getId()]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlRelatoriofuncionariomonitorSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model_restricao_export' => $model_restricao_export,
        ]);
    }

    public function actionExport() {
        $query = VwRelatorioFuncionarioMonitor::find();
        $export = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
            ]),
            'columns' => [
                [
                    'attribute' => 'unidade_fk',
                    'value' => function ($data) {
                        return ($data->unidadeFk->unidade_pai_fk ?
                            ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                            : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                            : $data->unidadeFk->unidade);
                    },
                ],
                'funcionaroio_setor',
                'funcao',
                'nome',
                [
                    'attribute' => 'codigo_monitor',
                    'value' => function ($data) {
                        return ($data->codigo_monitor ? $data->codigo_monitor : "");
                    },
                ],
                [
                    'attribute' => 'monitor_unidade_fk',
                    'value' => function ($data) {
                        return ($data->codigo_monitor ?
                            ($data->monitorunidadeFk->unidade_pai_fk ?
                                ($data->monitorunidadeFk->unidadePaiFk->unidade_pai_fk ?
                                    ($data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                            ($data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidade
                                                : $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidade)
                                            : $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidade)
                                        : $data->monitorunidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidade)
                                    : $data->monitorunidadeFk->unidadePaiFk->unidade . " - " . $data->monitorunidadeFk->unidade)
                                : $data->monitorunidadeFk->unidade)
                            : "" );
                    },
                ],
                [
                    'attribute' => 'monitor_setor',
                    'value' => function ($data) {
                        return ($data->monitor_setor ? $data->monitor_setor : "");
                    },
                ],
                [
                    'attribute' => 'tipo',
                    'value' => function ($data) {
                        return ($data->tipo ? $data->tipo : "");
                    },
                ],
                [
                    'attribute' => 'marca',
                    'value' => function ($data) {
                        return ($data->marca ? $data->marca : "");
                    },
                ],
                [
                    'attribute' => 'modelo',
                    'value' => function ($data) {
                        return ($data->modelo ? $data->modelo : "");
                    },
                ],
                [
                    'attribute' => 'numero_serie',
                    'value' => function ($data) {
                        return ($data->numero_serie ? $data->numero_serie : "");
                    },
                ],
                [
                    'attribute' => 'netbios',
                    'value' => function ($data) {
                        return ($data->netbios ? $data->netbios : "");
                    },
                ],
            ],
        ]);

                    
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        if (Yii::$app->user->identity->setor_temp_fk) {
            $query->andFilterWhere(['=', 'funcionaroio_setor_fk', Yii::$app->user->identity->setor_temp_fk]);
        }
        
        if (Yii::$app->user->identity->tipo_temp_fk) {
            $query->andFilterWhere(['=', 'tipo_fk', Yii::$app->user->identity->tipo_temp_fk]);
        }
        
        if (Yii::$app->user->identity->marca_temp_fk) {
            $query->andFilterWhere(['=', 'marca_fk', Yii::$app->user->identity->marca_temp_fk]);
        }
        
        if (Yii::$app->user->identity->modelo_temp_fk) {
            $query->andFilterWhere(['=', 'modelo_fk', Yii::$app->user->identity->modelo_temp_fk]);
        }
        
        return $export->send('relatorio_funcionario_monitor.xls');
    }

    public function actionSetSessionSetor() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->setor_temp_fk = $request['setor_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->setor_temp_fk = $request['setor_temp_fk'];
    }
    
    public function actionSetSessionTipo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->tipo_temp_fk = $request['tipo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->tipo_temp_fk = $request['tipo_temp_fk'];
    }
    
    public function actionSetSessionMarca() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->marca_temp_fk = $request['marca_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->marca_temp_fk = $request['marca_temp_fk'];
    }
    
    public function actionSetSessionModelo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->modelo_temp_fk = $request['modelo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->modelo_temp_fk = $request['modelo_temp_fk'];
    }

    public function actionSetSessionFuncao() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->funcao_temp = $request['funcao_temp'];
            $model->save();
        }
        Yii::$app->user->identity->funcao_temp = $request['funcao_temp'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->setor_temp_fk = NULL;
            $model->funcao_temp = NULL;
            $model->tipo_temp_fk = NULL;
            $model->marca_temp_fk = NULL;
            $model->modelo_temp_fk = NULL;
            $model->save();
        }
        return $this->redirect([Yii::$app->session->get('urlRelatoriofuncionariomonitorSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = VwRelatorioFuncionarioMonitor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
