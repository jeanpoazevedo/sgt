<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\Funcionario;
use common\models\VwRestricaoMenu;
use common\models\Voip;
use common\models\Usuario;
use common\models\UsuarioPerfil;
use common\models\UsuarioUnidade;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii2tech\spreadsheet\Spreadsheet;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class FuncionarioController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'inportuser', 'export'],
                'rules' => AccessRulesControl::getRulesControl('funcionario'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Funcionario();
        
        $model_restricao_export = VwRestricaoMenu::findAll(['controller' => 'funcionario', 'restricao' => 'export', 'usuario_fk' => Yii::$app->user->getId()]);
        $model_restricao_usuario = VwRestricaoMenu::findAll(['controller' => 'funcionario', 'restricao' => 'inportuser', 'usuario_fk' => Yii::$app->user->getId()]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlFuncionarioSearch', Yii::$app->request->url);
        return $this->render('index', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_restricao_export' => $model_restricao_export,
            'model_restricao_usuario' => $model_restricao_usuario,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes do Funcionário',
                    'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCreate() {
        $model = new Funcionario();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlFuncionarioSearch', array())]);
                }
            } else {
                $model->status = true;
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar novo Usuario',
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlFuncionarioSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Usuario',
                    'model' => $model,
        ]);
    }
    
    public function actionDelete($id) {
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        if($usuarioperfil->perfil_fk != 1){
            try {
                $model = $this->findModel($id);
                $model->status = 0;
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
                } else {
                    Yii::$app->session->setFlash('error', 'Não foi possivel remover!');
                }
            } catch (IntegrityException $e) {
                if ($e->getCode() == 23503) {
                    $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
                } else {
                    $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
                }
                Yii::$app->session->setFlash('error', $mensagem);
            }
        } else {
            try {
                $this->findModel($id)->delete();
                Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
            } catch (IntegrityException $e) {
                if ($e->getCode() == 23503) {
                    $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
                } else {
                    $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
                }
                Yii::$app->session->setFlash('error', $mensagem);
            }
        }
        return $this->redirect([Yii::$app->session->get('urlFuncionarioSearch', array())]);
    }
    
    public function actionExport() {
        $query = Funcionario::find();
        $export = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
            ]),
            'columns' => [
                'nome',
                [
                    'attribute' => 'unidade_fk',
                    'value' => function ($data) {
                        return ($data->unidadeFk->unidade_pai_fk ?
                            ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                            : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                            : $data->unidadeFk->unidade);
                    },
                ],
                [
                    'attribute' => 'setor_fk',
                    'value' => function ($data) {
                        return $data->setorFk->setor;
                    },
                ],
                'funcao',
                [
                    'attribute' => 'ramal',
                    'value' => function ($data) {
                        $ramal = Voip::find()->andwhere(['funcionario_fk' => $data->id])->andWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->one();
                        return ($ramal ? "(" . $ramal->unidadeFk->ddd . ") " . $ramal->voip : "");
                    },
                ],
                [
                    'attribute' => 'email_1',
                    'value' => function ($data) {
                        return ($data->email_1 ? $data->email_1 : "");
                    },
                ],
                [
                    'attribute' => 'email_2',
                    'value' => function ($data) {
                        return ($data->email_2 ? $data->email_2 : "");
                    },
                ],
                [
                    'attribute' => 'telefone',
                    'value' => function ($data) {
                        return ($data->telefone ? $data->telefone : "");
                    },
                ],
                [
                    'attribute' => 'celular',
                    'value' => function ($data) {
                        return ($data->celular ? $data->celular : "");
                    },
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($data) {
                        return $data->status ? 'Sim' : 'Não';
                    },
                ],
                [
                    'attribute' => 'so_contato',
                    'value' => function ($data) {
                        return $data->so_contato ? 'Sim' : 'Não';
                    },
                ],
                [
                    'attribute' => 'nota',
                    'value' => function ($data) {
                        return ($data->nota ? $data->nota : "");
                    },
                ],
            ],
        ]);

        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }

        return $export->send('funcionarios.xls');
    }

    public function actionInportuser($id) {
        $model = $this->findModel($id);

        $model_restricao_usuario = VwRestricaoMenu::findAll(['controller' => 'funcionario', 'restricao' => 'inportuser', 'usuario_fk' => Yii::$app->user->getId()]);

        if($model_restricao_usuario){
            $model_usuario = new Usuario();
            $model_perfil = new UsuarioPerfil();

            if (Yii::$app->request->isAjax) {
                if ($model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            } else {
                if ($model->load(Yii::$app->request->post())) {
                    $model_usuario->load(Yii::$app->request->post());
                    $model_usuario->nome = $model->nome;
                    $model_usuario->unidade_fk = $model->unidade_fk;
                    $model_usuario->email = $model->email_1;
                    $email =  $model_usuario->email;
                    $partes = explode("@", $email);
                    if (count($partes) == 2) {
                        $model_usuario->usuario = $partes[0];
                    }
                    $model_usuario->help_desk_master = 0;
                    $model_usuario->status = 1;
                    $model_usuario->trocar_senha = 1;
                    $model_usuario->senha = Yii::$app->security->generatePasswordHash($model_usuario->senha);
                    $model_usuario->senha_hash = $model_usuario->senha;
                    $model_usuario->unidade_temp_fk = $model_usuario->unidade_fk;
                    $model_usuario->auth_key = Yii::$app->security->generateRandomString();
                    $model_usuario->token = Yii::$app->security->generateRandomString();
                    if ($model_usuario->save()) {
                        $model_perfil->load(Yii::$app->request->post());
                        $model_perfil->usuario_fk = $model_usuario->id;
                        if ($model_perfil->save()) {
                            if ($model->save()) {
                                Yii::$app->session->setFlash('success', 'Usuário importado com sucesso!');
                                return $this->redirect([Yii::$app->session->get('urlFuncionarioSearch', array())]);
                            }
                        }
                    }
                } else {
                    $model_usuario->email = $model->email_1;
                    $model_usuario->telefone = $model->celular;
                    $model_perfil->perfil_fk = 7;
                    $model_usuario->help_desk_master = 0;
                    $model_usuario->status = 1;
                    $email =  $model->email_1;
            
                    $email =  $model->email_1;
                    $partes = explode("@", $email);
                    if (count($partes) == 2) {
                        $model_usuario->usuario = $partes[0];
                    }
                }
            }
            return $this->render('_form_inport_user', [
                'title' => 'Importa Funcionário para Usuario',
                'model' => $model,
                'model_usuario' => $model_usuario,
                'model_perfil' => $model_perfil,
            ]);
        } else {
            Yii::$app->session->setFlash('danger', 'Ops. Esta senha não está disponivel!');
            return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
        }

        
    }

    protected function findModel($id) {
        if (($model = Funcionario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    public function actionSetSessionSetor() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->setor_temp_fk = $request['setor_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->setor_temp_fk = $request['setor_temp_fk'];
    }

    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->setor_temp_fk = NULL;
            $model->tipo_temp_fk = NULL;
            $model->marca_temp_fk = NULL;
            $model->modelo_temp_fk = NULL;
            $model->save();
        }
        return $this->redirect([Yii::$app->session->get('urlFuncionarioSearch', array())]);
    }

}
