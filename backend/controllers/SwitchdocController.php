<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\Switchdoc;
use common\models\SwitchArquivo;
use common\models\VwSwitch;
use common\models\Vlan;
use common\components\XML\Switchxml;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Json;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class SwitchdocController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'arquivo'],
                'rules' => AccessRulesControl::getRulesControl('switchdoc'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwSwitch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlSwitchdocSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes da Switch',
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Switchdoc();

        $vlan = Vlan::find()->Where(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])->andWhere(['id_vlan' => '1'])->one();

        if ($vlan){
            if ($vlan->id_vlan != 1 ) {
                $modelvlan = new Vlan();
                $modelvlan->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
                $modelvlan->id_vlan = 1;
                $modelvlan->vlan = 'Default';
                $modelvlan->descricao_vlan = 'VLAN criada automáticamente';
                $modelvlan->save();
            }
        } else {
            $modelvlan = new Vlan();
            $modelvlan->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
            $modelvlan->id_vlan = 1;
            $modelvlan->vlan = 'Default';
            $modelvlan->descricao_vlan = 'VLAN criada automáticamente';
            $modelvlan->save();
        }

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $request = Yii::$app->request->post();
                $array_portas = [];
                for ($i = 0; $i < $request['Switchdoc']['numero_portas']; $i++) {
                    $array_portas[] = [
                        'porta' => $i + 1,
                        'status' => $request['status_' . $i],
                        'vlan' => $request['vlan_' . $i],
                        'type' => $request['type_' . $i],
                        'tagged' => $request['tagged_' . $i],
                        'description' => $request['descricao_' . $i]
                    ];
                }
                $model->xml_portas = Switchxml::generationXML($array_portas);
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlSwitchdocSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar nova Switch',
                    'model' => $model,
        ]);
    }

    public function actionArquivo($id) {
        $model = $this->findModel($id);
        $model_arquivo = new SwitchArquivo();

        if (Yii::$app->request->isAjax) {
            if ($model_arquivo->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model_arquivo);
            }
        } else {
            if ($model_arquivo->load(Yii::$app->request->post())) {
                $arquivo = UploadedFile::getInstance($model_arquivo, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/switcharquivo');
                $gravar = false;
                if ($arquivo) {
                    $model_arquivo->url = $model->id . "_" . date('YmdHis') . "." . $arquivo->extension;
                    $gravar = true;
                } else {
                    $model_arquivo->url = NULL;
                }
                $model_arquivo->switch_fk = $model->id;
                $model_arquivo->data = date('d/m/Y H:i:s');
                if ($model_arquivo->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/switcharquivo/' . $model_arquivo->url);
                    }
                    Yii::$app->session->setFlash('success', 'Ocorencia inserido com sucesso!');
                    return $this->redirect(['switchdoc/update/'.$model->id]);
                }
            }
        }
        return $this->render('_form_arquivo', [
                    'title' => 'Adicionar Novo Arquivo',
                    'model' => $model,
                    'model_arquivo' => $model_arquivo,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $model_arquivo = new SwitchArquivo();

        $params['SwitchArquivo']['switch_fk'] = $model->id;
        $dataProviderArquivo = $model_arquivo->search($params);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $request = Yii::$app->request->post();
                $array_portas = [];
                for ($i = 0; $i < $model->numero_portas; $i++) {
                    $array_portas[] = [
                        'porta' => $i + 1,
                        'status' => isset($request['status_' . $i]) ? ($request['status_' . $i] == 'on' ? '1' : '0') : '0',
                        'vlan' => $request['vlan_' . $i],
                        'type' => $request['type_' . $i],
                        'tagged' => $request['tagged_' . $i],
                        'description' => $request['descricao_' . $i]
                    ];
                }
                $model->xml_portas = Switchxml::generationXML($array_portas);
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlSwitchdocSearch', array())]);
                }
            }
        }
        return $this->render('_form_edit', [
                    'title' => 'Alterar Switch',
                    'model' => $model,
                    'array_item_arquivo' => $model_arquivo->renderiza(SwitchArquivo::find()->where(['switch_fk' => $model->id])->all()),
                    'model_arquivo' => $model_arquivo,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlSwitchdocSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Switchdoc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionGetPorts() {
        $request = Yii::$app->request->post();
        return $this->renderPartial('_lines_ports', [
                    'request' => $request,
                    'model' => new Switchdoc(),
        ]);
    }

}
