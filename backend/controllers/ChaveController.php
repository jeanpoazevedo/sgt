<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\Chave;
use common\models\UsuarioPerfil;
use common\models\UsuarioUnidade;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class ChaveController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => AccessRulesControl::getRulesControl('chave'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Chave();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlChaveSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $usuariounidade = UsuarioUnidade::find()
        ->select('unidade_fk')
        ->Where(['usuario_fk' => Yii::$app->user->identity->id])
        ->andWhere(['unidade_fk' => $model->unidade_fk])
        ->one();

        if($usuarioperfil->perfil_fk != 1){
            if($usuariounidade->unidade_fk != $model->unidade_fk){
                if ($model->unidade_fk != 1){
                    if ($model->unidade_fk != Yii::$app->user->identity->unidade_temp_fk || $model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este passo a passo não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlChaveSearch', array())]);
                    }
                } else {
                    if ($model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este passo a passo não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlChaveSearch', array())]);
                    }
                }
            } 
        }
        
        return $this->render('view', [
                    'title' => 'Detalhes da Chave',
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Chave();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {                    
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlChaveSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar nova Chave',
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $usuariounidade = UsuarioUnidade::find()
        ->select('unidade_fk')
        ->Where(['usuario_fk' => Yii::$app->user->identity->id])
        ->andWhere(['unidade_fk' => $model->unidade_fk])
        ->one();

        if($usuarioperfil->perfil_fk != 1){
            if($usuariounidade->unidade_fk != $model->unidade_fk){
                if ($model->unidade_fk != 1){
                    if ($model->unidade_fk != Yii::$app->user->identity->unidade_temp_fk || $model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este passo a passo não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlChaveSearch', array())]);
                    }
                } else {
                    if ($model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este passo a passo não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlChaveSearch', array())]);
                    }
                }
            } 
        }

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlChaveSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Chave',
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlChaveSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Chave::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
