<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\UsuarioGerenteUnidade;
use common\models\VwUsuarioGerenteUnidade;
use common\models\Usuario;
use common\models\Unidade;
use common\models\UsuarioPerfil;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class UsuariogerenteunidadeController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => AccessRulesControl::getRulesControl('usuariogerenteunidade'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwUsuariogerenteunidade();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlUsuariogerenteunidadeSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes da Unidade',
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Usuariogerenteunidade();

        $model_restricao_perfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $model_restricao_unidade = 0;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlUsuariogerenteunidadeSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar nova Usuário Gernte de Unidade',
                    'model' => $model,
                    'model_restricao_perfil' => $model_restricao_perfil,
                    'model_restricao_unidade' => $model_restricao_unidade,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $model_restricao_perfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $model_restricao_unidade_temp = UsuarioGerenteUnidade::find()->andWhere(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['unidade_fk' => $model->unidade_fk])->one();
        if ($model_restricao_unidade_temp->gerencia == 1) {
            $model_restricao_unidade = 1;
        } else {
            $model_restricao_unidade = 0;
        }

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlUsuariogerenteunidadeSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Usuário Gernte de Unidade',
                    'model' => $model,
                    'model_restricao_perfil' => $model_restricao_perfil,
                    'model_restricao_unidade' => $model_restricao_unidade,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlUsuariogerenteunidadeSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Usuariogerenteunidade::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
