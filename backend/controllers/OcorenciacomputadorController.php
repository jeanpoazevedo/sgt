<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\OcorenciaComputador;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\web\Response;

class OcorenciacomputadorController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'view', 'pdf'],
                'rules' => AccessRulesControl::getRulesControl('ocorenciacomputador'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new OcorenciaComputador();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlOcorenciacomputadorSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes da Ocorencia',
                    'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCreate() {
        $model = new OcorenciaComputador();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $arquivo = UploadedFile::getInstance($model, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/ocorenciacomputador');
                $gravar = false;
                if ($arquivo) {
                    $model->url = $model->codigo_computador_fk . "_" . date('YmdHis') . "." . $arquivo->extension;
                    $gravar = true;
                } else {
                    $model->url = NULL;
                }
                $model->usuario_nome = Yii::$app->user->identity->nome;
                $model->data = date('d/m/Y H:i:s');
                if ($model->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/ocorenciacomputador/' . $model->url);
                    }
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlOcorenciacomputadorSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar nova Ocorencia',
                    'model' => $model,
        ]);
    }
    
    protected function findModel($id) {
        if (($model = OcorenciaComputador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
