<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\components\Setup;
use common\models\Computador;
use common\models\VwComputador;
use common\models\OcorenciaComputador;
use common\models\VwRestricaoMenu;
use common\components\SendMail;
use common\models\Usuario;
use common\models\PlacaVideo;
use common\models\Office;
use common\models\SistemaOperacional;
use common\models\Monitor;
use common\models\UsuarioUnidade;
use common\models\Codigo;
use common\models\SequenciaCodigo;
use common\models\VwCodigo;
use common\models\Funcionario;
use common\models\FuncionarioComputador;
use common\models\VwFuncionarioComputador;
use yii\data\ActiveDataProvider;
use yii2tech\spreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use kartik\date\DatePicker;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class ComputadorController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'pdf', 'ocorenciapdf', 'export', 'clean', 'createocorencia', 'deletefuncionario'],
                'rules' => AccessRulesControl::getRulesControl('computador'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwComputador();
        
        $model_restricao_create = VwRestricaoMenu::findAll(['controller' => 'computador', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);
        $model_restricao_export = VwRestricaoMenu::findAll(['controller' => 'computador', 'restricao' => 'export', 'usuario_fk' => Yii::$app->user->getId()]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlComputadorSearch', Yii::$app->request->url);

        return $this->render('index', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_restricao_create' => $model_restricao_create,
            'model_restricao_export' => $model_restricao_export,
        ]);
    }

    public function actionView($id) {
        $model_ocorencia = new OcorenciaComputador();
        $params['OcorenciaComputador']['codigo_computador_fk'] = $id;
        $dataProviderOcorencia = $model_ocorencia->searchocorencia($params);
        
        return $this->render('view', [
                    'title' => 'Detalhes do Computador',
                    'model' => $this->findModel($id),
                    'model_ocorencia' => $model_ocorencia,
                    'dataProviderOcorencia' => $dataProviderOcorencia,
        ]);
    }

    public function actionCreate() {
        $model = new Computador();
        $model_ocorencia = new OcorenciaComputador();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
        $model->setor_fk = Yii::$app->user->identity->setor_temp_fk;
        $model->tipo_fk = Yii::$app->user->identity->tipo_temp_fk;
        $model->modelo_fk = Yii::$app->user->identity->modelo_temp_fk;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $data = explode("/", $model->data_compra);
                list($dia, $mes, $ano) = $data;
                $ano_curto_arry = str_split($ano, 2);
                $ano_curto = $ano_curto_arry[1];
                if (($codigo = VwCodigo::find()->where(['ano' => $ano_curto])->andWhere(['mes' => $mes])->andWhere(['dispositivo' => 'C'])->one()) !== null) {
                    $codigo_temp = $codigo->sequencia + 1;
                    $codigo_computador_arry = SequenciaCodigo::find()->where(['sequencia' => $codigo_temp])->one();
                    $codigo_computador = $codigo_computador_arry->codigo;
                    $model_codigo = Codigo::find()->where(['id' => $codigo->id])->one();
                    $model_codigo->codigo_fk = $codigo_computador_arry->id;
                    if($model_codigo->save()){}
                } else {
                    $codigo_computador = '01';
                    $model_codigo = new Codigo();
                    $model_codigo->ano = $ano_curto;
                    $model_codigo->mes = $mes;
                    $model_codigo->dispositivo = 'C';
                    $model_codigo->codigo_fk = 1;
                    if($model_codigo->save()){}
                }
                $model->codigo_computador = 'C'.$ano_curto.$mes.$codigo_computador;
                $arquivo = UploadedFile::getInstance($model, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/computador');
                $gravar = false;
                $model->codigo_monitor1_fk = NULL;
                $model->codigo_monitor2_fk = NULL;
                $model->status = 1;
                $model->netbios = strtoupper($model->netbios);
                if ($arquivo) {
                    $model->url = $model->codigo_computador . ".pdf";
                    $gravar = true;
                } else {
                    $model->url = NULL;
                }
                if ($model->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/computador/' . $model->url);
                    }
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 4;
                    $model_ocorencia->descricao = 'Inserido no sistema';
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if ($model_ocorencia->save()) {
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submit(
                                $emails,
                                $model,
                                'sgt_email_computador_criado_html'
                            );  
                        }
                        Yii::$app->session->setFlash('success', 'Registro '.$model->codigo_computador.' inserido com sucesso!');
                        return $this->redirect([Yii::$app->session->get('urlComputadorSearch', array())]);
                    }
                } else {
                    $model->attributes = Yii::$app->request->post();
                }
            }
        }
        return $this->render('_form_create', [
                    'title' => 'Adicionar novo Computador',
                    'model' => $model,
        ]);
    }

    public function actionCreateocorencia($id) {
        $model = $this->findModel($id);
        $modelocorencia = new OcorenciaComputador();

        if (Yii::$app->request->isAjax) {
            if ($modelocorencia->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($modelocorencia);
            }
        } else {
            if ($modelocorencia->load(Yii::$app->request->post())) {
                $arquivo = UploadedFile::getInstance($modelocorencia, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/ocorenciacomputador');
                $gravar = false;
                if ($arquivo) {
                    $modelocorencia->url = $model->id . "_" . date('YmdHis') . "." . $arquivo->extension;
                    $gravar = true;
                } else {
                    $modelocorencia->url = NULL;
                }
                $modelocorencia->usuario_nome = Yii::$app->user->identity->nome;
                $modelocorencia->codigo_computador_fk = $model->id;
                $modelocorencia->data = date('d/m/Y H:i:s');
                if ($modelocorencia->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/ocorenciacomputador/' . $modelocorencia->url);
                    }
                    Yii::$app->session->setFlash('success', 'Ocorencia inserido com sucesso!');
                    return $this->redirect(['computador/update/'.$model->id]);
                }
            }
        }
        return $this->render('_form_ocorencia', [
                    'title' => 'Adicionar nova Ocorencia',
                    'model' => $model,
                    'modelocorencia' => $modelocorencia,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model_ocorencia = new OcorenciaComputador();
        $model_funcionariocomputador = new VwFuncionarioComputador();
        
        $model_restricao = VwRestricaoMenu::findAll(['controller' => 'computador', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);
        
        $params_ocorencia['OcorenciaComputador']['codigo_computador_fk'] = $model->id;
        $dataProviderOcorencia = $model_ocorencia->searchocorencia($params_ocorencia);

        $params_funcionariocomputador['VwFuncionarioComputador']['computador_fk'] = $model->id;
        $dataProviderFuncionariocomputador = $model_funcionariocomputador->searchfuncionariocomputador($params_funcionariocomputador);
        
        $modelsistemaoperacional = ArrayHelper::map(SistemaOperacional::find()->all(), 'id', function($model) {return $model->sistema;});
        $datasistemaoperacional = [ NULL => "Sem Sistema Operacional"];
        $sistemaoperacional = ArrayHelper::merge($datasistemaoperacional, $modelsistemaoperacional);
        
        $modeloffice = ArrayHelper::map(Office::find()->all(), 'id', function($model) {return $model->office;});
        $dataoffice = [ NULL => "Sem Offce"];
        $office = ArrayHelper::merge($dataoffice, $modeloffice);
        
        $modelmonitor1 = ArrayHelper::map(Monitor::find()->all(), 'id', function($model) {return $model->codigo_monitor;});
        $datamonitor1 = [ NULL => "Sem Monitor"];
        $monitor1 = ArrayHelper::merge($datamonitor1, $modelmonitor1);
        
        $modelmonitor2 = ArrayHelper::map(Monitor::find()->all(), 'id', function($model) {return $model->codigo_monitor;});
        $datamonitor2 = [ NULL => "Sem Monitor"];
        $monitor2 = ArrayHelper::merge($datamonitor2, $modelmonitor2);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model_anterior = $this->findModel($id);
                $arquivo = UploadedFile::getInstance($model, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/computador');
                $gravar = false;
                $model->netbios = strtoupper($model->netbios);
                $model->mac_c = Setup::removeFormatterMAC($model->mac_c);
                $model_anterior->mac_c = Setup::removeFormatterMAC($model_anterior->mac_c);
                $model->mac_w = Setup::removeFormatterMAC($model->mac_w);
                $model_anterior->mac_w = Setup::removeFormatterMAC($model_anterior->mac_w);
                if ($arquivo) {
                    $model->url = $model->codigo_computador . ".pdf";
                    $gravar = true;
                } else {
                    $model->url = $model_anterior->url;
                }
                if(!$model_restricao){
                    $model->status = 1;
                }
                if ($model_anterior->unidade_fk != $model->unidade_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $ocorencia_unidade_anterior = ($model_anterior->unidadeFk->unidade_pai_fk ?
                        ($model_anterior->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                            ($model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                            : $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                                        : $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                                    : $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                            : $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                        : $model_anterior->unidadeFk->unidade);
                    $ocorencia_unidade = ($model->unidadeFk->unidade_pai_fk ?
                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                        : $model->unidadeFk->unidade);
                    $model_ocorencia->descricao = $model->getAttributeLabel('unidade_fk').': '.$ocorencia_unidade_anterior.' > '.$ocorencia_unidade;
                    if ($model_ocorencia->save()){
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submitanterior(
                                $emails,
                                $model,
                                $model_anterior,
                                $ocorencia_unidade_anterior,
                                $ocorencia_unidade,
                                'sgt_email_computador_unidade_html'
                            );
                        }
                    }
                }
                if ($model_anterior->setor_fk != $model->setor_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->setor_fk != NULL){
                        $model_ocorencia->descricao = $model->getAttributeLabel('setor_fk').': '.$model_anterior->setorFk->setor.' > '.$model->setorFk->setor;
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('setor_fk').': '.$model->setorFk->setor.' foi atribuido.';
                    }
                    if ($model_ocorencia->save()){
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submitanterior(
                                $emails,
                                $model,
                                $model_anterior,
                                $model_anterior->setorFk->setor,
                                $model->setorFk->setor,
                                'sgt_email_computador_setor_html'
                            );
                        }
                    }
                }
                if ($model_anterior->ram != $model->ram) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('ram').': '.$model_anterior->ram.' '.$model_anterior->ramMedidaFk->simbolo.' > '.$model->ram.' '.$model->ramMedidaFk->simbolo;
                    $model_ocorencia->save();
                }
                if ($model_anterior->ram_clok_fk != $model->ram_clok_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('ram_clok_fk') .':'. $model_anterior->ramClokFk->clok .' MHz > '. $model->ramClokFk->clok .' MHz';
                    $model_ocorencia->save();
                }
                if ($model_anterior->hd != $model->hd) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('hd').': '.$model_anterior->hd.' '.$model_anterior->hdMedidaFk->simbolo.' > '.$model->hd.' '.$model->hdMedidaFk->simbolo;
                    $model_ocorencia->save();
                }
                if ($model_anterior->tipo_hd_fk != $model->tipo_hd_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->tipo_hd_fk != NULL){
                        $model_ocorencia->descricao = $model->getAttributeLabel('tipo_hd_fk').': '.$model_anterior->tipoHdFk->tipo.' > '.$model->tipoHdFk->tipo;
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('tipo_hd_fk').': '.$model->tipoHdFk->tipo.' foi atribuido.';
                    }
                    $model_ocorencia->save();
                }
                if ($model_anterior->netbios != $model->netbios) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('netbios').': '.$model_anterior->netbios.' > '.$model->netbios;
                    $model_ocorencia->save();
                }
                if ($model_anterior->placa_mae != $model->placa_mae) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('placa_mae').': '.$model_anterior->placa_mae.' > '.$model->placa_mae;
                    $model_ocorencia->save();
                }
                if ($model_anterior->numero_serie != $model->numero_serie) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('numero_serie').': '.$model_anterior->numero_serie.' > '.$model->numero_serie;
                    $model_ocorencia->save();
                }
                if ($model_anterior->numero_lacre != $model->numero_lacre) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('numero_lacre').': '.$model_anterior->numero_lacre.' > '.$model->numero_lacre;
                    $model_ocorencia->save();
                }
                if ($model_anterior->garantia != $model->garantia) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('garantia').': '.$model_anterior->garantia.' > '.$model->garantia;
                    $model_ocorencia->save();
                }
                if ($model_anterior->status != $model->status) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if ( $model->status == 1 ){
                        $ocorencia_status_anterior = "Inativo";
                        $ocorencia_status = "Ativo";
                        $model_ocorencia->descricao = $model->getAttributeLabel('status').': '.'Inativo > Ativo';
                    } else {
                        $ocorencia_status_anterior = "Ativo";
                        $ocorencia_status = "Inativo";
                        $model_ocorencia->descricao = $model->getAttributeLabel('status').': '.'Ativo > Inativo';
                        $model->status = 0;
                    }
                    if ($model_ocorencia->save()){
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submitanterior(
                                $emails,
                                $model,
                                $model_anterior,
                                $ocorencia_status_anterior,
                                $ocorencia_status,
                                'sgt_email_computador_desativado_html'
                            );
                        }
                    }
                }
                if ($model_anterior->tipo_fk != $model->tipo_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->tipo_fk != NULL){
                        $model_ocorencia->descricao = $model->getAttributeLabel('tipo').': '.$model_anterior->tipoFk->tipo.' > '.$model->tipoFk->tipo;
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('tipo').': '.$model->tipoFk->tipo.' foi atribuido.';
                    }
                    $model_ocorencia->save();
                }
                if ($model_anterior->mac_c != $model->mac_c) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('mac_c').': '.Setup::formatterMAC($model_anterior->mac_c).' > '.Setup::formatterMAC($model->mac_c);
                    $model_ocorencia->save();
                }
                if ($model_anterior->mac_w != $model->mac_w) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('mac_w').': '.Setup::formatterMAC($model_anterior->mac_w).' > '.Setup::formatterMAC($model->mac_w);
                    $model_ocorencia->save();
                }
                if ($model_anterior->placa_video_fk != $model->placa_video_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->placa_video_fk != NULL){
                        $model_ocorencia->descricao = $model->getAttributeLabel('placa_video_fk').': '.$model_anterior->placaVideoFk->descricao_placa_video.' > '.$model->placaVideoFk->descricao_placa_video;
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('placa_video_fk').': '.$model->placaVideoFk->descricao_placa_video.' foi atribuido.';
                    }
                    $model_ocorencia->save();
                }
                if ($model_anterior->codigo_monitor1_fk != $model->codigo_monitor1_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->codigo_monitor1_fk != NULL){
                        if (isset($model->codigoMonitor1Fk->codigo_monitor)) {
                            $model_ocorencia->descricao = $model->getAttributeLabel('codigo_monitor1_fk').': '.$model_anterior->codigoMonitor1Fk->codigo_monitor.' > '.$model->codigoMonitor1Fk->codigo_monitor;
                        } else {
                            $model_ocorencia->descricao = $model->getAttributeLabel('codigo_monitor1_fk').': '.$model_anterior->codigoMonitor1Fk->codigo_monitor.' > Sem Monitor 1.';
                        }
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('codigo_monitor1_fk').': '.$model->codigoMonitor1Fk->codigo_monitor.' foi atribuido.';
                    }
                    $model_ocorencia->save();
                }
                if ($model_anterior->codigo_monitor2_fk != $model->codigo_monitor2_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->codigo_monitor2_fk != NULL){
                        if (isset($model->codigoMonitor2Fk->codigo_monitor)) {
                            $model_ocorencia->descricao = $model->getAttributeLabel('codigo_monitor2_fk').': '.$model_anterior->codigoMonitor2Fk->codigo_monitor.' > '.$model->codigoMonitor2Fk->codigo_monitor;
                        } else {
                            $model_ocorencia->descricao = $model->getAttributeLabel('codigo_monitor2_fk').': '.$model_anterior->codigoMonitor2Fk->codigo_monitor.' > Sem Monitor 1.';
                        }
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('codigo_monitor2_fk').': '.$model->codigoMonitor2Fk->codigo_monitor.' foi atribuido.';
                    }
                    $model_ocorencia->save();
                }
                if ($model_anterior->office_fk != $model->office_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->office_fk != NULL){
                        if (isset($model->officeFk->office)) {
                            $model_ocorencia->descricao = $model->getAttributeLabel('office_fk').': '.$model_anterior->officeFk->office.' > '.$model->officeFk->office;
                        } else {
                            $model_ocorencia->descricao = $model->getAttributeLabel('office_fk').': '.$model_anterior->officeFk->office.' > Sem Office.';
                        }
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('office_fk').': '.$model->officeFk->office.' foi atribuido.';
                    }
                    $model_ocorencia->save();
                }
                if ($model_anterior->sistema_operacional_fk != $model->sistema_operacional_fk) {
                    $model_ocorencia = new OcorenciaComputador();
                    $model_ocorencia->codigo_computador_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->sistema_operacional_fk != NULL){
                        if (isset($model->sistemaOperacionalFk->sistema)) {
                            $model_ocorencia->descricao = $model->getAttributeLabel('sistema_operacional_fk').': '.$model_anterior->sistemaOperacionalFk->sistema.' > '.$model->sistemaOperacionalFk->sistema;
                        } else {
                            $model_ocorencia->descricao = $model->getAttributeLabel('sistema_operacional_fk').': '.$model_anterior->sistemaOperacionalFk->sistema.' > Sem Sistema Operacional.';
                        }
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('sistema_operacional_fk').': '.$model->sistemaOperacionalFk->sistema.' foi atribuido.';
                    }
                    $model_ocorencia->save();
                }
                if ($model->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/computador/' . $model->url);
                    }
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlComputadorSearch', array())]);
                }
            }
        }
        
        return $this->render('_form', [
            'title' => 'Alterar Computador',
            'model' => $model,
            'model_ocorencia' => $model_ocorencia,
            'dataProviderOcorencia' => $dataProviderOcorencia,
            'dataProviderFuncionariocomputador' => $dataProviderFuncionariocomputador,
            'model_restricao' => $model_restricao,
            'sistemaoperacional' => $sistemaoperacional,
            'office' => $office,
            'monitor1' => $monitor1,
            'monitor2' => $monitor2,
        ]);
    }

    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            $model->ativo = 0;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
            } else {
                Yii::$app->session->setFlash('error', 'Não foi possivel remover!');
            }
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlComputadorSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Computador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    public function actionDeletefuncionario($id) {
        try {
            $modelfuncionario = $this->findModelfuncionario($id);
            $this->findModelfuncionario($id)->delete();
            Yii::$app->session->setFlash('success', 'Funcionario removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect(['computador/update/'.$modelfuncionario->computador_fk]);
    }

    protected function findModelfuncionario($id) {
        if (($modelfuncionario = FuncionarioComputador::findOne($id)) !== null) {
            return $modelfuncionario;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    protected function findModelocorencia($id) {
        if (($model = OcorenciaComputador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    public function actionPdf($id) {
        $directory = \Yii::getAlias('@backend/web/arquivo');
        $model = $this->findModel($id);
        $pdf = file_get_contents($directory . '/computador/' . $model->url);
        header('Content-Type: application/pdf');
        echo $pdf;
    }

    public function actionOcorenciapdf($id) {
        $directory = \Yii::getAlias('@backend/web/arquivo');
        $model = $this->findModelocorencia($id);
        if ($model->url != null ) {
            $pdf = file_get_contents($directory . '/ocorenciacomputador/' . $model->url);
            header('Content-Type: application/pdf');
            echo $pdf;
        } else {
            Yii::$app->session->setFlash('danger', 'Ops. Esta ocorencia não tem anexo!');
            return $this->redirect(['computador/update/'.$model->codigo_computador_fk]);
        }
    }
    
    public function actionSetSessionSetor() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->setor_temp_fk = $request['setor_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->setor_temp_fk = $request['setor_temp_fk'];
    }
    
    public function actionSetSessionTipo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->tipo_temp_fk = $request['tipo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->tipo_temp_fk = $request['tipo_temp_fk'];
    }
    
    public function actionSetSessionMarca() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->marca_temp_fk = $request['marca_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->marca_temp_fk = $request['marca_temp_fk'];
    }

    public function actionSetSessionFuncionario() {
        $model = new FuncionarioComputador();
        $request = Yii::$app->request->post();
        if ($model) {
            $model->funcionario_fk = $request['funcionario_fk'];
            $model->computador_fk = $request['computador_fk'];
            $model->save();
        }
        Yii::$app->session->setFlash('success', 'Funcionario vinculado com sucesso!');
    }

    public function actionSetSessionModelo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->modelo_temp_fk = $request['modelo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->modelo_temp_fk = $request['modelo_temp_fk'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->setor_temp_fk = NULL;
            $model->tipo_temp_fk = NULL;
            $model->marca_temp_fk = NULL;
            $model->modelo_temp_fk = NULL;
            $model->save();
        }
        return $this->redirect([Yii::$app->session->get('urlComputadorSearch', array())]);
    }

    public function actionExport() {
        $query = Computador::find();
        $export = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
            ]),
            'columns' => [
                'codigo_computador',
                [
                    'attribute' => 'unidade_fk',
                    'value' => function ($data) {
                        return ($data->unidadeFk->unidade_pai_fk ?
                            ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                            : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                            : $data->unidadeFk->unidade);
                    },
                ],
                [
                    'attribute' => 'setor_fk',
                    'value' => function ($data) {
                        return ($data->setor_fk ? $data->setorFk->setor : "Setor não informado" );
                    },
                ],
                [
                    'attribute' => 'tipo_fk',
                    'value' => function ($data) {
                        return $data->tipoFk->tipo;
                    },
                ],
                [
                    'attribute' => 'Marca',
                    'value' => function ($data) {
                        return $data->modeloFk->marcaFk->marca;
                    },
                ],
                [
                    'attribute' => 'modelo_fk',
                    'value' => function ($data) {
                        return $data->modeloFk->modelo;
                    },
                ],
                'numero_serie',
                'data_compra',
                'garantia',
                'placa_mae',
                'numero_processador',
                [
                    'attribute' => 'processador_fk',
                    'value' => function ($data) {
                        return ($data->processador_fk ? $data->processadorFk->descricao_processador : "Sem Processador" );
                    },
                ],
                [
                    'attribute' => 'RAM',
                    'value' => function ($data) {
                        return $data->ram . ' ' . $data->ramMedidaFk->simbolo;
                    },
                ],
                [
                    'attribute' => 'Clok RAM',
                    'value' => function ($data) {
                        return ($data->ram_clok_fk ? $data->ramClokFk->clok.' MHz' : "Sem Clok" );
                    },
                ],
                [
                    'attribute' => 'HD',
                    'value' => function ($data) {
                        return $data->hd . ' ' . $data->hdMedidaFk->simbolo;
                    },
                ],
                'loja',
                'numero_nota',
                'valor',
                'numero_patrimonio',
                'numero_lacre',
                'netbios',
                'mac_c',
                'mac_w',
                 [
                    'attribute' => 'sistema_operacional_fk',
                    'value' => function ($data) {
                        return ($data->sistema_operacional_fk ? $data->sistemaOperacionalFk->sistema : "Sem Sistema Operacional" );
                    },
                ],
                [
                    'attribute' => 'office_fk',
                    'value' => function ($data) {
                        return ($data->office_fk ? $data->officeFk->office : "Sem Office" );
                    },
                ],
                [
                    'attribute' => 'placa_video_fk',
                    'value' => function ($data) {
                        return ($data->placa_video_fk ? $data->placaVideoFk->descricao_placa_video : "Sem Placa de Vídeo" );
                    },
                ],
                'numero_palca_video',
                [
                    'attribute' => 'codigo_monitor1_fk',
                    'value' => function ($data) {
                        return ($data->codigo_monitor1_fk ? $data->codigoMonitor1Fk->codigo_monitor : "Sem Moniotor" );
                    },
                ],
                [
                    'attribute' => 'codigo_monitor2_fk',
                    'value' => function ($data) {
                        return ($data->codigo_monitor2_fk ? $data->codigoMonitor2Fk->codigo_monitor : "Sem Moniotor" );
                    },
                ],
            ],
        ]);
                    
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        return $export->send('computadores.xls');
    }

}
