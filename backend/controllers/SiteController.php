<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Usuario;
use common\models\Sistema;
use common\models\Configuracao;
use common\models\UsuarioPerfil;
use common\models\UsuarioUnidade;
use common\components\SendMail;
use common\components\Setup;

use common\models\Computador;
use common\models\Monitor;
use common\models\Equipamento;
use common\models\HdCadastro;
use common\models\HdUsuarioUnidade;
use common\models\ApiJavaComputador;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'qr', 'apijavacomp', 'new-password', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'set-session-unidade', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->session->isActive) {
            $rodape = Configuracao::findOne(1);
            Yii::$app->session->set("rodape_interno", $rodape->variavel2);
            
            $url = Sistema::findOne(1);
            Yii::$app->session->set("url", $url->url);
            
            $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
            Yii::$app->session->set("usuarioperfil", $usuarioperfil->perfil_fk);
            
        }

        $usuario_trocar_senha =  Usuario::find()->where(['id' => Yii::$app->user->identity->id])->one();

        if ($usuario_trocar_senha->trocar_senha == 1) {
            return $this->redirect(['site/new-password?token=' . $usuario_trocar_senha->token ]);
        } else {
            $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);
            
            $usuariounidadehd = HdUsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);

            $computador_total = Computador::find()->distinct()
                ->Where(['=', 'status', '1'])
                ->andWhere(['IN', 'unidade_fk', $usuariounidade])
                ->all();
            $monitor_total = Monitor::find()->distinct()
                ->Where(['=', 'status', '1'])
                ->andWhere(['IN', 'unidade_fk', $usuariounidade])
                ->all();
            $equipamento_total = Equipamento::find()->distinct()
                ->Where(['=', 'status', '1'])
                ->andWhere(['IN', 'unidade_fk', $usuariounidade])
                ->all();
            $hd_total = HdCadastro::find()->distinct()
                ->Where(['=', 'status', '4'])
                ->andWhere(['IN', 'unidade_fk', $usuariounidadehd])
                ->all();
            $usuario_total = Usuario::find()->distinct()
                ->Where(['=', 'status', '1'])
                ->andWhere(['IN', 'unidade_fk', $usuariounidade])
                ->all();
            
            
            return $this->render('index', [
                'computador_total' => $computador_total,
                'monitor_total' => $monitor_total,
                'equipamento_total' => $equipamento_total,
                'hd_total' => $hd_total,
                'usuario_total' => $usuario_total,
            ]);
        }
    }

    public function actionQr($codigo) {
        $this->layout = "main_qr";
        
        switch($codigo[0]) { 
            case "C" :
                $model = Computador::findOne(['codigo_computador' => $codigo]);
                return $this->render('qrcomputador', [
                    'model' => $model,
                ]);
                break; 
            case "M" :
                $model = Monitor::findOne(['codigo_monitor' => $codigo]);
                return $this->render('qrmonitor', [
                    'model' => $model,
                ]);
                break; 
            case "E" :
                $model = Equipamento::findOne(['codigo_equipamento' => $codigo]);
                return $this->render('qrequipamento', [
                    'model' => $model,
                ]);
                break;
        }
        return $this->render('qr');
    }

    public function actionApijavacomp() {
        
        // Recebe o JSON enviado por uma requisição POST
        $json = file_get_contents('php://input');

        // Converte o JSON em um array associativo
        $data = json_decode($json, true);

        // Verifica se o JSON foi decodificado com sucesso
        if ($data === null) {
            // Erro na decodificação do JSON
            http_response_code(400); // Define o código de resposta HTTP para 400 Bad Request
            echo "Erro na decodificação do JSON.";
            exit;
        }

        $model_codigo_computador = Computador::findOne(['codigo_computador' => $data['codComp']]);

        $model_busca_computador = ApiJavaComputador::findOne(['codigo_computador_fk' => $model_codigo_computador->id]);

        if ($model_busca_computador != null) {
            $model = $this->findModel($model_busca_computador->id);
            $model->data = date('d/m/Y H:i:s');

            $model->versao_so = $data['osVersion'];
            $model->nome_so = $data['osName'];
            $model->arquitetura_so = $data['osArch'];
            $model->memoria_ram = $data['totalMemory'];
            $model->memoria_live = $data['freeMemory'];
            $model->processador = $data['processor'];
            $model->macaddres_1 = $data['macAddress1'];
            $model->macaddres_2 = $data['macAddress2'];
            $model->macaddres_3 = $data['macAddress3'];
    
            $model->save();
        } else {
            $model = new ApiJavaComputador();
            $model->codigo_computador_fk = $model_codigo_computador->id;
            $model->unidade_fk = $model_codigo_computador->unidade_fk;
            $model->data = date('d/m/Y H:i:s');

            $model->versao_so = $data['osVersion'];
            $model->nome_so = $data['osName'];
            $model->arquitetura_so = $data['osArch'];
            $model->memoria_ram = $data['totalMemory'];
            $model->memoria_live = $data['freeMemory'];
            $model->processador = $data['processor'];
            $model->macaddres_1 = $data['macAddress1'];
            $model->macaddres_2 = $data['macAddress2'];
            $model->macaddres_3 = $data['macAddress3'];

            $model->save();
        }
        
    }

    protected function findModel($id) {
        if (($model = ApiJavaComputador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        $this->layout = "main_login";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionSetSessionUnidade() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->unidade_temp_fk = $request['unidade_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->unidade_temp_fk = $request['unidade_temp_fk'];
    }
    
    public function actionResetPassword() {
        $this->layout = "main_login";
        $model = new Usuario();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model = Usuario::find()->where(['email' => $model->email])->all();
                foreach ($model as $key => $value) {
                    $value->token = Yii::$app->security->generateRandomString();
                    if ($value->save()) {
                        Yii::$app->mailer->compose([
                                    'html' => 'novo_cadastro_insignare-html',
                                    'text' => 'novo_cadastro_insignare-text'
                                        ], [
                                    'model' => $value
                                ])->setFrom([
                                    'insignare@acpr.org.br' => 'SGT'
                                ])->setTo($value->email)
                                ->setSubject('Atualização de senha!')
                                ->send();
                    } else {
                        throw new \Exception(Setup::setMessageErrorModel('Atenção:', $model->getErrors()));
                    }
                }
                Yii::$app->session->setFlash('success', 'Registro atualizado com sucesso!');
                return $this->redirect(['site/login']);
            } else {
                $model->senha = '';
            }
        }
        return $this->render('reset_password', [
                    'model' => $model,
        ]);
    }

    public function actionNewPassword($token) {
        $this->layout = "main_login";
        $model = Usuario::findOne(['token' => $token]);

        if ($model) {
            if (Yii::$app->request->isAjax) {
                if ($model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            } else {
                if ($model->load(Yii::$app->request->post())) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                        $model->senha_hash = $model->senha;
                        $model->auth_key = Yii::$app->security->generateRandomString();
                        $model->token = Yii::$app->security->generateRandomString();
                        $model->trocar_senha = null;
                        if ($model->save()) {
                            $transaction->commit();
                            $emails = [ $model->email ];
                            SendMail::submitpassword($emails, $model, 'sgt_email_usuario_senha_atualizacao-html');
                            return $this->redirect(['site/login']);
                        } else {
                            throw new \Exception(Setup::setMessageErrorModel('Atenção:', $model->getErrors()));
                        }
                    } catch (\Exception $e) {
                        $mensagem = $e->getMessage();
                        $transaction->rollBack();
                        $model->load(Yii::$app->request->post());
                        Yii::$app->session->setFlash('error', $mensagem);
                    }
                } else {
                    $model->senha = '';
                }
            }
            return $this->render('new_password', [
                'model' => $model,
            ]);
        } else {
            return $this->goHome();
        }
    }
    
    public function actionProfile() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $model->senha = '';
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model_anterior = Usuario::findOne(Yii::$app->user->identity->id);
                if ($model_anterior->senha != $model->senha){
                    $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                    $model->senha_hash = $model->senha;
                    $model->trocar_senha = null;
                }
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect(['site/profile']);
                }
            }
        }
        return $this->render('perfil', [
                    'title' => 'Editar meu Usuario',
                    'model' => $model,
        ]);
    }
    
}
