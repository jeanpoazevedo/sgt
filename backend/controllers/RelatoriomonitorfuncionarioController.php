<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\VwRelatorioMonitorFuncionario;
use common\models\Usuario;
use common\models\UsuarioUnidade;
use common\models\VwRestricaoMenu;
use common\components\Setup;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii2tech\spreadsheet\Spreadsheet;
use yii\data\ActiveDataProvider;

class RelatoriomonitorfuncionarioController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'export', 'clean'],
                'rules' => AccessRulesControl::getRulesControl('relatoriomonitorfuncionario'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwRelatorioMonitorFuncionario();

        $model_restricao_export = VwRestricaoMenu::findAll(['controller' => 'relatoriomonitorfuncionario', 'restricao' => 'export', 'usuario_fk' => Yii::$app->user->getId()]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlRelatoriomonitorfuncionarioSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model_restricao_export' => $model_restricao_export,
        ]);
    }

    public function actionExport() {
        $query = VwRelatorioMonitorFuncionario::find();
        $export = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
            ]),
            'columns' => [
                [
                    'attribute' => 'unidade_fk',
                    'value' => function ($data) {
                        return ($data->unidadeFk->unidade_pai_fk ?
                            ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                            : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                            : $data->unidadeFk->unidade);
                    },
                ],
                'codigo_monitor',
                'monitor_setor',
                'tipo',
                'marca',
                'modelo',
                [
                    'attribute' => 'funcionario_unidade_fk',
                    'value' => function ($data) {
                        return ($data->nome ?
                            ($data->funcionariounidadeFk->unidade_pai_fk ?
                                ($data->funcionariounidadeFk->unidadePaiFk->unidade_pai_fk ?
                                    ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                            ($data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                                $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade
                                                : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                            : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                        : $data->funcionariounidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                    : $data->funcionariounidadeFk->unidadePaiFk->unidade . " - " . $data->funcionariounidadeFk->unidade)
                                : $data->funcionariounidadeFk->unidade)
                            : "" );
                    },
                ],
                [
                    'attribute' => 'funcionario_setor',
                    'value' => function ($data) {
                        return ($data->funcionario_setor ? $data->funcionario_setor : "");
                    },
                ],
                [
                    'attribute' => 'funcao',
                    'value' => function ($data) {
                        return ($data->funcao ? $data->funcao : "");
                    },
                ],
                [
                    'attribute' => 'nome',
                    'value' => function ($data) {
                        return ($data->nome ? $data->nome : "");
                    },
                ],
            ],
        ]);

                    
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        return $export->send('relatorio_monitor_funcionario.xls');
    }

    public function actionSetSessionSetor() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->setor_temp_fk = $request['setor_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->setor_temp_fk = $request['setor_temp_fk'];
    }
    
    public function actionSetSessionTipo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->tipo_temp_fk = $request['tipo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->tipo_temp_fk = $request['tipo_temp_fk'];
    }
    
    public function actionSetSessionMarca() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->marca_temp_fk = $request['marca_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->marca_temp_fk = $request['marca_temp_fk'];
    }
    
    public function actionSetSessionModelo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->modelo_temp_fk = $request['modelo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->modelo_temp_fk = $request['modelo_temp_fk'];
    }
    
    public function actionSetSessionFuncao() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->funcao_temp = $request['funcao_temp'];
            $model->save();
        }
        Yii::$app->user->identity->funcao_temp = $request['funcao_temp'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->setor_temp_fk = NULL;
            $model->funcao_temp = NULL;
            $model->tipo_temp_fk = NULL;
            $model->marca_temp_fk = NULL;
            $model->modelo_temp_fk = NULL;
            $model->save();
        }
        return $this->redirect([Yii::$app->session->get('urlRelatoriomonitorfuncionarioSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = VwRelatorioMonitorFuncionario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
