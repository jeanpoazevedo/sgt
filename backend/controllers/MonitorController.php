<?php

namespace backend\controllers;


use Yii;
use common\components\AccessRulesControl;
use common\components\Setup;
use common\components\SendMail;
use common\models\Monitor;
use common\models\VwMonitor;
use common\models\OcorenciaMonitor;
use common\models\VwRestricaoMenu;
use common\models\Usuario;
use common\models\UsuarioUnidade;
use common\models\Codigo;
use common\models\SequenciaCodigo;
use common\models\VwCodigo;
use common\models\FuncionarioMonitor;
use common\models\Funcionario;
use common\models\VwFuncionarioMonitor;
use yii\data\ActiveDataProvider;
use yii2tech\spreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use kartik\date\DatePicker;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class MonitorController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'pdf', 'ocorenciapdf', 'export', 'clean', 'createocorencia', 'deletefuncionario'],
                'rules' => AccessRulesControl::getRulesControl('monitor'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwMonitor();
        
        $model_restricao_create = VwRestricaoMenu::findAll(['controller' => 'monitor', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);
        $model_restricao_export = VwRestricaoMenu::findAll(['controller' => 'monitor', 'restricao' => 'export', 'usuario_fk' => Yii::$app->user->getId()]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlMonitorSearch', Yii::$app->request->url);

        return $this->render('index', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_restricao_create' => $model_restricao_create,
            'model_restricao_export' => $model_restricao_export,
        ]);
    }

    public function actionView($id) {
        $model_ocorencia = new OcorenciaMonitor();
        $params['OcorenciaMonitor']['codigo_monitor_fk'] = $id;
        $dataProviderOcorencia = $model_ocorencia->searchocorencia($params);
        
        return $this->render('view', [
                    'title' => 'Detalhes do Monitor',
                    'model' => $this->findModel($id),
                    'model_ocorencia' => $model_ocorencia,
                    'dataProviderOcorencia' => $dataProviderOcorencia,
        ]);
    }

    public function actionCreate() {
        $model = new Monitor();
        $model_ocorencia = new OcorenciaMonitor();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
        $model->setor_fk = Yii::$app->user->identity->setor_temp_fk;
        $model->tipo_fk = Yii::$app->user->identity->tipo_temp_fk;
        $model->modelo_fk = Yii::$app->user->identity->modelo_temp_fk;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $data = explode("/", $model->data_compra);
                list($dia, $mes, $ano) = $data;
                $ano_curto_arry = str_split($ano, 2);
                $ano_curto = $ano_curto_arry[1];
                if (($codigo = VwCodigo::find()->where(['ano' => $ano_curto])->andWhere(['mes' => $mes])->andWhere(['dispositivo' => 'M'])->one()) !== null) {
                    $codigo_temp = $codigo->sequencia + 1;
                    $codigo_monitor_arry = SequenciaCodigo::find()->where(['sequencia' => $codigo_temp])->one();
                    $codigo_monitor = $codigo_monitor_arry->codigo;
                    $model_codigo = Codigo::find()->where(['id' => $codigo->id])->one();
                    $model_codigo->codigo_fk = $codigo_monitor_arry->id;
                    if($model_codigo->save()){}
                } else {
                    $codigo_monitor = '01';
                    $model_codigo = new Codigo();
                    $model_codigo->ano = $ano_curto;
                    $model_codigo->mes = $mes;
                    $model_codigo->dispositivo = 'M';
                    $model_codigo->codigo_fk = 1;
                    if($model_codigo->save()){}
                }
                $model->codigo_monitor = 'M'.$ano_curto.$mes.$codigo_monitor;
                $arquivo = UploadedFile::getInstance($model, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/monitor');
                $gravar = false;
                $model->status = 1;
                if ($arquivo) {
                    $model->url = $model->codigo_monitor . ".pdf";
                    $gravar = true;
                } else {
                    $model->url = NULL;
                }
                if ($model->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/monitor/' . $model->url);
                    }
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 4;
                    $model_ocorencia->descricao = 'Inserido no sistema';
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if ($model_ocorencia->save()) {
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submit(
                                $emails,
                                $model,
                                'sgt_email_monitor_criado_html'
                            );
                        }
                        Yii::$app->session->setFlash('success', 'Registro '.$model->codigo_monitor.' inserido com sucesso!');
                        return $this->redirect([Yii::$app->session->get('urlMonitorSearch', array())]);
                    }
                } else {
                    $model->attributes = Yii::$app->request->post();
                }
            }
        }
        return $this->render('_form_create', [
                    'title' => 'Adicionar novo Monitor',
                    'model' => $model,
        ]);
    }

    public function actionCreateocorencia($id) {
        $model = $this->findModel($id);
        $modelocorencia = new OcorenciaMonitor();

        if (Yii::$app->request->isAjax) {
            if ($modelocorencia->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($modelocorencia);
            }
        } else {
            if ($modelocorencia->load(Yii::$app->request->post())) {
                $arquivo = UploadedFile::getInstance($modelocorencia, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/ocorenciamonitor');
                $gravar = false;
                if ($arquivo) {
                    $modelocorencia->url = $model->id . "_" . date('YmdHis') . "." . $arquivo->extension;
                    $gravar = true;
                } else {
                    $modelocorencia->url = NULL;
                }
                $modelocorencia->usuario_nome = Yii::$app->user->identity->nome;
                $modelocorencia->codigo_monitor_fk = $model->id;
                $modelocorencia->data = date('d/m/Y H:i:s');
                if ($modelocorencia->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/ocorenciamonitor/' . $modelocorencia->url);
                    }
                    Yii::$app->session->setFlash('success', 'Ocorencia inserido com sucesso!');
                    return $this->redirect(['monitor/update/'.$model->id]);
                }
            }
        }
        return $this->render('_form_ocorencia', [
                    'title' => 'Adicionar nova Ocorencia',
                    'model' => $model,
                    'modelocorencia' => $modelocorencia,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model_ocorencia = new OcorenciaMonitor();
        $model_funcionariomonitor = new VwFuncionarioMonitor();
        
        $model_restricao = VwRestricaoMenu::findAll(['controller' => 'monitor', 'restricao' => 'create', 'usuario_fk' => Yii::$app->user->getId()]);

        $params['OcorenciaMonitor']['codigo_monitor_fk'] = $model->id;
        $dataProviderOcorencia = $model_ocorencia->searchocorencia($params);

        $params_funcionariomonitor['VwFuncionarioMonitor']['monitor_fk'] = $model->id;
        $dataProviderFuncionariomonitor = $model_funcionariomonitor->searchfuncionariomonitor($params_funcionariomonitor);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model_anterior = $this->findModel($id);
                $arquivo = UploadedFile::getInstance($model, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/monitor');
                $gravar = false;
                $model->netbios = strtoupper($model->netbios);
                $model->mac = Setup::removeFormatterMAC($model->mac);
                $model_anterior->mac = Setup::removeFormatterMAC($model_anterior->mac);
                if ($arquivo) {
                    $model->url = $model->codigo_monitor . ".pdf";
                    $gravar = true;
                } else {
                    $model->url = $model_anterior->url;
                }
                if(!$model_restricao){
                    $model->status = 1;
                }
                if ($model_anterior->unidade_fk != $model->unidade_fk) {
                    $model_ocorencia = new OcorenciaMonitor();
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $ocorencia_unidade_anterior = ($model_anterior->unidadeFk->unidade_pai_fk ?
                        ($model_anterior->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                            ($model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                            : $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                                        : $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                                    : $model_anterior->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                            : $model_anterior->unidadeFk->unidadePaiFk->unidade . " - " . $model_anterior->unidadeFk->unidade)
                        : $model_anterior->unidadeFk->unidade);
                    $ocorencia_unidade = ($model->unidadeFk->unidade_pai_fk ?
                        ($model->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                            ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade
                                            : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                        : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                                    : $model->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                            : $model->unidadeFk->unidadePaiFk->unidade . " - " . $model->unidadeFk->unidade)
                        : $model->unidadeFk->unidade);
                    $model_ocorencia->descricao = $model->getAttributeLabel('unidade_fk').': '.$ocorencia_unidade_anterior.' > '.$ocorencia_unidade;
                    if ($model_ocorencia->save()){
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submitanterior(
                                $emails,
                                $model,
                                $model_anterior,
                                $ocorencia_unidade_anterior,
                                $ocorencia_unidade,
                                'sgt_email_monitor_unidade_html'
                            );
                        }
                    }
                }
                if ($model_anterior->setor_fk != $model->setor_fk) {
                    $model_ocorencia = new OcorenciaMonitor();
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if($model_anterior->setor_fk != NULL){
                        $model_ocorencia->descricao = $model->getAttributeLabel('setor_fk').': '.$model_anterior->setorFk->setor.' > '.$model->setorFk->setor;
                    } else {
                        $model_ocorencia->descricao = $model->getAttributeLabel('setor_fk').': '.$model->setorFk->setor.' foi atribuido.';
                    }
                    if ($model_ocorencia->save()){
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submitanterior(
                                $emails,
                                $model,
                                $model_anterior,
                                $model_anterior->setorFk->setor,
                                $model->setorFk->setor,
                                'sgt_email_monitor_setor_html'
                            );
                        }
                    }
                }
                if ($model_anterior->numero_serie != $model->numero_serie) {
                    $model_ocorencia = new OcorenciaMonitor();
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('numero_serie').': '.$model_anterior->numero_serie.' > '.$model->numero_serie;
                    $model_ocorencia->save();
                }
                if ($model_anterior->garantia != $model->garantia) {
                    $model_ocorencia = new OcorenciaMonitor();
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('garantia').': '.$model_anterior->garantia.' > '.$model->garantia;
                    $model_ocorencia->save();
                }
                if ($model_anterior->status != $model->status) {
                    $model_ocorencia = new OcorenciaMonitor();
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    if ( $model->status == 1 ){
                        $ocorencia_status_anterior = "Inativo";
                        $ocorencia_status = "Ativo";
                        $model_ocorencia->descricao = $model->getAttributeLabel('status').': '.'Inativo > Ativo';
                    } else {
                        $ocorencia_status_anterior = "Ativo";
                        $ocorencia_status = "Inativo";
                        $model_ocorencia->descricao = $model->getAttributeLabel('status').': '.'Ativo > Inativo';
                        $model->status = 0;
                    }
                    if ($model_ocorencia->save()){
                        $emails = [];
                        if (!empty($model->unidadeFk->email_patrimonio1)) {
                            $emails[] = $model->unidadeFk->email_patrimonio1;
                        }
                        if (!empty($model->unidadeFk->email_patrimonio2)) {
                            $emails[] = $model->unidadeFk->email_patrimonio2;
                        }
                        if ($emails != NULL){
                            SendMail::submitanterior(
                                $emails,
                                $model,
                                $model_anterior,
                                $ocorencia_status_anterior,
                                $ocorencia_status,
                                'sgt_email_monitor_desativado_html'
                            );
                        }
                    }
                }
                if ($model_anterior->tipo_fk != $model->tipo_fk) {
                    $model_ocorencia = new OcorenciaMonitor();
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('tipo').': '.$model_anterior->tipoFk->tipo.' > '.$model->tipoFk->tipo;
                    $model_ocorencia->save();
                }
                if ($model_anterior->mac != $model->mac) {
                    $model_ocorencia = new OcorenciaMonitor();
                    $model_ocorencia->codigo_monitor_fk = $model->id;
                    $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
                    $model_ocorencia->tipo_fk = 1;
                    $model_ocorencia->data = date('d/m/Y H:i:s');
                    $model_ocorencia->descricao = $model->getAttributeLabel('mac').': '.Setup::formatterMAC($model_anterior->mac).' > '.Setup::formatterMAC($model->mac);
                    $model_ocorencia->save();
                }
                if ($model->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/monitor/' . $model->url);
                    }
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlMonitorSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Monitor',
                    'model' => $model,
                    'model_ocorencia' => $model_ocorencia,
                    'dataProviderOcorencia' => $dataProviderOcorencia,
                    'dataProviderFuncionariomonitor' => $dataProviderFuncionariomonitor,
                    'model_restricao' => $model_restricao,
        ]);
    }

    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            $model->ativo = 0;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
            } else {
                Yii::$app->session->setFlash('error', 'Não foi possivel remover!');
            }
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlMonitorSearch', array())]);
    }

    public function actionDeletefuncionario($id) {
        try {
            $modelfuncionario = $this->findModelfuncionario($id);
            $this->findModelfuncionario($id)->delete();
            Yii::$app->session->setFlash('success', 'Funcionario removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect(['monitor/update/'.$modelfuncionario->monitor_fk]);
    }

    protected function findModelfuncionario($id) {
        if (($modelfuncionario = FuncionarioMonitor::findOne($id)) !== null) {
            return $modelfuncionario;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    protected function findModel($id) {
        if (($model = Monitor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    protected function findModelocorencia($id) {
        if (($model = OcorenciaMonitor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    public function actionPdf($id) {
        $directory = \Yii::getAlias('@backend/web/arquivo');
        $model = $this->findModel($id);
        $pdf = file_get_contents($directory . '/monitor/' . $model->url);
        header('Content-Type: application/pdf');
        echo $pdf;
    }

    public function actionOcorenciapdf($id) {
        $directory = \Yii::getAlias('@backend/web/arquivo');
        $model = $this->findModelocorencia($id);
        if ($model->url != null ) {
            $pdf = file_get_contents($directory . '/ocorenciamonitor/' . $model->url);
            header('Content-Type: application/pdf');
            echo $pdf;
        } else {
            Yii::$app->session->setFlash('danger', 'Ops. Esta ocorencia não tem anexo!');
            return $this->redirect(['monitor/update/'.$model->codigo_monitor_fk]);
        }
    }
    
    public function actionExport() {
        $query = Monitor::find();
        $export = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
            ]),
            'columns' => [
                'codigo_monitor',
                [
                    'attribute' => 'unidade_fk',
                    'value' => function ($data) {
                        return ($data->unidadeFk->unidade_pai_fk ?
                            ($data->unidadeFk->unidadePaiFk->unidade_pai_fk ?
                                ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                    ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ?
                                        ($data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade_pai_fk ? 
                                            $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade
                                            : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                        : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                    : $data->unidadeFk->unidadePaiFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                                : $data->unidadeFk->unidadePaiFk->unidade . " - " . $data->unidadeFk->unidade)
                            : $data->unidadeFk->unidade);
                    },
                ],
                [
                    'attribute' => 'setor_fk',
                    'value' => function ($data) {
                        return ($data->setor_fk ? $data->setorFk->setor : "Setor não informado" );
                    },
                ],
                [
                    'attribute' => 'tipo_fk',
                    'value' => function ($data) {
                        return $data->tipoFk->tipo;
                    },
                ],
                [
                    'attribute' => 'Marca',
                    'value' => function ($data) {
                        return $data->modeloFk->marcaFk->marca;
                    },
                ],
                [
                    'attribute' => 'modelo_fk',
                    'value' => function ($data) {
                        return $data->modeloFk->modelo;
                    },
                ],
                'numero_serie',
                'data_compra',
                'garantia',
                'loja',
                'numero_nota',
                'valor',
                'numero_patrimonio',
                'netbios',
                'mac',
            ],
        ]);
                    
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->where(['usuario_fk' => Yii::$app->user->identity->id])->all();
        
        if (Yii::$app->user->identity->unidade_temp_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => [1, Yii::$app->user->identity->unidade_temp_fk]]);
        } else {
            $query->andFilterWhere(['IN', 'unidade_fk', $usuariounidade]);
        }
        
        return $export->send('monitores.xls');
    }
    
    public function actionSetSessionSetor() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->setor_temp_fk = $request['setor_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->setor_temp_fk = $request['setor_temp_fk'];
    }
    
    public function actionSetSessionTipo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->tipo_temp_fk = $request['tipo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->tipo_temp_fk = $request['tipo_temp_fk'];
    }
    
    public function actionSetSessionMarca() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->marca_temp_fk = $request['marca_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->marca_temp_fk = $request['marca_temp_fk'];
    }

    public function actionSetSessionFuncionario() {
        $model = new FuncionarioMonitor();
        $request = Yii::$app->request->post();

        $id_funcionario = $request['funcionario_fk'];
        $id_monitor = $request['monitor_fk'];

        $model_funcionario = Funcionario::findAll(['id' => $id_funcionario]);
        
        if ($model) {

            $model_ocorencia = new OcorenciaMonitor();
            $model_ocorencia->codigo_monitor_fk = $id_monitor;
            $model_ocorencia->usuario_nome = Yii::$app->user->identity->nome;
            $model_ocorencia->tipo_fk = 3;
            $model_ocorencia->data = date('d/m/Y H:i:s');
            $model_ocorencia->descricao = 'Foi atribuido para o usuário: '.$model_funcionario->nome;
            $model_ocorencia->save();

            $model->funcionario_fk = $id_funcionario;
            $model->monitor_fk = $id_monitor;
            $model->save();
        }
        Yii::$app->session->setFlash('success', 'Funcionario vinculado com sucesso!');
    }
    
    public function actionSetSessionModelo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->modelo_temp_fk = $request['modelo_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->modelo_temp_fk = $request['modelo_temp_fk'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->setor_temp_fk = NULL;
            $model->tipo_temp_fk = NULL;
            $model->marca_temp_fk = NULL;
            $model->modelo_temp_fk = NULL;
            $model->save();
        }
        return $this->redirect([Yii::$app->session->get('urlMonitorSearch', array())]);
    }

}
