<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\Senha;
use common\models\HistoricoSenha;
use common\models\UsuarioPerfil;
use common\models\UsuarioUnidade;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class SenhaController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'novasenha'],
                'rules' => AccessRulesControl::getRulesControl('senha'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Senha();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlSenhaSearch', Yii::$app->request->url);

        function gerarSenha($tamanho) {
            $caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*';
            // Embaralhar os caracteres
            $caracteresembaralhados = str_shuffle($caracteres);
            // Pegar uma substring com o tamanho desejado
            $senha = substr($caracteresembaralhados, 0, $tamanho);
            return $senha;
        }
        $senhagerada = gerarSenha(12);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'senhagerada' => $senhagerada,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $usuariounidade = UsuarioUnidade::find()
        ->select('unidade_fk')
        ->Where(['usuario_fk' => Yii::$app->user->identity->id])
        ->andWhere(['unidade_fk' => $model->unidade_fk])
        ->one();

        $params['HistoricoSenha']['id_senha_fk'] = $model->id;
        $dataProviderHistorico = $model_historico->searchhistorico($params);
        
        if($usuarioperfil->perfil_fk != 1){
            if($usuariounidade->unidade_fk != $model->unidade_fk){
                if ($model->unidade_fk != 1){
                    if ($model->unidade_fk != Yii::$app->user->identity->unidade_temp_fk || $model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este senha não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
                    }
                } else {
                    if ($model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este senha não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
                    }
                }
            }
        }
        
        $model->senha = base64_decode($model->senha);
        return $this->render('view', [
                    'title' => 'Detalhes da Senha',
                    'model' => $model,
                    'dataProviderHistorico' => $dataProviderHistorico,
        ]);
    }

    public function actionCreate() {
        $model = new Senha();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;

        $model_historico = new HistoricoSenha();
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $model_perfil = $usuarioperfil->perfil_fk;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model->status = 1;
                $model->senha = base64_encode($model->senha);

                if ($model->save()) {

                    $model_historico = new HistoricoSenha();
                    $model_historico->id_senha_fk = $model->id;
                    $model_historico->usuario_nome = Yii::$app->user->identity->nome;
                    $model_historico->tipo_fk = 4;
                    $model_historico->data = date('d/m/Y H:i:s');
                    $model_historico->descricao = 'Senha cadastrada no sistema.';
                    $model_historico->save();

                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
                }
            }
        }
        return $this->render('_form_create', [
            'title' => 'Adicionar nova Senha',
            'model' => $model,
            'model_perfil' => $model_perfil,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model_historico = new HistoricoSenha();
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $model_perfil = $usuarioperfil->perfil_fk;
        $usuariounidade = UsuarioUnidade::find()
        ->select('unidade_fk')
        ->Where(['usuario_fk' => Yii::$app->user->identity->id])
        ->andWhere(['unidade_fk' => $model->unidade_fk])
        ->one();

        $params['HistoricoSenha']['id_senha_fk'] = $model->id;
        $dataProviderHistorico = $model_historico->searchhistorico($params);
        
        if($usuarioperfil->perfil_fk != 1){
            if($usuariounidade->unidade_fk != $model->unidade_fk){
                if ($model->unidade_fk != 1){
                    if ($model->unidade_fk != Yii::$app->user->identity->unidade_temp_fk || $model->perfil_fk < $usuarioperfil->perfil_fk ){
                        
                        Yii::$app->session->setFlash('danger', 'Ops. Esta senha não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
                    }
                } else {
                    if ($model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Esta senha não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
                    }
                }
            }
        }

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model->senha = base64_encode($model->senha);

                $model_anterior = $this->findModel($id);

                if ($model_anterior->unidade_fk != $model->unidade_fk) {
                    $model_historico = new HistoricoSenha();
                    $model_historico->id_senha_fk = $model->id;
                    $model_historico->usuario_nome = Yii::$app->user->identity->nome;
                    $model_historico->tipo_fk = 3;
                    $model_historico->data = date('d/m/Y H:i:s');
                    $model_historico->descricao = $model->getAttributeLabel('unidade_fk').' '.$model_anterior->unidadeFk->unidade.' > '.$model->unidadeFk->unidade;
                    $model_historico->save();
                }

                if ($model_anterior->perfil_fk != $model->perfil_fk) {
                    $model_historico = new HistoricoSenha();
                    $model_historico->id_senha_fk = $model->id;
                    $model_historico->usuario_nome = Yii::$app->user->identity->nome;
                    $model_historico->tipo_fk = 1;
                    $model_historico->data = date('d/m/Y H:i:s');
                    $model_historico->descricao = $model->getAttributeLabel('perfil_fk').' '.$model_anterior->perfilFk->nome.' > '.$model->perfilFk->nome;
                    $model_historico->save();
                }

                if ($model_anterior->senha != $model->senha) {
                    $model_historico = new HistoricoSenha();
                    $model_historico->id_senha_fk = $model->id;
                    $model_historico->usuario_nome = Yii::$app->user->identity->nome;
                    $model_historico->tipo_fk = 1;
                    $model_historico->data = date('d/m/Y H:i:s');
                    $model_historico->descricao = 'Alteraçao da senha.';
                    $model_historico->senha_antiga = $model_anterior->senha;
                    $model_historico->save();
                }

                if ($model_anterior->descricao != $model->descricao) {
                    $model_historico = new HistoricoSenha();
                    $model_historico->id_senha_fk = $model->id;
                    $model_historico->usuario_nome = Yii::$app->user->identity->nome;
                    $model_historico->tipo_fk = 1;
                    $model_historico->data = date('d/m/Y H:i:s');
                    $model_historico->descricao = 'Descrição: '. $model_anterior->descricao .' > '. $model->descricao;
                    $model_historico->save();
                }

                if ($model_anterior->status != $model->status) {
                    $model_historico = new HistoricoSenha();
                    $model_historico->id_senha_fk = $model->id;
                    $model_historico->usuario_nome = Yii::$app->user->identity->nome;
                    $model_historico->tipo_fk = 1;
                    $model_historico->data = date('d/m/Y H:i:s');
                    $model_historico->descricao = $model->getAttributeLabel('status').' '.$model->status ? 'Status: Inativo > Ativo' : 'Ativo > Inativo';
                    $model_historico->save();
                        
                }

                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
                }
            } else {
                $model->senha = base64_decode($model->senha);                
            }
        }
        return $this->render('_form', [
            'title' => 'Alterar Senha',
            'model' => $model,
            'model_perfil' => $model_perfil,
            'model_historico' => $model_historico,
            'dataProviderHistorico' => $dataProviderHistorico,
        ]);
    }

    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            $model->status = 0;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
            } else {
                Yii::$app->session->setFlash('error', 'Não foi possivel remover!');
            }
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Senha::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    public function actionNovasenha() {
        return $this->redirect([Yii::$app->session->get('urlSenhaSearch', array())]);
    }

}
