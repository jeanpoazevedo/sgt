<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\Pon;
use common\models\PonOnu;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class PonController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'createonu', 'updateonu', 'deleteonu'],
                'rules' => AccessRulesControl::getRulesControl('pon'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Pon();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlPonSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes da PON',
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Pon();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlPonSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar novo PON',
                    'model' => $model,
        ]);
    }

    public function actionCreateonu($id) {
        $model = new PonOnu();
        $olt_id_temp = $id;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model->olt_fk = $olt_id_temp;
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect(['/pon/update/'.$olt_id_temp]);
                }
            }
        }
        return $this->render('_form_onu', [
                    'title' => 'Adicionar nova ONU',
                    'model' => $model,
                    'olt_id_temp' => $olt_id_temp,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $model_onu = new PonOnu();

        $params['PonOnu']['olt_fk'] = $model->id;
        $dataProviderOnu = $model_onu->searchonu($params);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlPonSearch', array())]);
                }
            }
        }
        return $this->render('_form_edit', [
                    'title' => 'Alterar PON',
                    'model' => $model,
                    'model_onu' => $model_onu,
                    'dataProviderOnu' => $dataProviderOnu,
        ]);
    }

    public function actionUpdateonu($id) {
        $model = $this->findModelonu($id);

        $olt_id_temp = $model->olt_fk;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect(['/pon/update/'.$olt_id_temp]);
                }
            }
        }
        return $this->render('_form_onu', [
                    'title' => 'Alterar ONU',
                    'model' => $model,
                    'olt_id_temp' => $olt_id_temp,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlPonSearch', array())]);
    }

    public function actionDeleteonu($id) {
        $model = $this->findModelonu($id);
        try {
            $this->findModelonu($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect(['/pon/update/'.$model->olt_fk]);
    }

    protected function findModel($id) {
        if (($model = Pon::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    protected function findModelonu($id) {
        if (($model = PonOnu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
