<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\MapaCanal;
use common\models\Usuario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class MapacanalController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'clean'],
                'rules' => AccessRulesControl::getRulesControl('mapacanal'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Mapacanal();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlMapacanalSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes da Mapacanal',
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Mapacanal();
        $model->patch_cord_switch = 0;
        $model->patch_cord_dispositivo = 0;
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
        
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlMapacanalSearch', array())]);
                }
            }
        }
        return $this->render('_form_creat', [
                    'title' => 'Adicionar nova Mapacanal',
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlMapacanalSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Mapacanal',
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlMapacanalSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Mapacanal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionSetSessionLocal() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->local_temp_fk = $request['local_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->local_temp_fk = $request['local_temp_fk'];
    }
    
    public function actionSetSessionSwitch() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->switch_temp = $request['switch_temp'];
            $model->save();
        }
        Yii::$app->user->identity->switch_temp = $request['switch_temp'];
    }
    
    public function actionSetSessionPatchpanel() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->patch_panel_temp = $request['patch_panel_temp'];
            $model->save();
        }
        Yii::$app->user->identity->patch_panel_temp = $request['patch_panel_temp'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->local_temp_fk = NULL;
            $model->switch_temp = NULL;
            $model->patch_panel_temp = NULL;
            $model->save();
        }
        return $this->redirect([Yii::$app->session->get('urlMapacanalSearch', array())]);
    }

}
