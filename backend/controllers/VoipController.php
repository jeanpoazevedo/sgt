<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\Voip;
use common\models\UsuarioUnidade;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class VoipController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => AccessRulesControl::getRulesControl('voip'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Voip();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlVoipSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes do VoIP',
                    'model' => $this->findPin($id),
        ]);
    }

    public function actionCreate() {
        $model = new Voip();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlVoipSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar novo VoIP',
                    'model' => $model,
                    'usuariounidade' => $usuariounidade,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findVoip($id);
        $usuariounidade = UsuarioUnidade::find()->select('unidade_fk')->Where(['usuario_fk' => Yii::$app->user->identity->id])->andWhere(['>', 'unidade_fk', '1']);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlVoipSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar VoIP',
                    'model' => $model,
                    'usuariounidade' => $usuariounidade,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findVoip($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlVoipSearch', array())]);
    }

    protected function findVoip($id) {
        if (($model = Voip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
