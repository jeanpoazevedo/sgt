<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\Contrato;
use common\models\UsuarioPerfil;
use common\models\UsuarioUnidade;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use kartik\date\DatePicker;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class ContratoController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'arquivo'],
                'rules' => AccessRulesControl::getRulesControl('contrato'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Contrato();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlContratoSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $usuariounidade = UsuarioUnidade::find()
        ->select('unidade_fk')
        ->Where(['usuario_fk' => Yii::$app->user->identity->id])
        ->andWhere(['unidade_fk' => $model->unidade_fk])
        ->one();
        
        if($usuarioperfil->perfil_fk != 1){
            if($usuariounidade->unidade_fk != $model->unidade_fk){
                if ($model->unidade_fk != 1){
                    if ($model->unidade_fk != Yii::$app->user->identity->unidade_temp_fk || $model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este contrato não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
                    }
                } else {
                    if ($model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Este contrato não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
                    }
                }
            }
        }
        
        return $this->render('view', [
                    'title' => 'Detalhes da contrato',
                    'model' => $model,
        ]);
    }

    public function actionCreate() {
        $model = new contrato();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $model_perfil = $usuarioperfil->perfil_fk; 

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $arquivo = UploadedFile::getInstance($model, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/contrato');
                $gravar = false;
                if ($arquivo) {
                    $model->url = date('YmdHis') . '.' . $arquivo->extension;
                    $gravar = true;
                } else {
                    $model->url = NULL;
                }
                $model->data = date('d/m/Y H:i:s');
                $model->data_alteracao = NULL;
                if ($model->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/contrato/' . $model->url);
                    }
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
            'title' => 'Adicionar novo contrato',
            'model' => $model,
            'model_perfil' => $model_perfil,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        $model_perfil = $usuarioperfil->perfil_fk;
        $usuariounidade = UsuarioUnidade::find()
        ->select('unidade_fk')
        ->Where(['usuario_fk' => Yii::$app->user->identity->id])
        ->andWhere(['unidade_fk' => $model->unidade_fk])
        ->one();

        if($model->url != null){
            if($model->data_fim != null){
                Yii::$app->session->setFlash('danger', 'Este documento já está completo e não pode ser alterado`!');
                return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
            }
        }
        
        if($usuarioperfil->perfil_fk != 1){
            if($usuariounidade->unidade_fk != $model->unidade_fk){
                if ($model->unidade_fk != 1){
                    if ($model->unidade_fk != Yii::$app->user->identity->unidade_temp_fk || $model->perfil_fk < $usuarioperfil->perfil_fk ){
                        
                        Yii::$app->session->setFlash('danger', 'Ops. Esta contrato não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
                    }
                } else {
                    if ($model->perfil_fk < $usuarioperfil->perfil_fk ){
                        Yii::$app->session->setFlash('danger', 'Ops. Esta contrato não está disponivel!');
                        return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
                    }
                }
            }
        }

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $arquivo = UploadedFile::getInstance($model, 'url');
                $directory = \Yii::getAlias('@backend/web/');
                FileHelper::createDirectory($directory . 'arquivo/contrato');
                $gravar = false;
                if ($arquivo) {
                    $model->url = date('YmdHis') . '.' . $arquivo->extension;
                    $gravar = true;
                } else {
                    $model->url = NULL;
                }
                $model->data_alteracao = date('d/m/Y H:i:s');
                if ($model->save()) {
                    if ($gravar) {
                        $arquivo->saveAs($directory . 'arquivo/contrato/' . $model->url);
                    }
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
            'title' => 'Alterar Contrato',
            'model' => $model,
            'model_perfil' => $model_perfil,
        ]);
    }

    public function actionArquivo($id) {
        $directory = \Yii::getAlias('@backend/web/arquivo');
        $model = $this->findModel($id);
        $pdf = file_get_contents($directory . '/contrato/' . $model->url);
        header('Content-Type: application/pdf');
        echo $pdf;
    }

    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            if($model->url != null){
                $directory = \Yii::getAlias('@backend/web/arquivo/');
                if (file_exists($directory . '/contrato/' . $model->url)) {
                    unlink($directory . '/contrato/' . $model->url);
                }
            }
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlContratoSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Contrato::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
