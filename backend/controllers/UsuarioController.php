<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\components\SendMail;
use common\models\Usuario;
use common\models\VwUsuario;
use common\models\UsuarioPerfil;
use common\models\UsuarioUnidade;
use common\models\UsuarioGerenteUnidade;
use common\models\Unidade;
use common\models\HdUsuarioUnidade;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Json;
use yii\helpers\Html;

class UsuarioController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update','updatepassword' , 'view', 'delete', 'password', 'unidade', 'hdunidade'],
                'rules' => AccessRulesControl::getRulesControl('usuario'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwUsuario();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlUsuarioSearch', Yii::$app->request->url);
        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes do Usuario',
                    'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCreate() {
        $model = new Usuario();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;

        $model_perfil = new UsuarioPerfil();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model->senha = date('d/m/Y H:i:s');
                $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                $model->senha_hash = $model->senha;
                $model->unidade_temp_fk = $model->unidade_fk;
                $model->senha = Yii::$app->security->generatePasswordHash(date('Ymd'));
                $model->senha_hash = $model->senha;
                $model->auth_key = Yii::$app->security->generateRandomString();
                $model->token = Yii::$app->security->generateRandomString();
                if ($model->save()) {
                    $model_perfil = new UsuarioPerfil();
                    $model_perfil->load(Yii::$app->request->post());
                    $model_perfil->usuario_fk = $model->id;
                    if ($model_perfil->save()) {
                        Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                        return $this->redirect(['usuario/index']);
                    }
                }
            } else {
                $model->status = true;
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar novo Usuario',
                    'model' => $model,
                    'model_perfil' => $model_perfil,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => $model->id])->one();
        $usuarioperfilid = $usuarioperfil->id;
        $model_perfil = $this->findModelusuarioperfil($usuarioperfilid);
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    $model_perfil->load(Yii::$app->request->post());
                    if ($model_perfil->save()) {
                        Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                        return $this->redirect(['usuario/index']);
                    }
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Usuario',
                    'model' => $model,
                    'model_perfil' => $model_perfil,
        ]);
    }
    
    public function actionUpdatepassword($id) {
        $model = $this->findModel($id);
        $model->senha = '';
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => $model->id])->one();
        $usuarioperfilid = $usuarioperfil->id;
        $model_perfil = $this->findModelusuarioperfil($usuarioperfilid);
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                $model->senha_hash = $model->senha;
                $model->trocar_senha = null;
                if ($model->save()) {
                    $model_perfil->load(Yii::$app->request->post());
                    if ($model_perfil->save()) {
                        Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                        return $this->redirect(['usuario/index']);
                    }
                }
            }
        }
        return $this->render('password', [
                    'title' => 'Alterar senha do Usuário',
                    'model' => $model,
                    'model_perfil' => $model_perfil,
        ]);
    }
    
    public function actionUnidade($id) {
        $data = ''; 
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        if($usuarioperfil->perfil_fk != 1){
            $model_unindade_nivel_0 = Unidade::find()->where(['nivel' => '0'])->orderBy(['unidade' => SORT_ASC])->all();
            if ($model_unindade_nivel_0) {
                $data .= '<li data-jstree=\'{"opened":true}\'> Todas';
                $data .= '<ul>';
                foreach ($model_unindade_nivel_0 as $key_nivel_0 => $value_nivel_0) {
                    $model_busca_gerente_nivel_0 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_0->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                    $model_busca_unindade_nivel_0 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_0->id, 'usuario_fk' => $id])->one(); 
                    $model_unidade_nivel_1 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_0->id])->orderBy(['unidade' => SORT_ASC])->all();
                    if ($model_unidade_nivel_1) {
                        if($model_busca_gerente_nivel_0){
                            $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_0->unidade;
                            $data .= '<ul>';
                            foreach ($model_unidade_nivel_1 as $key_nivel_1 => $value_nivel_1) {
                                $model_busca_gerente_nivel_1 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_1->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                $model_busca_unindade_nivel_1 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_1->id, 'usuario_fk' => $id])->one();
                                $model_unidade_nivel_2 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_1->id])->orderBy(['unidade' => SORT_ASC])->all();
                                if ($model_unidade_nivel_2) {
                                    if($model_busca_gerente_nivel_1){
                                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_1->unidade;
                                        $data .= '<ul>';
                                        foreach ($model_unidade_nivel_2 as $key_nivel_2 => $value_nivel_2) {
                                            $model_busca_gerente_nivel_2 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_2->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                            $model_busca_unindade_nivel_2 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_2->id, 'usuario_fk' => $id])->one();
                                            $model_unidade_nivel_3 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_2->id])->orderBy(['unidade' => SORT_ASC])->all();
                                            if ($model_unidade_nivel_3) {
                                                if($model_busca_gerente_nivel_2){
                                                    $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_2->unidade;
                                                    $data .= '<ul>';
                                                    foreach ($model_unidade_nivel_3 as $key_nivel_3 => $value_nivel_3) {
                                                        $model_busca_gerente_nivel_3 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_3->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                                        $model_busca_unindade_nivel_3 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_3->id, 'usuario_fk' => $id])->one();
                                                        $model_unidade_nivel_4 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_3->id])->orderBy(['unidade' => SORT_ASC])->all();
                                                        if ($model_unidade_nivel_4) {
                                                            if($model_busca_gerente_nivel_3){
                                                                $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_3->unidade;
                                                                $data .= '<ul>';
                                                                foreach ($model_unidade_nivel_4 as $key_nivel_4 => $value_nivel_4) {
                                                                    $model_busca_gerente_nivel_4 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_4->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                                                    $model_busca_unindade_nivel_4 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_4->id, 'usuario_fk' => $id])->one();
                                                                    $model_unidade_nivel_5 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_4->id])->orderBy(['unidade' => SORT_ASC])->all();
                                                                    if ($model_unidade_nivel_5) {
                                                                        if($model_busca_gerente_nivel_4){
                                                                            $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_4->unidade;
                                                                            $data .= '<ul>';
                                                                            foreach ($model_unidade_nivel_5 as $key_nivel_5 => $value_nivel_5) {
                                                                                $model_busca_unindade_nivel_5 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_5->id, 'usuario_fk' => $id])->one();
                                                                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_5->id . '", ' . (!empty($model_busca_unindade_nivel_5) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_5->unidade . '</li>';
                                                                            }
                                                                            $data .= '</ul>';
                                                                            $data .= '</li>';
                                                                        }
                                                                    } else if ($value_nivel_4->entidade == 1 && $model_busca_gerente_nivel_3 && $model_busca_gerente_nivel_3->gerencia == 1) {
                                                                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_4->id . '", ' . (!empty($model_busca_unindade_nivel_4) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_4->unidade . '</li>';
                                                                    }
                                                                }
                                                                $data .= '</ul>';
                                                                $data .= '</li>';
                                                            }
                                                        } else if ($value_nivel_3->entidade == 1 && $model_busca_gerente_nivel_2 && $model_busca_gerente_nivel_2->gerencia == 1) {
                                                            $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_3->id . '", ' . (!empty($model_busca_unindade_nivel_3) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_3->unidade . '</li>';
                                                        }
                                                    }
                                                    $data .= '</ul>';
                                                    $data .= '</li>';
                                                }
                                            } else if ($value_nivel_2->entidade == 1 && $model_busca_gerente_nivel_1 && $model_busca_gerente_nivel_1->gerencia == 1) {
                                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_2->id . '", ' . (!empty($model_busca_unindade_nivel_2) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_2->unidade . '</li>';
                                            }
                                        }
                                        $data .= '</ul>';
                                        $data .= '</li>';
                                    }
                                } else if ($value_nivel_1->entidade == 1 && $model_busca_gerente_nivel_0 && $model_busca_gerente_nivel_0->gerencia == 1) {
                                    $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_1->id . '", ' . (!empty($model_busca_unindade_nivel_1) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_1->unidade . '</li>';
                                }
                            }
                            $data .= '</ul>';
                            $data .= '</li>';
                        }
                    } else if ($value_nivel_0->entidade == 1 || $value_nivel_0->id == 1) {
                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_0->id . '", ' . (!empty($model_busca_unindade_nivel_0) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_0->unidade . '</li>';
                    }
                }
                $data .= '</ul>';
                $data .= '</li>';
            } else {
                $data .= '<li data-jstree=\'{"opened":true\'>' . $value_nivel_0->nome;
            }
        } else {
            $model_unindade_nivel_0 = Unidade::find()->where(['nivel' => '0'])->orderBy(['unidade' => SORT_ASC])->all();
            if ($model_unindade_nivel_0) {
                $data .= '<li data-jstree=\'{"opened":true}\'> Todas';
                $data .= '<ul>';
                foreach ($model_unindade_nivel_0 as $key_nivel_0 => $value_nivel_0) {
                    $model_busca_unindade_nivel_0 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_0->id, 'usuario_fk' => $id])->one();
                    $model_unidade_nivel_1 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_0->id])->orderBy(['unidade' => SORT_ASC])->all();
                    if ($model_unidade_nivel_1) {
                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_0->unidade;
                        $data .= '<ul>';
                        foreach ($model_unidade_nivel_1 as $key_nivel_1 => $value_nivel_1) {
                            $model_busca_unindade_nivel_1 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_1->id, 'usuario_fk' => $id])->one();
                            $model_unidade_nivel_2 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_1->id])->orderBy(['unidade' => SORT_ASC])->all();
                            if ($model_unidade_nivel_2) {
                                $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_1->unidade;
                                $data .= '<ul>';
                                foreach ($model_unidade_nivel_2 as $key_nivel_2 => $value_nivel_2) {
                                    $model_busca_unindade_nivel_2 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_2->id, 'usuario_fk' => $id])->one();
                                    $model_unidade_nivel_3 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_2->id])->orderBy(['unidade' => SORT_ASC])->all();
                                    if ($model_unidade_nivel_3) {
                                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_2->unidade;
                                        $data .= '<ul>';
                                        foreach ($model_unidade_nivel_3 as $key_nivel_3 => $value_nivel_3) {
                                            $model_busca_unindade_nivel_3 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_3->id, 'usuario_fk' => $id])->one();
                                            $model_unidade_nivel_4 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_3->id])->orderBy(['unidade' => SORT_ASC])->all();
                                            if ($model_unidade_nivel_4) {
                                                $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_3->unidade;
                                                $data .= '<ul>';
                                                foreach ($model_unidade_nivel_4 as $key_nivel_4 => $value_nivel_4) {
                                                    $model_busca_unindade_nivel_4 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_4->id, 'usuario_fk' => $id])->one();
                                                    $model_unidade_nivel_5 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_4->id])->orderBy(['unidade' => SORT_ASC])->all();
                                                    if ($model_unidade_nivel_5) {
                                                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_4->unidade;
                                                        $data .= '<ul>';
                                                        foreach ($model_unidade_nivel_5 as $key_nivel_5 => $value_nivel_5) {
                                                            $model_busca_unindade_nivel_5 = UsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_5->id, 'usuario_fk' => $id])->one();
                                                            $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_5->id . '", ' . (!empty($model_busca_unindade_nivel_5) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_5->unidade . '</li>';
                                                        }
                                                        $data .= '</ul>';
                                                        $data .= '</li>';
                                                    } else {
                                                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_4->id . '", ' . (!empty($model_busca_unindade_nivel_4) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_4->unidade . '</li>';
                                                    }
                                                }
                                                $data .= '</ul>';
                                                $data .= '</li>';
                                            } else {
                                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_3->id . '", ' . (!empty($model_busca_unindade_nivel_3) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_3->unidade . '</li>';
                                            }
                                        }
                                        $data .= '</ul>';
                                        $data .= '</li>';
                                    } else {
                                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_2->id . '", ' . (!empty($model_busca_unindade_nivel_2) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_2->unidade . '</li>';
                                    }
                                }
                                $data .= '</ul>';
                                $data .= '</li>';
                            } else {
                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_1->id . '", ' . (!empty($model_busca_unindade_nivel_1) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_1->unidade . '</li>';
                            }
                        }
                        $data .= '</ul>';
                        $data .= '</li>';
                    } else {
                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_0->id . '", ' . (!empty($model_busca_unindade_nivel_0) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_0->unidade . '</li>';
                    }
                }
                $data .= '</ul>';
                $data .= '</li>';
            } else {
                $data .= '<li data-jstree=\'{"opened":true\'>' . $value_nivel_0->nome;
            }
        }

        return $this->render('unidade', [
                    'title' => 'Associação Usuário ao Unidade',
                    'data' => $data,
                    'model' => Usuario::findOne($id),
        ]);
    }
    
    public function actionHdunidade($id) {
        $data = ''; 
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        if($usuarioperfil->perfil_fk != 1){
            $model_unindade_nivel_0 = Unidade::find()->where(['nivel' => '0'])->orderBy(['unidade' => SORT_ASC])->all();
            if ($model_unindade_nivel_0) {
                $data .= '<li data-jstree=\'{"opened":true}\'> Todas';
                $data .= '<ul>';
                foreach ($model_unindade_nivel_0 as $key_nivel_0 => $value_nivel_0) {
                    $model_busca_gerente_nivel_0 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_0->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                    $model_busca_unindade_nivel_0 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_0->id, 'usuario_fk' => $id])->one(); 
                    $model_unidade_nivel_1 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_0->id])->orderBy(['unidade' => SORT_ASC])->all();
                    if ($model_unidade_nivel_1) {
                        if($model_busca_gerente_nivel_0){
                            $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_0->unidade;
                            $data .= '<ul>';
                            foreach ($model_unidade_nivel_1 as $key_nivel_1 => $value_nivel_1) {
                                $model_busca_gerente_nivel_1 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_1->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                $model_busca_unindade_nivel_1 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_1->id, 'usuario_fk' => $id])->one();
                                $model_unidade_nivel_2 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_1->id])->orderBy(['unidade' => SORT_ASC])->all();
                                if ($model_unidade_nivel_2) {
                                    if($model_busca_gerente_nivel_1){
                                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_1->unidade;
                                        $data .= '<ul>';
                                        foreach ($model_unidade_nivel_2 as $key_nivel_2 => $value_nivel_2) {
                                            $model_busca_gerente_nivel_2 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_2->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                            $model_busca_unindade_nivel_2 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_2->id, 'usuario_fk' => $id])->one();
                                            $model_unidade_nivel_3 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_2->id])->orderBy(['unidade' => SORT_ASC])->all();
                                            if ($model_unidade_nivel_3) {
                                                if($model_busca_gerente_nivel_2){
                                                    $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_2->unidade;
                                                    $data .= '<ul>';
                                                    foreach ($model_unidade_nivel_3 as $key_nivel_3 => $value_nivel_3) {
                                                        $model_busca_gerente_nivel_3 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_3->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                                        $model_busca_unindade_nivel_3 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_3->id, 'usuario_fk' => $id])->one();
                                                        $model_unidade_nivel_4 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_3->id])->orderBy(['unidade' => SORT_ASC])->all();
                                                        if ($model_unidade_nivel_4) {
                                                            if($model_busca_gerente_nivel_3){
                                                                $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_3->unidade;
                                                                $data .= '<ul>';
                                                                foreach ($model_unidade_nivel_4 as $key_nivel_4 => $value_nivel_4) {
                                                                    $model_busca_gerente_nivel_4 = UsuarioGerenteUnidade::find()->where(['unidade_fk' => $value_nivel_4->id, 'usuario_fk' => Yii::$app->user->identity->id])->one();
                                                                    $model_busca_unindade_nivel_4 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_4->id, 'usuario_fk' => $id])->one();
                                                                    $model_unidade_nivel_5 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_4->id])->orderBy(['unidade' => SORT_ASC])->all();
                                                                    if ($model_unidade_nivel_5) {
                                                                        if($model_busca_gerente_nivel_4){
                                                                            $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_4->unidade;
                                                                            $data .= '<ul>';
                                                                            foreach ($model_unidade_nivel_5 as $key_nivel_5 => $value_nivel_5) {
                                                                                $model_busca_unindade_nivel_5 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_5->id, 'usuario_fk' => $id])->one();
                                                                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_5->id . '", ' . (!empty($model_busca_unindade_nivel_5) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_5->unidade . '</li>';
                                                                            }
                                                                            $data .= '</ul>';
                                                                            $data .= '</li>';
                                                                        }
                                                                    } else if ($value_nivel_4->entidade == 1 && $model_busca_gerente_nivel_3 && $model_busca_gerente_nivel_3->gerencia == 1) {
                                                                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_4->id . '", ' . (!empty($model_busca_unindade_nivel_4) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_4->unidade . '</li>';
                                                                    }
                                                                }
                                                                $data .= '</ul>';
                                                                $data .= '</li>';
                                                            }
                                                        } else if ($value_nivel_3->entidade == 1 && $model_busca_gerente_nivel_2 && $model_busca_gerente_nivel_2->gerencia == 1) {
                                                            $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_3->id . '", ' . (!empty($model_busca_unindade_nivel_3) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_3->unidade . '</li>';
                                                        }
                                                    }
                                                    $data .= '</ul>';
                                                    $data .= '</li>';
                                                }
                                            } else if ($value_nivel_2->entidade == 1 && $model_busca_gerente_nivel_1 && $model_busca_gerente_nivel_1->gerencia == 1) {
                                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_2->id . '", ' . (!empty($model_busca_unindade_nivel_2) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_2->unidade . '</li>';
                                            }
                                        }
                                        $data .= '</ul>';
                                        $data .= '</li>';
                                    }
                                } else if ($value_nivel_1->entidade == 1 && $model_busca_gerente_nivel_0 && $model_busca_gerente_nivel_0->gerencia == 1) {
                                    $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_1->id . '", ' . (!empty($model_busca_unindade_nivel_1) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_1->unidade . '</li>';
                                }
                            }
                            $data .= '</ul>';
                            $data .= '</li>';
                        }
                    } else if ($value_nivel_0->entidade == 1 || $value_nivel_0->id == 1) {
                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_0->id . '", ' . (!empty($model_busca_unindade_nivel_0) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_0->unidade . '</li>';
                    }
                }
                $data .= '</ul>';
                $data .= '</li>';
            } else {
                $data .= '<li data-jstree=\'{"opened":true\'>' . $value_nivel_0->nome;
            }
        } else {
            $model_unindade_nivel_0 = Unidade::find()->where(['nivel' => '0'])->orderBy(['unidade' => SORT_ASC])->all();
            if ($model_unindade_nivel_0) {
                $data .= '<li data-jstree=\'{"opened":true}\'> Todas';
                $data .= '<ul>';
                foreach ($model_unindade_nivel_0 as $key_nivel_0 => $value_nivel_0) {
                    $model_busca_unindade_nivel_0 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_0->id, 'usuario_fk' => $id])->one();
                    $model_unidade_nivel_1 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_0->id])->orderBy(['unidade' => SORT_ASC])->all();
                    if ($model_unidade_nivel_1) {
                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_0->unidade;
                        $data .= '<ul>';
                        foreach ($model_unidade_nivel_1 as $key_nivel_1 => $value_nivel_1) {
                            $model_busca_unindade_nivel_1 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_1->id, 'usuario_fk' => $id])->one();
                            $model_unidade_nivel_2 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_1->id])->orderBy(['unidade' => SORT_ASC])->all();
                            if ($model_unidade_nivel_2) {
                                $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_1->unidade;
                                $data .= '<ul>';
                                foreach ($model_unidade_nivel_2 as $key_nivel_2 => $value_nivel_2) {
                                    $model_busca_unindade_nivel_2 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_2->id, 'usuario_fk' => $id])->one();
                                    $model_unidade_nivel_3 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_2->id])->orderBy(['unidade' => SORT_ASC])->all();
                                    if ($model_unidade_nivel_3) {
                                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_2->unidade;
                                        $data .= '<ul>';
                                        foreach ($model_unidade_nivel_3 as $key_nivel_3 => $value_nivel_3) {
                                            $model_busca_unindade_nivel_3 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_3->id, 'usuario_fk' => $id])->one();
                                            $model_unidade_nivel_4 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_3->id])->orderBy(['unidade' => SORT_ASC])->all();
                                            if ($model_unidade_nivel_4) {
                                                $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_3->unidade;
                                                $data .= '<ul>';
                                                foreach ($model_unidade_nivel_4 as $key_nivel_4 => $value_nivel_4) {
                                                    $model_busca_unindade_nivel_4 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_4->id, 'usuario_fk' => $id])->one();
                                                    $model_unidade_nivel_5 = Unidade::find()->where(['unidade_pai_fk' => $value_nivel_4->id])->orderBy(['unidade' => SORT_ASC])->all();
                                                    if ($model_unidade_nivel_5) {
                                                        $data .= '<li data-jstree=\'{"opened":true}\'>' . $value_nivel_4->unidade;
                                                        $data .= '<ul>';
                                                        foreach ($model_unidade_nivel_5 as $key_nivel_5 => $value_nivel_5) {
                                                            $model_busca_unindade_nivel_5 = HdUsuarioUnidade::find()->where(['unidade_fk' => $value_nivel_5->id, 'usuario_fk' => $id])->one();
                                                            $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_5->id . '", ' . (!empty($model_busca_unindade_nivel_5) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_5->unidade . '</li>';
                                                        }
                                                        $data .= '</ul>';
                                                        $data .= '</li>';
                                                    } else {
                                                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_4->id . '", ' . (!empty($model_busca_unindade_nivel_4) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_4->unidade . '</li>';
                                                    }
                                                }
                                                $data .= '</ul>';
                                                $data .= '</li>';
                                            } else {
                                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_3->id . '", ' . (!empty($model_busca_unindade_nivel_3) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_3->unidade . '</li>';
                                            }
                                        }
                                        $data .= '</ul>';
                                        $data .= '</li>';
                                    } else {
                                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_2->id . '", ' . (!empty($model_busca_unindade_nivel_2) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_2->unidade . '</li>';
                                    }
                                }
                                $data .= '</ul>';
                                $data .= '</li>';
                            } else {
                                $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_1->id . '", ' . (!empty($model_busca_unindade_nivel_1) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_1->unidade . '</li>';
                            }
                        }
                        $data .= '</ul>';
                        $data .= '</li>';
                    } else {
                        $data .= '<li data-jstree=\'{"tipo": "unidade", "key": "' . $value_nivel_0->id . '", ' . (!empty($model_busca_unindade_nivel_0) ? '"selected":true' : '"selected":false') . '}\'>' . $value_nivel_0->unidade . '</li>';
                    }
                }
                $data .= '</ul>';
                $data .= '</li>';
            } else {
                $data .= '<li data-jstree=\'{"opened":true\'>' . $value_nivel_0->nome;
            }
        }

        return $this->render('hdunidade', [
                    'title' => 'Associação Usuário ao Unidade',
                    'data' => $data,
                    'model' => Usuario::findOne($id),
        ]);
    }
    
    public function actionSaveUnidadeUsuario() {
        $request = Yii::$app->request->post();
        $array = json_decode($request['dados']);
        UsuarioUnidade::deleteAll(['usuario_fk' => $request['id']]);
        foreach ($array as $key => $value) {
            if ($value->selected == 'true') {
                $unidade = Unidade::findOne($value->key);
                $unidade_usuario = UsuarioUnidade::findOne(['usuario_fk' => $request['id'], 'unidade_fk' => $value->key]);
                if (!$unidade_usuario) {
                    $userunidade = new UsuarioUnidade();
                    $userunidade->usuario_fk = $request['id'];
                    $userunidade->unidade_fk = $value->key;
                    $userunidade->save();
                }
            }
        }
        $results['status'] = 'success';
        return Json::encode($results);
    }

    public function actionSaveHdUnidadeUsuario() {
        $request = Yii::$app->request->post();
        $array = json_decode($request['dados']);
        HdUsuarioUnidade::deleteAll(['usuario_fk' => $request['id']]);
        foreach ($array as $key => $value) {
            if ($value->selected == 'true') {
                $unidade = Unidade::findOne($value->key);
                $unidade_usuario = HdUsuarioUnidade::findOne(['usuario_fk' => $request['id'], 'unidade_fk' => $value->key]);
                if (!$unidade_usuario) {
                    $userunidade = new HdUsuarioUnidade();
                    $userunidade->usuario_fk = $request['id'];
                    $userunidade->unidade_fk = $value->key;
                    $userunidade->save();
                }
            }
        }
        $results['status'] = 'success';
        return Json::encode($results);
    }

    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            $model->status = 0;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
            } else {
                Yii::$app->session->setFlash('error', 'Não foi possivel remover!');
            }
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlUsuarioSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    protected function findModelusuarioperfil($usuarioperfilid) {
        if (($model_perfil = UsuarioPerfil::findOne($usuarioperfilid)) !== null) {
            return $model_perfil;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionPassword($id) {
        $model = Usuario::findOne($id);
        $model->token = Yii::$app->security->generateRandomString();
        $model->save();
        $emails = [ $model->email ];
        SendMail::submitpassword($emails, $model, 'sgt_email_usuario_senha-html');
        Yii::$app->session->setFlash('success', 'Enviado link para atualizar a senha!');
        return $this->redirect(['usuario/index']);
    }

}
