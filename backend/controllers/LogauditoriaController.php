<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\LogAuditoria;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class LogauditoriaController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view'],
                'rules' => AccessRulesControl::getRulesControl('logauditoria'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new LogAuditoria();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlLogauditoriaSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        
        return $this->render('view', [
                    'title' => 'Detalhes da Log Auditoria',
                    'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id) {
        if (($model = LogAuditoria::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
