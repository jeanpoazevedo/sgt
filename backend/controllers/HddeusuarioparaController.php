<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\models\HdDeUsuarioPara;
use common\models\VwHdDeUsuarioPara;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class HddeusuarioparaController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => AccessRulesControl::getRulesControl('hddeusuariopara'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new VwHdDeUsuarioPara();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlHddeusuarioparaSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes De Usuário Para',
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Hddeusuariopara();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlHddeusuarioparaSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar novo De Usuário Para',
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlHddeusuarioparaSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Usuario De Para',
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlHddeusuarioparaSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Hddeusuariopara::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

}
