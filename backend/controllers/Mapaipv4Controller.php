<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\components\Setup;
use common\models\MapaIpV4;
use common\components\CalculaIPV4;
use common\models\Usuario;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use kartik\date\DatePicker;


class Mapaipv4Controller extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete', 'clean'],
                'rules' => AccessRulesControl::getRulesControl('mapaipv4'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new MapaIpV4();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlMapaipv4Search', Yii::$app->request->url);

        return $this->render('index', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new MapaIpV4();
        $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $request = Yii::$app->request->post();
                $calcula = new CalculaIPV4($request['MapaIpV4']['ip']);
                
                $ip_inicial = explode( '.', $calcula->primeiro_ip($request['MapaIpV4']['ip']));
                $ip_final = explode( '.', $calcula->ultimo_ip($request['MapaIpV4']['ip']));
                $cidr = $calcula->cidr($request['MapaIpV4']['ip']);
                $unidade = $request['MapaIpV4']['unidade_fk'];
                $rede = $request['MapaIpV4']['rede'];
                
                $ip_1 = $ip_inicial[0];
                $ip_2 = $ip_inicial[1];
                $ip_3 = $ip_inicial[2];
                $ip_11 = $ip_final[0] + 1;
                $ip_22 = $ip_final[1] + 1;
                $ip_33 = $ip_final[2] + 1;
                $ip_44_ = $ip_final[3] + 1;
                $ipfinal = "$ip_final[0].$ip_final[1].$ip_final[2].$ip_final[3]";
                
                while ($ip_33 > $ip_3){
                    $ipc = $ip_inicial[3];
                    if($ip_3 == $ip_final[2]){
                        if($ip_inicial[2] == $ip_3){
                            while ($ipc < $ip_44_){
                                $ip = "$ip_1.$ip_2.$ip_3.$ipc";
                                $model = new Mapaipv4();
                                $model->unidade_fk = $unidade;
                                $model->ip = $ip;
                                $model->cidr = $cidr;
                                $model->rede = $rede;
                                $model->mac = NULL;
                                if ($model->save()) {
                                    $ipc ++;
                                } else {
                                    print_r($model->getErrors());
                                    exit;
                                }
                            }
                            $ip_3 ++;
                        } else {
                            $ipc = 0;
                            while ($ipc < $ip_44_){
                                $ip = "$ip_1.$ip_2.$ip_3.$ipc";
                                $model = new Mapaipv4();
                                $model->unidade_fk = $unidade;
                                $model->ip = $ip;
                                $model->cidr = $cidr;
                                $model->rede = $rede;
                                $model->mac = NULL;
                                if ($model->save()) {
                                    $ipc ++;
                                } else {
                                    print_r($model->getErrors());
                                    exit;
                                }
                            }
                            $ip_3 ++;
                        }
                    } else {
                        if($ip_inicial[2] == $ip_3){
                            while ($ipc < 256){
                                $ip = "$ip_1.$ip_2.$ip_3.$ipc";
                                $model = new Mapaipv4();
                                $model->unidade_fk = $unidade;
                                $model->ip = $ip;
                                $model->cidr = $cidr;
                                $model->rede = $rede;
                                if ($model->save()) {
                                    $ipc ++;
                                } else {
                                    print_r($model->getErrors());
                                    exit;
                                }
                            }
                            $ip_3 ++;
                        } else {
                            $ipc = 0;
                            while ($ipc < 256){
                                $ip = "$ip_1.$ip_2.$ip_3.$ipc";
                                $model = new Mapaipv4();
                                $model->unidade_fk = $unidade;
                                $model->ip = $ip;
                                $model->cidr = $cidr;
                                $model->rede = $rede;
                                if ($model->save()) {
                                    $ipc ++;
                                } else {
                                    print_r($model->getErrors());
                                    exit;
                                }
                            }
                            $ip_3 ++;
                        }
                    }
                }
                Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                return $this->redirect([Yii::$app->session->get('urlMapaipv4Search', array())]);
            }
        }
        
        return $this->render('_form', [
                    'title' => 'Adicionar nova Mapa IP V4',
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlMapaipv4Search', array())]);
                }
            }
        }
        return $this->render('_form_edit', [
                    'title' => 'Alterar Mapa IP V4',
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            $model_rede = MapaIpV4::find()->andwhere(['unidade_fk' => $model->unidade_fk])->andwhere(['rede' => $model->rede])->all();
            
            foreach($model_rede as $array){
                $this->findModel($array->id)->delete();
            }
            Yii::$app->session->setFlash('success', 'Rede removida com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlMapaipv4Search', array())]);
    }

    protected function findModel($id) {
        if (($model = MapaIpV4::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    public function actionCalculaRede() {
        $request = Yii::$app->request->post();
        $calcula = new CalculaIPV4($request['ip']);
        $conteudo = '';
        $staus = 'success';
        if ($calcula->valida_endereco()) {
            $conteudo = '<p>Configurações de rede para ' . $request['ip'] . '</p>'
                    . '<p class="text-justify">'
                    . '<b>Endereço/Rede: </b>' . $calcula->endereco_completo() . '<br>'
                    . "<b>Endereço: </b>" . $calcula->endereco() . '<br>'
                    . "<b>Prefixo CIDR: </b>/" . $calcula->cidr() . '<br>'
                    . "<b>Máscara de sub-rede: </b>" . $calcula->mascara() . '<br>'
                    . "<b>IP da Rede: </b>" . $calcula->rede() . '/' . $calcula->cidr() . '<br>'
                    . "<b>Broadcast da Rede: </b>" . $calcula->broadcast() . '<br>'
                    . "<b>Primeiro Host: </b>" . $calcula->primeiro_ip() . '<br>'
                    . "<b>Último Host: </b>" . $calcula->ultimo_ip() . '<br>'
                    . "<b>Total de IPs:  </b>" . $calcula->total_ips() . '<br>'
                    . "<b>Hosts: </b>" . $calcula->ips_rede()
                    . "</p>";
            $retorno['ip_inicial'] = $calcula->primeiro_ip();
            $retorno['ip_final'] = $calcula->ultimo_ip();
        } else {
            $conteudo = 'Endereço IPv4 inválido!';
            $staus = 'error';
        }
        $retorno['conteudo'] = $conteudo;
        $retorno['status'] = $staus;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $retorno;
    }
    
    public function actionSetSessionRede() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->rede_temp = $request['rede_temp'];
            $model->save();
        }
        Yii::$app->user->identity->rede_temp = $request['rede_temp'];
    }
    
    public function actionSetSessionGrupo() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->grupo_temp = $request['grupo_temp'];
            $model->save();
        }
        Yii::$app->user->identity->grupo_temp = $request['grupo_temp'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->rede_temp = NULL;
            $model->grupo_temp = NULL;
            $model->save();
        }
        return $this->redirect([Yii::$app->session->get('urlMapaipv4Search', array())]);
    }

}
