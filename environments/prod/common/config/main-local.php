<?php

$banco = "pgsql";

switch ($banco) {
    case "mysql" :
        return [
            'language' => 'pt',
            'sourceLanguage' => 'pt',
            'name' => 'SGT',
            'timeZone' => 'America/Sao_Paulo',
            'components' => [
                'db' => [
                    'class' => 'yii\db\Connection',
                    'dsn' => 'mysql:host=127.0.0.1;dbname=bd_sgt',
                    'username' => 'root',
                    'password' => '',
                    'charset' => 'utf8',
                ],
                'mailer' => [
                    'class' => 'yii\swiftmailer\Mailer',
                    'viewPath' => '@common/mail',
                    'useFileTransport' => false,
                    'transport' => [
                        'class' => 'Swift_SmtpTransport',
                        'host' => 'smtp.gmail.com',
                        'username' => 'usuario',
                        'password' => 'senha',
                        'port' => '587',
                        'encryption' => 'tls',
                    ],
                ],
            ],
        ];
        ;
        break;
    case "pgsql" :
        return [
            'language' => 'pt',
            'sourceLanguage' => 'pt',
            'name' => 'SGT',
            'timeZone' => 'America/Sao_Paulo',
            'components' => [
                'db' => [
                    'class' => 'yii\db\Connection',
                    'dsn' => 'pgsql:host=localhost;port=5432;dbname=bd_sgt',
                    'username' => 'postgres',
                    'password' => '',
                    'schemaMap' => [
                        'pgsql' => [
                            'class' => 'yii\db\pgsql\Schema',
                            'defaultSchema' => 'public'
                        ]
                    ],
                ],
                'mailer' => [
                    'class' => 'yii\swiftmailer\Mailer',
                    'viewPath' => '@common/mail',
                    'useFileTransport' => false,
                    'transport' => [
                        'class' => 'Swift_SmtpTransport',
                        'host' => 'smtp.gmail.com',
                        'username' => 'usuario',
                        'password' => 'senha',
                        'port' => '587',
                        'encryption' => 'tls',
                    ],
                ],
            ],
        ];
        ;
        break;
};
