<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_monitor ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        if($ver['url'] != ''){
            $url = $ver['codigo_monitor'].'.pdf';
            $query_ = "UPDATE tb_monitor SET url=:url WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":url", $url);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        } else {
            $url = NULL;
            $query_ = "UPDATE tb_monitor SET url=:url WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":url", $url);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['numero_patrimonio'] === ''){
            $numero_patrimonio = $ver['codigo_monitor'].$ver['id'];
            $query_ = "UPDATE tb_monitor SET numero_patrimonio=:numero_patrimonio WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":numero_patrimonio", $numero_patrimonio);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['setor_fk'] == NULL){
            $setor_fk = 31;
            $query_ = "UPDATE tb_monitor SET setor_fk=:setor_fk WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":setor_fk", $setor_fk);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
    }
?>