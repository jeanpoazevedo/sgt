<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_funcionario ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $telefone = $ver['tel_d'].$ver['tel_p'].$ver['tel_u'];
        $celular = $ver['cel_d'].$ver['cel_p'].$ver['cel_u'];
        $query_ = "UPDATE tb_funcionario SET telefone=:telefone, celular=:celular WHERE id=:id";
        $atualizar=$pdo_sgt->prepare("$query_");
        $atualizar->bindValue(":telefone", $telefone);
        $atualizar->bindValue(":celular", $celular);
        $atualizar->bindValue(":id", $id);
        $atualizar->execute();
    }
?>