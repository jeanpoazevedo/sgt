<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_voip ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $telefone = $ver['tel_d'].$ver['tel_p'].$ver['tel_u'];
        $query_ = "UPDATE tb_voip SET telefone=:telefone WHERE id=:id";
        $atualizar=$pdo_sgt->prepare("$query_");
        $atualizar->bindValue(":telefone", $telefone);
        $atualizar->bindValue(":id", $id);
        $atualizar->execute();
    }
?>