<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_computador ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        if($ver['url'] != ''){
            $url = $ver['codigo_computador'].'.pdf';
            $query_ = "UPDATE tb_computador SET url=:url WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":url", $url);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        } else {
            $url = NULL;
            $query_ = "UPDATE tb_computador SET url=:url WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":url", $url);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['numero_patrimonio'] === ''){
            $numero_patrimonio = $ver['codigo_computador'].$ver['id'];
            $query_ = "UPDATE tb_computador SET numero_patrimonio=:numero_patrimonio WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":numero_patrimonio", $numero_patrimonio);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['setor_fk'] == NULL){
            $setor_fk = 31;
            $query_ = "UPDATE tb_computador SET setor_fk=:setor_fk WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":setor_fk", $setor_fk);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['ram_medida_fk'] == NULL){
            $ram_medida_fk = 5;
            $query_ = "UPDATE tb_computador SET ram_medida_fk=:ram_medida_fk WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":ram_medida_fk", $ram_medida_fk);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['hd_medida_fk'] == NULL){
            $hd_medida_fk = 5;
            $query_ = "UPDATE tb_computador SET hd_medida_fk=:hd_medida_fk WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":hd_medida_fk", $hd_medida_fk);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
    }
?>