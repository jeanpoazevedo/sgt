<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_helpdesk_etapa ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $chamado_fk = $ver['chamado_fk'];
        $de_fk = $ver['de_fk'];
        $data = $ver['data'];
        $descricao = $ver['descricao'];
        
        $query_ = "INSERT INTO public.tb_helpdesk_etapa (id, chamado_fk, de_fk, data, descricao)"
                . "VALUES (:id, :chamado_fk, :de_fk, :data, :descricao)";
        $migra=$pdo_pg->prepare("$query_");
        $migra->bindValue(":id", $id);
        $migra->bindValue(":chamado_fk", $chamado_fk);
        $migra->bindValue(":de_fk", $de_fk);
        $migra->bindValue(":data", $data);
        $migra->bindValue(":descricao", $descricao);
        $migra->execute();
    }
?>