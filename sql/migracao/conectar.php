<?php
#   SGT - The SGT for Internet - http://sgt.eti.br
#   Copyright (c) 2016-2018, Jean Azevedo. All rights reserved.

#   This file is part of SGT.

#   SGT is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   any later version.

#   SGT is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    ini_set('default_charset','UTF-8');
    
    $sgthost = "";
    $sgtdb   = "bd_sgt";
    $sgtuser = "";
    $sgtpass = "";
    
    try {
        $pdo_sgt = new PDO('mysql:host='.$sgthost.';dbname='.$sgtdb, $sgtuser, $sgtpass);
    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
    
    $host = "";
    $db = "bd_sgt";
    $username = "";
    $password = "";
    
    $pg = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password";
 
    try{
        $pdo_pg = new PDO($pg);
    }catch (PDOException $e){
        echo $e->getMessage();
    }
    
?>
