<?php
    include "conectar.php";
    require "criptografia_sgt.php";
    include "chave_sgt.php";
   
    $buscar_query = ("SELECT * FROM tb_senha ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $sgt_cript = new SGT_KEY();
        $sgt_cript -> chave = $chave_num;
        $sgt_cript -> add_text = md5(sha1($chave_txt));
        $senha = $sgt_cript -> ver($ver['senha']);
        $senha = base64_encode($senha);
        $query_ = "UPDATE tb_senha SET senha=:senha WHERE id=:id";
        $atualizar=$pdo_sgt->prepare("$query_");
        $atualizar->bindValue(":senha", $senha);
        $atualizar->bindValue(":id", $id);
        $atualizar->execute();
    }
?>