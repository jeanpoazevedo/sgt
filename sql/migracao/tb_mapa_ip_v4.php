<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_map_ip ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $unidade_fk = $ver['unidade'];
        $rede = $ver['rede'];
        $ip = $ver['ip'];
        $cidr = $ver['cidr'];
        $grupo = $ver['grupo'];
        $mac = $ver['mac'];
        $descricao = $ver['descricao'];
        $mac_ = NULL;
        
        
        if($mac != '' || $mac != NULL){
            $mac_teste = substr($mac, 2, -14);
            if ($mac_teste == ':'){
                $mac_array = explode(':', $mac);
                $mac_ = strtoupper($mac_array[0].$mac_array[1].$mac_array[2].$mac_array[3].$mac_array[4].$mac_array[5]);
            } elseif ($mac_teste == '-') {
                $mac_array = explode('-', $mac);
                $mac_ = strtoupper($mac_array[0].$mac_array[1].$mac_array[2].$mac_array[3].$mac_array[4].$mac_array[5]);
            }
        }
        
        $query_ = "INSERT INTO public.tb_mapa_ip_v4 (id, unidade_fk, rede, ip, cidr, grupo, mac, descricao) VALUES (:id, :unidade_fk, :rede, :ip, :cidr, :grupo, :mac, :descricao)";
        $migra=$pdo_pg->prepare("$query_");
        $migra->bindValue(":id", $id);
        $migra->bindValue(":unidade_fk", $unidade_fk);
        $migra->bindValue(":rede", $rede);
        $migra->bindValue(":ip", $ip);
        $migra->bindValue(":cidr", $cidr);
        $migra->bindValue(":grupo", $grupo);
        $migra->bindValue(":mac", $mac_);
        $migra->bindValue(":descricao", $descricao);
        $migra->execute();
    }
?>