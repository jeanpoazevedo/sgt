<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_usuario ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $nome = $ver['nome'].' '.$ver['sobrenome'];
        $token = 'MSBFsg5GlAT5_oXMzpyBWgRuGS2ADwZM';
        $auth_key = 'BSVNWgWwVS-z5XQLH7I39Qr65zATAEbq';
        $senha_hash = '$2y$13$V4tiTCp5lc0COnoHBHKnLeqP9sdtUIEE.3FDOeLO5hhIjgnGUJ64u';
        $query_ = "UPDATE tb_usuario SET nome=:nome, token=:token, auth_key=:auth_key, senha_hash=:senha_hash WHERE id=:id";
        $atualizar=$pdo_sgt->prepare("$query_");
        $atualizar->bindValue(":nome", $nome);
        $atualizar->bindValue(":token", $token);
        $atualizar->bindValue(":auth_key", $auth_key);
        $atualizar->bindValue(":senha_hash", $senha_hash);
        $atualizar->bindValue(":id", $id);
        $atualizar->execute();
    }
?>