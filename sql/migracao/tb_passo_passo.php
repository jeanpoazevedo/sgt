<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_passo_passo ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $unidade_fk = $ver['unidade_fk'];
        $perfil_fk = $ver['perfil_fk'];
        $titulo = $ver['titulo'];
        $descricao = $ver['descricao'];
        $passo_passo = $ver['passo_passo'];
        
        $query_ = "INSERT INTO public.tb_passo_passo (id, unidade_fk, perfil_fk, titulo, descricao, passo_passo) VALUES (:id, :unidade_fk, :perfil_fk, :titulo, :descricao, :passo_passo)";
        $migra=$pdo_pg->prepare("$query_");
        $migra->bindValue(":id", $id);
        $migra->bindValue(":unidade_fk", $unidade_fk);
        $migra->bindValue(":perfil_fk", $perfil_fk);
        $migra->bindValue(":titulo", $titulo);
        $migra->bindValue(":descricao", $descricao);
        $migra->bindValue(":passo_passo", $passo_passo);
        $migra->execute();
    }
?>