<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_helpdesk_cadastro ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        $de_fk = $ver['de_fk'];
        $data_abertura = $ver['data_abertura'];
        $data_encerrado = $ver['data_encerrado'];
        $produto_fk = $ver['produto_fk'];
        $prioridade = $ver['prioridade'];
        $para_fk = $ver['para_fk'];
        $resumo = $ver['resumo'];
        $descricao = $ver['descricao'];
        $ver_ = $ver['ver'];
        $status = $ver['status'];
        
        $query_ = "INSERT INTO public.tb_helpdesk_cadastro (id, de_fk, data_abertura, data_encerrado, produto_fk, prioridade, para_fk, resumo, descricao, ver, status)"
                . "VALUES (:id, :de_fk, :data_abertura, :data_encerrado, :produto_fk, :prioridade, :para_fk, :resumo, :descricao, :ver, :status)";
        $migra=$pdo_pg->prepare("$query_");
        $migra->bindValue(":id", $id);
        $migra->bindValue(":de_fk", $de_fk);
        $migra->bindValue(":data_abertura", $data_abertura);
        $migra->bindValue(":data_encerrado", $data_encerrado);
        $migra->bindValue(":produto_fk", $produto_fk);
        $migra->bindValue(":prioridade", $prioridade);
        $migra->bindValue(":para_fk", $para_fk);
        $migra->bindValue(":resumo", $resumo);
        $migra->bindValue(":descricao", $descricao);
        $migra->bindValue(":ver", $ver_);
        $migra->bindValue(":status", $status);
        $migra->execute();
    }
?>