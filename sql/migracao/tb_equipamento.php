<?php
    include "conectar.php";
   
    $buscar_query = ("SELECT * FROM tb_equipamento ORDER BY id");
    $buscar = $pdo_sgt->query($buscar_query);
    
    while($ver = $buscar->fetch(PDO::FETCH_ASSOC)){
        $id = $ver['id'];
        if($ver['url'] != ''){
            $url = $ver['codigo_equipamento'].'.pdf';
            $query_ = "UPDATE tb_equipamento SET url=:url WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":url", $url);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        } else {
            $url = NULL;
            $query_ = "UPDATE tb_equipamento SET url=:url WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":url", $url);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['numero_patrimonio'] === ''){
            $numero_patrimonio = $ver['codigo_equipamento'].$ver['id'];
            $query_ = "UPDATE tb_equipamento SET numero_patrimonio=:numero_patrimonio WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":numero_patrimonio", $numero_patrimonio);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
        if($ver['setor_fk'] == NULL){
            $setor_fk = 31;
            $query_ = "UPDATE tb_equipamento SET setor_fk=:setor_fk WHERE id=:id";
            $atualizar=$pdo_sgt->prepare("$query_");
            $atualizar->bindValue(":setor_fk", $setor_fk);
            $atualizar->bindValue(":id", $id);
            $atualizar->execute();
        }
    }
?>