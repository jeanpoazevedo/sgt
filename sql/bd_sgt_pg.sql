/*
Tabela: Sistema
*/
CREATE SEQUENCE tb_sistema_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_sistema_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_sistema_seq
    IS 'Sequencia de numeracao do ID da Sistema';

CREATE TABLE public.tb_sistema
(
    id bigint NOT NULL DEFAULT nextval(('tb_sistema_seq'::text)::regclass),
    nome character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    url character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_sistema PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_sistema
    OWNER to postgres;

CREATE UNIQUE INDEX idx_sistema_id ON public.tb_sistema USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_sistema_nome ON public.tb_sistema USING btree
    (nome COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Configuracao
*/
CREATE SEQUENCE public.tb_configuracao_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_configuracao_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_configuracao_seq
    IS 'Sequencia de numeracao do ID da Configuracao';

CREATE TABLE public.tb_configuracao
(
    id bigint NOT NULL DEFAULT nextval(('tb_configuracao_seq'::text)::regclass),
    configuracao character varying(20) COLLATE pg_catalog."default" NOT NULL,
    variavel1 character varying(100) COLLATE pg_catalog."default",
    variavel2 character varying(100) COLLATE pg_catalog."default",
    variavel3 character varying(100) COLLATE pg_catalog."default",
    variavel4 character varying(100) COLLATE pg_catalog."default",
    nome character varying(45) COLLATE pg_catalog."default" NOT NULL,
    ordem integer,
    ver integer NOT NULL,
    titulo_variavel1 character varying(40) COLLATE pg_catalog."default",
    titulo_variavel2 character varying(40) COLLATE pg_catalog."default",
    titulo_variavel3 character varying(40) COLLATE pg_catalog."default",
    titulo_variavel4 character varying(40) COLLATE pg_catalog."default",
    ativo_variavel1 integer,
    ativo_variavel2 integer,
    ativo_variavel3 integer,
    ativo_variavel4 integer,
    tipo_variavel1 integer,
    tipo_variavel2 integer,
    tipo_variavel3 integer,
    tipo_variavel4 integer,
    CONSTRAINT tb_configuracao_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_configuracao
    OWNER to postgres;

CREATE UNIQUE INDEX idx_configuracao_id ON public.tb_configuracao USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_configuracao_configuracao ON public.tb_configuracao USING btree
    (configuracao COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_configuracao_variavel1 ON public.tb_configuracao USING btree
    (variavel1 COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_configuracao_variavel2 ON public.tb_configuracao USING btree
    (variavel2 COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_configuracao_variavel3 ON public.tb_configuracao USING btree
    (variavel3 COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_configuracao_variavel4 ON public.tb_configuracao USING btree
    (variavel4 COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: E-mail
*/
CREATE SEQUENCE public.tb_email_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_email_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_email_seq
    IS 'Sequencia de numeracao do ID do E-mail';

CREATE TABLE public.tb_email
(
    id bigint NOT NULL DEFAULT nextval(('tb_email_seq'::text)::regclass),
    host character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    username character varying(120) COLLATE pg_catalog."default" NOT NULL,
    password character varying(120) COLLATE pg_catalog."default" NOT NULL,
    port integer NOT NULL,
    nome character varying(120) COLLATE pg_catalog."default" NOT NULL,
    encryption integer NOT NULL,
    CONSTRAINT pk_tb_email PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_email
    OWNER to postgres;

CREATE UNIQUE INDEX idx_email_id ON public.tb_email USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_email_host ON public.tb_email USING btree
    (host COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_email_nome ON public.tb_email USING btree
    (nome COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_email_password ON public.tb_email USING btree
    (password COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_email_port ON public.tb_email USING btree
    (port) TABLESPACE pg_default;

CREATE INDEX idx_email_encryption ON public.tb_email USING btree
    (encryption) TABLESPACE pg_default;

CREATE INDEX idx_email_username ON public.tb_email USING btree
    (username COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Perfil
*/
CREATE SEQUENCE tb_perfil_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_perfil_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_perfil_seq
    IS 'Sequencia de numeracao do ID do Perfil';

CREATE TABLE public.tb_perfil
(
    id bigint NOT NULL DEFAULT nextval(('tb_perfil_seq'::text)::regclass),
    nome character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    descricao character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_perfil PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_perfil
    OWNER to postgres;

CREATE UNIQUE INDEX idx_perfil_id ON public.tb_perfil USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_perfil_descricao ON public.tb_perfil USING btree
    (descricao COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_perfil_nome ON public.tb_perfil USING btree
    (nome COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Unidade
*/
CREATE SEQUENCE public.tb_unidade_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_unidade_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_unidade_seq
    IS 'Sequencia de numeracao do ID do Unidade';

CREATE TABLE public.tb_unidade
(
    id bigint NOT NULL DEFAULT nextval(('tb_unidade_seq'::text)::regclass),
    unidade character varying(40) COLLATE pg_catalog."default" NOT NULL,
    dominio character varying(60) COLLATE pg_catalog."default",
    ddd integer,
    nivel integer NOT NULL,
    unidade_pai_fk bigint,
    entidade integer,
    nome_patrimonio1 character varying(80) COLLATE pg_catalog."default",
    email_patrimonio1 character varying(100) COLLATE pg_catalog."default",
    nome_patrimonio2 character varying(80) COLLATE pg_catalog."default",
    email_patrimonio2 character varying(100) COLLATE pg_catalog."default",
    descricao_unidade character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_unidade PRIMARY KEY (id),
    CONSTRAINT uq_unidade UNIQUE (unidade, unidade_pai_fk, nivel),
    
    CONSTRAINT fk_unidade_unidade_pai FOREIGN KEY (unidade_pai_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_unidade
    OWNER to postgres;

CREATE INDEX idx_unidade_ddd ON public.tb_unidade USING btree
    (ddd) TABLESPACE pg_default;

CREATE INDEX idx_unidade_email_patrimonio1 ON public.tb_unidade USING btree
    (email_patrimonio1 COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_unidade_email_patrimonio2 ON public.tb_unidade USING btree
    (email_patrimonio2 COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE UNIQUE INDEX idx_unidade_id ON public.tb_unidade USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_unidade_nome_patrimonio1 ON public.tb_unidade USING btree
    (nome_patrimonio1 COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_unidade_nome_patrimonio2 ON public.tb_unidade USING btree
    (nome_patrimonio2 COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_unidade_unindade ON public.tb_unidade USING btree
    (unidade COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Unidade de Medidas
*/
CREATE SEQUENCE public.tb_medida_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_medida_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_medida_seq
    IS 'Sequencia de numeracao do ID do Medida';

CREATE TABLE public.tb_medida
(
    id bigint NOT NULL DEFAULT nextval(('tb_medida_seq'::text)::regclass),
    simbolo character varying(50) COLLATE pg_catalog."default" NOT NULL,
    medida character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_medida PRIMARY KEY (id),
    CONSTRAINT uq_medida UNIQUE (medida)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_medida
    OWNER to postgres;

CREATE UNIQUE INDEX idx_medida_id ON public.tb_medida USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_medida_simbolo ON public.tb_medida USING btree
    (simbolo COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_medida_medida ON public.tb_medida USING btree
    (medida COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Sequência Código
*/
CREATE SEQUENCE public.tb_sequencia_codigo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_sequencia_codigo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_sequencia_codigo_seq
    IS 'Sequência de código para computadores, monitores e equipamentos';

CREATE TABLE public.tb_sequencia_codigo
(
    id bigint NOT NULL DEFAULT nextval(('tb_sequencia_codigo_seq'::text)::regclass),
    sequencia integer NOT NULL,
    codigo character varying(2) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_sequencia_codigo PRIMARY KEY (id),
    CONSTRAINT uq_sequencia_codigo_sequencia UNIQUE (sequencia),
    CONSTRAINT uq_sequencia_codigo_codigo UNIQUE (codigo)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_sequencia_codigo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_sequencia_codigo_id ON public.tb_sequencia_codigo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_sequencia_codigo_sequencia ON public.tb_sequencia_codigo USING btree
    (sequencia) TABLESPACE pg_default;

CREATE INDEX idx_sequencia_codigo_codigo ON public.tb_sequencia_codigo USING btree
    (codigo COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Código
*/
CREATE SEQUENCE public.tb_codigo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_codigo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_codigo_seq
    IS 'Código para computadores, monitores e equipamentos';

CREATE TABLE public.tb_codigo
(
    id bigint NOT NULL DEFAULT nextval(('tb_codigo_seq'::text)::regclass),
    ano character varying(2) COLLATE pg_catalog."default" NOT NULL,
    mes character varying(2) COLLATE pg_catalog."default" NOT NULL,
    dispositivo character varying(1) COLLATE pg_catalog."default" NOT NULL,
    codigo_fk integer NOT NULL,
    CONSTRAINT pk_tb_codigo PRIMARY KEY (id),
    CONSTRAINT fk_codigo_fk_codigo FOREIGN KEY (codigo_fk)
        REFERENCES public.tb_sequencia_codigo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_codigo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_codigo_id ON public.tb_codigo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_codigo_ano ON public.tb_codigo USING btree
    (ano COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_codigo_mes ON public.tb_codigo USING btree
    (mes COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_codigo_dispositivo ON public.tb_codigo USING btree
    (dispositivo COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_codigo_codigo_fk ON public.tb_codigo USING btree
    (codigo_fk) TABLESPACE pg_default;

/*
Tabela: Setor
*/
CREATE SEQUENCE public.tb_setor_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_setor_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_setor_seq
    IS 'Sequencia de numeracao do ID do Setor';

CREATE TABLE public.tb_setor
(
    id bigint NOT NULL DEFAULT nextval(('tb_setor_seq'::text)::regclass),
    setor character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_setor PRIMARY KEY (id),
    CONSTRAINT uq_setor UNIQUE (setor)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_setor
    OWNER to postgres;

CREATE UNIQUE INDEX idx_setor_id ON public.tb_setor USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_setor_setor ON public.tb_setor USING btree
    (setor COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Local
*/
CREATE SEQUENCE public.tb_local_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_local_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_local_seq
    IS 'Sequencia de numeracao do ID do Local';

CREATE TABLE public.tb_local
(
    id bigint NOT NULL DEFAULT nextval(('tb_local_seq'::text)::regclass),
    local character varying(80) COLLATE pg_catalog."default" NOT NULL,
    descricao character varying(120) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_local PRIMARY KEY (id),
    CONSTRAINT uq_local UNIQUE (local)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_local
    OWNER to postgres;

CREATE UNIQUE INDEX idx_local_id ON public.tb_local USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_local_local ON public.tb_local USING btree
    (local COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Tipo
*/
CREATE SEQUENCE public.tb_tipo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_tipo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_tipo_seq
    IS 'Sequencia de numeracao do ID do Tipo';

CREATE TABLE public.tb_tipo
(
    id bigint NOT NULL DEFAULT nextval(('tb_tipo_seq'::text)::regclass),
    tipo character varying(40) COLLATE pg_catalog."default" NOT NULL,
    computador integer NOT NULL,
    monitor integer NOT NULL,
    equipamento integer NOT NULL,
    CONSTRAINT pk_tb_tipo PRIMARY KEY (id),
    CONSTRAINT uq_tipo UNIQUE (tipo)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_tipo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_tipo_id ON public.tb_tipo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_tipo_tipo ON public.tb_tipo USING btree
    (tipo COLLATE pg_catalog."default") TABLESPACE pg_default;


/*
Tabela: Tipo Processador
*/
CREATE SEQUENCE public.tb_tipo_processador_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_tipo_processador_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_tipo_processador_seq
    IS 'Sequencia de numeracao do ID do Tipo Processador';

CREATE TABLE public.tb_tipo_processador
(
    id bigint NOT NULL DEFAULT nextval(('tb_tipo_processador_seq'::text)::regclass),
    tipo character varying(40) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_tipo_processador PRIMARY KEY (id),
    CONSTRAINT uq_tipo_processador UNIQUE (tipo)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_tipo_processador
    OWNER to postgres;

CREATE UNIQUE INDEX idx_tipo_processador_id ON public.tb_tipo_processador USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_tipo_processador_tipo ON public.tb_tipo_processador USING btree
    (tipo COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Marca
*/
CREATE SEQUENCE public.tb_marca_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_marca_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_marca_seq
    IS 'Sequencia de numeracao do ID do Marca';

CREATE TABLE public.tb_marca
(
    id bigint NOT NULL DEFAULT nextval(('tb_marca_seq'::text)::regclass),
    marca character varying(40) COLLATE pg_catalog."default" NOT NULL,
    computador integer NOT NULL,
    monitor integer NOT NULL,
    equipamento integer NOT NULL,
    processador integer NOT NULL,
    placa_video integer NOT NULL,
    CONSTRAINT pk_tb_marca PRIMARY KEY (id),
    CONSTRAINT uq_marca UNIQUE (marca)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_marca
    OWNER to postgres;

CREATE UNIQUE INDEX idx_marca_id ON public.tb_marca USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_marca_marca ON public.tb_marca USING btree
    (marca COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Modelo
*/
CREATE SEQUENCE public.tb_modelo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_modelo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_modelo_seq
    IS 'Sequencia de numeracao do ID do Modelo';

CREATE TABLE public.tb_modelo
(
    id bigint NOT NULL DEFAULT nextval(('tb_modelo_seq'::text)::regclass),
    marca_fk bigint NOT NULL,
    modelo character varying(80) COLLATE pg_catalog."default" NOT NULL,
    tipo integer NOT NULL,
    CONSTRAINT pk_tb_modelo PRIMARY KEY (id),
    CONSTRAINT uq_modelo UNIQUE (marca_fk, modelo),

    CONSTRAINT fk_modelo_marca FOREIGN KEY (marca_fk)
        REFERENCES public.tb_marca (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_modelo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_modelo_id ON public.tb_modelo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_modelo_marca ON public.tb_modelo USING btree
    (marca_fk) TABLESPACE pg_default;

CREATE INDEX idx_modelo_modelo ON public.tb_modelo USING btree
    (modelo COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_modelo_tipo ON public.tb_modelo USING btree
    (tipo) TABLESPACE pg_default;

/*
Tabela: Help Desk - Produto
*/
CREATE SEQUENCE tb_helpdesk_produto_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_helpdesk_produto_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_helpdesk_produto_seq
    IS 'Sequencia de numeracao do ID do Help Desk - Produto';

CREATE TABLE public.tb_helpdesk_produto
(
    id bigint NOT NULL DEFAULT nextval(('tb_helpdesk_produto_seq'::text)::regclass),
    produto character varying(90) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_helpdesk_produto PRIMARY KEY (id),
    CONSTRAINT uq_helpdesk_produto UNIQUE (produto)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_helpdesk_produto
    OWNER to postgres;

CREATE UNIQUE INDEX idx_helpdesk_produto_id ON public.tb_helpdesk_produto USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_produto_produto ON public.tb_helpdesk_produto USING btree
    (produto COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Usuario
*/
CREATE SEQUENCE public.tb_usuario_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_usuario_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_usuario_seq
    IS 'Sequencia de numeracao do ID do Usuario';

CREATE TABLE public.tb_usuario
(
    id bigint NOT NULL DEFAULT nextval(('tb_usuario_seq'::text)::regclass),
    usuario character varying(40) COLLATE pg_catalog."default" NOT NULL,
    senha character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    nome character varying(80) COLLATE pg_catalog."default" NOT NULL,
    email character varying(100) COLLATE pg_catalog."default" NOT NULL,
    unidade_fk bigint NOT NULL,
    unidade_temp_fk bigint NOT NULL,
    setor_temp_fk bigint,
    local_temp_fk bigint,
    tipo_temp_fk bigint,
    marca_temp_fk bigint,
    modelo_temp_fk bigint,
    produto_temp_fk bigint,
    patch_panel_temp bigint,
    switch_temp bigint,
    hd_unidade_temp_fk bigint,
    hd_filtro bigint,
    rede_temp character varying(80) COLLATE pg_catalog."default",
    grupo_temp character varying(80) COLLATE pg_catalog."default",
    funcao_temp character varying(50) COLLATE pg_catalog."default",
    help_desk_master integer,
    status integer NOT NULL,
    telefone character varying(15) COLLATE pg_catalog."default",
    senha_hash character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    senha_reset_token character varying(1024) COLLATE pg_catalog."default",
    auth_key character varying(32) COLLATE pg_catalog."default" NOT NULL,
    token character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    trocar_senha integer,
    CONSTRAINT pk_tb_usuario PRIMARY KEY (id),
    CONSTRAINT uq_usuario UNIQUE (usuario),

    CONSTRAINT fk_usuario_local_temp FOREIGN KEY (local_temp_fk)
        REFERENCES public.tb_local (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_marca_temp FOREIGN KEY (marca_temp_fk)
        REFERENCES public.tb_marca (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_modelo_temp FOREIGN KEY (modelo_temp_fk)
        REFERENCES public.tb_modelo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_produto_temp FOREIGN KEY (produto_temp_fk)
        REFERENCES public.tb_helpdesk_produto (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_setor_temp FOREIGN KEY (setor_temp_fk)
        REFERENCES public.tb_setor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_unidade_temp FOREIGN KEY (unidade_temp_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_hd_unidade_temp FOREIGN KEY (hd_unidade_temp_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_usuario
    OWNER to postgres;

CREATE UNIQUE INDEX idx_usuario_id ON public.tb_usuario USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_usuario_usuario ON public.tb_usuario USING btree
    (usuario COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_usuario_nome ON public.tb_usuario USING btree
    (nome COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_usuario_email ON public.tb_usuario USING btree
    (email COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_usuario_local_temp ON public.tb_usuario USING btree
    (local_temp_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_marca_temp ON public.tb_usuario USING btree
    (marca_temp_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_modelo_temp ON public.tb_usuario USING btree
    (modelo_temp_fk)TABLESPACE pg_default;

CREATE INDEX idx_usuario_produto_temp ON public.tb_usuario USING btree
    (produto_temp_fk)TABLESPACE pg_default;

CREATE INDEX idx_usuario_patch_panel_temp ON public.tb_usuario USING btree
    (patch_panel_temp)TABLESPACE pg_default;

CREATE INDEX idx_usuario_switch_temp ON public.tb_usuario USING btree
    (switch_temp)TABLESPACE pg_default;

CREATE INDEX idx_usuario_setor_temp ON public.tb_usuario USING btree
    (setor_temp_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_tipo_temp ON public.tb_usuario USING btree
    (tipo_temp_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_unidade_temp ON public.tb_usuario USING btree
    (unidade_temp_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_hd_unidade_temp ON public.tb_usuario USING btree
    (hd_unidade_temp_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_hd_filtro ON public.tb_usuario USING btree
    (hd_filtro) TABLESPACE pg_default;

CREATE INDEX idx_usuario_unindade ON public.tb_usuario USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_rede_temp ON public.tb_usuario USING btree
    (rede_temp COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_usuario_grupo_temp ON public.tb_usuario USING btree
    (grupo_temp COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_usuario_funcao_temp ON public.tb_usuario USING btree
    (funcao_temp COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_usuario_help_desk_master ON public.tb_usuario USING btree
    (help_desk_master) TABLESPACE pg_default;

CREATE INDEX idx_usuario_status ON public.tb_usuario USING btree
    (status) TABLESPACE pg_default;

CREATE INDEX idx_usuario_trocar_senha ON public.tb_usuario USING btree
    (trocar_senha) TABLESPACE pg_default;

/*
Tabela: Usuario Perfil
*/
CREATE SEQUENCE tb_usuario_perfil_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_usuario_perfil_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_usuario_perfil_seq
    IS 'Sequencia de numeracao do ID do Usuario Perfil';

CREATE TABLE public.tb_usuario_perfil
(
    id bigint NOT NULL DEFAULT nextval(('tb_usuario_perfil_seq'::text)::regclass),
    usuario_fk bigint NOT NULL,
    perfil_fk bigint NOT NULL,
    CONSTRAINT pk_tb_usuario_perfil PRIMARY KEY (id),
    CONSTRAINT uq_usuario_perfil UNIQUE (usuario_fk),
    CONSTRAINT fk_usuario_perfil_usuario FOREIGN KEY (usuario_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_perfil_perfil FOREIGN KEY (perfil_fk)
        REFERENCES public.tb_perfil (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_usuario_perfil
    OWNER to postgres;

CREATE UNIQUE INDEX idx_usuario_perfil_id ON public.tb_usuario_perfil USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_usuario_perfil_usuario_fk ON public.tb_usuario_perfil USING btree
    (usuario_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_perfil_perfil_fk ON public.tb_usuario_perfil USING btree
    (perfil_fk) TABLESPACE pg_default;

/*
Tabela: Menu
*/
CREATE SEQUENCE tb_menu_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_menu_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_menu_seq
    IS 'Sequencia de numeracao do ID do Menu';

CREATE TABLE public.tb_menu
(
    id bigint NOT NULL DEFAULT nextval(('tb_menu_seq'::text)::regclass),
    sistema_fk bigint NOT NULL,
    nivel integer NOT NULL,
    menu_pai_fk bigint,
    titulo character varying(120) COLLATE pg_catalog."default" NOT NULL,
    controller character varying(200) COLLATE pg_catalog."default",
    icon character varying(200) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_menu PRIMARY KEY (id),
    CONSTRAINT fk_menu_menu_pai FOREIGN KEY (menu_pai_fk)
        REFERENCES public.tb_menu (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT fk_menu_sistema FOREIGN KEY (sistema_fk)
        REFERENCES public.tb_sistema (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_menu
    OWNER to postgres;

CREATE UNIQUE INDEX idx_menu_id ON public.tb_menu USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_menu_sistema_fk ON public.tb_menu USING btree
    (sistema_fk) TABLESPACE pg_default;

CREATE INDEX idx_menu_nivel ON public.tb_menu USING btree
    (nivel) TABLESPACE pg_default;

CREATE INDEX idx_menu_menu_pai_fk ON public.tb_menu USING btree
    (menu_pai_fk) TABLESPACE pg_default;

CREATE INDEX idx_menu_titulo ON public.tb_menu USING btree
    (titulo COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_menu_controller ON public.tb_menu USING btree
    (controller COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_menu_icon ON public.tb_menu USING btree
    (icon COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Menu Perfil
*/
CREATE SEQUENCE tb_menu_perfil_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_menu_perfil_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_menu_perfil_seq
    IS 'Sequencia de numeracao do ID do Menu Perfil';

CREATE TABLE public.tb_menu_perfil
(
    id bigint NOT NULL DEFAULT nextval(('tb_menu_perfil_seq'::text)::regclass),
    menu_fk bigint NOT NULL,
    perfil_fk bigint NOT NULL,
    CONSTRAINT pk_tb_menu_perfil PRIMARY KEY (id),
    CONSTRAINT fk_menu_perfil_menu FOREIGN KEY (menu_fk)
        REFERENCES public.tb_menu (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT fk_menu_perfil_perfil FOREIGN KEY (perfil_fk)
        REFERENCES public.tb_perfil (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_menu_perfil
    OWNER to postgres;

CREATE UNIQUE INDEX idx_menu_perfil_id ON public.tb_menu_perfil USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_menu_perfil_menu ON public.tb_menu_perfil USING btree
    (menu_fk) TABLESPACE pg_default;

CREATE INDEX idx_menu_perfil_perfil ON public.tb_menu_perfil USING btree
    (perfil_fk) TABLESPACE pg_default;

/*
Tabela: Restricao Menu Perfil
*/
CREATE SEQUENCE tb_restricao_menu_perfil_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_restricao_menu_perfil_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_restricao_menu_perfil_seq
    IS 'Sequencia de numeracao do ID da Restricao Menu Perfil';

CREATE TABLE public.tb_restricao_menu_perfil
(
    id bigint NOT NULL DEFAULT nextval(('tb_restricao_menu_perfil_seq'::text)::regclass),
    perfil_fk bigint NOT NULL,
    menu_fk bigint NOT NULL,
    restricao character varying(200) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_restricao_menu_perfil PRIMARY KEY (id),
    CONSTRAINT fk_restricao_menu_perfil_menu FOREIGN KEY (menu_fk)
        REFERENCES public.tb_menu (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT fk_restricao_menu_perfil_perfil FOREIGN KEY (perfil_fk)
        REFERENCES public.tb_perfil (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_restricao_menu_perfil
    OWNER to postgres;

CREATE UNIQUE INDEX idx_restricao_menu_perfil_id ON public.tb_restricao_menu_perfil USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_restricao_menu_perfil_perfil ON public.tb_restricao_menu_perfil USING btree
    (perfil_fk) TABLESPACE pg_default;

CREATE INDEX idx_restricao_menu_perfil_menu ON public.tb_restricao_menu_perfil USING btree
    (menu_fk) TABLESPACE pg_default;

CREATE INDEX idx_restricao_menu_perfil_restricao ON public.tb_restricao_menu_perfil USING btree
    (restricao) TABLESPACE pg_default;

/*
Tabela: Usuario Unidade
*/
CREATE SEQUENCE public.tb_usuario_unidade_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_usuario_unidade_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_usuario_unidade_seq
    IS 'Sequencia de numeracao do ID do Usuario Unidade';

CREATE TABLE public.tb_usuario_unidade
(
    id bigint NOT NULL DEFAULT nextval(('tb_usuario_unidade_seq'::text)::regclass),
    usuario_fk bigint NOT NULL,
    unidade_fk bigint NOT NULL,
    CONSTRAINT pk_tb_usuario_unidade PRIMARY KEY (id),
    CONSTRAINT uq_usuario_unidade UNIQUE (usuario_fk, unidade_fk),

    CONSTRAINT fk_usuario_unidade_usuario FOREIGN KEY (usuario_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_unidade_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_usuario_unidade
    OWNER to postgres;

CREATE UNIQUE INDEX idx_usuario_unidade_id ON public.tb_usuario_unidade USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_usuario_unidade_usuario ON public.tb_usuario_unidade USING btree
    (usuario_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_unidade_unidade ON public.tb_usuario_unidade USING btree
    (unidade_fk) TABLESPACE pg_default;

/*
Tabela: Usuario Gerente Unidade
*/
CREATE SEQUENCE public.tb_usuario_gerente_unidade_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_usuario_gerente_unidade_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_usuario_gerente_unidade_seq
    IS 'Sequencia de numeracao do ID do Usuario Gerente Unidade';

CREATE TABLE public.tb_usuario_gerente_unidade
(
    id bigint NOT NULL DEFAULT nextval(('tb_usuario_gerente_unidade_seq'::text)::regclass),
    usuario_fk bigint NOT NULL,
    unidade_fk bigint NOT NULL,
    gerencia integer NOT NULL DEFAULT 0,
    CONSTRAINT pk_tb_usuario_gerente_unidade PRIMARY KEY (id),
    CONSTRAINT uq_usuario_gerente_unidade UNIQUE (usuario_fk, unidade_fk),

    CONSTRAINT fk_usuario_unidade_gerente_usuario FOREIGN KEY (usuario_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_usuario_unidade_gerente_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_usuario_gerente_unidade
    OWNER to postgres;

CREATE UNIQUE INDEX idx_usuario_gerente_unidade_id ON public.tb_usuario_gerente_unidade USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_usuario_unidade_gerente_usuario ON public.tb_usuario_gerente_unidade USING btree
    (usuario_fk) TABLESPACE pg_default;

CREATE INDEX idx_usuario_unidade_gerente_unidade ON public.tb_usuario_gerente_unidade USING btree
    (unidade_fk) TABLESPACE pg_default;

/*
Tabela: Help Desk Usuario Unidade
*/
CREATE SEQUENCE public.tb_helpdesk_usuario_unidade_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_helpdesk_usuario_unidade_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_helpdesk_usuario_unidade_seq
    IS 'Sequencia de numeracao do ID do Help Desk Usuario Unidade';

CREATE TABLE public.tb_helpdesk_usuario_unidade
(
    id bigint NOT NULL DEFAULT nextval(('tb_helpdesk_usuario_unidade_seq'::text)::regclass),
    usuario_fk bigint NOT NULL,
    unidade_fk bigint NOT NULL,
    CONSTRAINT pk_tb_helpdesk_usuario_unidade PRIMARY KEY (id),
    CONSTRAINT uq_helpdesk_usuario_unidade UNIQUE (usuario_fk, unidade_fk),

    CONSTRAINT fk_helpdesk_usuario_unidade_usuario FOREIGN KEY (usuario_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_usuario_unidade_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_helpdesk_usuario_unidade
    OWNER to postgres;

CREATE UNIQUE INDEX idx_helpdesk_usuario_unidade_id ON public.tb_helpdesk_usuario_unidade USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_usuario_unidade_usuario ON public.tb_helpdesk_usuario_unidade USING btree
    (usuario_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_usuario_unidade_unidade ON public.tb_helpdesk_usuario_unidade USING btree
    (unidade_fk) TABLESPACE pg_default;

/*
Tabela: Placa de Video
*/
CREATE SEQUENCE public.tb_placa_video_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_placa_video_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_placa_video_seq
    IS 'Sequencia de numeracao do ID da Placa de Video';

CREATE TABLE public.tb_placa_video
(
    id bigint NOT NULL DEFAULT nextval(('tb_placa_video_seq'::text)::regclass),
    marca_fk bigint NOT NULL,
    modelo character varying(200) NOT NULL,
    descricao_placa_video character varying(255) NOT NULL,
    CONSTRAINT pk_tb_placa_video PRIMARY KEY (id),
    CONSTRAINT uq_placa_video UNIQUE (marca_fk, modelo),

    CONSTRAINT fk_placa_video_marca FOREIGN KEY (marca_fk)
        REFERENCES public.tb_marca (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_placa_video
    OWNER to postgres;

CREATE UNIQUE INDEX idx_placa_video_id ON public.tb_placa_video USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_placa_video_marca ON public.tb_placa_video USING btree
    (marca_fk) TABLESPACE pg_default;

CREATE INDEX idx_placa_video_modelo ON public.tb_placa_video USING btree
    (modelo COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_placa_video_descricao_placa_video ON public.tb_placa_video USING btree
    (descricao_placa_video COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Processador
*/
CREATE SEQUENCE public.tb_processador_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_processador_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_processador_seq
    IS 'Sequencia de numeracao do ID do Processador';

CREATE TABLE public.tb_processador
(
    id bigint NOT NULL DEFAULT nextval(('tb_processador_seq'::text)::regclass),
    marca_fk bigint NOT NULL,
    tipo_processador_fk bigint NOT NULL,
    processador character varying(200) NOT NULL,
    total_core integer NOT NULL,
    frequencia numeric(5,2),
    cache integer NOT NULL,
    banda_memoria numeric(8,2),
    descricao_processador character varying(255) NOT NULL,
    CONSTRAINT pk_tb_processador PRIMARY KEY (id),
    CONSTRAINT uq_processador UNIQUE (marca_fk, processador),

    CONSTRAINT fk_processador_marca FOREIGN KEY (marca_fk)
        REFERENCES public.tb_marca (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_processador_tipo_processador FOREIGN KEY (tipo_processador_fk)
        REFERENCES public.tb_tipo_processador (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_processador
    OWNER to postgres;

CREATE UNIQUE INDEX idx_processador_id ON public.tb_processador USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_processador_marca ON public.tb_processador USING btree
    (marca_fk) TABLESPACE pg_default;

CREATE INDEX idx_processador_processador ON public.tb_processador USING btree
    (processador COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_processador_descricao_processador ON public.tb_processador USING btree
    (descricao_processador COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Clok RAM
*/
CREATE SEQUENCE public.tb_ram_clok_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_ram_clok_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_ram_clok_seq
    IS 'Sequencia de numeracao do ID do Clok RAM';

CREATE TABLE public.tb_ram_clok
(
    id bigint NOT NULL DEFAULT nextval(('tb_ram_clok_seq'::text)::regclass),
    clok integer NOT NULL,
    CONSTRAINT pk_tb_ram_clok PRIMARY KEY (id),
    CONSTRAINT uq_ram_clok UNIQUE (clok)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_ram_clok
    OWNER to postgres;

CREATE UNIQUE INDEX idx_ram_clok_id ON public.tb_ram_clok USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_ram_clok_clok ON public.tb_ram_clok USING btree
    (clok) TABLESPACE pg_default;

/*
Tabela: Tipo HD
*/
CREATE SEQUENCE public.tb_tipo_hd_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_tipo_hd_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_tipo_hd_seq
    IS 'Sequencia de numeracao do ID do Tipo HD';

CREATE TABLE public.tb_tipo_hd
(
    id bigint NOT NULL DEFAULT nextval(('tb_tipo_hd_seq'::text)::regclass),
    tipo character varying(200) NOT NULL,
    CONSTRAINT pk_tb_tipo_hd PRIMARY KEY (id),
    CONSTRAINT uq_tipo_hd UNIQUE (tipo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_tipo_hd
    OWNER to postgres;

CREATE UNIQUE INDEX idx_tipo_hd_id ON public.tb_tipo_hd USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_tipo_hd_tipo ON public.tb_tipo_hd USING btree
    (tipo COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Ocorencia Tipo
*/
CREATE SEQUENCE tb_ocorencia_tipo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_ocorencia_tipo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_ocorencia_tipo_seq
    IS 'Sequencia de numeracao do ID da Ocorencia Tipo';

CREATE TABLE public.tb_ocorencia_tipo
(
    id bigint NOT NULL DEFAULT nextval(('tb_ocorencia_tipo_seq'::text)::regclass),
    tipo character varying(40) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_ocorencia_tipo PRIMARY KEY (id),
    CONSTRAINT uq_ocorencia_tipo UNIQUE (tipo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_ocorencia_tipo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_ocorencia_tipo_id ON public.tb_ocorencia_tipo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_ocorencia_tipo_tipo ON public.tb_ocorencia_tipo USING btree
    (tipo COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Office
*/
CREATE SEQUENCE public.tb_office_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_office_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_office_seq
    IS 'Sequencia de numeracao do ID do Office';

CREATE TABLE public.tb_office
(
    id bigint NOT NULL DEFAULT nextval(('tb_office_seq'::text)::regclass),
    office character varying(200) NOT NULL,
    CONSTRAINT pk_office PRIMARY KEY (id),
    CONSTRAINT uq_office UNIQUE (office)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_office
    OWNER to postgres;

CREATE UNIQUE INDEX idx_office_id ON public.tb_office USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_office_office ON public.tb_office USING btree
    (office COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Sistema Operacional
*/
CREATE SEQUENCE public.tb_sistema_operacional_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_sistema_operacional_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_sistema_operacional_seq
    IS 'Sequencia de numeracao do ID do Sistema Operacional';

CREATE TABLE public.tb_sistema_operacional
(
    id bigint NOT NULL DEFAULT nextval(('tb_sistema_operacional_seq'::text)::regclass),
    sistema character varying(200) NOT NULL,
    CONSTRAINT pk_sistema_operacional PRIMARY KEY (id),
    CONSTRAINT uq_sistema_operacional UNIQUE (sistema)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_sistema_operacional
    OWNER to postgres;

CREATE UNIQUE INDEX idx_sistema_operacional_id ON public.tb_sistema_operacional USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_sistema_operacional_sistema ON public.tb_sistema_operacional USING btree
    (sistema COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Funcionario
*/
CREATE SEQUENCE public.tb_funcionario_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_funcionario_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_funcionario_seq
    IS 'Sequencia de numeracao do ID do Funcionario';

CREATE TABLE public.tb_funcionario
(
    id bigint NOT NULL DEFAULT nextval(('tb_funcionario_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    setor_fk bigint NOT NULL,
    funcao character varying(50),
    nome character varying(60) NOT NULL,
    data_nascimento date,
    email_1 character varying(100),
    email_2 character varying(100),
    telefone character varying(15),
    celular character varying(15),
    so_contato integer NOT NULL,
    status integer NOT NULL,
    nota text COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_funcionario PRIMARY KEY (id),
    CONSTRAINT uq_funcionarion UNIQUE (unidade_fk, nome),

    CONSTRAINT fk_funcionario_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_funcionario_setor FOREIGN KEY (setor_fk)
        REFERENCES public.tb_setor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_funcionario
    OWNER to postgres;

CREATE UNIQUE INDEX idx_funcionario_id ON public.tb_funcionario USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_unidade ON public.tb_funcionario USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_setor ON public.tb_funcionario USING btree
    (setor_fk) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_nome ON public.tb_funcionario USING btree
    (nome COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: VLAN
*/
CREATE SEQUENCE public.tb_vlan_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_vlan_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_vlan_seq
    IS 'Sequencia de numeracao do ID da VLAN';

CREATE TABLE public.tb_vlan
(
    id bigint NOT NULL DEFAULT nextval(('tb_vlan_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    id_vlan integer NOT NULL,
    vlan character varying(20) NOT NULL,
    descricao_vlan character varying(80) NOT NULL,
    CONSTRAINT pk_tb_vlan PRIMARY KEY (id),
    CONSTRAINT uq_vlan UNIQUE (unidade_fk, id_vlan),

    CONSTRAINT fk_vlan_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_vlan
    OWNER to postgres;

CREATE UNIQUE INDEX idx_vlan_id ON public.tb_vlan USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_vlan_unidade ON public.tb_vlan USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_vlan_id_vlan ON public.tb_vlan USING btree
    (id_vlan) TABLESPACE pg_default;

CREATE INDEX idx_vlan_vlan ON public.tb_vlan USING btree
    (vlan COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Chave
*/
CREATE SEQUENCE tb_chave_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_chave_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_chave_seq
    IS 'Sequencia de numeracao do ID da Chave';

CREATE TABLE public.tb_chave
(
    id bigint NOT NULL DEFAULT nextval(('tb_chave_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    perfil_fk bigint NOT NULL,
    produto character varying(80) COLLATE pg_catalog."default" NOT NULL,
    chave character varying(120) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_chave PRIMARY KEY (id),
    CONSTRAINT uq_chave UNIQUE (unidade_fk, produto, chave),

    CONSTRAINT fk_chave_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_chave_perfil FOREIGN KEY (perfil_fk)
        REFERENCES public.tb_perfil (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_chave
    OWNER to postgres;

CREATE UNIQUE INDEX idx_chave_id ON public.tb_chave USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_chave_unindade ON public.tb_chave USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_chave_perfil ON public.tb_chave USING btree
    (perfil_fk) TABLESPACE pg_default;

CREATE INDEX idx_chave_produto ON public.tb_chave USING btree
    (produto COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_chave_chave ON public.tb_chave USING btree
    (chave COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Senha
*/
CREATE SEQUENCE tb_senha_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_senha_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_senha_seq
    IS 'Sequencia de numeracao do ID da Senha';

CREATE TABLE public.tb_senha
(
    id bigint NOT NULL DEFAULT nextval(('tb_senha_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    perfil_fk bigint NOT NULL,
    usuario character varying(120) COLLATE pg_catalog."default" NOT NULL,
    senha character varying(255) COLLATE pg_catalog."default" NOT NULL,
    descricao character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status integer NOT NULL,
    CONSTRAINT pk_tb_senha PRIMARY KEY (id),
    CONSTRAINT uq_senha UNIQUE (unidade_fk, usuario, senha, descricao),

    CONSTRAINT fk_senha_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_senha_perfil FOREIGN KEY (perfil_fk)
        REFERENCES public.tb_perfil (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_senha
    OWNER to postgres;

CREATE UNIQUE INDEX idx_senha_id ON public.tb_senha USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_senha_unindade ON public.tb_senha USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_senha_perfil ON public.tb_senha USING btree
    (perfil_fk) TABLESPACE pg_default;

CREATE INDEX idx_senha_usuario ON public.tb_senha USING btree
    (usuario COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_senha_senha ON public.tb_senha USING btree
    (senha COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_senha_descricao ON public.tb_senha USING btree
    (descricao COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Historico Senha
*/
CREATE SEQUENCE tb_historico_senha_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_historico_senha_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_historico_senha_seq
    IS 'Sequencia de numeracao do ID da Histórico Senha';

CREATE TABLE public.tb_historico_senha
(
    id bigint NOT NULL DEFAULT nextval(('tb_historico_senha_seq'::text)::regclass),
    data timestamp without time zone,
    usuario_nome character varying(80) COLLATE pg_catalog."default" NOT NULL,
    id_senha_fk bigint NOT NULL,
    tipo_fk bigint NOT NULL,
    descricao character varying(255) COLLATE pg_catalog."default" NOT NULL,
    senha_antiga character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_historico_senha PRIMARY KEY (id),
    CONSTRAINT fk_historico_senha_id_senha FOREIGN KEY (id_senha_fk)
        REFERENCES public.tb_senha (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_historico_senha_tipo FOREIGN KEY (tipo_fk)
        REFERENCES public.tb_ocorencia_tipo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_historico_senha
    OWNER to postgres;

CREATE UNIQUE INDEX idx_historico_senha_id ON public.tb_historico_senha USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_historico_senha_id_senha ON public.tb_historico_senha USING btree
    (id_senha_fk) TABLESPACE pg_default;

CREATE INDEX idx_historico_senha_tipo ON public.tb_historico_senha USING btree
    (tipo_fk) TABLESPACE pg_default;

/*
Tabela: Passo a Passo
*/
CREATE SEQUENCE tb_passo_passo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_passo_passo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_passo_passo_seq
    IS 'Sequencia de numeracao do ID da Passo a Passo';

CREATE TABLE public.tb_passo_passo
(
    id bigint NOT NULL DEFAULT nextval(('tb_passo_passo_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    perfil_fk bigint NOT NULL,
    titulo character varying(60) COLLATE pg_catalog."default" NOT NULL,
    descricao character varying(120) COLLATE pg_catalog."default" NOT NULL,
    passo_passo text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_passo_passo PRIMARY KEY (id),
    CONSTRAINT uq_passo_passo UNIQUE (unidade_fk, titulo, descricao),

    CONSTRAINT fk_passo_passo_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_passo_passo_perfil FOREIGN KEY (perfil_fk)
        REFERENCES public.tb_perfil (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_passo_passo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_passo_passo_id ON public.tb_passo_passo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_passo_passo_unindade ON public.tb_passo_passo USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_passo_passo_perfil ON public.tb_passo_passo USING btree
    (perfil_fk) TABLESPACE pg_default;

CREATE INDEX idx_passo_passo_titulo ON public.tb_passo_passo USING btree
    (titulo COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_passo_passo_descricao ON public.tb_passo_passo USING btree
    (descricao COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Mapa IP
*/
CREATE SEQUENCE tb_mapa_ip_v4_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_mapa_ip_v4_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_mapa_ip_v4_seq
    IS 'Sequencia de numeracao do ID da Mapa IP';

CREATE TABLE public.tb_mapa_ip_v4
(
    id bigint NOT NULL DEFAULT nextval(('tb_mapa_ip_v4_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    rede character varying(80) COLLATE pg_catalog."default" NOT NULL,
    ip character varying(17) COLLATE pg_catalog."default" NOT NULL,
    cidr integer NOT NULL,
    grupo character varying(80) COLLATE pg_catalog."default",
    mac character varying(12) COLLATE pg_catalog."default",
    descricao character varying(100) COLLATE pg_catalog."default" ,
    CONSTRAINT pk_tb_mapa_ip_v4 PRIMARY KEY (id),
    CONSTRAINT uq_mapa_ip_v4 UNIQUE (unidade_fk, rede, ip, mac),
    CONSTRAINT fk_mapa_ip_v4_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_mapa_ip_v4
    OWNER to postgres;

CREATE UNIQUE INDEX idx_mapa_ip_v4_id ON public.tb_mapa_ip_v4 USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_mapa_ip_v4_unindade ON public.tb_mapa_ip_v4 USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_mapa_ip_v4_rede ON public.tb_mapa_ip_v4 USING btree
    (rede COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_mapa_ip_v4_ip ON public.tb_mapa_ip_v4 USING btree
    (ip COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_mapa_ip_v4_grupo ON public.tb_mapa_ip_v4 USING btree
    (grupo COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_mapa_ip_v4_mac ON public.tb_mapa_ip_v4 USING btree
    (mac COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Moniotr
*/
CREATE SEQUENCE tb_monitor_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_monitor_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_monitor_seq
    IS 'Sequencia de numeracao do ID da Monitor';

CREATE TABLE public.tb_monitor
(
    id bigint NOT NULL DEFAULT nextval(('tb_monitor_seq'::text)::regclass),
    codigo_monitor character varying(7) COLLATE pg_catalog."default" NOT NULL,
    unidade_fk bigint NOT NULL,
    setor_fk bigint,
    tipo_fk bigint NOT NULL,
    modelo_fk bigint NOT NULL,
    numero_serie character varying(80) COLLATE pg_catalog."default" NOT NULL,
    data_compra date NOT NULL,
    garantia date NOT NULL,
    loja character varying(80) COLLATE pg_catalog."default",
    numero_nota integer,
    valor numeric(20,2),
    numero_patrimonio character varying(40) COLLATE pg_catalog."default",
    netbios character varying(30) COLLATE pg_catalog."default",
    mac character varying(12) COLLATE pg_catalog."default",
    descricao_monitor text COLLATE pg_catalog."default",
    status integer NOT NULL,
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_monitor PRIMARY KEY (id),
    CONSTRAINT uq_monitor_codigo_monitor UNIQUE (codigo_monitor),
    CONSTRAINT fk_monitor_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_monitor_setor FOREIGN KEY (setor_fk)
        REFERENCES public.tb_setor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_monitor_modelo FOREIGN KEY (modelo_fk)
        REFERENCES public.tb_modelo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_monitor_tipo FOREIGN KEY (tipo_fk)
        REFERENCES public.tb_tipo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_monitor
    OWNER to postgres;

CREATE UNIQUE INDEX idx_monitor_id ON public.tb_monitor USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_monitor_codigo_monitor ON public.tb_monitor USING btree
    (codigo_monitor COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_monitor_unindade ON public.tb_monitor USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_monitor_setor ON public.tb_monitor USING btree
    (setor_fk) TABLESPACE pg_default;

CREATE INDEX idx_monitor_tipo ON public.tb_monitor USING btree
    (tipo_fk) TABLESPACE pg_default;

CREATE INDEX idx_monitor_modelo ON public.tb_monitor USING btree
    (modelo_fk) TABLESPACE pg_default;

CREATE INDEX idx_monitor_numero_numero_serie ON public.tb_monitor USING btree
    (numero_serie COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_monitor_numero_patrimonio ON public.tb_monitor USING btree
    (numero_patrimonio COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_monitor_numero_netbios ON public.tb_monitor USING btree
    (netbios COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_monitor_numero_mac ON public.tb_monitor USING btree
    (mac COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Computador
*/
CREATE SEQUENCE tb_computador_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_computador_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_chave_seq
    IS 'Sequencia de numeracao do ID da Computador';

CREATE TABLE public.tb_computador
(
    id bigint NOT NULL DEFAULT nextval(('tb_computador_seq'::text)::regclass),
    codigo_computador character varying(7) COLLATE pg_catalog."default" NOT NULL,
    unidade_fk bigint NOT NULL,
    setor_fk bigint,
    tipo_fk bigint NOT NULL,
    modelo_fk bigint NOT NULL,
    numero_serie character varying(80) COLLATE pg_catalog."default" NOT NULL,
    data_compra date NOT NULL,
    garantia date NOT NULL,
    placa_mae character varying(80) COLLATE pg_catalog."default" NOT NULL,
    processador_fk bigint,
    numero_processador integer NOT NULL,
    ram integer NOT NULL,
    ram_medida_fk bigint NOT NULL,
    ram_clok_fk bigint NOT NULL,
    hd integer NOT NULL,
    hd_medida_fk bigint NOT NULL,
    tipo_hd_fk bigint NOT NULL,
    leitura_hd integer NOT NULL,
    escrita_hd integer NOT NULL,
    descricao_hd character varying(120) COLLATE pg_catalog."default",
    loja character varying(80) COLLATE pg_catalog."default",
    numero_nota integer,
    valor numeric(20,2),
    numero_patrimonio character varying(40) COLLATE pg_catalog."default",
    numero_lacre character varying(20) COLLATE pg_catalog."default",
    netbios character varying(30) COLLATE pg_catalog."default",
    mac_c character varying(12) COLLATE pg_catalog."default",
    mac_w character varying(12) COLLATE pg_catalog."default",
    sistema_operacional_fk bigint,
    office_fk bigint,
    placa_video_fk bigint,
    numero_palca_video integer,
    codigo_monitor1_fk bigint,
    codigo_monitor2_fk bigint,
    descricao_computador text COLLATE pg_catalog."default",
    status integer NOT NULL,
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_computador PRIMARY KEY (id),
    CONSTRAINT uq_computador_codigo_computador UNIQUE (codigo_computador),
    CONSTRAINT uq_computador_codigo_monitor1_fk UNIQUE (codigo_monitor1_fk),
    CONSTRAINT uq_computador_codigo_monitor2_fk UNIQUE (codigo_monitor2_fk),
    CONSTRAINT fk_computador_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_setor FOREIGN KEY (setor_fk)
        REFERENCES public.tb_setor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_tipo FOREIGN KEY (tipo_fk)
        REFERENCES public.tb_tipo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_modelo FOREIGN KEY (modelo_fk)
        REFERENCES public.tb_modelo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_monitor1 FOREIGN KEY (codigo_monitor1_fk)
        REFERENCES public.tb_monitor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_monitor2 FOREIGN KEY (codigo_monitor2_fk)
        REFERENCES public.tb_monitor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_office FOREIGN KEY (office_fk)
        REFERENCES public.tb_office (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_placa_video FOREIGN KEY (placa_video_fk)
        REFERENCES public.tb_placa_video (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_processador FOREIGN KEY (processador_fk)
        REFERENCES public.tb_processador (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_ram_clok FOREIGN KEY (ram_clok_fk)
        REFERENCES public.tb_ram_clok (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_ram_medida FOREIGN KEY (ram_medida_fk)
        REFERENCES public.tb_medida (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_sistema_operacional FOREIGN KEY (sistema_operacional_fk)
        REFERENCES public.tb_sistema_operacional (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_hd_medida FOREIGN KEY (hd_medida_fk)
        REFERENCES public.tb_medida (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_computador_tipo_hd FOREIGN KEY (tipo_hd_fk)
        REFERENCES public.tb_tipo_hd (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_computador
    OWNER to postgres;

CREATE UNIQUE INDEX idx_computador_id ON public.tb_computador USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_computador_codigo_computador ON public.tb_computador USING btree
    (codigo_computador COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_computador_unindade ON public.tb_computador USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_setor ON public.tb_computador USING btree
    (setor_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_tipo ON public.tb_computador USING btree
    (tipo_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_processador ON public.tb_computador USING btree
    (processador_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_sistema ON public.tb_computador USING btree
    (sistema_operacional_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_office ON public.tb_computador USING btree
    (office_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_modelo ON public.tb_computador USING btree
    (modelo_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_ram_clok ON public.tb_computador USING btree
    (ram_clok_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_ram_medida ON public.tb_computador USING btree
    (ram_medida_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_tipo_hd ON public.tb_computador USING btree
    (tipo_hd_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_hd_medida ON public.tb_computador USING btree
    (hd_medida_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_placa_video ON public.tb_computador USING btree
    (placa_video_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_monitor1 ON public.tb_computador USING btree
    (codigo_monitor1_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_monitor2 ON public.tb_computador USING btree
    (codigo_monitor2_fk) TABLESPACE pg_default;

CREATE INDEX idx_computador_numero_numero_serie ON public.tb_computador USING btree
    (numero_serie COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_computador_numero_patrimonio ON public.tb_computador USING btree
    (numero_patrimonio COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_computador_netbios ON public.tb_computador USING btree
    (netbios COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_computador_mac_c ON public.tb_computador USING btree
    (mac_c COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_computador_mac_w ON public.tb_computador USING btree
    (mac_w COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Equipamento
*/
CREATE SEQUENCE tb_equipamento_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_equipamento_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_equipamento_seq
    IS 'Sequencia de numeracao do ID da Monitor';

CREATE TABLE public.tb_equipamento
(
    id bigint NOT NULL DEFAULT nextval(('tb_equipamento_seq'::text)::regclass),
    codigo_equipamento character varying(7) COLLATE pg_catalog."default" NOT NULL,
    unidade_fk bigint NOT NULL,
    setor_fk bigint,
    tipo_fk bigint NOT NULL,
    modelo_fk bigint NOT NULL,
    numero_serie character varying(80) COLLATE pg_catalog."default" NOT NULL,
    data_compra date NOT NULL,
    garantia date NOT NULL,
    loja character varying(80) COLLATE pg_catalog."default",
    numero_nota integer,
    valor numeric(20,2),
    numero_patrimonio character varying(40) COLLATE pg_catalog."default",
    netbios character varying(30) COLLATE pg_catalog."default",
    mac character varying(12) COLLATE pg_catalog."default" DEFAULT NULL,
    descricao_equipamento text COLLATE pg_catalog."default",
    status integer NOT NULL,
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_equipamento PRIMARY KEY (id),
    CONSTRAINT uq_equipamento_codigo_equipamento UNIQUE (codigo_equipamento),
    CONSTRAINT fk_equipamento_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_equipamento_setor FOREIGN KEY (setor_fk)
        REFERENCES public.tb_setor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_equipamento_tipo FOREIGN KEY (tipo_fk)
        REFERENCES public.tb_tipo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_equipamento_modelo FOREIGN KEY (modelo_fk)
        REFERENCES public.tb_modelo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_equipamento
    OWNER to postgres;

CREATE UNIQUE INDEX idx_equipamento_id ON public.tb_equipamento USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_equipamento_codigo_equipamento ON public.tb_equipamento USING btree
    (codigo_equipamento COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_equipamento_unindade ON public.tb_equipamento USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_equipamento_setor ON public.tb_equipamento USING btree
    (setor_fk) TABLESPACE pg_default;

CREATE INDEX idx_equipamento_tipo ON public.tb_equipamento USING btree
    (tipo_fk) TABLESPACE pg_default;

CREATE INDEX idx_equipamento_modelo ON public.tb_equipamento USING btree
    (modelo_fk) TABLESPACE pg_default;

CREATE INDEX idx_equipamento_numero_patrimonio ON public.tb_equipamento USING btree
    (numero_patrimonio COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_equipamento_netbios ON public.tb_equipamento USING btree
    (netbios COLLATE pg_catalog."default") TABLESPACE pg_default;

CREATE INDEX idx_equipamento_mac ON public.tb_equipamento USING btree
    (mac COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: VoIP - PIN
*/
CREATE SEQUENCE tb_voip_pin_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_voip_pin_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_voip_pin_seq
    IS 'Sequencia de numeracao do ID da VoIP - PIN';

CREATE TABLE public.tb_voip_pin
(
    id bigint NOT NULL DEFAULT nextval(('tb_voip_pin_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    funcionario_fk bigint NOT NULL,
    pin integer NOT NULL,
    particular integer NOT NULL,
    CONSTRAINT pk_tb_voip_pin PRIMARY KEY (id),
    CONSTRAINT uq_voip_pin UNIQUE (unidade_fk, pin),
    CONSTRAINT fk_voip_pin_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_voip_pin_funcionario FOREIGN KEY (funcionario_fk)
        REFERENCES public.tb_funcionario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_voip_pin
    OWNER to postgres;

CREATE UNIQUE INDEX idx_voip_pin_id ON public.tb_voip_pin USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_voip_pin_unindade ON public.tb_voip_pin USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_pin_funcionario ON public.tb_voip_pin USING btree
    (funcionario_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_pin_pin ON public.tb_voip_pin USING btree
    (pin) TABLESPACE pg_default;

/*
Tabela: VoIP - Captura
*/
CREATE SEQUENCE tb_voip_grupo_captura_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_voip_grupo_captura_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_voip_grupo_captura_seq
    IS 'Sequencia de numeracao do ID da VoIP - Grupo de Captura';

CREATE TABLE public.tb_voip_grupo_captura
(
    id bigint NOT NULL DEFAULT nextval(('tb_voip_grupo_captura_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    grupo integer NOT NULL,
    nome character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_voip_grupo_captura PRIMARY KEY (id),
    CONSTRAINT uq_voip_grupo_captura UNIQUE (unidade_fk, grupo),
    CONSTRAINT fk_voip_grupo_captura_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_voip_grupo_captura
    OWNER to postgres;

CREATE UNIQUE INDEX idx_voip_grupo_captura_id ON public.tb_voip_grupo_captura USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_voip_grupo_captura_unindade ON public.tb_voip_grupo_captura USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_grupo_captura_grupo ON public.tb_voip_grupo_captura USING btree
    (grupo) TABLESPACE pg_default;

CREATE INDEX idx_voip_grupo_captura_nome ON public.tb_voip_grupo_captura USING btree
    (nome COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: VoIP
*/
CREATE SEQUENCE tb_voip_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_voip_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_voip_grupo_captura_seq
    IS 'Sequencia de numeracao do ID da VoIP';

CREATE TABLE public.tb_voip
(
    id bigint NOT NULL DEFAULT nextval(('tb_voip_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    setor_fk bigint NOT NULL,
    voip integer NOT NULL,
    senha character varying(50) COLLATE pg_catalog."default" NOT NULL,
    telefone character varying(15) COLLATE pg_catalog."default",
    tempo_toque integer NOT NULL,
    transferencia_automatica_fk bigint,
    toque_simultaneo character varying(50) COLLATE pg_catalog."default",
    grupo_fk bigint,
    equipamento_fk bigint,
    funcionario_fk bigint,
    informacao character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_voip PRIMARY KEY (id),
    CONSTRAINT uq_voip UNIQUE (unidade_fk, voip),
    CONSTRAINT fk_voip_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_voip_setor FOREIGN KEY (setor_fk)
        REFERENCES public.tb_setor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_voip_transferencia_automatica FOREIGN KEY (transferencia_automatica_fk)
        REFERENCES public.tb_voip (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_voip_grupo_captura FOREIGN KEY (grupo_fk)
        REFERENCES public.tb_voip_grupo_captura (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_voip_equipamento FOREIGN KEY (equipamento_fk)
        REFERENCES public.tb_equipamento (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_voip_funcionario FOREIGN KEY (funcionario_fk)
        REFERENCES public.tb_funcionario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_voip
    OWNER to postgres;

CREATE UNIQUE INDEX idx_voip_id ON public.tb_voip USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_voip_unindade ON public.tb_voip USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_setor ON public.tb_voip USING btree
    (setor_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_voip ON public.tb_voip USING btree
    (voip) TABLESPACE pg_default;

CREATE INDEX idx_voip_transferencia_automatica ON public.tb_voip USING btree
    (transferencia_automatica_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_grupo ON public.tb_voip USING btree
    (grupo_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_equipamento ON public.tb_voip USING btree
    (equipamento_fk) TABLESPACE pg_default;

CREATE INDEX idx_voip_funcionario ON public.tb_voip USING btree
    (funcionario_fk) TABLESPACE pg_default;

/*
Tabela: Switch
*/
CREATE SEQUENCE tb_switch_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_switch_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_switch_seq
    IS 'Sequencia de numeracao do ID da Switch';

CREATE TABLE public.tb_switch
(
    id bigint NOT NULL DEFAULT nextval(('tb_switch_seq'::text)::regclass),
    codigo_switch_fk bigint NOT NULL,
    local_fk bigint NOT NULL,
    id_switch integer NOT NULL,
    numero_portas integer NOT NULL,
    descricao_switch character varying(80) COLLATE pg_catalog."default",
    xml_portas text COLLATE pg_catalog."default" NOT NULL,
    configuracao text COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_switch PRIMARY KEY (id),
    CONSTRAINT uq_switch UNIQUE (codigo_switch_fk, local_fk, id_switch),
    CONSTRAINT fk_switch_codigo_switch FOREIGN KEY (codigo_switch_fk)
        REFERENCES public.tb_equipamento (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_switch_local FOREIGN KEY (local_fk)
        REFERENCES public.tb_local (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_switch
    OWNER to postgres;

CREATE UNIQUE INDEX idx_switch_id ON public.tb_switch USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_switch_codigo_switch ON public.tb_switch USING btree
    (codigo_switch_fk) TABLESPACE pg_default;

CREATE INDEX idx_switch_local ON public.tb_switch USING btree
    (local_fk) TABLESPACE pg_default;

CREATE INDEX idx_switch_id_switch ON public.tb_switch USING btree
    (id_switch) TABLESPACE pg_default;

CREATE INDEX idx_switch_numero_portas ON public.tb_switch USING btree
    (numero_portas) TABLESPACE pg_default;

/*
Tabela: Switch - Arquivo
*/
CREATE SEQUENCE tb_switch_arquivo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_switch_arquivo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_switch_arquivo_seq
    IS 'Sequencia de numeracao do ID do Switch - Arquivo';

CREATE TABLE public.tb_switch_arquivo
(
    id bigint NOT NULL DEFAULT nextval(('tb_switch_arquivo_seq'::text)::regclass),
    switch_fk bigint NOT NULL,
    data timestamp without time zone,
    descricao_arquivo character varying(120) COLLATE pg_catalog."default",
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_switch_arquivo PRIMARY KEY (id),
    CONSTRAINT fk_switch_arquivo_switch FOREIGN KEY (switch_fk)
        REFERENCES public.tb_switch (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_switch_arquivo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_switch_arquivo_id ON public.tb_switch_arquivo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_switch_arquivo_switch ON public.tb_switch_arquivo USING btree
    (Switch_fk) TABLESPACE pg_default;

/*
Tabela: Patch Panel
*/
CREATE SEQUENCE tb_patch_panel_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_patch_panel_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_patch_panel_seq
    IS 'Sequencia de numeracao do ID da Switch';

CREATE TABLE public.tb_patch_panel
(
    id bigint NOT NULL DEFAULT nextval(('tb_patch_panel_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    local_fk bigint NOT NULL,
    id_patch_panel integer NOT NULL,
    numero_portas integer NOT NULL,
    descricao_patch_panel character varying(80) COLLATE pg_catalog."default",
    xml_portas text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_patch_panel PRIMARY KEY (id),
    CONSTRAINT uq_patch_panel UNIQUE (unidade_fk, local_fk, id_patch_panel),
    CONSTRAINT fk_patch_panel_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_patch_panel_local FOREIGN KEY (local_fk)
        REFERENCES public.tb_local (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_patch_panel
    OWNER to postgres;

CREATE UNIQUE INDEX idx_patch_panel_id ON public.tb_patch_panel USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_patch_panel_unidade ON public.tb_patch_panel USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_patch_panel_local ON public.tb_patch_panel USING btree
    (local_fk) TABLESPACE pg_default;

CREATE INDEX idx_patch_panel_id_patch_panel ON public.tb_patch_panel USING btree
    (id_patch_panel) TABLESPACE pg_default;

CREATE INDEX idx_patch_panel_numero_portas ON public.tb_patch_panel USING btree
    (numero_portas) TABLESPACE pg_default;

/*
Tabela: Pon
*/
CREATE SEQUENCE tb_pon_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_pon_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_pon_seq
    IS 'Sequencia de numeracao do ID da PON';

CREATE TABLE public.tb_pon
(
    id bigint NOT NULL DEFAULT nextval(('tb_pon_seq'::text)::regclass),
    codigo_pon_fk bigint NOT NULL,
    local_fk bigint NOT NULL,
    id_pon integer NOT NULL,
    numero_portas_pon integer NOT NULL,
    numero_portas_bridge integer NOT NULL,
    running_geral text COLLATE pg_catalog."default",
    running_bridge text COLLATE pg_catalog."default",
    running_pon_dba_profile text COLLATE pg_catalog."default",
    running_pon_extended_profile text COLLATE pg_catalog."default",
    running_pon_traffic_profile text COLLATE pg_catalog."default",
    running_pon_onu_profile text COLLATE pg_catalog."default",
    running_pon_olt text COLLATE pg_catalog."default",
    conf text COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_pon PRIMARY KEY (id),
    CONSTRAINT uq_pon UNIQUE (codigo_pon_fk, local_fk, id_pon),
    CONSTRAINT fk_pon_codigo_pon FOREIGN KEY (codigo_pon_fk)
        REFERENCES public.tb_equipamento (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_pon_local FOREIGN KEY (local_fk)
        REFERENCES public.tb_local (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_pon
    OWNER to postgres;

CREATE UNIQUE INDEX idx_pon_id ON public.tb_pon USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_codigo_pon ON public.tb_pon USING btree
    (codigo_pon_fk) TABLESPACE pg_default;

CREATE INDEX idx_pon_local ON public.tb_pon USING btree
    (local_fk) TABLESPACE pg_default;

CREATE INDEX idx_pon_id_gpon ON public.tb_pon USING btree
    (id_pon) TABLESPACE pg_default;

/*
Tabela: PON ONU
*/
CREATE SEQUENCE tb_pon_onu_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_pon_onu_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_pon_onu_seq
    IS 'Sequencia de numeracao do ID da PON ONU';
CREATE TABLE public.tb_pon_onu
(
    id bigint NOT NULL DEFAULT nextval(('tb_pon_onu_seq'::text)::regclass),
    olt_fk bigint NOT NULL,
    local_fk bigint NOT NULL,
    codigo_onu_fk bigint NOT NULL,
    porta_pon bigint NOT NULL,
    onu_profile text,
    ponto text,
    CONSTRAINT pk_pon_onu PRIMARY KEY (id),
    CONSTRAINT uq_pon_onu UNIQUE (olt_fk, porta_pon, codigo_onu_fk),
    CONSTRAINT fk_pon_onu_codigo_onu FOREIGN KEY (codigo_onu_fk)
        REFERENCES public.tb_equipamento (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_pon_onu_olt FOREIGN KEY (olt_fk)
        REFERENCES public.tb_pon (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_pon_onu_local FOREIGN KEY (local_fk)
        REFERENCES public.tb_local (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION

)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.tb_pon_onu
    OWNER to postgres;

CREATE UNIQUE INDEX idx_pon_onu_id ON public.tb_pon_onu USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_codigo_pon_onu ON public.tb_pon_onu USING btree
    (codigo_onu_fk) TABLESPACE pg_default;

CREATE INDEX idx_pon_onu_local ON public.tb_pon_onu USING btree
    (local_fk) TABLESPACE pg_default;

/*
Tabela: Mapa Canal
*/
CREATE SEQUENCE tb_mapa_canal_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_mapa_canal_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_mapa_canal_seq
    IS 'Sequencia de numeracao do ID da Mapa Canal';

CREATE TABLE public.tb_mapa_canal
(
    id bigint NOT NULL DEFAULT nextval(('tb_mapa_canal_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    local_fk bigint NOT NULL,
    switch_fk integer,
    switch_porta integer,
    patch_panel integer NOT NULL,
    patch_panel_porta integer NOT NULL,
    patch_cord_switch numeric(20,2),
    link numeric(20,2),
    patch_cord_dispositivo numeric(20,2),
    descricao character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_mapa_canal PRIMARY KEY (id),
    CONSTRAINT uq_mapa_canal UNIQUE (unidade_fk, local_fk, patch_panel, patch_panel_porta,switch_fk,switch_porta),
    CONSTRAINT fk_mapa_canal_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_mapa_canal_local FOREIGN KEY (local_fk)
        REFERENCES public.tb_local (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_mapa_canal_switch FOREIGN KEY (switch_fk)
        REFERENCES public.tb_switch (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE UNIQUE INDEX idx_mapa_canal_id ON public.tb_mapa_canal USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_mapa_canal_unindade ON public.tb_mapa_canal USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_mapa_canal_local ON public.tb_mapa_canal USING btree
    (local_fk) TABLESPACE pg_default;

CREATE INDEX idx_mapa_canal_switch ON public.tb_mapa_canal USING btree
    (switch_fk) TABLESPACE pg_default;

CREATE INDEX idx_mapa_canal_switch_porta ON public.tb_mapa_canal USING btree
    (switch_porta) TABLESPACE pg_default;

CREATE INDEX idx_mapa_canal_patch_panel ON public.tb_mapa_canal USING btree
    (patch_panel) TABLESPACE pg_default;

CREATE INDEX idx_mapa_canal_patch_panel_porta ON public.tb_mapa_canal USING btree
    (patch_panel_porta) TABLESPACE pg_default;

CREATE INDEX idx_mapa_canal_descricao ON public.tb_mapa_canal USING btree
    (descricao COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Ocorencia Monitor
*/
CREATE SEQUENCE tb_ocorencia_monitor_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_ocorencia_monitor_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_ocorencia_monitor_seq
    IS 'Sequencia de numeracao do ID da Ocorencia Monitor';

CREATE TABLE public.tb_ocorencia_monitor
(
    id bigint NOT NULL DEFAULT nextval(('tb_ocorencia_monitor_seq'::text)::regclass),
    data timestamp without time zone,
    usuario_nome character varying(80) COLLATE pg_catalog."default" NOT NULL,
    codigo_monitor_fk bigint NOT NULL,
    tipo_fk bigint NOT NULL,
    descricao text COLLATE pg_catalog."default",
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_ocorencia_monitor PRIMARY KEY (id),
    CONSTRAINT fk_ocorencia_monitor_codigo_monitor FOREIGN KEY (codigo_monitor_fk)
        REFERENCES public.tb_monitor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_ocorencia_monitor_tipo FOREIGN KEY (tipo_fk)
        REFERENCES public.tb_ocorencia_tipo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_ocorencia_monitor
    OWNER to postgres;

CREATE UNIQUE INDEX idx_ocorencia_monitor_id ON public.tb_ocorencia_monitor USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_ocorencia_monitor_codigo_monitor ON public.tb_ocorencia_monitor USING btree
    (codigo_monitor_fk) TABLESPACE pg_default;

CREATE INDEX idx_ocorencia_monitor_tipo ON public.tb_ocorencia_monitor USING btree
    (tipo_fk) TABLESPACE pg_default;

/*
Tabela: Ocorencia Computador
*/
CREATE SEQUENCE tb_ocorencia_computador_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_ocorencia_computador_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_ocorencia_computador_seq
    IS 'Sequencia de numeracao do ID da Ocorencia Computador';

CREATE TABLE public.tb_ocorencia_computador
(
    id bigint NOT NULL DEFAULT nextval(('tb_ocorencia_computador_seq'::text)::regclass),
    data timestamp without time zone,
    usuario_nome character varying(80) COLLATE pg_catalog."default" NOT NULL,
    codigo_computador_fk bigint NOT NULL,
    tipo_fk bigint NOT NULL,
    descricao text COLLATE pg_catalog."default",
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_ocorencia_computador PRIMARY KEY (id),
    CONSTRAINT fk_ocorencia_computador_codigo_computador FOREIGN KEY (codigo_computador_fk)
        REFERENCES public.tb_computador (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_ocorencia_computador_tipo FOREIGN KEY (tipo_fk)
        REFERENCES public.tb_ocorencia_tipo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_ocorencia_computador
    OWNER to postgres;

CREATE UNIQUE INDEX idx_ocorencia_computador_id ON public.tb_ocorencia_computador USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_ocorencia_computador_codigo_computador ON public.tb_ocorencia_computador USING btree
    (codigo_computador_fk) TABLESPACE pg_default;

CREATE INDEX idx_ocorencia_computador_tipo ON public.tb_ocorencia_computador USING btree
    (tipo_fk) TABLESPACE pg_default;

/*
Tabela: Ocorencia Equipamento
*/
CREATE SEQUENCE tb_ocorencia_equipamento_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_ocorencia_equipamento_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_ocorencia_equipamento_seq
    IS 'Sequencia de numeracao do ID da Ocorencia Equipamento';

CREATE TABLE public.tb_ocorencia_equipamento
(
    id bigint NOT NULL DEFAULT nextval(('tb_ocorencia_equipamento_seq'::text)::regclass),
    data timestamp without time zone,
    usuario_nome character varying(80) COLLATE pg_catalog."default" NOT NULL,
    codigo_equipamento_fk bigint NOT NULL,
    tipo_fk bigint NOT NULL,
    descricao text COLLATE pg_catalog."default",
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_ocorencia_equipamento PRIMARY KEY (id),
    CONSTRAINT fk_ocorencia_equipamento_codigo_computador FOREIGN KEY (codigo_equipamento_fk)
        REFERENCES public.tb_equipamento (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_ocorencia_equipamento_tipo FOREIGN KEY (tipo_fk)
        REFERENCES public.tb_ocorencia_tipo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_ocorencia_equipamento
    OWNER to postgres;

CREATE UNIQUE INDEX idx_ocorencia_equipamento_id ON public.tb_ocorencia_equipamento USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_ocorencia_equipamento_codigo_equipamento ON public.tb_ocorencia_equipamento USING btree
    (codigo_equipamento_fk) TABLESPACE pg_default;

CREATE INDEX idx_ocorencia_equipamento_tipo ON public.tb_ocorencia_equipamento USING btree
    (tipo_fk) TABLESPACE pg_default;

/*
Tabela: Help Desk - Cadastro
*/
CREATE SEQUENCE tb_helpdesk_cadastro_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_helpdesk_cadastro_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_helpdesk_cadastro_seq
    IS 'Sequencia de numeracao do ID do Help Desk - Cadastro';

CREATE TABLE public.tb_helpdesk_cadastro
(
    id bigint NOT NULL DEFAULT nextval(('tb_helpdesk_cadastro_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    de_fk bigint NOT NULL,
    data_abertura timestamp without time zone,
    data_encerrado timestamp without time zone,
    produto_fk bigint NOT NULL,
    prioridade integer NOT NULL,
    para_fk bigint NOT NULL,
    resumo character varying(255) COLLATE pg_catalog."default" NOT NULL,
    descricao text COLLATE pg_catalog."default",
    ver integer NOT NULL,
    status integer NOT NULL,
    CONSTRAINT pk_tb_helpdesk_cadastro PRIMARY KEY (id),
    CONSTRAINT fk_helpdesk_cadastro_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_cadastro_de FOREIGN KEY (de_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_cadastro_para FOREIGN KEY (para_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_cadastro_produto FOREIGN KEY (produto_fk)
        REFERENCES public.tb_helpdesk_produto (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_helpdesk_cadastro
    OWNER to postgres;

CREATE UNIQUE INDEX idx_helpdesk_cadastro_id ON public.tb_helpdesk_cadastro USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_cadastro_unidade ON public.tb_helpdesk_cadastro USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_cadastro_de ON public.tb_helpdesk_cadastro USING btree
    (de_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_cadastro_para ON public.tb_helpdesk_cadastro USING btree
    (para_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_cadastro_produto ON public.tb_helpdesk_cadastro USING btree
    (produto_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_cadastro_status ON public.tb_helpdesk_cadastro USING btree
    (status) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_cadastro_ver ON public.tb_helpdesk_cadastro USING btree
    (ver) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_cadastro_resumo ON public.tb_helpdesk_cadastro USING btree
    (resumo COLLATE pg_catalog."default") TABLESPACE pg_default;

/*
Tabela: Help Desk - De Usuario Para
*/
CREATE SEQUENCE tb_helpdesk_de_usuario_para_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_helpdesk_de_usuario_para_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_helpdesk_de_usuario_para_seq
    IS 'Sequencia de numeracao do ID do Help Desk - De Usuario Para';

CREATE TABLE public.tb_helpdesk_de_usuario_para
(
    id bigint NOT NULL DEFAULT nextval(('tb_helpdesk_de_usuario_para_seq'::text)::regclass),
    usuario_de_fk bigint NOT NULL,
    usuario_para_fk bigint NOT NULL,
    CONSTRAINT pk_tb_helpdesk_de_usuario_para PRIMARY KEY (id),
    CONSTRAINT uq_helpdesk_de_usuario_para UNIQUE (usuario_de_fk, usuario_para_fk),
    CONSTRAINT fk_helpdesk_de_usuario_para_usuario_de FOREIGN KEY (usuario_de_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_de_usuario_para_usuario_para FOREIGN KEY (usuario_para_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_helpdesk_de_usuario_para
    OWNER to postgres;

CREATE UNIQUE INDEX idx_helpdesk_de_usuario_para_id ON public.tb_helpdesk_de_usuario_para USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_de_usuario_para_usuario_de ON public.tb_helpdesk_de_usuario_para USING btree
    (usuario_de_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_de_usuario_para_usuario_para ON public.tb_helpdesk_de_usuario_para USING btree
    (usuario_para_fk) TABLESPACE pg_default;

/*
Tabela: Help Desk - Etapas
*/
CREATE SEQUENCE tb_helpdesk_etapa_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_helpdesk_etapa_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_helpdesk_etapa_seq
    IS 'Sequencia de numeracao do ID do Help Desk - Etapa';

CREATE TABLE public.tb_helpdesk_etapa
(
    id bigint NOT NULL DEFAULT nextval(('tb_helpdesk_etapa_seq'::text)::regclass),
    chamado_fk bigint NOT NULL,
    de_fk bigint NOT NULL,
    data timestamp without time zone,
    descricao text COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_helpdesk_etapa PRIMARY KEY (id),
    CONSTRAINT fk_helpdesk_etapa_chamado FOREIGN KEY (chamado_fk)
        REFERENCES public.tb_helpdesk_cadastro (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_etapa_de FOREIGN KEY (de_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_helpdesk_etapa
    OWNER to postgres;

CREATE UNIQUE INDEX idx_helpdesk_etapa_id ON public.tb_helpdesk_etapa USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_etapa_de ON public.tb_helpdesk_etapa USING btree
    (de_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_etapa_produto ON public.tb_helpdesk_etapa USING btree
    (chamado_fk) TABLESPACE pg_default;

/*
Tabela: Help Desk - Historico
*/
CREATE SEQUENCE tb_helpdesk_historico_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_helpdesk_historico_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_helpdesk_historico_seq
    IS 'Sequencia de numeracao do ID do Help Desk - Historico';

CREATE TABLE public.tb_helpdesk_historico
(
    id bigint NOT NULL DEFAULT nextval(('tb_helpdesk_historico_seq'::text)::regclass),
    chamado_fk bigint NOT NULL,
    de_fk bigint NOT NULL,
    data timestamp without time zone,
    acao character varying(90) COLLATE pg_catalog."default" NOT NULL,
    alteracao character varying(120) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_tb_helpdesk_historico PRIMARY KEY (id),
    CONSTRAINT fk_helpdesk_historico_de FOREIGN KEY (de_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_historico_chamado FOREIGN KEY (chamado_fk)
        REFERENCES public.tb_helpdesk_cadastro (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_helpdesk_historico
    OWNER to postgres;

CREATE UNIQUE INDEX idx_helpdesk_historico_id ON public.tb_helpdesk_historico USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_historico_de ON public.tb_helpdesk_historico USING btree
    (de_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_historico_produto ON public.tb_helpdesk_historico USING btree
    (chamado_fk) TABLESPACE pg_default;

/*
Tabela: Help Desk - Anexo
*/
CREATE SEQUENCE tb_helpdesk_anexo_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_helpdesk_anexo_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_helpdesk_anexo_seq
    IS 'Sequencia de numeracao do ID do Help Desk - Anexo';

CREATE TABLE public.tb_helpdesk_anexo
(
    id bigint NOT NULL DEFAULT nextval(('tb_helpdesk_anexo_seq'::text)::regclass),
    chamado_fk bigint NOT NULL,
    de_fk bigint NOT NULL,
    data timestamp without time zone,
    descricao_anexo character varying(120) COLLATE pg_catalog."default",
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_helpdesk_anexo PRIMARY KEY (id),
    CONSTRAINT fk_helpdesk_anexo_de FOREIGN KEY (de_fk)
        REFERENCES public.tb_usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_helpdesk_anexo_chamado FOREIGN KEY (chamado_fk)
        REFERENCES public.tb_helpdesk_cadastro (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_helpdesk_anexo
    OWNER to postgres;

CREATE UNIQUE INDEX idx_helpdesk_anexo_id ON public.tb_helpdesk_anexo USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_anexo_de ON public.tb_helpdesk_anexo USING btree
    (de_fk) TABLESPACE pg_default;

CREATE INDEX idx_helpdesk_anexo_chamado ON public.tb_helpdesk_anexo USING btree
    (chamado_fk) TABLESPACE pg_default;

/*
Tabela: Funcionario - Computador
*/
CREATE SEQUENCE tb_funcionario_computador_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_funcionario_computador_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_funcionario_computador_seq
    IS 'Sequencia de numeracao do ID do Funcionario - Computador';

CREATE TABLE public.tb_funcionario_computador
(
    id bigint NOT NULL DEFAULT nextval(('tb_funcionario_computador_seq'::text)::regclass),
    funcionario_fk bigint NOT NULL,
    computador_fk bigint NOT NULL,
    CONSTRAINT pk_tb_funcionario_computador PRIMARY KEY (id),
    CONSTRAINT uq_funcionario_computador UNIQUE (funcionario_fk, computador_fk),
    CONSTRAINT fk_funcionario_computador_funcionario FOREIGN KEY (funcionario_fk)
        REFERENCES public.tb_funcionario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_funcionario_computador_computador FOREIGN KEY (computador_fk)
        REFERENCES public.tb_computador (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_funcionario_computador
    OWNER to postgres;

CREATE UNIQUE INDEX idx_funcionario_computador_id ON public.tb_funcionario_computador USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_computador_funcionario ON public.tb_funcionario_computador USING btree
    (funcionario_fk) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_computador_computador ON public.tb_funcionario_computador USING btree
    (computador_fk) TABLESPACE pg_default;

/*
Tabela: Funcionario - Monitor
*/
CREATE SEQUENCE tb_funcionario_monitor_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_funcionario_monitor_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_funcionario_monitor_seq
    IS 'Sequencia de numeracao do ID do Funcionario - Computador';

CREATE TABLE public.tb_funcionario_monitor
(
    id bigint NOT NULL DEFAULT nextval(('tb_funcionario_monitor_seq'::text)::regclass),
    funcionario_fk bigint NOT NULL,
    monitor_fk bigint NOT NULL,
    CONSTRAINT pk_tb_funcionario_monitor PRIMARY KEY (id),
    CONSTRAINT uq_funcionario_monitor UNIQUE (funcionario_fk, monitor_fk),
    CONSTRAINT fk_funcionario_monitor_funcionario FOREIGN KEY (funcionario_fk)
        REFERENCES public.tb_funcionario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_funcionario_monitor_monitor FOREIGN KEY (monitor_fk)
        REFERENCES public.tb_monitor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_funcionario_monitor
    OWNER to postgres;

CREATE UNIQUE INDEX idx_funcionario_monitor_id ON public.tb_funcionario_monitor USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_monitor_funcionario ON public.tb_funcionario_monitor USING btree
    (funcionario_fk) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_monitor_monitor ON public.tb_funcionario_monitor USING btree
    (monitor_fk) TABLESPACE pg_default;

/*
Tabela: Funcionario - Equipametno
*/
CREATE SEQUENCE tb_funcionario_equipamento_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_funcionario_equipamento_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_funcionario_equipamento_seq
    IS 'Sequencia de numeracao do ID do Funcionario - Computador';

CREATE TABLE public.tb_funcionario_equipamento
(
    id bigint NOT NULL DEFAULT nextval(('tb_funcionario_equipamento_seq'::text)::regclass),
    funcionario_fk bigint NOT NULL,
    equipamento_fk bigint NOT NULL,
    CONSTRAINT pk_tb_funcionario_equipamento PRIMARY KEY (id),
    CONSTRAINT uq_funcionario_equipamento UNIQUE (funcionario_fk, equipamento_fk),
    CONSTRAINT fk_funcionario_equipamento_funcionario FOREIGN KEY (funcionario_fk)
        REFERENCES public.tb_funcionario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_funcionario_equipamento_equipamento FOREIGN KEY (equipamento_fk)
        REFERENCES public.tb_equipamento (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_funcionario_equipamento
    OWNER to postgres;

CREATE UNIQUE INDEX idx_funcionario_equipamento_id ON public.tb_funcionario_equipamento USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_equipamento_funcionario ON public.tb_funcionario_equipamento USING btree
    (funcionario_fk) TABLESPACE pg_default;

CREATE INDEX idx_funcionario_equipamento_equipamento ON public.tb_funcionario_equipamento USING btree
    (equipamento_fk) TABLESPACE pg_default;

/*
Tabela: Contratos
*/
CREATE SEQUENCE tb_contrato_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_contrato_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_contrato_seq
    IS 'Sequencia de numeracao do ID do Contrato';

CREATE TABLE public.tb_contrato
(
    id bigint NOT NULL DEFAULT nextval(('tb_contrato_seq'::text)::regclass),
    unidade_fk bigint NOT NULL,
    perfil_fk bigint NOT NULL,
    data timestamp without time zone,
    data_alteracao timestamp without time zone,
    data_fim date,
    descricao character varying(120) COLLATE pg_catalog."default" NOT NULL,
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_contrato PRIMARY KEY (id),
    CONSTRAINT uq_contrato UNIQUE (unidade_fk, descricao),
    CONSTRAINT fk_contrato_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_contrato_perfil FOREIGN KEY (perfil_fk)
        REFERENCES public.tb_perfil (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_contrato
    OWNER to postgres;

CREATE UNIQUE INDEX idx_contrato_id ON public.tb_contrato USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_contrato_unidade ON public.tb_contrato USING btree
    (unidade_fk) TABLESPACE pg_default;

CREATE INDEX idx_contrato_perfil ON public.tb_contrato USING btree
    (perfil_fk) TABLESPACE pg_default;

CREATE INDEX idx_contrato_data ON public.tb_contrato USING btree
    (data) TABLESPACE pg_default;

CREATE INDEX idx_contrato_descricao ON public.tb_contrato USING btree
    (descricao) TABLESPACE pg_default;

CREATE INDEX idx_contrato_url ON public.tb_contrato USING btree
    (url) TABLESPACE pg_default;

/*
Tabela: API Java Computador
*/
CREATE SEQUENCE tb_api_java_computador_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_api_java_computador_seq
    OWNER TO postgres;

COMMENT ON SEQUENCE public.tb_api_java_computador_seq
    IS 'Sequencia de numeracao do ID da API Java Computadores';

CREATE TABLE public.tb_api_java_computador
(
    id bigint NOT NULL DEFAULT nextval(('tb_api_java_computador_seq'::text)::regclass),
    data timestamp without time zone,
    codigo_computador_fk bigint NOT NULL,
    unidade_fk bigint NOT NULL,
    versao_so character varying(255) COLLATE pg_catalog."default",
    nome_so character varying(255) COLLATE pg_catalog."default",
    arquitetura_so character varying(255) COLLATE pg_catalog."default",
    memoria_ram character varying(255) COLLATE pg_catalog."default",
    memoria_live character varying(255) COLLATE pg_catalog."default",
    processador character varying(255) COLLATE pg_catalog."default",
    macaddres_1 character varying(255) COLLATE pg_catalog."default",
    macaddres_2 character varying(255) COLLATE pg_catalog."default",
    macaddres_3 character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pk_tb_api_java_computador PRIMARY KEY (id),
    CONSTRAINT fk_api_java_computador_codigo_computador FOREIGN KEY (codigo_computador_fk)
        REFERENCES public.tb_computador (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_api_java_computador_unidade FOREIGN KEY (unidade_fk)
        REFERENCES public.tb_unidade (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_api_java_computador
    OWNER to postgres;

CREATE UNIQUE INDEX idx_api_java_computador_id ON public.tb_api_java_computador USING btree
    (id) TABLESPACE pg_default;

CREATE INDEX idx_api_java_computador_codigo_computador ON public.tb_api_java_computador USING btree
    (codigo_computador_fk) TABLESPACE pg_default;

CREATE INDEX idx_api_java_computador_unidade ON public.tb_api_java_computador USING btree
    (unidade_fk) TABLESPACE pg_default;

/*
View: Código
*/
CREATE OR REPLACE VIEW public.vw_codigo AS 
 SELECT codigo.id,
    codigo.ano,
    codigo.mes,
    codigo.dispositivo,
    codigo.codigo_fk,
    sequencia_codigo.sequencia,
    sequencia_codigo.codigo
   FROM tb_codigo codigo
    JOIN tb_sequencia_codigo sequencia_codigo ON sequencia_codigo.id = codigo.codigo_fk;
ALTER TABLE vw_codigo OWNER TO postgres;

/*
View: Menu
*/
CREATE OR REPLACE VIEW public.vw_menu AS 
 SELECT menu.id,
    user_perfil.usuario_fk,
    menu.sistema_fk,
    menu.menu_pai_fk,
    menu.nivel,
    menu.titulo,
    menu.controller,
    menu.icon
   FROM tb_usuario_perfil user_perfil
    JOIN tb_menu_perfil menu_perfil ON menu_perfil.perfil_fk = user_perfil.perfil_fk
    JOIN tb_menu menu ON menu.id = menu_perfil.menu_fk;
ALTER TABLE vw_menu OWNER TO postgres;

/*
View: Restricao Menu
*/
CREATE OR REPLACE VIEW public.vw_restricao_menu AS 
 SELECT menu.id,
    menu.controller,
    restricao.restricao,
    usuario.usuario_fk
   FROM tb_menu menu
    JOIN tb_restricao_menu_perfil restricao ON restricao.menu_fk = menu.id
    JOIN tb_perfil perfil ON perfil.id = restricao.perfil_fk
    JOIN tb_usuario_perfil usuario ON usuario.perfil_fk = perfil.id;

ALTER TABLE public.vw_restricao_menu OWNER TO postgres;

/*
View: De Usuario Para
*/
CREATE OR REPLACE VIEW public.vw_helpdesk_de_usuario_para AS 
 SELECT usuario.id,
    usuario.usuario_de_fk,
    usuario_de.nome AS nome_de,
    usuario.usuario_para_fk,
    usuario_para.nome AS nome_para
   FROM tb_helpdesk_de_usuario_para usuario
    JOIN tb_usuario usuario_de ON usuario_de.id = usuario.usuario_de_fk
    JOIN tb_usuario usuario_para ON usuario_para.id = usuario.usuario_para_fk;
ALTER TABLE vw_helpdesk_de_usuario_para OWNER TO postgres;

/*
View: Marca Modelo
*/
CREATE OR REPLACE VIEW public.vw_marca_modelo AS 
 SELECT modelo.id,
    marca_modelo.marca,
    modelo.marca_fk,
    modelo.modelo,
    modelo.tipo
   FROM tb_modelo modelo
    JOIN tb_marca marca_modelo ON marca_modelo.id = modelo.marca_fk;
ALTER TABLE vw_marca_modelo OWNER TO postgres;

/*
View: Marca Placa Video
*/
CREATE OR REPLACE VIEW public.vw_marca_placa_video AS 
 SELECT placa_video.id,
    marca_placa_video.marca,
    placa_video.marca_fk,
    placa_video.modelo,
    placa_video.descricao_placa_video
   FROM tb_placa_video placa_video
    JOIN tb_marca marca_placa_video ON marca_placa_video.id = placa_video.marca_fk;
ALTER TABLE vw_marca_placa_video OWNER TO postgres;

/*
View: Marca Precessador
*/
CREATE OR REPLACE VIEW public.vw_marca_processador AS 
 SELECT processador.id,
    marca_processador.marca,
    processador.marca_fk,
    processador.processador,
    processador.descricao_processador
   FROM tb_processador processador
    JOIN tb_marca marca_processador ON marca_processador.id = processador.marca_fk;
ALTER TABLE vw_marca_processador OWNER TO postgres;

/*
View: Marca Precessador
*/
CREATE OR REPLACE VIEW public.vw_usuario AS 
 SELECT usuario.id,
    usuario.usuario,
    usuario.senha,
    usuario.nome AS nome_usuario,
    usuario.email,
    usuario.status,
    usuario.telefone,
    usuario.unidade_fk,
    unidade.unidade,
    usuario_perfil.perfil_fk,
    perfil.nome AS nome_perfil
   FROM tb_usuario usuario
    JOIN tb_unidade unidade ON unidade.id = usuario.unidade_fk
    JOIN tb_usuario_perfil usuario_perfil ON usuario_perfil.usuario_fk = usuario.id
    JOIN tb_perfil perfil ON perfil.id = usuario_perfil.perfil_fk;
ALTER TABLE vw_usuario OWNER TO postgres;

/*
View: Switch
*/
CREATE OR REPLACE VIEW public.vw_switch AS 
 SELECT switch.id,
    switch.codigo_switch_fk,
    equipamento.codigo_equipamento,
    equipamento.unidade_fk,
    unidade.unidade,
    switch.local_fk,
    local.local,
    switch.id_switch,
    switch.descricao_switch,
    switch.numero_portas
   FROM tb_switch switch
    JOIN tb_equipamento equipamento ON equipamento.id = switch.codigo_switch_fk
    JOIN tb_unidade unidade ON unidade.id = equipamento.unidade_fk
    JOIN tb_local local ON local.id = switch.local_fk;
ALTER TABLE vw_switch OWNER TO postgres;

/*
View: Patch Panel
*/
CREATE OR REPLACE VIEW public.vw_patch_panel AS 
 SELECT patch_panel.id,
    patch_panel.unidade_fk,
    unidade.unidade,
    patch_panel.local_fk,
    local.local,
    patch_panel.id_patch_panel,
    patch_panel.descricao_patch_panel,
    patch_panel.numero_portas,
    patch_panel.xml_postas
   FROM tb_patch_panel patch_panel
    JOIN tb_unidade unidade ON unidade.id = patch_panel.unidade_fk
    JOIN tb_local local ON local.id = patch_panel.local_fk;
ALTER TABLE vw_patch_panel OWNER TO postgres;

/*
View: Computador
*/
CREATE OR REPLACE VIEW public.vw_computador AS 
 SELECT computador.id,
    computador.codigo_computador,
    unidade_computador.unidade,
    computador.unidade_fk,
    setor_computador.setor,
    computador.setor_fk,
    tipo_computador.tipo,
    computador.tipo_fk,
    modelo_computador.marca,
    marca_computador.marca_fk,
    modelo_computador.modelo,
    computador.modelo_fk,
    computador.numero_serie,
    computador.loja,
    computador.numero_nota,
    computador.valor,
    computador.numero_patrimonio,
    computador.numero_lacre,
    computador.descricao_computador,
    computador.netbios,
    computador.mac_c,
    computador.mac_w,
    computador.status,
    computador.url
   FROM tb_computador computador
    JOIN tb_unidade unidade_computador ON unidade_computador.id = computador.unidade_fk
    JOIN tb_setor setor_computador ON setor_computador.id = computador.setor_fk
    JOIN tb_tipo tipo_computador ON tipo_computador.id = computador.tipo_fk
    JOIN tb_modelo marca_computador ON marca_computador.id = computador.modelo_fk
    JOIN vw_marca_modelo modelo_computador ON modelo_computador.id = computador.modelo_fk
  ORDER BY unidade_computador.unidade , setor_computador.setor , computador.status;
ALTER TABLE vw_computador OWNER TO postgres;

/*
View: Monitor
*/
CREATE OR REPLACE VIEW public.vw_monitor AS 
 SELECT monitor.id,
    monitor.codigo_monitor,
    unidade_monitor.unidade,
    monitor.unidade_fk,
    setor_monitor.setor,
    monitor.setor_fk,
    tipo_monitor.tipo,
    monitor.tipo_fk,
    modelo_monitor.marca,
    marca_monitor.marca_fk,
    modelo_monitor.modelo,
    monitor.modelo_fk,
    monitor.numero_serie,
    monitor.loja,
    monitor.numero_nota,
    monitor.valor,
    monitor.numero_patrimonio,
    monitor.descricao_monitor,
    monitor.netbios,
    monitor.mac,
    monitor.status,
    monitor.url
   FROM tb_monitor monitor
    JOIN tb_unidade unidade_monitor ON unidade_monitor.id = monitor.unidade_fk
    JOIN tb_setor setor_monitor ON setor_monitor.id = monitor.setor_fk
    JOIN tb_tipo tipo_monitor ON tipo_monitor.id = monitor.tipo_fk
    JOIN tb_modelo marca_monitor ON marca_monitor.id = monitor.modelo_fk
    JOIN vw_marca_modelo modelo_monitor ON modelo_monitor.id = monitor.modelo_fk
  ORDER BY unidade_monitor.unidade , setor_monitor.setor , monitor.status;
ALTER TABLE vw_monitor OWNER TO postgres;

/*
View: Equipamento
*/
CREATE OR REPLACE VIEW public.vw_equipamento AS 
 SELECT equipamento.id,
    equipamento.codigo_equipamento,
    unidade_equipamento.unidade,
    equipamento.unidade_fk,
    setor_equipamento.setor,
    equipamento.setor_fk,
    tipo_equipamento.tipo,
    equipamento.tipo_fk,
    modelo_equipamento.marca,
    marca_equipamento.marca_fk,
    modelo_equipamento.modelo,
    equipamento.modelo_fk,
    equipamento.numero_serie,
    equipamento.loja,
    equipamento.numero_nota,
    equipamento.valor,
    equipamento.numero_patrimonio,
    equipamento.netbios,
    equipamento.mac,
    equipamento.descricao_equipamento,
    equipamento.status,
    equipamento.url
   FROM tb_equipamento equipamento
    JOIN tb_unidade unidade_equipamento ON unidade_equipamento.id = equipamento.unidade_fk
    JOIN tb_setor setor_equipamento ON setor_equipamento.id = equipamento.setor_fk
    JOIN tb_tipo tipo_equipamento ON tipo_equipamento.id = equipamento.tipo_fk
    JOIN tb_modelo marca_equipamento ON marca_equipamento.id = equipamento.modelo_fk
    JOIN vw_marca_modelo modelo_equipamento ON modelo_equipamento.id = equipamento.modelo_fk
  ORDER BY unidade_equipamento.unidade , setor_equipamento.setor , equipamento.status;
ALTER TABLE vw_equipamento OWNER TO postgres;

/*
View: Help Desk
*/
CREATE OR REPLACE VIEW public.vw_helpdesk AS 
 SELECT helpdesk.id,
    helpdesk.de_fk,
    usuario_de.nome AS nome_de,
    usuario_de.unidade_fk AS unidade_fk_de,
    unidade_de.unidade AS unidade_de,
    helpdesk.para_fk,
    usuario_para.nome AS nome_para,
    usuario_para.unidade_fk AS unidade_fk_para,
    unidade_para.unidade AS unidade_para,
    helpdesk.data_abertura,
    helpdesk.data_encerrado,
    helpdesk.produto_fk,
    produto.produto,
    helpdesk.resumo,
    helpdesk.descricao,
    helpdesk.prioridade,
    helpdesk.ver,
    helpdesk.status
   FROM tb_helpdesk_cadastro helpdesk
    JOIN tb_usuario usuario_de ON usuario_de.id = helpdesk.de_fk
    JOIN tb_usuario usuario_para ON usuario_para.id = helpdesk.para_fk
    JOIN tb_unidade unidade_de ON unidade_de.id = usuario_de.unidade_fk
    JOIN tb_unidade unidade_para ON unidade_para.id = usuario_para.unidade_fk
    JOIN tb_helpdesk_produto produto ON produto.id = helpdesk.produto_fk
  ORDER BY helpdesk.status , unidade_de.unidade , helpdesk.id;
ALTER TABLE vw_helpdesk OWNER TO postgres;

/*
View: Funcionario Computador
*/
CREATE OR REPLACE VIEW public.vw_funcionario_computador AS 
 SELECT funcionario_computador.id,
    funcionario_computador.funcionario_fk,
    funcionaroio.unidade_fk AS funcionario_unidade_fk,
    funcionaroio.nome,
    funcionario_computador.computador_fk,
    computador.unidade_fk AS computador_unidade_fk,
    computador.codigo_computador,
    computador.numero_serie,
    computador.netbios,
    computador.mac_c,
    computador.mac_w
   FROM tb_funcionario_computador funcionario_computador
    JOIN tb_funcionario funcionaroio ON funcionaroio.id = funcionario_computador.funcionario_fk
    JOIN tb_computador computador ON computador.id = funcionario_computador.computador_fk;
ALTER TABLE vw_funcionario_computador OWNER TO postgres;

/*
View: Funcionario Monitor
*/
CREATE OR REPLACE VIEW public.vw_funcionario_monitor AS 
 SELECT funcionario_monitor.id,
    funcionario_monitor.funcionario_fk,
    funcionaroio.unidade_fk AS funcionario_unidade_fk,
    funcionaroio.nome,
    funcionario_monitor.monitor_fk,
    monitor.unidade_fk AS monitor_unidade_fk,
    monitor.codigo_monitor,
    monitor.numero_serie,
    monitor.netbios,
    monitor.mac
   FROM tb_funcionario_monitor funcionario_monitor
    JOIN tb_funcionario funcionaroio ON funcionaroio.id = funcionario_monitor.funcionario_fk
    JOIN tb_monitor monitor ON monitor.id = funcionario_monitor.monitor_fk;
ALTER TABLE vw_funcionario_monitor OWNER TO postgres;

/*
View: Funcionario Equipaemnto
*/
CREATE OR REPLACE VIEW public.vw_funcionario_equipamento AS 
 SELECT funcionario_equipamento.id,
    funcionario_equipamento.funcionario_fk,
    funcionaroio.unidade_fk AS funcionario_unidade_fk,
    funcionaroio.nome,
    funcionario_equipamento.equipamento_fk,
    equipamento.unidade_fk AS equipamento_unidade_fk,
    equipamento.codigo_equipamento,
    equipamento.numero_serie,
    equipamento.netbios,
    equipamento.mac
   FROM tb_funcionario_equipamento funcionario_equipamento
    JOIN tb_funcionario funcionaroio ON funcionaroio.id = funcionario_equipamento.funcionario_fk
    JOIN tb_equipamento equipamento ON equipamento.id = funcionario_equipamento.equipamento_fk;
ALTER TABLE vw_funcionario_equipamento OWNER TO postgres;

/*
View: Usuario Gerente de Unidade
*/
CREATE OR REPLACE VIEW public.vw_usuario_gerente_unidade AS 
 SELECT usuariogerenteunidade.id,
    usuariogerenteunidade.usuario_fk,
    usuario.unidade_fk AS usuario_unidade_fk,
    usuario.nome,
    usuariogerenteunidade.unidade_fk,
    unidade.unidade,
    usuariogerenteunidade.gerencia
   FROM tb_usuario_gerente_unidade usuariogerenteunidade
    JOIN tb_usuario usuario ON usuario.id = usuariogerenteunidade.usuario_fk
    JOIN tb_unidade unidade ON unidade.id = usuariogerenteunidade.unidade_fk;
ALTER TABLE vw_usuario_gerente_unidade OWNER TO postgres;

/*
View: Relatorio Computador Para Funcionario
*/
CREATE OR REPLACE VIEW public.vw_relatorio_computador_funcionario AS 
 SELECT computador.id,
    computador.unidade_fk,
	setor_computador.setor AS computador_setor,
    computador.setor_fk AS computador_setor_fk,
    tipo_computador.tipo,
    computador.tipo_fk,
    modelo_computador.marca,
    marca_computador.marca_fk,
    modelo_computador.modelo,
    computador.modelo_fk,
    computador.codigo_computador,
    computador.numero_serie,
    computador.netbios,
    computador.mac_c,
    computador.mac_w,
    funcionario.unidade_fk AS funcionario_unidade_fk,
	setor_funcionario.setor AS funcionario_setor,
    funcionario.setor_fk AS funcionario_setor_fk,
    funcionario.funcao,
    funcionario.nome
   FROM tb_computador computador
    LEFT JOIN tb_funcionario_computador funcionario_computador on computador.id = funcionario_computador.computador_fk
    LEFT JOIN tb_funcionario funcionario ON funcionario.id = funcionario_computador.funcionario_fk
	LEFT JOIN tb_setor setor_computador ON setor_computador.id = computador.setor_fk
    LEFT JOIN tb_setor setor_funcionario ON setor_funcionario.id = funcionario.setor_fk
    LEFT JOIN tb_tipo tipo_computador ON tipo_computador.id = computador.tipo_fk
    LEFT JOIN tb_modelo marca_computador ON marca_computador.id = computador.modelo_fk
    LEFT JOIN vw_marca_modelo modelo_computador ON modelo_computador.id = computador.modelo_fk
    WHERE computador.status = 1;
ALTER TABLE vw_relatorio_computador_funcionario OWNER TO postgres;

/*
View: Relatorio Funcionario Para Computador
*/
CREATE OR REPLACE VIEW public.vw_relatorio_funcionario_computador AS 
 SELECT funcionario.id,
    funcionario.unidade_fk,
    setor_funcionario.setor AS funcionario_setor,
    funcionario.setor_fk AS funcionario_setor_fk,
    funcionario.funcao,
    funcionario.nome,
    computador.unidade_fk AS computador_unidade_fk,
	setor_computador.setor AS computador_setor,
    computador.setor_fk AS computador_setor_fk,
    tipo_computador.tipo,
    computador.tipo_fk,
    modelo_computador.marca,
    marca_computador.marca_fk,
    modelo_computador.modelo,
    computador.modelo_fk,
    computador.codigo_computador,
    computador.numero_serie,
    computador.netbios,
    computador.mac_c,
    computador.mac_w
   FROM tb_funcionario funcionario
    LEFT JOIN tb_funcionario_computador funcionario_computador on funcionario.id = funcionario_computador.funcionario_fk
    LEFT JOIN tb_computador computador ON computador.id = funcionario_computador.computador_fk
	LEFT JOIN tb_setor setor_computador ON setor_computador.id = computador.setor_fk
    LEFT JOIN tb_setor setor_funcionario ON setor_funcionario.id = funcionario.setor_fk
    LEFT JOIN tb_tipo tipo_computador ON tipo_computador.id = computador.tipo_fk
    LEFT JOIN tb_modelo marca_computador ON marca_computador.id = computador.modelo_fk
    LEFT JOIN vw_marca_modelo modelo_computador ON modelo_computador.id = computador.modelo_fk
    WHERE funcionario.so_contato = 0 and funcionario.status = 1;
ALTER TABLE vw_relatorio_funcionario_computador OWNER TO postgres;

/*
View: Relatorio Monitor Para Funcionario
*/
CREATE OR REPLACE VIEW public.vw_relatorio_monitor_funcionario AS 
 SELECT monitor.id,
    monitor.unidade_fk,
	setor_monitor.setor AS monitor_setor,
    monitor.setor_fk AS monitor_setor_fk,
    tipo_monitor.tipo,
    monitor.tipo_fk,
    modelo_monitor.marca,
    marca_monitor.marca_fk,
    modelo_monitor.modelo,
    monitor.modelo_fk,
    monitor.codigo_monitor,
    monitor.numero_serie,
    monitor.netbios,
    funcionario.unidade_fk AS funcionario_unidade_fk,
	setor_funcionario.setor AS funcionario_setor,
    funcionario.setor_fk AS funcionario_setor_fk,
    funcionario.funcao,
    funcionario.nome
   FROM tb_monitor monitor
    LEFT JOIN tb_funcionario_monitor funcionario_monitor on monitor.id = funcionario_monitor.monitor_fk
    LEFT JOIN tb_funcionario funcionario ON funcionario.id = funcionario_monitor.funcionario_fk
	LEFT JOIN tb_setor setor_monitor ON setor_monitor.id = monitor.setor_fk
    LEFT JOIN tb_setor setor_funcionario ON setor_funcionario.id = funcionario.setor_fk
    LEFT JOIN tb_tipo tipo_monitor ON tipo_monitor.id = monitor.tipo_fk
    LEFT JOIN tb_modelo marca_monitor ON marca_monitor.id = monitor.modelo_fk
    LEFT JOIN vw_marca_modelo modelo_monitor ON modelo_monitor.id = monitor.modelo_fk
    WHERE monitor.status = 1;
ALTER TABLE vw_relatorio_monitor_funcionario OWNER TO postgres;

/*
View: Relatorio Funcionario Para Monitor
*/
CREATE OR REPLACE VIEW public.vw_relatorio_funcionario_monitor AS 
 SELECT funcionario.id,
    funcionario.unidade_fk,
    setor_funcionario.setor AS funcionario_setor,
    funcionario.setor_fk AS funcionario_setor_fk,
    funcionario.funcao,
    funcionario.nome,
    monitor.unidade_fk AS monitor_unidade_fk,
	setor_monitor.setor AS monitor_setor,
    monitor.setor_fk AS monitor_setor_fk,
    tipo_monitor.tipo,
    monitor.tipo_fk,
    modelo_monitor.marca,
    marca_monitor.marca_fk,
    modelo_monitor.modelo,
    monitor.modelo_fk,
    monitor.codigo_monitor,
    monitor.numero_serie,
    monitor.netbios
   FROM tb_funcionario funcionario
    LEFT JOIN tb_funcionario_monitor funcionario_monitor on funcionario.id = funcionario_monitor.funcionario_fk
    LEFT JOIN tb_monitor monitor ON monitor.id = funcionario_monitor.monitor_fk
	LEFT JOIN tb_setor setor_monitor ON setor_monitor.id = monitor.setor_fk
    LEFT JOIN tb_setor setor_funcionario ON setor_funcionario.id = funcionario.setor_fk
    LEFT JOIN tb_tipo tipo_monitor ON tipo_monitor.id = monitor.tipo_fk
    LEFT JOIN tb_modelo marca_monitor ON marca_monitor.id = monitor.modelo_fk
    LEFT JOIN vw_marca_modelo modelo_monitor ON modelo_monitor.id = monitor.modelo_fk
    WHERE funcionario.so_contato = 0 and funcionario.status = 1;
ALTER TABLE vw_relatorio_funcionario_monitor OWNER TO postgres;

/*
View: Relatorio equipamento Para Funcionario
*/
CREATE OR REPLACE VIEW public.vw_relatorio_equipamento_funcionario AS 
 SELECT equipamento.id,
    equipamento.unidade_fk,
	setor_equipamento.setor AS equipamento_setor,
    equipamento.setor_fk AS equipamento_setor_fk,
    tipo_equipamento.tipo,
    equipamento.tipo_fk,
    modelo_equipamento.marca,
    marca_equipamento.marca_fk,
    modelo_equipamento.modelo,
    equipamento.modelo_fk,
    equipamento.codigo_equipamento,
    equipamento.numero_serie,
    equipamento.netbios,
    funcionario.unidade_fk AS funcionario_unidade_fk,
	setor_funcionario.setor AS funcionario_setor,
    funcionario.setor_fk AS funcionario_setor_fk,
    funcionario.funcao,
    funcionario.nome
   FROM tb_equipamento equipamento
    LEFT JOIN tb_funcionario_equipamento funcionario_equipamento on equipamento.id = funcionario_equipamento.equipamento_fk
    LEFT JOIN tb_funcionario funcionario ON funcionario.id = funcionario_equipamento.funcionario_fk
	LEFT JOIN tb_setor setor_equipamento ON setor_equipamento.id = equipamento.setor_fk
    LEFT JOIN tb_setor setor_funcionario ON setor_funcionario.id = funcionario.setor_fk
    LEFT JOIN tb_tipo tipo_equipamento ON tipo_equipamento.id = equipamento.tipo_fk
    LEFT JOIN tb_modelo marca_equipamento ON marca_equipamento.id = equipamento.modelo_fk
    LEFT JOIN vw_marca_modelo modelo_equipamento ON modelo_equipamento.id = equipamento.modelo_fk
    WHERE equipamento.status = 1;
ALTER TABLE vw_relatorio_equipamento_funcionario OWNER TO postgres;

/*
View: Relatorio Funcionario Para equipamento
*/
CREATE OR REPLACE VIEW public.vw_relatorio_funcionario_equipamento AS 
 SELECT funcionario.id,
    funcionario.unidade_fk,
    setor_funcionario.setor AS funcionario_setor,
    funcionario.setor_fk AS funcionario_setor_fk,
    funcionario.funcao,
    funcionario.nome,
    equipamento.unidade_fk AS equipamento_unidade_fk,
	setor_equipamento.setor AS equipamento_setor,
    equipamento.setor_fk AS equipamento_setor_fk,
    tipo_equipamento.tipo,
    equipamento.tipo_fk,
    modelo_equipamento.marca,
    marca_equipamento.marca_fk,
    modelo_equipamento.modelo,
    equipamento.modelo_fk,
    equipamento.codigo_equipamento,
    equipamento.numero_serie,
    equipamento.netbios
   FROM tb_funcionario funcionario
    LEFT JOIN tb_funcionario_equipamento funcionario_equipamento on funcionario.id = funcionario_equipamento.funcionario_fk
    LEFT JOIN tb_equipamento equipamento ON equipamento.id = funcionario_equipamento.equipamento_fk
	LEFT JOIN tb_setor setor_equipamento ON setor_equipamento.id = equipamento.setor_fk
    LEFT JOIN tb_setor setor_funcionario ON setor_funcionario.id = funcionario.setor_fk
    LEFT JOIN tb_tipo tipo_equipamento ON tipo_equipamento.id = equipamento.tipo_fk
    LEFT JOIN tb_modelo marca_equipamento ON marca_equipamento.id = equipamento.modelo_fk
    LEFT JOIN vw_marca_modelo modelo_equipamento ON modelo_equipamento.id = equipamento.modelo_fk
    WHERE funcionario.so_contato = 0 and funcionario.status = 1;
ALTER TABLE vw_relatorio_funcionario_equipamento OWNER TO postgres;

/*
Funcao: Remove Acento
*/
CREATE OR REPLACE FUNCTION fc_remove_acento(text)
  RETURNS text AS
$BODY$

SELECT TRANSLATE($1,'áàãâäÁÀÃÂÄéèêëÉÈÊËíìîïÍÌÎÏóòõôöÓÒÕÔÖúùûüÚÙÛÜñÑçÇÿýÝ','aaaaaAAAAAeeeeEEEEiiiiIIIIoooooOOOOOuuuuUUUUnNcCyyY')

$BODY$
  LANGUAGE sql IMMUTABLE STRICT COST 100;
ALTER FUNCTION fc_remove_acento(text) OWNER TO postgres;

/*
Funcao: Formatar
*/
CREATE OR REPLACE FUNCTION fc_formatar(character varying, character varying)
  RETURNS character varying AS
$BODY$
DECLARE

Retorno VARCHAR;

BEGIN
  Retorno = null;
  IF $2 = 'cnpj' THEN
    Retorno = SUBSTR($1, 1, 2) || '.' || SUBSTR($1, 3, 3) || '.' || SUBSTR($1, 6, 3) || '/' || SUBSTR($1, 9, 4) || '-' || SUBSTR($1, 13);
  END IF;
  IF $2 = 'cpf' THEN
    Retorno = SUBSTR($1, 1, 3) || '.' || SUBSTR($1, 4, 3) || '.' || SUBSTR($1, 7, 3) || '-' || SUBSTR($1, 10);
  END IF;
  IF $2 = 'cep' THEN
    Retorno = SUBSTRING($1, 1, 2) || '.' || SUBSTRING($1, 3, 3) || '-' || SUBSTRING($1, 6);
  END IF;
  IF $2 = 'fone' THEN
    IF char_length($1)<=10 THEN
        Retorno = '('|| SUBSTRING($1, 1, 2) || ')' || SUBSTRING($1, 3, 4) || '-' || SUBSTRING($1, 7);
    ELSE
        Retorno = '('|| SUBSTRING($1, 1, 2) || ')' || SUBSTRING($1, 3, 5) || '-' || SUBSTRING($1, 8);
    END IF;
 END IF;
RETURN Retorno;

END;
$BODY$
  LANGUAGE plpgsql STABLE COST 100;
ALTER FUNCTION fc_formatar(character varying, character varying) OWNER TO postgres;

/*
Tabela: Log de Auditoria
*/
CREATE SEQUENCE tb_log_auditoria_seq
    INCREMENT 1
    START 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.tb_log_auditoria_seq
    OWNER TO sgt;

COMMENT ON SEQUENCE public.tb_log_auditoria_seq
    IS 'Sequencia de numeracao do ID do Log de Auditoria';

CREATE TABLE tb_log_auditoria (
    id bigint NOT NULL DEFAULT nextval(('tb_log_auditoria_seq'::text)::regclass),
    acao VARCHAR(10),
    usuario VARCHAR(50),
    data_hora TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    detalhes VARCHAR(120),
	query_executada TEXT,
	CONSTRAINT pk_tb_tb_log_auditoria PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tb_log_auditoria
    OWNER to sgt;

/*
Funcao: Log Auditoria
*/
CREATE OR REPLACE FUNCTION log_auditoria()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO tb_log_auditoria (acao, usuario, data_hora, detalhes, query_executada)
        VALUES ('INSERT',
				current_user,
				current_timestamp,
				TG_TABLE_NAME || ' inserido ',
				TG_OP || ' (' || array_to_string(ARRAY(SELECT column_name::text FROM information_schema.columns WHERE table_name = TG_TABLE_NAME), ', ') || ') ' || TG_TABLE_NAME || ' VALUES (' || NEW::text || ')'
			   );
    ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO tb_log_auditoria (acao, usuario, data_hora, detalhes, query_executada)
		VALUES ('UPDATE',
				current_user,
				current_timestamp,
				TG_TABLE_NAME || ' atualizado ',
				TG_OP || ' (' || array_to_string(ARRAY(SELECT column_name::text FROM information_schema.columns WHERE table_name = TG_TABLE_NAME), ', ') || ') ' || TG_TABLE_NAME || ' SET ' ||  OLD::text || ' > ' || NEW::text
			   );
    ELSIF (TG_OP = 'DELETE') THEN
        INSERT INTO tb_log_auditoria (acao, usuario, data_hora, detalhes, query_executada)
        VALUES ('DELETE',
				current_user,
				current_timestamp,
				TG_TABLE_NAME || ' deletado ',
				TG_OP || ' (' || array_to_string(ARRAY(SELECT column_name::text FROM information_schema.columns WHERE table_name = TG_TABLE_NAME), ', ') || ') ' || TG_TABLE_NAME || ' WHERE (' || OLD::text || ')'
			   );
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

/*
Trigger: Tabela Mapa IPv4
*/
CREATE TRIGGER tb_mapa_ip_v4_trigger_log
AFTER INSERT OR UPDATE OR DELETE ON tb_mapa_ip_v4
FOR EACH ROW EXECUTE FUNCTION log_auditoria();

/*
Trigger: Tabela Senha
*/
CREATE TRIGGER tb_senha_trigger_log
AFTER INSERT OR UPDATE OR DELETE ON tb_senha
FOR EACH ROW EXECUTE FUNCTION log_auditoria();

/*
Trigger: Tabela Usuário
*/
CREATE TRIGGER tb_usuario_trigger_log
AFTER INSERT OR UPDATE OR DELETE ON tb_usuario
FOR EACH ROW EXECUTE FUNCTION log_auditoria();

/*
Trigger: Tabela VLAN
*/
CREATE TRIGGER tb_vlan_trigger_log
AFTER INSERT OR UPDATE OR DELETE ON tb_vlan
FOR EACH ROW EXECUTE FUNCTION log_auditoria();

/*
Trigger: Tabela VoIP
*/
CREATE TRIGGER tb_voip_trigger_log
AFTER INSERT OR UPDATE OR DELETE ON tb_voip
FOR EACH ROW EXECUTE FUNCTION log_auditoria();