CREATE DATABASE `bd_sgt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `bd_sgt`;

CREATE TABLE `tb_sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(1024) NOT NULL,
  `url` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_sistema_id` (`id`),
  KEY `idx_sistema_nome` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_configuracao` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `configuracao` varchar(20) DEFAULT NULL,
  `variavel1` varchar(100) NOT NULL,
  `variavel2` varchar(100) NOT NULL,
  `variavel3` varchar(100) NOT NULL,
  `variavel4` varchar(100) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `ordem` int(11),
  `ver` int(4) NOT NULL,
  `titulo_variavel1` varchar(40) DEFAULT NULL,
  `titulo_variavel2` varchar(40) DEFAULT NULL,
  `titulo_variavel3` varchar(40) DEFAULT NULL,
  `titulo_variavel4` varchar(40) DEFAULT NULL,
  `ativo_variavel1` int(4) NULL,
  `ativo_variavel2` int(4) NULL,
  `ativo_variavel3` int(4) NULL,
  `ativo_variavel4` int(4) NULL,
  `tipo_variavel1` int(4) NULL,
  `tipo_variavel2` int(4) NULL,
  `tipo_variavel3` int(4) NULL,
  `tipo_variavel4` int(4) NULL,
  PRIMARY KEY (`id`),
  KEY `idx_configuracao_id` (`id`),
  KEY `idx_configuracao_configuracao` (`configuracao`),
  KEY `idx_configuracao_var1` (`variavel1`),
  KEY `idx_configuracao_var2` (`variavel2`),
  KEY `idx_configuracao_var3` (`variavel3`),
  KEY `idx_configuracao_var4` (`variavel4`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(1024) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `port` int(15) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `encryption` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_id` (`id`),
  KEY `idx_email_host` (`host`),
  KEY `idx_email_username` (`username`),
  KEY `idx_email_password` (`password`),
  KEY `idx_email_port` (`port`),
  KEY `idx_email_nome` (`nome`),
  KEY `idx_email_encryption` (`encryption`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(1024) NOT NULL,
  `descricao` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_perfil_id` (`id`),
  KEY `idx_perfil_nome` (`nome`),
  KEY `idx_perfil_descricao` (`descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_unidade` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `unidade` varchar(40) NOT NULL,
  `dominio` varchar(60),
  `ddd` int(3),
  `nivel` int(6) NOT NULL,
  `unidade_pai_fk` int(6),
  `nome_patrimonio1` varchar(80),
  `email_patrimonio1` varchar(100),
  `nome_patrimonio2` varchar(80),
  `email_patrimonio2` varchar(100),
  `descricao_unidade` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_unidade_id` (`id`),
  KEY `idx_unidade_unidade` (`unidade`),
  KEY `idx_unidade_ddd` (`ddd`),
  KEY `idx_unidade_nome_pt1` (`nome_patrimonio1`),
  KEY `idx_unidade_nome_pt2` (`nome_patrimonio2`),
  KEY `idx_unidade_email_pt1` (`email_patrimonio1`),
  KEY `idx_unidade_email_pt2` (`email_patrimonio2`),
  CONSTRAINT fk_tb_unidade
    FOREIGN KEY (unidade_pai_fk)
    REFERENCES tb_unidade(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_medida` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `simbolo` varchar(50) NOT NULL,
  `medida` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_medida` (`medida`),
  KEY `idx_medida_id` (`id`),
  KEY `idx_medida_simbolo` (`simbolo`),
  KEY `idx_medida_medida` (`medida`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_setor` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `setor` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_setor` (`setor`),
  KEY `idx_setor_id` (`id`),
  KEY `idx_setor_setor` (`setor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_local` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `local` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descricao` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_local` (`local`),
  KEY `idx_local_id` (`id`),
  KEY `idx_local_local` (`local`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_tipo` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `computador` tinyint(4) NOT NULL,
  `equipamento` tinyint(4) NOT NULL,
  `monitor` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_tipo` (`tipo`),
  KEY `idx_tipo_id` (`id`),
  KEY `idx_tipo_tipo` (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_marca` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `marca` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `computador` tinyint(4) NOT NULL,
  `monitor` tinyint(4) NOT NULL,
  `equipamento` tinyint(4) NOT NULL,
  `processador` tinyint(4) NOT NULL,
  `placa_video` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_marca` (`marca`),
  KEY `idx_marca_id` (`id`),
  KEY `idx_marca_marca` (`marca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_modelo` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `marca_fk` int(6) NOT NULL,
  `modelo` varchar(80) NOT NULL,
  `tipo` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_modelo` (`marca_fk`, `modelo`),
  KEY `idx_modelo_id` (`id`),
  KEY `idx_modelo_marca` (`marca_fk`),
  KEY `idx_modelo_modelo` (`modelo`),
  KEY `idx_modelo_tipo` (`tipo`),
  CONSTRAINT `fk_modelo_marca` FOREIGN KEY (`marca_fk`) REFERENCES `tb_marca` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_helpdesk_produto` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `produto` varchar(90) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_helpdesk_produto` (`produto`),
  KEY `idx_helpdesk_produto_id` (`id`),
  KEY `idx_helpdesk_produto_produto` (`produto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_usuario` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `senha` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nome` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `unidade_fk` int(6) NOT NULL,
  `unidade_temp_fk` int(6) NOT NULL,
  `setor_temp_fk` int(6),
  `local_temp_fk` int(6),
  `tipo_temp_fk` int(6),
  `marca_temp_fk` int(6),
  `modelo_temp_fk` int(6),
  `produto_temp_fk` int(6),
  `patch_panel_temp` int(6),
  `switch_temp` int(6),
  `hd_unidade_temp_fk` int(6),
  `hd_filtro` int(6),
  `rede_temp` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `grupo_temp` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `telefone` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci,
  `senha_hash` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `senha_reset_token` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_usuario` (`usuario`),
  KEY `idx_usuario_id` (`id`),
  KEY `idx_usuario_usuario` (`usuario`),
  KEY `idx_usuario_nome` (`nome`),
  KEY `idx_usuario_email` (`email`),
  KEY `idx_usuario_local_temp` (`local_temp_fk`),
  KEY `idx_usuario_marca_temp` (`marca_temp_fk`),
  KEY `idx_usuario_modelo_temp` (`modelo_temp_fk`),
  KEY `idx_usuario_produto_temp` (`produto_temp_fk`),
  KEY `idx_usuario_setor_temp` (`setor_temp_fk`),
  KEY `idx_usuario_tipo_temp` (`tipo_temp_fk`),
  KEY `idx_usuario_unidade_temp` (`unidade_temp_fk`),
  KEY `idx_usuario_hd_unidade_temp` (`hd_unidade_temp_fk`),
  KEY `idx_usuario_hd_filtro` (`filtro`),
  KEY `idx_usuario_unidade` (`unidade_fk`),
  KEY `idx_usuario_patch_panel_temp` (`patch_panel_temp`),
  KEY `idx_usuario_switch_temp` (`switch_temp`),
  CONSTRAINT `fk_usuario_local_temp`
    FOREIGN KEY (`local_temp_fk`)
    REFERENCES `tb_local` (`id`),
  CONSTRAINT `fk_usuario_marca_temp`
    FOREIGN KEY (`marca_temp_fk`)
    REFERENCES `tb_marca` (`id`),
  CONSTRAINT `fk_usuario_modelo_temp`
    FOREIGN KEY (`modelo_temp_fk`)
    REFERENCES `tb_modelo` (`id`),
  CONSTRAINT `fk_usuario_produto_temp`
    FOREIGN KEY (`produto_temp_fk`)
    REFERENCES `tb_helpdesk_produto` (`id`),
  CONSTRAINT `fk_usuario_setor_temp`
    FOREIGN KEY (`setor_temp_fk`)
    REFERENCES `tb_setor` (`id`),
  CONSTRAINT `fk_usuario_unidade`
    FOREIGN KEY (`unidade_fk`)
    REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_usuario_unidade_temp`
    FOREIGN KEY (`unidade_temp_fk`)
    REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_usuario_hd_unidade_temp`
    FOREIGN KEY (`hd_unidade_temp_fk`)
    REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_usuario_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_fk` int(11) NOT NULL,
  `perfil_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_usuario_perfil` (`usuario_fk`),
  KEY `idx_usuario_perfil_id` (`id`),
  KEY `idx_usuario_perfil_usuario_fk` (`usuario_fk`),
  KEY `idx_usuario_perfil_perfil_fk` (`perfil_fk`),
  CONSTRAINT `fk_usuario_perfil_usuario` FOREIGN KEY (`usuario_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_usuario_perfil_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_fk` int(11) NOT NULL,
  `nivel` int(11) NOT NULL,
  `menu_pai_fk` int(11) DEFAULT NULL,
  `titulo` varchar(120) NOT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_menu_id` (`id`),
  KEY `idx_menu_sistema_fk` (`sistema_fk`),
  KEY `idx_menu_nivel` (`nivel`),
  KEY `idx_menu_menu_pai_fk` (`menu_pai_fk`),
  KEY `idx_menu_titulo` (`titulo`),
  KEY `idx_menu_controller` (`controller`),
  KEY `idx_menu_icon` (`icon`),
  CONSTRAINT `fk_menu_menu_pai` FOREIGN KEY (`menu_pai_fk`) REFERENCES `tb_menu` (`id`),
  CONSTRAINT `fk_menu_sistema` FOREIGN KEY (`sistema_fk`) REFERENCES `tb_sistema` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_menu_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_fk` int(11) NOT NULL,
  `perfil_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_menu_perfil_id` (`id`),
  KEY `idx_menu_perfil_menu` (`menu_fk`),
  KEY `idx_menu_perfil_perfil` (`perfil_fk`),
  CONSTRAINT `fk_menu_perfil_menu` FOREIGN KEY (`menu_fk`) REFERENCES `tb_menu` (`id`),
  CONSTRAINT `fk_menu_perfil_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_restricao_menu_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_fk` int(11) NOT NULL,
  `menu_fk` int(11) NOT NULL,
  `restricao` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_restricao_menu_perfil_id` (`id`),
  KEY `idx_restricao_menu_perfil_perfil` (`perfil_fk`),
  KEY `idx_restricao_menu_perfil_menu` (`menu_fk`),
  KEY `idx_restricao_menu_perfil_restricao` (`restricao`),
  CONSTRAINT `fk_restricao_menu_perfil_menu` FOREIGN KEY (`menu_fk`) REFERENCES `tb_menu` (`id`),
  CONSTRAINT `fk_restricao_menu_perfil_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_usuario_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_fk` int(11) NOT NULL,
  `unidade_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_usuario_unidade` (`usuario_fk`,`unidade_fk`),
  KEY `idx_usuario_fk` (`usuario_fk`),
  KEY `idx_unidade_fk` (`unidade_fk`),
  CONSTRAINT `fk_usuario_unidade_usuario` FOREIGN KEY (`usuario_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_usuario_unidade_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_helpdesk_usuario_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_fk` int(11) NOT NULL,
  `unidade_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_helpdesk_usuario_unidade` (`usuario_fk`,`unidade_fk`),
  KEY `idx_usuario_fk` (`usuario_fk`),
  KEY `idx_unidade_fk` (`unidade_fk`),
  CONSTRAINT `fk_helpdesk_usuario_unidade_usuario` FOREIGN KEY (`usuario_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_helpdesk_usuario_unidade_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_placa_video` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `marca_fk` int(6) NOT NULL,
  `modelo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descricao_placa_video` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_placa_video` (`marca_fk`,`modelo`),
  KEY `idx_placa_video_id` (`id`),
  KEY `idx_placa_video_marca` (`marca_fk`),
  KEY `idx_placa_video_modelo` (`modelo`),
  KEY `idx_placa_video_descricao_placa_video` (`descricao_placa_video`),
  CONSTRAINT `fk_placa_video_marca` FOREIGN KEY (`marca_fk`) REFERENCES `tb_marca` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_processador` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `marca_fk` int(6) NOT NULL,
  `processador` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descricao_processador` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_processador` (`marca_fk`,`processador`),
  KEY `idx_processador_id` (`id`),
  KEY `idx_processador_marca` (`marca_fk`),
  KEY `idx_processador_processador` (`processador`),
  KEY `idx_processador_descricao_processador` (`descricao_processador`),
  CONSTRAINT `fk_processador_marca` FOREIGN KEY (`marca_fk`) REFERENCES `tb_marca` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_ram_clok` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `clok` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_ram_clok` (`clok`),
  KEY `idx_ram_clok_id` (`id`),
  KEY `idx_ram_clok_clok` (`clok`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_tipo_hd` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_tipo_hd` (`tipo`),
  KEY `idx_tipo_hd_id` (`id`),
  KEY `idx_tipo_hd_tipo` (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_office` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `office` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_office` (`office`),
  KEY `idx_office_id` (`id`),
  KEY `idx_office_office` (`office`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_sistema_operacional` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `sistema` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_sistema_operacional` (`sistema`),
  KEY `idx_sistema_id` (`id`),
  KEY `idx_sistema_sistema` (`sistema`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_funcionario` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `setor_fk` int(6) NOT NULL,
  `funcao` varchar(50),
  `nome` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email_1` varchar(100),
  `email_2` varchar(100),
  `telefone` varchar(15),
  `celular` varchar(15),
  `so_contato` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_funcionarion` (`unidade_fk`, `nome`),
  KEY `idx_funcionario_id` (`id`),
  KEY `idx_funcionario_unidade` (`unidade_fk`),
  KEY `idx_funcionario_setor` (`setor_fk`),
  KEY `idx_funcionario_nome` (`nome`),
  CONSTRAINT `fk_funcionario_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_funcionario_setor` FOREIGN KEY (`setor_fk`) REFERENCES `tb_setor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_vlan` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(11) NOT NULL,
  `id_vlan` int(6) NOT NULL,
  `vlan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descricao` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_funcionarion` (`unidade_fk`, `id_vlan`),
  KEY `idx_vlan_id` (`id`),
  KEY `idx_unidade_fk` (`unidade_fk`),
  KEY `idx_vlan_id_vlan` (`id_vlan`),
  KEY `idx_vlan_vlan` (`vlan`),
  CONSTRAINT `fk_vlan_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_chave` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `perfil_fk` int(6) NOT NULL,
  `produto` varchar(80) NOT NULL,
  `chave` varchar(120) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_chave` (`unidade_fk`, `produto`, `chave`),
  KEY `idx_chave_id` (`id`),
  KEY `idx_chave_unindade` (`unidade_fk`),
  KEY `idx_chave_perfil` (`perfil_fk`),
  KEY `idx_chave_produto` (`produto`),
  KEY `idx_chave_chave` (`chave`),
  CONSTRAINT `fk_chave_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_chave_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_senha` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `perfil_fk` int(6) NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `senha` varchar(120) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_senha` (`unidade_fk`, `usuario`, `senha`, `descricao`),
  KEY `idx_senha_id` (`id`),
  KEY `idx_senha_unindade` (`unidade_fk`),
  KEY `idx_senha_perfil` (`perfil_fk`),
  KEY `idx_senha_usuario` (`usuario`),
  KEY `idx_senha_senha` (`senha`),
  KEY `idx_senha_descricao` (`descricao`),
  CONSTRAINT `fk_senha_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_senha_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_passo_passo` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `perfil_fk` int(6) NOT NULL,
  `titulo` varchar(60) NOT NULL,
  `descricao` varchar(120) NOT NULL,
  `passo_passo` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_senha` (`unidade_fk`, `titulo`, `descricao`),
  KEY `idx_passo_passo_id` (`id`),
  KEY `idx_passo_passo_unindade` (`unidade_fk`),
  KEY `idx_passo_passo_perfil` (`perfil_fk`),
  KEY `idx_passo_passo_titulo` (`titulo`),
  KEY `idx_passo_passo_descricao` (`descricao`),
  CONSTRAINT `fk_passo_passo_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_passo_passo_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_mapa_ip_v4` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `rede` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(17) NOT NULL,
  `cidr` int(2) NOT NULL,
  `grupo` varchar(80) NOT NULL,
  `mac` varchar(17) DEFAULT NULL,
  `descricao` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_mapa_ip_v4` (`unidade_fk`,`rede`,`ip`,`mac`),
  KEY `idx_mapa_ip_v4_id` (`id`),
  KEY `idx_mapa_ip_v4_unindade` (`unidade_fk`),
  KEY `idx_mapa_ip_v4_rede` (`rede`),
  KEY `idx_mapa_ip_v4_ip` (`ip`),
  KEY `idx_mapa_ip_v4_grupo` (`grupo`),
  KEY `idx_mapa_ip_v4_mac` (`mac`),
  CONSTRAINT `fk_mapa_ip_v4_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_monitor` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `codigo_monitor` varchar(7) NOT NULL,
  `unidade_fk` int(6) NOT NULL,
  `setor_fk` int(6) DEFAULT NULL,
  `tipo_fk` int(6) NOT NULL,
  `modelo_fk` int(6) NOT NULL,
  `numero_serie` varchar(80) NOT NULL,
  `data_compra` date NOT NULL,
  `garantia` date NOT NULL,
  `loja` varchar(80) NOT NULL,
  `numero_nota` varchar(40) NOT NULL,
  `valor` double NOT NULL,
  `numero_patrimonio` varchar(40) NOT NULL,
  `netbios` varchar(30) DEFAULT NULL,
  `mac` varchar(17) DEFAULT NULL,
  `descricao_monitor` text,
  `status` tinyint(4) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_monitor` (`codigo_monitor`,`numero_patrimonio`),
  KEY `idx_monitor_id` (`id`),
  KEY `idx_monitor_codigo_monitor` (`codigo_monitor`),
  KEY `idx_monitor_unidade` (`unidade_fk`),
  KEY `idx_monitor_setor` (`setor_fk`),
  KEY `idx_monitor_tipo` (`tipo_fk`),
  KEY `idx_monitor_modelo` (`modelo_fk`),
  KEY `idx_monitor_numero_numero_serie` (`numero_serie`),
  KEY `idx_monitor_numero_patrimonio` (`numero_patrimonio`),
  KEY `idx_monitor_numero_netbios` (`netbios`),
  KEY `idx_monitor_numero_mac` (`mac`),
  CONSTRAINT `fk_monitor_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_monitor_setor` FOREIGN KEY (`setor_fk`) REFERENCES `tb_setor` (`id`),
  CONSTRAINT `fk_monitor_tipo` FOREIGN KEY (`tipo_fk`) REFERENCES `tb_tipo` (`id`),
  CONSTRAINT `fk_monitor_modelo` FOREIGN KEY (`modelo_fk`) REFERENCES `tb_modelo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_computador` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `codigo_computador` varchar(7) NOT NULL,
  `unidade_fk` int(6) NOT NULL,
  `setor_fk` int(6) DEFAULT NULL,
  `tipo_fk` int(6) NOT NULL,
  `modelo_fk` int(6) NOT NULL,
  `numero_serie` varchar(80) NOT NULL,
  `data_compra` date NOT NULL,
  `garantia` date NOT NULL,
  `placa_mae` varchar(80) NOT NULL,
  `processador_fk` int(6) NOT NULL,
  `numero_processador` int(4) NOT NULL,
  `ram` float NOT NULL,
  `ram_medida_fk` int(6) NOT NULL,
  `ram_clok_fk` int(6) NOT NULL,
  `hd` float NOT NULL,
  `hd_medida_fk` int(6) NOT NULL,
  `tipo_hd_fk` int(6) NOT NULL,
  `descricao_hd` varchar(120) DEFAULT NULL,
  `loja` varchar(80) DEFAULT NULL,
  `numero_nota` int(40) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `numero_patrimonio` varchar(40) NOT NULL,
  `numero_lacre` varchar(20) DEFAULT NULL,
  `netbios` varchar(30) DEFAULT NULL,
  `mac_c` varchar(17) DEFAULT NULL,
  `mac_w` varchar(17) DEFAULT NULL,
  `sistema_operacional_fk` int(6) DEFAULT NULL,
  `office_fk` int(6) DEFAULT NULL,
  `placa_video_fk` int(6) DEFAULT NULL,
  `numero_palca_video` int(4) DEFAULT NULL,
  `codigo_monitor1_fk` int(6) DEFAULT NULL,
  `codigo_monitor2_fk` int(6) DEFAULT NULL,
  `descricao_computador` text,
  `status` tinyint(4) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_computador` (`codigo_computador`,`numero_patrimonio`,`codigo_monitor1_fk`,`codigo_monitor2_fk`),
  KEY `idx_computador_id` (`id`),
  KEY `idx_computador_unidade` (`unidade_fk`),
  KEY `idx_computador_setor` (`setor_fk`),
  KEY `idx_computador_tipo` (`tipo_fk`),
  KEY `idx_computador_modelo` (`modelo_fk`),
  KEY `idx_computador_processador` (`processador_fk`),
  KEY `idx_computador_sistema` (`sistema_operacional_fk`),
  KEY `idx_computador_office` (`office_fk`),
  KEY `idx_computador_ram_clok` (`ram_clok_fk`),
  KEY `idx_computador_ram_medida` (`ram_medida_fk`),
  KEY `idx_computador_tipo_hd` (`tipo_hd_fk`),
  KEY `idx_computador_hd_medida` (`hd_medida_fk`),
  KEY `idx_computador_placa_video` (`placa_video_fk`),
  KEY `idx_computador_monitor1` (`codigo_monitor1_fk`),
  KEY `idx_computador_moniotr2` (`codigo_monitor2_fk`),
  KEY `idx_computador_numero_numero_serie` (`numero_serie`),
  KEY `idx_computador_numero_patrimonio` (`numero_patrimonio`),
  KEY `idx_computador_netbios` (`netbios`),
  KEY `idx_computador_mac_c` (`mac_c`),
  KEY `idx_computador_mac_w` (`mac_w`),
  CONSTRAINT `fk_computador_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_computador_setor` FOREIGN KEY (`setor_fk`) REFERENCES `tb_setor` (`id`),
  CONSTRAINT `fk_computador_tipo` FOREIGN KEY (`tipo_fk`) REFERENCES `tb_tipo` (`id`),
  CONSTRAINT `fk_computador_modelo` FOREIGN KEY (`modelo_fk`) REFERENCES `tb_modelo` (`id`),
  CONSTRAINT `fk_computador_moniotr2` FOREIGN KEY (`codigo_monitor2_fk`) REFERENCES `tb_monitor` (`id`),
  CONSTRAINT `fk_computador_monitor1` FOREIGN KEY (`codigo_monitor1_fk`) REFERENCES `tb_monitor` (`id`),
  CONSTRAINT `fk_computador_office` FOREIGN KEY (`office_fk`) REFERENCES `tb_office` (`id`),
  CONSTRAINT `fk_computador_placa_video` FOREIGN KEY (`placa_video_fk`) REFERENCES `tb_placa_video` (`id`),
  CONSTRAINT `fk_computador_processador` FOREIGN KEY (`processador_fk`) REFERENCES `tb_processador` (`id`),
  CONSTRAINT `fk_computador_ram_clok` FOREIGN KEY (`ram_clok_fk`) REFERENCES `tb_ram_clok` (`id`),
  CONSTRAINT `fk_computador_ram_medida` FOREIGN KEY (`ram_medida_fk`) REFERENCES `tb_medida` (`id`),
  CONSTRAINT `fk_computador_sistema_operacional` FOREIGN KEY (`sistema_operacional_fk`) REFERENCES `tb_sistema_operacional` (`id`),
  CONSTRAINT `fk_computador_tipo_hd` FOREIGN KEY (`tipo_hd_fk`) REFERENCES `tb_tipo_hd` (`id`),
  CONSTRAINT `fk_computador_hd_medida` FOREIGN KEY (`hd_medida_fk`) REFERENCES `tb_medida` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_equipamento` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `codigo_equipamento` varchar(7) NOT NULL,
  `unidade_fk` int(6) NOT NULL,
  `setor_fk` int(6) DEFAULT NULL,
  `tipo_fk` int(6) NOT NULL,
  `modelo_fk` int(6) NOT NULL,
  `numero_serie` varchar(80) NOT NULL,
  `data_compra` date NOT NULL,
  `garantia` date NOT NULL,
  `loja` varchar(80) NOT NULL,
  `numero_nota` varchar(40) NOT NULL,
  `valor` double NOT NULL,
  `numero_patrimonio` varchar(40) NOT NULL,
  `netbios` varchar(30) DEFAULT NULL,
  `mac` varchar(17) NOT NULL,
  `descricao_equipamento` text,
  `status` tinyint(4) NOT NULL,
  `url` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_equipamento` (`codigo_equipamento`,`numero_patrimonio`),
  KEY `idx_equipamento_id` (`id`),
  KEY `idx_equipamento_codigo_equipamento` (`codigo_equipamento`),
  KEY `idx_equipamento_unidade` (`unidade_fk`),
  KEY `idx_equipamento_setor` (`setor_fk`),
  KEY `idx_equipamento_tipo` (`tipo_fk`),
  KEY `idx_equipamento_modelo` (`modelo_fk`),
  KEY `idx_equipamento_numero_patrimonio` (`numero_patrimonio`),
  KEY `idx_equipamento_netbios` (`netbios`),
  KEY `idx_equipamento_mac` (`mac`),
  CONSTRAINT `fk_equipamento_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_equipamento_setor` FOREIGN KEY (`setor_fk`) REFERENCES `tb_setor` (`id`),
  CONSTRAINT `fk_equipamento_tipo` FOREIGN KEY (`tipo_fk`) REFERENCES `tb_tipo` (`id`),
  CONSTRAINT `fk_equipamento_modelo` FOREIGN KEY (`modelo_fk`) REFERENCES `tb_modelo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_voip_pin` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `funcionario_fk` int(6) NOT NULL,
  `pin` varchar(10) NOT NULL,
  `particular` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_voip_pin` (`unidade_fk`,`pin`),
  KEY `idx_voip_pin_id` (`id`),
  KEY `idx_voip_pin_unindade` (`unidade_fk`),
  KEY `idx_voip_pin_funcionario` (`funcionario_fk`),
  KEY `idx_voip_pin_pin` (`pin`),
  CONSTRAINT `fk_voip_pin_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_voip_pin_func` FOREIGN KEY (`funcionario_fk`) REFERENCES `tb_funcionario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_voip_grupo_captura` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `grupo` int(3) NOT NULL,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_voip_grupo_captura` (`unidade_fk`,`grupo`),
  KEY `idx_voip_grupo_captura_id` (`id`),
  KEY `idx_voip_grupo_captura_unindade` (`unidade_fk`),
  KEY `idx_voip_grupo_captura_grupo` (`grupo`),
  KEY `idx_voip_grupo_captura_nome` (`nome`),
  CONSTRAINT `fk_voip_grupo_captura_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_voip` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `setor_fk` int(6) NOT NULL,
  `voip` int(6) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `tempo_toque` int(6) NOT NULL,
  `transferencia_automatica_fk` int(6) DEFAULT NULL,
  `toque_simultaneo` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `grupo_fk` int(6) DEFAULT NULL,
  `equipamento_fk` int(6) DEFAULT NULL,
  `funcionario_fk` int(6) DEFAULT NULL,
  `informacao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_voip_id` (`id`),
  KEY `idx_voip_unidade` (`unidade_fk`),
  KEY `idx_voip_setor` (`setor_fk`),
  KEY `idx_voip_voip` (`voip`),
  KEY `idx_voip_transferencia_automatica` (`transferencia_automatica_fk`),
  KEY `idx_voip_grupo` (`grupo_fk`),
  KEY `idx_voip_equipamento` (`equipamento_fk`),
  KEY `idx_voip_funcionario` (`funcionario_fk`),
  CONSTRAINT `fk_voip_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_voip_setor` FOREIGN KEY (`setor_fk`) REFERENCES `tb_setor` (`id`),
  CONSTRAINT `fk_voip_transferencia_automatica` FOREIGN KEY (`transferencia_automatica_fk`) REFERENCES `tb_voip` (`id`),
  CONSTRAINT `fk_voip_grupo_captura` FOREIGN KEY (`grupo_fk`) REFERENCES `tb_voip_grupo_captura` (`id`),
  CONSTRAINT `fk_voip_equipamento` FOREIGN KEY (`equipamento_fk`) REFERENCES `tb_equipamento` (`id`),
  CONSTRAINT `fk_voip_funcionario` FOREIGN KEY (`funcionario_fk`) REFERENCES `tb_funcionario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_switch` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `codigo_switch_fk` int(3) NOT NULL,
  `local_fk` int(6) NOT NULL,
  `id_switch` int(6) NOT NULL,
  `xml_portas` text NOT NULL,
  `numero_portas` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_switch` (`codigo_switch_fk`,`local_fk`,`id_switch`),
  KEY `idx_switch_id` (`id`),
  KEY `idx_switch_codigo_switch` (`codigo_switch_fk`),
  KEY `idx_switch_local` (`local_fk`),
  KEY `idx_switch_id_switch` (`id_switch`),
  KEY `idx_switch_numero_portas` (`numero_portas`),
  CONSTRAINT `fk_switch_codigo_switch` FOREIGN KEY (`codigo_switch_fk`) REFERENCES `tb_equipamento` (`id`),
  CONSTRAINT `fk_switch_local` FOREIGN KEY (`local_fk`) REFERENCES `tb_local` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_pon` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `codigo_pon_fk` int(3) NOT NULL,
  `local_fk` int(6) NOT NULL,
  `id_pon` int(6) NOT NULL,
  `numero_portas_pon` int(6) NOT NULL,
  `numero_portas_bridge` int(6) NOT NULL,
  `running_geral` text,
  `running_bridge` text,
  `running_pon_dba_profile` text,
  `running_pon_extended_profile` text,
  `running_pon_traffic_profile` text,
  `running_pon_onu_profile` text,
  `running_pon_olt` text,
  `conf` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_pon` (`codigo_pon_fk`,`local_fk`,`id_pon`),
  KEY `idx_pon_id` (`id`),
  KEY `idx_pon_codigo_switch` (`codigo_pon_fk`),
  KEY `idx_pon_local` (`local_fk`),
  KEY `idx_pon_id_pon` (`id_pon`),
  CONSTRAINT `fk_pon_codigo_pon` FOREIGN KEY (`codigo_pon_fk`) REFERENCES `tb_equipamento` (`id`),
  CONSTRAINT `fk_pon_local` FOREIGN KEY (`local_fk`) REFERENCES `tb_local` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_mapa_canal` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(6) NOT NULL,
  `local_fk` int(6) NOT NULL,
  `switch_fk` int(3),
  `switch_porta` int(2),
  `patch_panel` int(3) NOT NULL,
  `patch_panel_porta` int(2) NOT NULL,
  `patch_cord_switch` float,
  `link` float,
  `patch_cord_dispositivo` float,
  `descricao` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_mapa_ip_v4` (`unidade_fk`,`local_fk`,`patch_panel`,`patch_panel_porta`,`switch_fk`,`switch_porta`),
  KEY `idx_mapa_canal_id` (`id`),
  KEY `idx_mapa_canal_unindade` (`unidade_fk`),
  KEY `idx_mapa_canal_local` (`local_fk`),
  KEY `idx_mapa_canal_switch` (`switch_fk`),
  KEY `idx_mapa_canal_switch_porta` (`switch_porta`),
  KEY `idx_mapa_canal_patch_panel` (`patch_panel`),
  KEY `idx_mapa_canal_patch_panel_porta` (`patch_panel_porta`),
  KEY `idx_mapa_canal_descricao` (`descricao`),
  CONSTRAINT `fk_mapa_canal_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_mapa_canal_local` FOREIGN KEY (`local_fk`) REFERENCES `tb_local` (`id`),
  CONSTRAINT `fk_mapa_canal_switch` FOREIGN KEY (`switch_fk`) REFERENCES `tb_switch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_ocorencia_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_ocorencia_tipo` (`tipo`),
  KEY `idx_ocorencia_tipo_id` (`id`),
  KEY `idx_ocorencia_tipo_tipo` (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_ocorencia_monitor` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `data` datetime DEFAULT NULL,
  `usuario_nome` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `codigo_monitor_fk` int(6) NOT NULL,
  `tipo_fk` int(6) NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  KEY `idx_ocorencia_monitor_id` (`id`),
  KEY `idx_ocorencia_monitor_codigo_monitor` (`codigo_monitor_fk`),
  KEY `idx_ocorencia_monitor_tipo` (`tipo_fk`),
  CONSTRAINT `fk_ocorencia_monitor_codigo_monitor` FOREIGN KEY (`codigo_monitor_fk`) REFERENCES `tb_monitor` (`id`),
  CONSTRAINT `fk_ocorencia_monitor_tipo` FOREIGN KEY (`tipo_fk`) REFERENCES `tb_ocorencia_tipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_ocorencia_computador` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `data` datetime DEFAULT NULL,
  `usuario_nome` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `codigo_computador_fk` int(6) NOT NULL,
  `tipo_fk` int(6) NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  KEY `idx_ocorencia_computador_id` (`id`),
  KEY `idx_ocorencia_computador_codigo_computador` (`codigo_computador_fk`),
  KEY `idx_ocorencia_computador_tipo` (`tipo_fk`),
  CONSTRAINT `fk_ocorencia_computador_codigo_computador` FOREIGN KEY (`codigo_computador_fk`) REFERENCES `tb_computador` (`id`),
  CONSTRAINT `fk_ocorencia_computador_tipo` FOREIGN KEY (`tipo_fk`) REFERENCES `tb_ocorencia_tipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_ocorencia_equipamento` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `data` datetime DEFAULT NULL,
  `usuario_nome` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `codigo_equipamento_fk` int(6) NOT NULL,
  `tipo_fk` int(6) NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  KEY `idx_ocorencia_equipamento_id` (`id`),
  KEY `idx_ocorencia_equipamento_codigo_equipamento` (`codigo_equipamento_fk`),
  KEY `idx_ocorencia_equipamento_tipo` (`tipo_fk`),
  CONSTRAINT `fk_ocorencia_equipamento_codigo_computador` FOREIGN KEY (`codigo_equipamento_fk`) REFERENCES `tb_equipamento` (`id`),
  CONSTRAINT `fk_ocorencia_equipamento_tipo` FOREIGN KEY (`tipo_fk`) REFERENCES `tb_ocorencia_tipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_helpdesk_cadastro` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `de_fk` int(6) NOT NULL,
  `data_abertura` datetime DEFAULT NULL,
  `data_encerrado` datetime DEFAULT NULL,
  `produto_fk` int(6) NOT NULL,
  `prioridade` int(6) NOT NULL,
  `para_fk` int(6) NOT NULL,
  `resumo` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `ver` tinyint(4) NOT NULL,
  `status` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_helpdesk_cadastro_id` (`id`),
  KEY `idx_helpdesk_cadastro_de` (`de_fk`),
  KEY `idx_helpdesk_cadastro_para` (`para_fk`),
  KEY `idx_helpdesk_cadastro_produto` (`produto_fk`),
  KEY `idx_helpdesk_cadastro_status` (`status`),
  KEY `idx_helpdesk_cadastro_ver` (`ver`),
  KEY `idx_helpdesk_cadastro_resumo` (`resumo`),
  CONSTRAINT `fk_helpdesk_cadastro_de` FOREIGN KEY (`de_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_helpdesk_cadastro_para` FOREIGN KEY (`para_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_helpdesk_cadastro_produto` FOREIGN KEY (`produto_fk`) REFERENCES `tb_helpdesk_produto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_helpdesk_de_usuario_para` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_de_fk` int(11) NOT NULL,
  `usuario_para_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_helpdesk_de_usuario_para` (`usuario_de_fk`, `usuario_para_fk`),
  KEY `idx_helpdesk_de_usuario_para_id` (`id`),
  KEY `idx_helpdesk_de_usuario_para_usuario_de` (`usuario_de_fk`),
  KEY `idx_helpdesk_de_usuario_para_usuario_para` (`usuario_para_fk`),
  CONSTRAINT `fk_helpdesk_de_usuario_para_usuario_de` FOREIGN KEY (`usuario_de_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_helpdesk_de_usuario_para_usuario_para` FOREIGN KEY (`usuario_para_fk`) REFERENCES `tb_usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_helpdesk_etapa` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `chamado_fk` int(6) NOT NULL,
  `de_fk` int(6) NOT NULL,
  `data` datetime NOT NULL,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_helpdesk_etapa_id` (`id`),
  KEY `idx_helpdesk_etapa_de` (`de_fk`),
  KEY `idx_helpdesk_etapa_produto` (`chamado_fk`),
  CONSTRAINT `fk_helpdesk_etapa_de` FOREIGN KEY (`de_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_helpdesk_etapa_chamado` FOREIGN KEY (`chamado_fk`) REFERENCES `tb_helpdesk_cadastro` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_helpdesk_historico` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `chamado_fk` int(6) NOT NULL,
  `de_fk` int(6) NOT NULL,
  `data` datetime DEFAULT NULL,
  `acao` varchar(90) NOT NULL,
  `alteracao` varchar(120) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_helpdesk_historico_id` (`id`),
  KEY `idx_helpdesk_historico_de` (`de_fk`),
  KEY `idx_helpdesk_historico_produto` (`chamado_fk`),
  CONSTRAINT `fk_helpdesk_historico_de` FOREIGN KEY (`de_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_helpdesk_historico_chamado` FOREIGN KEY (`chamado_fk`) REFERENCES `tb_helpdesk_cadastro` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE
  ALGORITHM = UNDEFINED
  DEFINER = `root`@`localhost`
  SQL SECURITY DEFINER
VIEW `vw_menu` AS
  SELECT
  	`menu`.`id` AS `id`,
  	`user_perfil`.`usuario_fk` AS `usuario_fk`,
  	`menu`.`sistema_fk` AS `sistema_fk`,
  	`menu`.`menu_pai_fk` AS `menu_pai_fk`,
  	`menu`.`nivel` AS `nivel`,
  	`menu`.`titulo` AS `titulo`,
  	`menu`.`controller` AS `controller`,
  	`menu`.`icon` AS `icon`
  FROM
    ((`tb_usuario_perfil` `user_perfil`
  	JOIN `tb_menu_perfil` `menu_perfil` ON ((`menu_perfil`.`perfil_fk` = `user_perfil`.`perfil_fk`)))
  	JOIN `tb_menu` `menu` ON ((`menu`.`id` = `menu_perfil`.`menu_fk`)));

CREATE
  ALGORITHM = UNDEFINED
  DEFINER = `root`@`localhost`
  SQL SECURITY DEFINER
VIEW `vw_restricao_menu` AS
  SELECT
  	`menu`.`id` AS `id`,
    `menu`.`controller` AS `controller`,
    `restricao`.`restricao` AS `restricao`,
    `usuario`.`usuario_fk` AS `usuario_fk`
  FROM
    (((`tb_menu` `menu`
    JOIN `tb_restricao_menu_perfil` `restricao` ON ((`restricao`.`menu_fk` = `menu`.`id`)))
    JOIN `tb_perfil` `perfil` ON ((`perfil`.`id` = `restricao`.`perfil_fk`)))
    JOIN `tb_usuario_perfil` `usuario` ON ((`usuario`.`perfil_fk` = `perfil`.`id`)));
    
CREATE
  ALGORITHM = UNDEFINED
  DEFINER = `root`@`localhost`
  SQL SECURITY DEFINER
VIEW `vw_helpdesk_de_usuario_para` AS
  SELECT
  	`usuario`.`usuario_de_fk` AS `usuario_de_fk`,
    `usuario`.`usuario_para_fk` AS `usuario_para_fk`,
    `usuario_de`.`nome` AS `nome_de`,
    `usuario_para`.`nome` AS `nome_para`
  FROM
    ((`tb_helpdesk_de_usuario_para` `usuario`
    JOIN `tb_usuario` `usuario_de` ON ((`usuario_de`.`id` = `usuario`.`usuario_de_fk`)))
    JOIN `tb_usuario` `usuario_para` ON ((`usuario_para`.`id` = `usuario`.`usuario_para_fk`)));

CREATE 
  ALGORITHM = UNDEFINED 
  DEFINER = `root`@`localhost` 
  SQL SECURITY DEFINER
VIEW `vw_marca_modelo` AS
  SELECT 
    `modelo`.`id` AS `id`,
    `marca_modelo`.`marca` AS `marca`,
    `modelo`.`marca_fk` AS `marca_fk`,
    `modelo`.`modelo` AS `modelo`,
    `modelo`.`tipo` AS `tipo`
  FROM
    ((((`tb_modelo` `modelo`
    JOIN `tb_marca` `marca_modelo` ON ((`marca_modelo`.`id` = `modelo`.`marca_fk`)))
    JOIN `tb_marca` `marca_computador` ON ((`marca_computador`.`id` = `modelo`.`marca_fk`)))
    JOIN `tb_marca` `marca_equipamento` ON ((`marca_equipamento`.`id` = `modelo`.`marca_fk`)))
    JOIN `tb_marca` `marca_monitor` ON ((`marca_monitor`.`id` = `modelo`.`marca_fk`)));

CREATE 
  ALGORITHM = UNDEFINED 
  DEFINER = `root`@`localhost` 
  SQL SECURITY DEFINER
VIEW `vw_marca_placa_video` AS
  SELECT 
    `placa_video`.`id` AS `id`,
    `marca_placa_video`.`marca` AS `marca`,
    `placa_video`.`marca_fk` AS `marca_fk`,
    `placa_video`.`modelo` AS `modelo`,
    `placa_video`.`descricao_placa_video` AS `descricao_placa_video`
  FROM
    (`tb_placa_video` `placa_video`
    JOIN `tb_marca` `marca_placa_video` ON ((`marca_placa_video`.`id` = `placa_video`.`marca_fk`)));

CREATE 
  ALGORITHM = UNDEFINED 
  DEFINER = `root`@`localhost` 
  SQL SECURITY DEFINER
VIEW `vw_marca_processador` AS
  SELECT 
    `processador`.`id` AS `id`,
    `marca_processador`.`marca` AS `marca`,
    `processador`.`marca_fk` AS `marca_fk`,
    `processador`.`processador` AS `processador`,
    `processador`.`descricao_processador` AS `descricao_processador`
  FROM
    (`tb_processador` `processador`
    JOIN `tb_marca` `marca_processador` ON ((`marca_processador`.`id` = `processador`.`marca_fk`)));

CREATE 
  ALGORITHM = UNDEFINED 
  DEFINER = `root`@`localhost` 
  SQL SECURITY DEFINER
VIEW `vw_usuario` AS
  SELECT
    `usuario`.`id` AS `id`,
    `usuario`.`usuario` AS `usuario`,
    `usuario`.`senha` AS `senha`,
    `usuario`.`nome` AS `nome_usuario`,
    `usuario`.`email` AS `email`,
    `usuario`.`status` AS `status`,
    `usuario`.`telefone` AS `telefone`,
    `usuario`.`unidade_fk` AS `unidade_fk`,
    `unidade`.`unidade` AS `unidade`,
    `usuario_perfil`.`perfil_fk` AS `perfil_fk`,
    `perfil`.`nome` AS `nome_perfil`
  FROM
    (`tb_usuario` `usuario`
    JOIN `tb_unidade` `unidade` ON ((`unidade`.`id` = `usuario`.`unidade_fk`))
    JOIN `tb_usuario_perfil` `usuario_perfil` ON ((`usuario_perfil`.`usuario_fk` = `usuario`.`id`))
    JOIN `tb_perfil` `perfil` ON perfil.id = ((`usuario_perfil`.`perfil_fk`)));

CREATE 
  ALGORITHM = UNDEFINED 
  DEFINER = `root`@`localhost` 
  SQL SECURITY DEFINER
VIEW `vw_computador` AS
  SELECT 
    `computador`.`id` AS `id`,
    `computador`.`codigo_computador` AS `codigo_computador`,
    `unidade_computador`.`unidade` AS `unidade`,
    `computador`.`unidade_fk` AS `unidade_fk`,
    `setor_computador`.`setor` AS `setor`,
    `computador`.`setor_fk` AS `setor_fk`,
    `tipo_computador`.`tipo` AS `tipo`,
    `computador`.`tipo_fk` AS `tipo_fk`,
    `modelo_computador`.`marca` AS `marca`,
    `marca_computador`.`marca_fk` AS `marca_fk`,
    `modelo_computador`.`modelo` AS `modelo`,
    `computador`.`modelo_fk` AS `modelo_fk`,
    `computador`.`numero_serie` AS `numero_serie`,
    `computador`.`data_compra` AS `data_compra`,
    `computador`.`garantia` AS `garantia`,
    `computador`.`placa_mae` AS `placa_mae`,
    `computador`.`numero_processador` AS `numero_processador`,
    `processador_computador`.`processador` AS `processador`,
    `computador`.`ram` AS `ram`,
    `medida_ram_computador`.`medida`  AS medida_ram,
    `ram_clok_computador`.`clok` AS `clok`,
    `computador`.`hd` AS `hd`,
    `medida_hd_computador`.`medida` AS medida_hd,
    `computador`.`loja` AS `loja`,
    `computador`.`numero_nota` AS `numero_nota`,
    `computador`.`valor` AS `valor`,
    `computador`.`numero_patrimonio` AS `numero_patrimonio`,
    `computador`.`numero_lacre` AS `numero_lacre`,
    `computador`.`netbios` AS `netbios`,
    `computador`.`mac_c` AS `mac_c`,
    `computador`.`mac_w` AS `mac_w`,
    `sistema_operacional_computador`.`sistema` AS `sistema`,
    `office_computador`.`office` AS `office`,
    `placa_video_computador`.`modelo`  AS `palca_video`,
    `computador`.`numero_palca_video` AS `numero_palca_video`,
    `monitor1_computador`.`codigo_monitor` AS `codigo_monitor1`,
    `monitor2_computador`.`codigo_monitor` AS `codigo_monitor2`,
    `computador`.`status` AS `status`,
    `computador`.`url` AS `url`
  FROM
    ((((((((((((((`tb_computador` `computador`
    JOIN `tb_unidade` `unidade_computador` ON ((`unidade_computador`.`id` = `computador`.`unidade_fk`)))
    JOIN `tb_setor` `setor_computador` ON ((`setor_computador`.`id` = `computador`.`setor_fk`)))
    JOIN `tb_tipo` `tipo_computador` ON ((`tipo_computador`.`id` = `computador`.`tipo_fk`)))
    JOIN `tb_modelo` `marca_computador` ON ((`marca_computador`.`id` = `computador`.`modelo_fk`)))
    JOIN `vw_marca_modelo` `modelo_computador` ON ((`modelo_computador`.`id` = `computador`.`modelo_fk`)))
    JOIN `vw_marca_processador` `processador_computador` ON ((`processador_computador`.`id` = `computador`.`processador_fk`)))
    JOIN `tb_medida` `medida_ram_computador` ON ((`medida_ram_computador`.`id` = `computador`.`ram_medida_fk`)))
    JOIN `tb_ram_clok` `ram_clok_computador` ON ((`ram_clok_computador`.`id` = `computador`.`ram_clok_fk`)))
    JOIN `tb_medida` `medida_hd_computador` ON ((`medida_hd_computador`.`id` = `computador`.`hd_medida_fk`)))
    JOIN `tb_sistema_operacional` `sistema_operacional_computador` ON ((`sistema_operacional_computador`.`id` = `computador`.`sistema_operacional_fk`)))
    JOIN `tb_office` `office_computador` ON ((`office_computador`.`id` = `computador`.`office_fk`)))
    JOIN `vw_marca_placa_video` `placa_video_computador` ON ((`placa_video_computador`.`id` = `computador`.`placa_video_fk`)))
    JOIN `tb_monitor` `monitor1_computador` ON ((`monitor1_computador`.`id` = `computador`.`codigo_monitor1_fk`)))
    JOIN `tb_monitor` `monitor2_computador` ON ((`monitor2_computador`.`id` = `computador`.`codigo_monitor2_fk`)))
  ORDER BY `unidade_computador`.`unidade` , `setor_computador`.`setor` , `computador`.`status`;
    
CREATE 
  ALGORITHM = UNDEFINED 
  DEFINER = `root`@`localhost` 
  SQL SECURITY DEFINER
VIEW `vw_monitor` AS
  SELECT 
    `monitor`.`id` AS `id`,
    `monitor`.`codigo_monitor` AS `codigo_monitor`,
    `unidade_monitor`.`unidade` AS `unidade`,
    `monitor`.`unidade_fk` AS `unidade_fk`,
    `setor_monitor`.`setor` AS `setor`,
    `monitor`.`setor_fk` AS `setor_fk`,
    `tipo_monitor`.`tipo` AS `tipo`,
    `monitor`.`tipo_fk` AS `tipo_fk`,
    `modelo_monitor`.`marca` AS `marca`,
    `marca_monitor`.`marca_fk` AS `marca_fk`,
    `modelo_monitor`.`modelo` AS `modelo`,
    `monitor`.`modelo_fk` AS `modelo_fk`,
    `monitor`.`numero_serie` AS `numero_serie`,
    `monitor`.`data_compra` AS `data_compra`,
    `monitor`.`garantia` AS `garantia`,
    `monitor`.`loja` AS `loja`,
    `monitor`.`numero_nota` AS `numero_nota`,
    `monitor`.`valor` AS `valor`,
    `monitor`.`numero_patrimonio` AS `numero_patrimonio`,
    `monitor`.`netbios` AS `netbios`,
    `monitor`.`mac` AS `mac`,
    `monitor`.`status` AS `status`,
    `monitor`.`url` AS `url`
  FROM
    (((((`tb_monitor` `monitor`
    JOIN `tb_unidade` `unidade_monitor` ON ((`unidade_monitor`.`id` = `monitor`.`unidade_fk`)))
    JOIN `tb_setor` `setor_monitor` ON ((`setor_monitor`.`id` = `monitor`.`setor_fk`)))
    JOIN `tb_tipo` `tipo_monitor` ON ((`tipo_monitor`.`id` = `monitor`.`tipo_fk`)))
    JOIN `tb_modelo` `marca_monitor` ON ((`marca_monitor`.`id` = `monitor`.`modelo_fk`)))
    JOIN `vw_marca_modelo` `modelo_monitor` ON ((`modelo_monitor`.`id` = `monitor`.`modelo_fk`)));
        
CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vw_equipamento` AS
  SELECT 
    `equipamento`.`id` AS `id`,
    `equipamento`.`codigo_equipamento` AS `codigo_equipamento`,
    `unidade_equipamento`.`unidade` AS `unidade`,
    `equipamento`.`unidade_fk` AS `unidade_fk`,
    `setor_equipamento`.`setor` AS `setor`,
    `equipamento`.`setor_fk` AS `setor_fk`,
    `tipo_equipamento`.`tipo` AS `tipo`,
    `equipamento`.`tipo_fk` AS `tipo_fk`,
    `modelo_equipamento`.`marca` AS `marca`,
    `marca_equipamento`.`marca_fk` AS `marca_fk`,
    `modelo_equipamento`.`modelo` AS `modelo`,
    `equipamento`.`modelo_fk` AS `modelo_fk`,
    `equipamento`.`numero_serie` AS `numero_serie`,
    `equipamento`.`data_compra` AS `data_compra`,
    `equipamento`.`garantia` AS `garantia`,
    `equipamento`.`loja` AS `loja`,
    `equipamento`.`numero_nota` AS `numero_nota`,
    `equipamento`.`valor` AS `valor`,
    `equipamento`.`numero_patrimonio` AS `numero_patrimonio`,
    `equipamento`.`netbios` AS `netbios`,
    `equipamento`.`mac` AS `mac`,
    `equipamento`.`status` AS `status`,
    `equipamento`.`url` AS `url`
  FROM
    (((((`tb_equipamento` `equipamento`
    JOIN `tb_unidade` `unidade_equipamento` ON ((`unidade_equipamento`.`id` = `equipamento`.`unidade_fk`)))
    JOIN `tb_setor` `setor_equipamento` ON ((`setor_equipamento`.`id` = `equipamento`.`setor_fk`)))
    JOIN `tb_tipo` `tipo_equipamento` ON ((`tipo_equipamento`.`id` = `equipamento`.`tipo_fk`)))
    JOIN `tb_modelo` `marca_equipamento` ON ((`marca_equipamento`.`id` = `equipamento`.`modelo_fk`)))
    JOIN `vw_marca_modelo` `modelo_equipamento` ON ((`modelo_equipamento`.`id` = `equipamento`.`modelo_fk`)));

CREATE 
  ALGORITHM = UNDEFINED 
  DEFINER = `root`@`localhost` 
  SQL SECURITY DEFINER
VIEW `vw_helpdesk` AS
  SELECT 
    `helpdesk`.`id` AS `id`,
    `usuario_de`.`nome` AS `nome_de`,
    `usuario_de`.`unidade_fk` AS `unidade_fk_de`,
    `unidade_de`.`unidade` AS `unidade_de`,
    `helpdesk`.`para_fk` AS `para_fk`,
    `usuario_para`.`nome` AS `nome_para`,
    `usuario_para`.`unidade_fk` AS `unidade_fk_para`,
    `unidade_para`.`unidade` AS `unidade_para`,
    `helpdesk`.`data_abertura` AS `data_abertura`,
    `helpdesk`.`data_encerrado` AS `data_encerrado`,
    `helpdesk`.`produto_fk` AS `produto_fk`,
    `produto`.`produto` AS `produto`,
    `helpdesk`.`resumo` AS `resumo`,
    `helpdesk`.`descricao` AS `descricao`,
    `helpdesk`.`prioridade` AS `prioridade`,
    `helpdesk`.`ver` AS `ver`,
    `helpdesk`.`status` AS `status`
  FROM
    (`tb_helpdesk_cadastro` `helpdesk`
    JOIN `tb_usuario` `usuario_de` ON ((`usuario_de`.`id` = `helpdesk`.`de_fk`))
    JOIN `tb_usuario` `usuario_para` ON ((`usuario_para`.`id` = `helpdesk`.`para_fk`))
    JOIN `tb_unidade` `unidade_de` ON ((`unidade_de`.`id` = `usuario_de`.`unidade_fk`))
    JOIN `tb_unidade` `unidade_para` ON ((`unidade_para`.`id` = `usuario_para`.`unidade_fk`))
    JOIN `tb_helpdesk_produto` `produto` ON ((`produto`.`id` = `helpdesk`.`produto_fk`)))
  ORDER BY `helpdesk`.`status` , `unidade_de`.`unidade` , `helpdesk`.`id`;
        
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `fc_remove_acento`(`str` TEXT) RETURNS text CHARSET utf8
  NO SQL
  DETERMINISTIC
  SQL SECURITY INVOKER
BEGIN
  SET str = REPLACE(str,'Š','S');
  SET str = REPLACE(str,'š','s');
  SET str = REPLACE(str,'Ð','Dj');
  SET str = REPLACE(str,'Ž','Z');
  SET str = REPLACE(str,'ž','z');
  SET str = REPLACE(str,'À','A');
  SET str = REPLACE(str,'Á','A');
  SET str = REPLACE(str,'Â','A');
  SET str = REPLACE(str,'Ã','A');
  SET str = REPLACE(str,'Ä','A');
  SET str = REPLACE(str,'Å','A');
  SET str = REPLACE(str,'Æ','A');
  SET str = REPLACE(str,'Ç','C');
  SET str = REPLACE(str,'È','E');
  SET str = REPLACE(str,'É','E');
  SET str = REPLACE(str,'Ê','E');
  SET str = REPLACE(str,'Ë','E');
  SET str = REPLACE(str,'Ì','I');
  SET str = REPLACE(str,'Í','I');
  SET str = REPLACE(str,'Î','I');
  SET str = REPLACE(str,'Ï','I');
  SET str = REPLACE(str,'Ñ','N');
  SET str = REPLACE(str,'Ò','O');
  SET str = REPLACE(str,'Ó','O');
  SET str = REPLACE(str,'Ô','O');
  SET str = REPLACE(str,'Õ','O');
  SET str = REPLACE(str,'Ö','O');
  SET str = REPLACE(str,'Ø','O');
  SET str = REPLACE(str,'Ù','U');
  SET str = REPLACE(str,'Ú','U');
  SET str = REPLACE(str,'Û','U');
  SET str = REPLACE(str,'Ü','U');
  SET str = REPLACE(str,'Ý','Y');
  SET str = REPLACE(str,'Þ','B');
  SET str = REPLACE(str,'ß','Ss');
  SET str = REPLACE(str,'à','a');
  SET str = REPLACE(str,'á','a');
  SET str = REPLACE(str,'â','a');
  SET str = REPLACE(str,'ã','a');
  SET str = REPLACE(str,'ä','a');
  SET str = REPLACE(str,'å','a');
  SET str = REPLACE(str,'æ','a');
  SET str = REPLACE(str,'ç','c');
  SET str = REPLACE(str,'è','e');
  SET str = REPLACE(str,'é','e');
  SET str = REPLACE(str,'ê','e');
  SET str = REPLACE(str,'ë','e');
  SET str = REPLACE(str,'ì','i');
  SET str = REPLACE(str,'í','i');
  SET str = REPLACE(str,'î','i');
  SET str = REPLACE(str,'ï','i');
  SET str = REPLACE(str,'ð','o');
  SET str = REPLACE(str,'ñ','n');
  SET str = REPLACE(str,'ò','o');
  SET str = REPLACE(str,'ó','o');
  SET str = REPLACE(str,'ô','o');
  SET str = REPLACE(str,'õ','o');
  SET str = REPLACE(str,'ö','o');
  SET str = REPLACE(str,'ø','o');
  SET str = REPLACE(str,'ù','u');
  SET str = REPLACE(str,'ú','u');
  SET str = REPLACE(str,'û','u');
  SET str = REPLACE(str,'ý','y');
  SET str = REPLACE(str,'ý','y');
  SET str = REPLACE(str,'þ','b');
  SET str = REPLACE(str,'ÿ','y');
  SET str = REPLACE(str,'ƒ','f');
  RETURN str;
END$$
DELIMITER ;